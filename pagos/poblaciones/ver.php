<?php
include ("../seguridad_usuario.inc");
include ("../clases/class.poblaciones.php");

$poblaciones = new poblaciones();

$codigo = $_GET["codigo"];

$row = $poblaciones->select($codigo);
?>

<html>
    <head>
        <title>Principal</title>
        <link href="../estilos/estilos.css" type="text/css" rel="stylesheet">
        <script language="javascript">
		
            function aceptar() {
                location.href="index.php?cadena_busqueda=<? echo $cadena_busqueda ?>";
            }
		
            var cursor;
            if (document.all) {
                // Está utilizando EXPLORER
                cursor='hand';
            } else {
                // Está utilizando MOZILLA/NETSCAPE
                cursor='pointer';
            }
		
        </script>
    </head>
    <body>
        <div id="pagina">
            <div id="zonaContenido">
                <div align="center">
                    <div id="tituloForm" class="header">VER POBLACION</div>
                    <div id="frmBusqueda">
                        <hr>
                        <table class="fuente8" width="98%" cellspacing=0 cellpadding=3 border=0>
                            <tr>
                                <td width="15%">Pais</td>
                                <td width="85%" colspan="2"><input type="text" readonly class="cajaGrande" value="<?= $row[3] ?>"></td>
                            </tr>
                            <tr>
                                <td width="15%">Provincia</td>
                                <td width="85%" colspan="2"><input type="text" readonly class="cajaGrande" value="<?= $row[5] ?>"></td>
                            </tr>
                            
                            <tr>
                                <td width="15%">Poblacion</td>
                                <td width="85%" colspan="2"><input type="text" readonly class="cajaGrande" value="<?= $row[1] ?>"></td>
                            </tr>
                        </table>
                        <hr>
                    </div>
                    <div id="botonBusqueda">
                        <img src="../img/botonaceptar.jpg" width="85" height="22" onClick="aceptar()" border="1" onMouseOver="style.cursor=cursor">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
