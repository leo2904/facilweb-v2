<?php

class FACTURA
{
  public $ENC = Array();
  public $EMI = Array();
  public $ADQ = Array();
  public $TOT = Array();
  public $TIM = Array();
  //public $DSC = Array();
  public $DRF = Array();
  public $NOT = Array();
  public $MEP = Array();
  public $ITE = Array();
}

class ENC
{
	public $ENC_1;
	public $ENC_2;
	public $ENC_3;
	public $ENC_4;
	public $ENC_5;
	public $ENC_6;
	public $ENC_7;
	public $ENC_8;
	public $ENC_9;
	public $ENC_10;
	public $ENC_11;
	public $ENC_12;
	public $ENC_13;
	public $ENC_14;
	public $ENC_15;
	public $ENC_16;
	public $ENC_17;
	public $ENC_18;
	public $ENC_19;
	public $ENC_20;
	public $ENC_21;
}

class EMI
{
	public $EMI_1;
	public $EMI_2;
	public $EMI_3;
	public $EMI_4;
	public $EMI_5;
	public $EMI_6;
	public $EMI_7;
	public $EMI_8;
	public $EMI_9;
	public $EMI_10;
	public $EMI_11;
	public $EMI_12;
	public $EMI_13;
	public $EMI_14;
	public $EMI_15;
	public $EMI_16;
	public $EMI_17;
	public $EMI_18;
	public $EMI_19;
	public $EMI_20;
	public $EMI_21;
	public $EMI_22;
	public $EMI_23;
	public $EMI_24;
	
	public $TAC = Array();
	public $DFE = Array();
	public $ICC = Array();
	public $CDE = Array();
	public $GTE = Array();
}

class TAC 
{
	public $TAC_1;
}

class DFE
{
	public $DFE_1;
	public $DFE_2;
	public $DFE_3;
	public $DFE_4;
	public $DFE_5;
	public $DFE_6;
	public $DFE_7;
	public $DFE_8;
}

class ICC2
{
	public $ICC_1;
	public $ICC_9;
}

class CDE
{
	public $CDE_1;
	public $CDE_2;
	public $CDE_3;
	public $CDE_4;
}

class GTE
{
	public $GTE_1;
	public $GTE_2;
}

class ADQ
{
	public $ADQ_1;
	public $ADQ_2;
	public $ADQ_3;
	public $ADQ_4;
	public $ADQ_5;
	public $ADQ_6;
	public $ADQ_7;
	public $ADQ_8;
	public $ADQ_9;
	public $ADQ_10;
	public $ADQ_11;
	public $ADQ_12;
	public $ADQ_13;
	public $ADQ_14;
	public $ADQ_15;
	public $ADQ_16;
	public $ADQ_17;
	public $ADQ_18;
	public $ADQ_19;
	public $ADQ_20;
	public $ADQ_21;
	public $ADQ_22;
	public $ADQ_23;
	public $ADQ_24;
	
	public $TCR = Array();
	public $ILA = Array();
	public $DFA = Array();
	public $ICR = Array();
	public $CDA = Array();
	public $GTA = Array();
}

class TCR
{
	public $TCR_1;
}

class ILA
{
	public $ILA_1;
	public $ILA_2;
	public $ILA_3;
	public $ILA_4;
}

class DFA
{
	public $DFA_1;
	public $DFA_2;
	public $DFA_3;
	public $DFA_4;
	public $DFA_5;
	public $DFA_6;
	public $DFA_7;
	public $DFA_8;
}

class ICR
{
	public $ICR_1;
}

class CDA
{
	public $CDA_1;
	public $CDA_2;
	public $CDA_3;
	public $CDA_4;
}

class GTA
{
	public $GTA_1;
	public $GTA_2;
}

class TOT
{
	public $TOT_1;
	public $TOT_2;
	public $TOT_3;
	public $TOT_4;
	public $TOT_5;
	public $TOT_6;
	public $TOT_7;
	public $TOT_8;
	//se agregan campos
	public $TOT_9;
	public $TOT_10;
	public $TOT_11;
	public $TOT_12;
}

class TIM
{
	public $TIM_1;
	public $TIM_2;
	public $TIM_3;
	
	public $IMP = Array();
}

class IMP
{
	public $IMP_1;
	public $IMP_2;
	public $IMP_3;
	public $IMP_4;
	public $IMP_5;
	public $IMP_6;//no lo tienen en el web service
}
/*
class DSC
{
	public $DSC_1;
	public $DSC_2;
	public $DSC_3;
	public $DSC_4;
	public $DSC_5;
	public $DSC_6;
	public $DSC_7;
	public $DSC_8;
	public $DSC_9;
	public $DSC_10;
}
*/
class DRF
{
	public $DRF_1;
	public $DRF_2;
	public $DRF_3;
	public $DRF_4;
	public $DRF_5;
	public $DRF_6;
}

class NOT
{
	public $NOT_1;
}

class MEP
{
	public $MEP_1;
	public $MEP_2;
	public $MEP_3;
}

class ITE
{
	public $ITE_1;
	public $ITE_2;
	public $ITE_3;
	public $ITE_4;
	public $ITE_5;
	public $ITE_6;
	public $ITE_7;
	public $ITE_8;
	public $ITE_9;
	public $ITE_10;
	public $ITE_11;
	public $ITE_12;
	public $ITE_13;
	public $ITE_14;
	public $ITE_15;
	public $ITE_16;
	public $ITE_17;
	public $ITE_18;
	public $ITE_19;
	public $ITE_20;
	public $ITE_21;
	public $ITE_22;
	public $ITE_23;
	public $ITE_24;
	public $ITE_25;
	public $ITE_26;
	public $ITE_27;
	public $ITE_28;
	
	public $IAE = Array();
	//public $IDE = Array();
	public $TII = Array();
}

class IAE
{
	public $IAE_1;
	public $IAE_2;
	public $IAE_3;
	public $IAE_4;
}
/*
class IDE
{
	public $IDE_1;
	public $IDE_2;
	public $IDE_3;
	//public $IDE_4;
	public $IDE_5;
	public $IDE_6;
	public $IDE_7;
	public $IDE_8;
	public $IDE_9;
	public $IDE_10;
}
*/
class TII
{
	public $TII_1;
	public $TII_2;
	public $TII_3;
	
	public $IIM = Array();
}

class IIM
{
	public $IIM_1;
	public $IIM_2;
	public $IIM_3;
	public $IIM_4;
	public $IIM_5;
	public $IIM_6;//no incluido en web service de cadena
}

define("WSDL","https://ws.facturatech.co/21/index.php?wsdl");
date_default_timezone_set("America/Bogota");

class WebService{
    
    function enviar($wsdl,$usuario,$password,$xml_64)
    {
		$paramatros = array(
		
			'username' => $usuario,
			'password' => $password,
			'xmlBase64'=> $xml_64
		);
			
        try {
            set_time_limit(300);
        }
        catch(Exception $e) {
            var_dump($e);
        }
    }
	
	function qr($wsdl,$usuario,$password,$prefijo,$folio)
    {
		$paramatros = array(
		
			'username' => $usuario,
			'password' => $password,
			'prefijo'  => $prefijo,
			'folio'    => $folio
		);
			
        try {
            set_time_limit(300);
        }
        catch(Exception $e) {
            var_dump($e);
        }
    }
}
?>