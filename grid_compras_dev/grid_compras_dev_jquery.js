function grid_compras_dev_num_ndevolucion_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_num_ndevolucion" + seqRow).html();
}

function grid_compras_dev_num_ndevolucion_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_num_ndevolucion" + seqRow).html(newValue);
}

function grid_compras_dev_numfacom_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_numfacom" + seqRow).html();
}

function grid_compras_dev_numfacom_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_numfacom" + seqRow).html(newValue);
}

function grid_compras_dev_fechacom_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_fechacom" + seqRow).html();
}

function grid_compras_dev_fechacom_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_fechacom" + seqRow).html(newValue);
}

function grid_compras_dev_fechavenc_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_fechavenc" + seqRow).html();
}

function grid_compras_dev_fechavenc_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_fechavenc" + seqRow).html(newValue);
}

function grid_compras_dev_idprov_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_idprov" + seqRow).html();
}

function grid_compras_dev_idprov_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_idprov" + seqRow).html(newValue);
}

function grid_compras_dev_val_ret_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_val_ret" + seqRow).html();
}

function grid_compras_dev_val_ret_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_val_ret" + seqRow).html(newValue);
}

function grid_compras_dev_val_ica_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_val_ica" + seqRow).html();
}

function grid_compras_dev_val_ica_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_val_ica" + seqRow).html(newValue);
}

function grid_compras_dev_val_retiva_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_val_retiva" + seqRow).html();
}

function grid_compras_dev_val_retiva_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_val_retiva" + seqRow).html(newValue);
}

function grid_compras_dev_total_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_total" + seqRow).html();
}

function grid_compras_dev_total_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_total" + seqRow).html(newValue);
}

function grid_compras_dev_valoriva_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_valoriva" + seqRow).html();
}

function grid_compras_dev_valoriva_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_valoriva" + seqRow).html(newValue);
}

function grid_compras_dev_observaciones_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_observaciones" + seqRow).html();
}

function grid_compras_dev_observaciones_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_observaciones" + seqRow).html(newValue);
}

function grid_compras_dev_idfaccom_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_idfaccom" + seqRow).html();
}

function grid_compras_dev_idfaccom_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_idfaccom" + seqRow).html(newValue);
}

function grid_compras_dev_subtotal_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_subtotal" + seqRow).html();
}

function grid_compras_dev_subtotal_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_subtotal" + seqRow).html(newValue);
}

function grid_compras_dev_pagada_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_pagada" + seqRow).html();
}

function grid_compras_dev_pagada_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_pagada" + seqRow).html(newValue);
}

function grid_compras_dev_asentada_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_asentada" + seqRow).html();
}

function grid_compras_dev_asentada_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_asentada" + seqRow).html(newValue);
}

function grid_compras_dev_saldo_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_saldo" + seqRow).html();
}

function grid_compras_dev_saldo_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_saldo" + seqRow).html(newValue);
}

function grid_compras_dev_formapago_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_formapago" + seqRow).html();
}

function grid_compras_dev_formapago_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_formapago" + seqRow).html(newValue);
}

function grid_compras_dev_retencion_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_retencion" + seqRow).html();
}

function grid_compras_dev_retencion_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_retencion" + seqRow).html(newValue);
}

function grid_compras_dev_reteica_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_reteica" + seqRow).html();
}

function grid_compras_dev_reteica_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_reteica" + seqRow).html(newValue);
}

function grid_compras_dev_reteiva_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_reteiva" + seqRow).html();
}

function grid_compras_dev_reteiva_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_reteiva" + seqRow).html(newValue);
}

function grid_compras_dev_a_pagar_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_a_pagar" + seqRow).html();
}

function grid_compras_dev_a_pagar_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_a_pagar" + seqRow).html(newValue);
}

function grid_compras_dev_devolucion_getValue(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  return $("#id_sc_field_devolucion" + seqRow).find("i").attr("src");
}

function grid_compras_dev_devolucion_setValue(newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_devolucion" + seqRow).find("img").attr("src", newValue);
}

function grid_compras_dev_getValue(field, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  var SC_tmp = "";
  if ("num_ndevolucion" == field) {
    SC_tmp = grid_compras_dev_num_ndevolucion_getValue(seqRow);
  }
  if ("numfacom" == field) {
    SC_tmp = grid_compras_dev_numfacom_getValue(seqRow);
  }
  if ("fechacom" == field) {
    SC_tmp = grid_compras_dev_fechacom_getValue(seqRow);
  }
  if ("fechavenc" == field) {
    SC_tmp = grid_compras_dev_fechavenc_getValue(seqRow);
  }
  if ("idprov" == field) {
    SC_tmp = grid_compras_dev_idprov_getValue(seqRow);
  }
  if ("val_ret" == field) {
    SC_tmp = grid_compras_dev_val_ret_getValue(seqRow);
  }
  if ("val_ica" == field) {
    SC_tmp = grid_compras_dev_val_ica_getValue(seqRow);
  }
  if ("val_retiva" == field) {
    SC_tmp = grid_compras_dev_val_retiva_getValue(seqRow);
  }
  if ("total" == field) {
    SC_tmp = grid_compras_dev_total_getValue(seqRow);
  }
  if ("valoriva" == field) {
    SC_tmp = grid_compras_dev_valoriva_getValue(seqRow);
  }
  if ("observaciones" == field) {
    SC_tmp = grid_compras_dev_observaciones_getValue(seqRow);
  }
  if ("idfaccom" == field) {
    SC_tmp = grid_compras_dev_idfaccom_getValue(seqRow);
  }
  if ("subtotal" == field) {
    SC_tmp = grid_compras_dev_subtotal_getValue(seqRow);
  }
  if ("pagada" == field) {
    SC_tmp = grid_compras_dev_pagada_getValue(seqRow);
  }
  if ("asentada" == field) {
    SC_tmp = grid_compras_dev_asentada_getValue(seqRow);
  }
  if ("saldo" == field) {
    SC_tmp = grid_compras_dev_saldo_getValue(seqRow);
  }
  if ("formapago" == field) {
    SC_tmp = grid_compras_dev_formapago_getValue(seqRow);
  }
  if ("retencion" == field) {
    SC_tmp = grid_compras_dev_retencion_getValue(seqRow);
  }
  if ("reteica" == field) {
    SC_tmp = grid_compras_dev_reteica_getValue(seqRow);
  }
  if ("reteiva" == field) {
    SC_tmp = grid_compras_dev_reteiva_getValue(seqRow);
  }
  if ("a_pagar" == field) {
    SC_tmp = grid_compras_dev_a_pagar_getValue(seqRow);
  }
  if ("devolucion" == field) {
    SC_tmp = grid_compras_dev_devolucion_getValue(seqRow);
  }
  if (typeof(SC_tmp) == 'undefined') {
      SC_tmp = "";
  }
  else {
      SC_tmp = SC_tmp.replace(/[+]/g, "__NM_PLUS__");
      while (SC_tmp.lastIndexOf("&amp;") != -1)
      {
         SC_tmp = SC_tmp.replace("&amp;" , "__NM_AMP__");
      }
      SC_tmp = SC_tmp.replace(/[&]/g, "__NM_AMP__");
      SC_tmp = SC_tmp.replace(/[%]/g, "__NM_PRC__");
  }
  return SC_tmp;
}

function grid_compras_dev_setValue(field, newValue, seqRow) {
  seqRow = scSeqNormalize(seqRow);
  if ("num_ndevolucion" == field) {
    grid_compras_dev_num_ndevolucion_setValue(newValue, seqRow);
  }
  if ("numfacom" == field) {
    grid_compras_dev_numfacom_setValue(newValue, seqRow);
  }
  if ("fechacom" == field) {
    grid_compras_dev_fechacom_setValue(newValue, seqRow);
  }
  if ("fechavenc" == field) {
    grid_compras_dev_fechavenc_setValue(newValue, seqRow);
  }
  if ("idprov" == field) {
    grid_compras_dev_idprov_setValue(newValue, seqRow);
  }
  if ("val_ret" == field) {
    grid_compras_dev_val_ret_setValue(newValue, seqRow);
  }
  if ("val_ica" == field) {
    grid_compras_dev_val_ica_setValue(newValue, seqRow);
  }
  if ("val_retiva" == field) {
    grid_compras_dev_val_retiva_setValue(newValue, seqRow);
  }
  if ("total" == field) {
    grid_compras_dev_total_setValue(newValue, seqRow);
  }
  if ("valoriva" == field) {
    grid_compras_dev_valoriva_setValue(newValue, seqRow);
  }
  if ("observaciones" == field) {
    grid_compras_dev_observaciones_setValue(newValue, seqRow);
  }
  if ("idfaccom" == field) {
    grid_compras_dev_idfaccom_setValue(newValue, seqRow);
  }
  if ("subtotal" == field) {
    grid_compras_dev_subtotal_setValue(newValue, seqRow);
  }
  if ("pagada" == field) {
    grid_compras_dev_pagada_setValue(newValue, seqRow);
  }
  if ("asentada" == field) {
    grid_compras_dev_asentada_setValue(newValue, seqRow);
  }
  if ("saldo" == field) {
    grid_compras_dev_saldo_setValue(newValue, seqRow);
  }
  if ("formapago" == field) {
    grid_compras_dev_formapago_setValue(newValue, seqRow);
  }
  if ("retencion" == field) {
    grid_compras_dev_retencion_setValue(newValue, seqRow);
  }
  if ("reteica" == field) {
    grid_compras_dev_reteica_setValue(newValue, seqRow);
  }
  if ("reteiva" == field) {
    grid_compras_dev_reteiva_setValue(newValue, seqRow);
  }
  if ("a_pagar" == field) {
    grid_compras_dev_a_pagar_setValue(newValue, seqRow);
  }
  if ("devolucion" == field) {
    grid_compras_dev_devolucion_setValue(newValue, seqRow);
  }
}

function scJQAddEvents(seqRow) {
  seqRow = scSeqNormalize(seqRow);
  $("#id_sc_field_devolucion" + seqRow).click(function () {
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_event&nmgp_event=devolucion_onClick&script_case_init=" + document.F3.script_case_init.value + "&devolucion=" + grid_compras_dev_getValue("devolucion", seqRow) + ""
     })
     .done(function(jsonReturn) {
        var i, fieldDisplay, oResp;
        Tst_integrid = jsonReturn.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (jsonReturn);
            return;
        }
        eval("oResp = " + jsonReturn);
        if (oResp["setValue"]) {
          for (i = 0; i < oResp["setValue"].length; i++) {
            grid_compras_dev_setValue(oResp["setValue"][i]["field"], oResp["setValue"][i]["value"], seqRow);
          }
        }
        if (oResp["fieldDisplay"]) {
          for (i = 0; i < oResp["fieldDisplay"].length; i++) {
            if ("off" == oResp["fieldDisplay"][i]["status"]) {
              $("#id_sc_field_" + oResp["fieldDisplay"][i]["field"] + seqRow).hide();
            }
            else {
              $("#id_sc_field_" + oResp["fieldDisplay"][i]["field"] + seqRow).show();
            }
          }
        }
        if (oResp["Refresh"]) {
           nm_gp_move('igual');
        }
        if (oResp["redirInfo"]) {
           nmAjaxRedir(oResp);
        }
        if (oResp["htmOutput"]) {
           nmAjaxShowDebug(oResp);
        }
    });
  }).mouseover(function() { $(this).css("cursor", "pointer"); })
    .mouseout(function() { $(this).css("cursor", "pointer"); })
    .addClass(($("#id_sc_field_devolucion" + seqRow).parent().hasClass("scGridFieldOddFont") ? "scGridFieldOddLink" : "scGridFieldEvenLink"));

}

function scSeqNormalize(seqRow) {
  var newSeqRow = seqRow.toString();
  if ("" == newSeqRow) {
    return "";
  }
  if ("_" != newSeqRow.substr(0, 1)) {
    return "_" + newSeqRow;
  }
  return newSeqRow;
}
  function nmAjaxRedir(oTemp)
  {
    if (oTemp && oTemp != null) {
        oResp = oTemp;
    }
    if (!oResp["redirInfo"]) {
      return;
    }
    sMetodo = oResp["redirInfo"]["metodo"];
    sAction = oResp["redirInfo"]["action"];
    if ("location" == sMetodo) {
      if ("parent.parent" == oResp["redirInfo"]["target"]) {
        parent.parent.location = sAction;
      }
      else if ("parent" == oResp["redirInfo"]["target"]) {
        parent.location = sAction;
      }
      else if ("_blank" == oResp["redirInfo"]["target"]) {
        window.open(sAction, "_blank");
      }
      else {
        document.location = sAction;
      }
    }
    else if ("html" == sMetodo) {
        document.write(nmAjaxSpecCharParser(oResp["redirInfo"]["action"]));
    }
    else {
      if (oResp["redirInfo"]["target"] == "modal") {
          tb_show('', sAction + '?script_case_init=' +  oResp["redirInfo"]["script_case_init"] + '&nmgp_parms=' + oResp["redirInfo"]["nmgp_parms"] + '&nmgp_outra_jan=true&nmgp_url_saida=modal&NMSC_modal=ok&TB_iframe=true&modal=true&height=' +  oResp["redirInfo"]["h_modal"] + '&width=' + oResp["redirInfo"]["w_modal"], '');
          return;
      }
      sFormRedir = (oResp["redirInfo"]["nmgp_outra_jan"]) ? "form_ajax_redir_1" : "form_ajax_redir_2";
      document.forms[sFormRedir].action           = sAction;
      document.forms[sFormRedir].target           = oResp["redirInfo"]["target"];
      document.forms[sFormRedir].nmgp_parms.value = oResp["redirInfo"]["nmgp_parms"];
      if ("form_ajax_redir_1" == sFormRedir) {
        document.forms[sFormRedir].nmgp_outra_jan.value = oResp["redirInfo"]["nmgp_outra_jan"];
      }
      else {
        document.forms[sFormRedir].nmgp_url_saida.value   = oResp["redirInfo"]["nmgp_url_saida"];
        document.forms[sFormRedir].script_case_init.value = oResp["redirInfo"]["script_case_init"];
      }
      document.forms[sFormRedir].submit();
    }
  }
var json_err_crtl    = 1;
var Id_Btn_selected  = new Array();
var Css_Btn_selected = new Array();
Id_Btn_selected[0] = "selcmp_top";
Id_Btn_selected[1] = "ordcmp_top";
Id_Btn_selected[2] = "sel_groupby_top";
Id_Btn_selected[3] = "dynamic_search_top";
function ajax_navigate(opc, parm)
{
    var scrollNavigateReload = false, extraParams = "", iEvt, iStart = 0;
    for (ibtn = 0; ibtn < 4; ibtn++) {
        Css_Btn_selected[ibtn] = $("#" + Id_Btn_selected[ibtn]).attr('class');
    }
    nmAjaxProcOn();
    if (scrollNavigateReload) {
      extraParams += "&scrollNavigateReload=Y";
    }
    if (typeof parm !== "string") {
      parm = parm.toString();
    }
    parm = parm.replace(/[+]/g, "__NM_PLUS__");
    while (parm.lastIndexOf("&amp;") != -1)
    {
       parm = parm.replace("&amp;" , "__NM_AMP__");
    }
    parm = parm.replace(/[&]/g, "__NM_AMP__");
    parm = parm.replace(/[%]/g, "__NM_PRC__");
    parm_save = parm;
    return new Promise(function(resolve, reject) {$.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_navigate&script_case_init=" + document.F4.script_case_init.value + "&opc=" + opc  + "&parm=" + parm + extraParams
     })
     .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        if (json_err_crtl == 1) {
            json_err_crtl++;
            ajax_navigate(opc, parm);
        }
        else {
            nmAjaxProcOff();
            json_err_crtl = 1;
            alert (err);
        }
     })
     .done(function(jsonNavigate) {
        var i, oResp;
        json_err_crtl = 1;
        Tst_integrid = jsonNavigate.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (jsonNavigate);
            return;
        }
        eval("oResp = " + jsonNavigate);
        if (oResp["ss_time_out"]) {
            nmAjaxProcOff();
            nm_move();
        }
        document.getElementById('nmsc_iframe_liga_A_grid_compras_dev').src = 'NM_Blank_Page.htm';
        document.getElementById('nmsc_iframe_liga_E_grid_compras_dev').src = 'NM_Blank_Page.htm';
        document.getElementById('nmsc_iframe_liga_D_grid_compras_dev').src = 'NM_Blank_Page.htm';
        document.getElementById('nmsc_iframe_liga_B_grid_compras_dev').src = 'NM_Blank_Page.htm';
        document.getElementById('nmsc_iframe_liga_A_grid_compras_dev').style.height = '0px';
        document.getElementById('nmsc_iframe_liga_E_grid_compras_dev').style.height = '0px';
        document.getElementById('nmsc_iframe_liga_D_grid_compras_dev').style.height = '0px';
        document.getElementById('nmsc_iframe_liga_B_grid_compras_dev').style.height = '0px';
        document.getElementById('nmsc_iframe_liga_A_grid_compras_dev').style.width  = '0px';
        document.getElementById('nmsc_iframe_liga_E_grid_compras_dev').style.width  = '0px';
        document.getElementById('nmsc_iframe_liga_D_grid_compras_dev').style.width  = '0px';
        document.getElementById('nmsc_iframe_liga_B_grid_compras_dev').style.width  = '0px';
        if (oResp["setValue"]) {
          for (i = 0; i < oResp["setValue"].length; i++) {
            $("#" + oResp["setValue"][i]["field"]).html(oResp["setValue"][i]["value"]);
          }
        }
        if (oResp["setTitle"]) {
          for (i = 0; i < oResp["setTitle"].length; i++) {
            $("#" + oResp["setTitle"][i]["field"]).attr('title', oResp["setTitle"][i]["value"]);
          }
        }
        if (oResp["setArr"]) {
          for (i = 0; i < oResp["setArr"].length; i++) {
               eval (oResp["setArr"][i]["var"] + ' = new Array()');
          }
        }
        if (oResp["setVar"]) {
          for (i = 0; i < oResp["setVar"].length; i++) {
               eval (oResp["setVar"][i]["var"] + ' = \"' + oResp["setVar"][i]["value"] + '\"');
          }
        }
        if (oResp["fillArr"]) {
          for (i = 0; i < oResp["fillArr"].length; i++) {
               eval (oResp["fillArr"][i]["var"] + ' = {' + oResp["fillArr"][i]["value"] + '}');
          }
        }
        if (oResp["setDisplay"]) {
          for (i = 0; i < oResp["setDisplay"].length; i++) {
            if (document.getElementById(oResp["setDisplay"][i]["field"])) {
              document.getElementById(oResp["setDisplay"][i]["field"]).style.display = oResp["setDisplay"][i]["value"];
            }
          }
        }
        if (oResp["setVisibility"]) {
          for (i = 0; i < oResp["setVisibility"].length; i++) {
            if (document.getElementById(oResp["setVisibility"][i]["field"])) {
              document.getElementById(oResp["setVisibility"][i]["field"]).style.visibility = oResp["setVisibility"][i]["value"];
            }
          }
        }
        if (oResp["setDisabled"]) {
          for (i = 0; i < oResp["setDisabled"].length; i++) {
            if (document.getElementById(oResp["setDisabled"][i]["field"])) {
              document.getElementById(oResp["setDisabled"][i]["field"]).disabled = oResp["setDisabled"][i]["value"];
            }
          }
        }
        if (oResp["setClass"]) {
          for (i = 0; i < oResp["setClass"].length; i++) {
            if (document.getElementById(oResp["setClass"][i]["field"])) {
              document.getElementById(oResp["setClass"][i]["field"]).className = oResp["setClass"][i]["value"];
            }
          }
        }
        if (oResp["setSrc"]) {
          for (i = 0; i < oResp["setSrc"].length; i++) {
            if (document.getElementById(oResp["setSrc"][i]["field"])) {
              document.getElementById(oResp["setSrc"][i]["field"]).src = oResp["setSrc"][i]["value"];
            }
          }
        }
        if (oResp["remove_Obj"]) {
          for (i = 0; i < oResp["remove_Obj"].length; i++) {
            if (document.getElementById(oResp["remove_Obj"][i])) {
              document.getElementById(oResp["remove_Obj"][i]).remove();
            }
          }
        }
        if (oResp["redirInfo"]) {
           nmAjaxRedir(oResp);
        }
        if (oResp["AlertJS"]) {
          for (i = 0; i < oResp["AlertJS"].length; i++) {
              jsAlertParams = oResp["AlertJSParam"][i] ? oResp["AlertJSParam"][i] : {};
              scJs_alert (oResp["AlertJS"][i], jsAlertParams);
          }
        }
        if (oResp["exec_JS"]) {
          for (i = 0; i < oResp["exec_JS"].length; i++) {
               eval (oResp["exec_JS"][i]["function"] + '("' + oResp["exec_JS"][i]["parm"] + '");');
          }
        }
        if (oResp["htmOutput"]) {
           nmAjaxShowDebug(oResp);
        }
        if (!SC_Link_View)
        {
            if (Qsearch_ok)
            {
                scQuickSearchKeyUp('top', null);
            }
            SC_init_jquery(false);
            tb_init('a.thickbox, area.thickbox, input.thickbox');
        }
        for (ibtn = 0; ibtn < 4; ibtn++) {
             $("#" + Id_Btn_selected[ibtn]).attr('class', Css_Btn_selected[ibtn]);
        }
        nmAjaxProcOff();
        if (typeof(bootstrapMobile) === typeof(function(){})) bootstrapMobile();
        resolve();
    })});
}
function ajax_navigate_res(opc, parm)
{
    nmAjaxProcOn();
    parm_save = parm;
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_navigate&script_case_init=" + document.FRES.script_case_init.value + "&opc=" + opc  + "&parm=" + parm
     })
     .done(function(jsonNavigate) {
        var i, oResp;
        Tst_integrid = jsonNavigate.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (jsonNavigate);
            return;
        }
        eval("oResp = " + jsonNavigate);
        if (oResp["ss_time_out"]) {
            nmAjaxProcOff();
            nm_move();
        }
        if (oResp["setValue"]) {
          for (i = 0; i < oResp["setValue"].length; i++) {
            $("#" + oResp["setValue"][i]["field"]).html(oResp["setValue"][i]["value"]);
          }
        }
        if (oResp["setTitle"]) {
          for (i = 0; i < oResp["setTitle"].length; i++) {
            $("#" + oResp["setTitle"][i]["field"]).attr('title', oResp["setTitle"][i]["value"]);
          }
        }
        if (oResp["setArr"]) {
          for (i = 0; i < oResp["setArr"].length; i++) {
               eval (oResp["setArr"][i]["var"] + ' = new Array()');
          }
        }
        if (oResp["setVar"]) {
          for (i = 0; i < oResp["setVar"].length; i++) {
               eval (oResp["setVar"][i]["var"] + ' = \"' + oResp["setVar"][i]["value"] + '\"');
          }
        }
        if (oResp["fillArr"]) {
          for (i = 0; i < oResp["fillArr"].length; i++) {
               eval (oResp["fillArr"][i]["var"] + ' = {' + oResp["fillArr"][i]["value"] + '}');
          }
        }
        if (oResp["setDisplay"]) {
          for (i = 0; i < oResp["setDisplay"].length; i++) {
            if (document.getElementById(oResp["setDisplay"][i]["field"])) {
              document.getElementById(oResp["setDisplay"][i]["field"]).style.display = oResp["setDisplay"][i]["value"];
            }
          }
        }
        if (oResp["setVisibility"]) {
          for (i = 0; i < oResp["setVisibility"].length; i++) {
            if (document.getElementById(oResp["setVisibility"][i]["field"])) {
              document.getElementById(oResp["setVisibility"][i]["field"]).style.visibility = oResp["setVisibility"][i]["value"];
            }
          }
        }
        if (oResp["setDisabled"]) {
          for (i = 0; i < oResp["setDisabled"].length; i++) {
            if (document.getElementById(oResp["setDisabled"][i]["field"])) {
              document.getElementById(oResp["setDisabled"][i]["field"]).disabled = oResp["setDisabled"][i]["value"];
            }
          }
        }
        if (oResp["setClass"]) {
          for (i = 0; i < oResp["setClass"].length; i++) {
            if (document.getElementById(oResp["setClass"][i]["field"])) {
              document.getElementById(oResp["setClass"][i]["field"]).className = oResp["setClass"][i]["value"];
            }
          }
        }
        if (oResp["setSrc"]) {
          for (i = 0; i < oResp["setSrc"].length; i++) {
            if (document.getElementById(oResp["setSrc"][i]["field"])) {
              document.getElementById(oResp["setSrc"][i]["field"]).src = oResp["setSrc"][i]["value"];
            }
          }
        }
        if (oResp["redirInfo"]) {
           nmAjaxRedir(oResp);
        }
        if (oResp["AlertJS"]) {
          for (i = 0; i < oResp["AlertJS"].length; i++) {
              jsAlertParams = oResp["AlertJSParam"][i] ? oResp["AlertJSParam"][i] : {};
              scJs_alert (oResp["AlertJS"][i], jsAlertParams);
          }
        }
        if (oResp["exec_JS"]) {
          for (i = 0; i < oResp["exec_JS"].length; i++) {
               eval (oResp["exec_JS"][i]["function"] + '("' + oResp["exec_JS"][i]["parm"] + '");');
          }
        }
        if (oResp["htmOutput"]) {
           nmAjaxShowDebug(oResp);
        }
        nmAjaxProcOff();
        if (oResp["exec_script"]) {
          for (i = 0; i < oResp["exec_script"].length; i++) {
              eval (oResp["exec_script"][i]);
          }
        }
    });
}
function ajax_save_ancor(f_submit, ancor)
{
    nmAjaxProcOn();
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_save_ancor&script_case_init=" + document.F3.script_case_init.value + "&ancor_save=" + ancor
     })
     .done(function(jsonSaveAncor) {
        nmAjaxProcOff();
        eval ("document." + f_submit + ".submit()");
    });
}
var strPath         = '';
var strTitle        = '';
var showAjaxProcess = true;
function ajax_export(tp_export, parms, strCallback, showAjax)
{
    strPath  = '';
    strTitle  = '';
    showAjaxProcess = showAjax;
    if(showAjaxProcess)
    {
        nmAjaxProcOn();
    }
    $.ajax({
      type: "POST",
      url: "index.php",
      data: "nmgp_opcao=ajax_export&export_opc=" + tp_export + parms + "&script_case_init=" + document.F3.script_case_init.value,
      complete: strCallback,
     })
     .done(function(jsonFile) {
        var oResp;
        Tst_integrid = jsonFile.trim();
        if ("{" != Tst_integrid.substr(0, 1)) {
            nmAjaxProcOff();
            alert (jsonFile);
            return;
        }
        eval("oResp = " + jsonFile);
        if (oResp["ss_time_out"]) {
            nmAjaxProcOff();
            nm_move();
        }
        if (oResp["htmOutput"]) {
           nmAjaxShowDebug(oResp);
        }
        if (oResp["file_export"]) {
            strPath = oResp['file_export'];
            strTitle = oResp['title_export'];
        }
        if(showAjaxProcess)
        {
            nmAjaxProcOff();
        }
    });
}

