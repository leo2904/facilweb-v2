<?php
//
class form_webservicefe_apl
{
   var $has_where_params = false;
   var $NM_is_redirected = false;
   var $NM_non_ajax_info = false;
   var $formatado = false;
   var $use_100perc_fields = false;
   var $classes_100perc_fields = array();
   var $NM_ajax_flag    = false;
   var $NM_ajax_opcao   = '';
   var $NM_ajax_retorno = '';
   var $NM_ajax_info    = array('result'            => '',
                                'param'             => array(),
                                'autoComp'          => '',
                                'rsSize'            => '',
                                'msgDisplay'        => '',
                                'errList'           => array(),
                                'fldList'           => array(),
                                'varList'           => array(),
                                'focus'             => '',
                                'navStatus'         => array(),
                                'redir'             => array(),
                                'blockDisplay'      => array(),
                                'fieldDisplay'      => array(),
                                'fieldLabel'        => array(),
                                'readOnly'          => array(),
                                'btnVars'           => array(),
                                'ajaxAlert'         => array(),
                                'ajaxMessage'       => array(),
                                'ajaxJavascript'    => array(),
                                'buttonDisplay'     => array(),
                                'buttonDisplayVert' => array(),
                                'calendarReload'    => false,
                                'quickSearchRes'    => false,
                                'displayMsg'        => false,
                                'displayMsgTxt'     => '',
                                'dyn_search'        => array(),
                                'empty_filter'      => '',
                                'event_field'       => '',
                                'fieldsWithErrors'  => array(),
                               );
   var $NM_ajax_force_values = false;
   var $Nav_permite_ava     = true;
   var $Nav_permite_ret     = true;
   var $Apl_com_erro        = false;
   var $app_is_initializing = false;
   var $Ini;
   var $Erro;
   var $Db;
   var $idwebservicefe;
   var $proveedor;
   var $proveedor_1;
   var $servidor1;
   var $servidor2;
   var $tokenempresa;
   var $tokenpassword;
   var $servidor_prueba1;
   var $servidor_prueba2;
   var $token_prueba;
   var $password_prueba;
   var $modo;
   var $modo_1;
   var $enviar_dian;
   var $enviar_dian_1;
   var $enviar_cliente;
   var $enviar_cliente_1;
   var $servidor3;
   var $servidor_prueba3;
   var $url_api_pdfs;
   var $url_api_sendmail;
   var $envio_credenciales;
   var $envio_credenciales_1;
   var $plantillas_correo;
   var $plantillas_correo_1;
   var $copia_factura_a;
   var $plantilla_pordefecto;
   var $plantilla_pordefecto_1;
   var $proveedor_anterior;
   var $proveedor_anterior_1;
   var $servidor_anterior1;
   var $servidor_anterior2;
   var $servidor_anterior3;
   var $token_anterior;
   var $password_anterior;
   var $servidor4;
   var $servidor5;
   var $nm_data;
   var $nmgp_opcao;
   var $nmgp_opc_ant;
   var $sc_evento;
   var $nmgp_clone;
   var $nmgp_return_img = array();
   var $nmgp_dados_form = array();
   var $nmgp_dados_select = array();
   var $nm_location;
   var $nm_flag_iframe;
   var $nm_flag_saida_novo;
   var $nmgp_botoes = array();
   var $nmgp_url_saida;
   var $nmgp_form_show;
   var $nmgp_form_empty;
   var $nmgp_cmp_readonly = array();
   var $nmgp_cmp_hidden = array();
   var $form_paginacao = 'parcial';
   var $lig_edit_lookup      = false;
   var $lig_edit_lookup_call = false;
   var $lig_edit_lookup_cb   = '';
   var $lig_edit_lookup_row  = '';
   var $is_calendar_app = false;
   var $Embutida_call  = false;
   var $Embutida_ronly = false;
   var $Embutida_proc  = false;
   var $Embutida_form  = false;
   var $Grid_editavel  = false;
   var $url_webhelp = '';
   var $nm_todas_criticas;
   var $Campos_Mens_erro;
   var $nm_new_label = array();
   var $record_insert_ok = false;
   var $record_delete_ok = false;
//
//----- 
   function ini_controle()
   {
        global $nm_url_saida, $teste_validade, $script_case_init, 
               $glo_senha_protect, $nm_apl_dependente, $nm_form_submit, $sc_check_excl, $nm_opc_form_php, $nm_call_php, $nm_opc_lookup;


      if ($this->NM_ajax_flag)
      {
          if (isset($this->NM_ajax_info['param']['copia_factura_a']))
          {
              $this->copia_factura_a = $this->NM_ajax_info['param']['copia_factura_a'];
          }
          if (isset($this->NM_ajax_info['param']['csrf_token']))
          {
              $this->csrf_token = $this->NM_ajax_info['param']['csrf_token'];
          }
          if (isset($this->NM_ajax_info['param']['enviar_cliente']))
          {
              $this->enviar_cliente = $this->NM_ajax_info['param']['enviar_cliente'];
          }
          if (isset($this->NM_ajax_info['param']['enviar_dian']))
          {
              $this->enviar_dian = $this->NM_ajax_info['param']['enviar_dian'];
          }
          if (isset($this->NM_ajax_info['param']['envio_credenciales']))
          {
              $this->envio_credenciales = $this->NM_ajax_info['param']['envio_credenciales'];
          }
          if (isset($this->NM_ajax_info['param']['idwebservicefe']))
          {
              $this->idwebservicefe = $this->NM_ajax_info['param']['idwebservicefe'];
          }
          if (isset($this->NM_ajax_info['param']['modo']))
          {
              $this->modo = $this->NM_ajax_info['param']['modo'];
          }
          if (isset($this->NM_ajax_info['param']['nm_form_submit']))
          {
              $this->nm_form_submit = $this->NM_ajax_info['param']['nm_form_submit'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_ancora']))
          {
              $this->nmgp_ancora = $this->NM_ajax_info['param']['nmgp_ancora'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_arg_dyn_search']))
          {
              $this->nmgp_arg_dyn_search = $this->NM_ajax_info['param']['nmgp_arg_dyn_search'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_num_form']))
          {
              $this->nmgp_num_form = $this->NM_ajax_info['param']['nmgp_num_form'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_opcao']))
          {
              $this->nmgp_opcao = $this->NM_ajax_info['param']['nmgp_opcao'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_ordem']))
          {
              $this->nmgp_ordem = $this->NM_ajax_info['param']['nmgp_ordem'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_parms']))
          {
              $this->nmgp_parms = $this->NM_ajax_info['param']['nmgp_parms'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_url_saida']))
          {
              $this->nmgp_url_saida = $this->NM_ajax_info['param']['nmgp_url_saida'];
          }
          if (isset($this->NM_ajax_info['param']['password_anterior']))
          {
              $this->password_anterior = $this->NM_ajax_info['param']['password_anterior'];
          }
          if (isset($this->NM_ajax_info['param']['password_prueba']))
          {
              $this->password_prueba = $this->NM_ajax_info['param']['password_prueba'];
          }
          if (isset($this->NM_ajax_info['param']['plantilla_pordefecto']))
          {
              $this->plantilla_pordefecto = $this->NM_ajax_info['param']['plantilla_pordefecto'];
          }
          if (isset($this->NM_ajax_info['param']['plantillas_correo']))
          {
              $this->plantillas_correo = $this->NM_ajax_info['param']['plantillas_correo'];
          }
          if (isset($this->NM_ajax_info['param']['proveedor']))
          {
              $this->proveedor = $this->NM_ajax_info['param']['proveedor'];
          }
          if (isset($this->NM_ajax_info['param']['proveedor_anterior']))
          {
              $this->proveedor_anterior = $this->NM_ajax_info['param']['proveedor_anterior'];
          }
          if (isset($this->NM_ajax_info['param']['script_case_init']))
          {
              $this->script_case_init = $this->NM_ajax_info['param']['script_case_init'];
          }
          if (isset($this->NM_ajax_info['param']['servidor1']))
          {
              $this->servidor1 = $this->NM_ajax_info['param']['servidor1'];
          }
          if (isset($this->NM_ajax_info['param']['servidor2']))
          {
              $this->servidor2 = $this->NM_ajax_info['param']['servidor2'];
          }
          if (isset($this->NM_ajax_info['param']['servidor3']))
          {
              $this->servidor3 = $this->NM_ajax_info['param']['servidor3'];
          }
          if (isset($this->NM_ajax_info['param']['servidor4']))
          {
              $this->servidor4 = $this->NM_ajax_info['param']['servidor4'];
          }
          if (isset($this->NM_ajax_info['param']['servidor5']))
          {
              $this->servidor5 = $this->NM_ajax_info['param']['servidor5'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_anterior1']))
          {
              $this->servidor_anterior1 = $this->NM_ajax_info['param']['servidor_anterior1'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_anterior2']))
          {
              $this->servidor_anterior2 = $this->NM_ajax_info['param']['servidor_anterior2'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_anterior3']))
          {
              $this->servidor_anterior3 = $this->NM_ajax_info['param']['servidor_anterior3'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_prueba1']))
          {
              $this->servidor_prueba1 = $this->NM_ajax_info['param']['servidor_prueba1'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_prueba2']))
          {
              $this->servidor_prueba2 = $this->NM_ajax_info['param']['servidor_prueba2'];
          }
          if (isset($this->NM_ajax_info['param']['servidor_prueba3']))
          {
              $this->servidor_prueba3 = $this->NM_ajax_info['param']['servidor_prueba3'];
          }
          if (isset($this->NM_ajax_info['param']['token_anterior']))
          {
              $this->token_anterior = $this->NM_ajax_info['param']['token_anterior'];
          }
          if (isset($this->NM_ajax_info['param']['token_prueba']))
          {
              $this->token_prueba = $this->NM_ajax_info['param']['token_prueba'];
          }
          if (isset($this->NM_ajax_info['param']['tokenempresa']))
          {
              $this->tokenempresa = $this->NM_ajax_info['param']['tokenempresa'];
          }
          if (isset($this->NM_ajax_info['param']['tokenpassword']))
          {
              $this->tokenpassword = $this->NM_ajax_info['param']['tokenpassword'];
          }
          if (isset($this->NM_ajax_info['param']['url_api_pdfs']))
          {
              $this->url_api_pdfs = $this->NM_ajax_info['param']['url_api_pdfs'];
          }
          if (isset($this->NM_ajax_info['param']['url_api_sendmail']))
          {
              $this->url_api_sendmail = $this->NM_ajax_info['param']['url_api_sendmail'];
          }
          if (isset($this->nmgp_refresh_fields))
          {
              $this->nmgp_refresh_fields = explode('_#fld#_', $this->nmgp_refresh_fields);
              $this->nmgp_opcao          = 'recarga';
          }
          if (!isset($this->nmgp_refresh_row))
          {
              $this->nmgp_refresh_row = '';
          }
      }

      $this->sc_conv_var = array();
      if (!empty($_FILES))
      {
          foreach ($_FILES as $nmgp_campo => $nmgp_valores)
          {
               if (isset($this->sc_conv_var[$nmgp_campo]))
               {
                   $nmgp_campo = $this->sc_conv_var[$nmgp_campo];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_campo)]))
               {
                   $nmgp_campo = $this->sc_conv_var[strtolower($nmgp_campo)];
               }
               $tmp_scfile_name     = $nmgp_campo . "_scfile_name";
               $tmp_scfile_type     = $nmgp_campo . "_scfile_type";
               $this->$nmgp_campo = is_array($nmgp_valores['tmp_name']) ? $nmgp_valores['tmp_name'][0] : $nmgp_valores['tmp_name'];
               $this->$tmp_scfile_type   = is_array($nmgp_valores['type'])     ? $nmgp_valores['type'][0]     : $nmgp_valores['type'];
               $this->$tmp_scfile_name   = is_array($nmgp_valores['name'])     ? $nmgp_valores['name'][0]     : $nmgp_valores['name'];
          }
      }
      $Sc_lig_md5 = false;
      if (!empty($_POST))
      {
          foreach ($_POST as $nmgp_var => $nmgp_val)
          {
               if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
               {
                   $nmgp_var = substr($nmgp_var, 11);
                   $nmgp_val = $_SESSION[$nmgp_val];
               }
              if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
              {
                  $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                  if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                  {
                      $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                      $Sc_lig_md5 = true;
                  }
                  else
                  {
                      $_SESSION['sc_session']['SC_parm_violation'] = true;
                  }
              }
               if (isset($this->sc_conv_var[$nmgp_var]))
               {
                   $nmgp_var = $this->sc_conv_var[$nmgp_var];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_var)]))
               {
                   $nmgp_var = $this->sc_conv_var[strtolower($nmgp_var)];
               }
               $nmgp_val = NM_decode_input($nmgp_val);
               $this->$nmgp_var = $nmgp_val;
          }
      }
      if (!empty($_GET))
      {
          foreach ($_GET as $nmgp_var => $nmgp_val)
          {
               if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
               {
                   $nmgp_var = substr($nmgp_var, 11);
                   $nmgp_val = $_SESSION[$nmgp_val];
               }
              if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
              {
                  $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                  if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                  {
                      $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                      $Sc_lig_md5 = true;
                  }
                  else
                  {
                       $_SESSION['sc_session']['SC_parm_violation'] = true;
                  }
              }
               if (isset($this->sc_conv_var[$nmgp_var]))
               {
                   $nmgp_var = $this->sc_conv_var[$nmgp_var];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_var)]))
               {
                   $nmgp_var = $this->sc_conv_var[strtolower($nmgp_var)];
               }
               $nmgp_val = NM_decode_input($nmgp_val);
               $this->$nmgp_var = $nmgp_val;
          }
      }
      if (isset($SC_lig_apl_orig) && !$Sc_lig_md5 && (!isset($nmgp_parms) || ($nmgp_parms != "SC_null" && substr($nmgp_parms, 0, 8) != "OrScLink")))
      {
          $_SESSION['sc_session']['SC_parm_violation'] = true;
      }
      if (isset($nmgp_parms) && $nmgp_parms == "SC_null")
      {
          $nmgp_parms = "";
      }
      if (isset($this->nmgp_opcao) && $this->nmgp_opcao == "reload_novo") {
          $_POST['nmgp_opcao'] = "novo";
          $this->nmgp_opcao    = "novo";
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['opcao']   = "novo";
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['opc_ant'] = "inicio";
      }
      if (isset($_SESSION['sc_session'][$script_case_init]['form_webservicefe']['embutida_parms']))
      { 
          $this->nmgp_parms = $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['embutida_parms'];
          unset($_SESSION['sc_session'][$script_case_init]['form_webservicefe']['embutida_parms']);
      } 
      if (isset($this->nmgp_parms) && !empty($this->nmgp_parms)) 
      { 
          if (isset($_SESSION['nm_aba_bg_color'])) 
          { 
              unset($_SESSION['nm_aba_bg_color']);
          }   
          $nmgp_parms = NM_decode_input($nmgp_parms);
          $nmgp_parms = str_replace("@aspass@", "'", $this->nmgp_parms);
          $nmgp_parms = str_replace("*scout", "?@?", $nmgp_parms);
          $nmgp_parms = str_replace("*scin", "?#?", $nmgp_parms);
          $todox = str_replace("?#?@?@?", "?#?@ ?@?", $nmgp_parms);
          $todo  = explode("?@?", $todox);
          $ix = 0;
          while (!empty($todo[$ix]))
          {
             $cadapar = explode("?#?", $todo[$ix]);
             if (1 < sizeof($cadapar))
             {
                if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                {
                    $cadapar[0] = substr($cadapar[0], 11);
                    $cadapar[1] = $_SESSION[$cadapar[1]];
                }
                 if (isset($this->sc_conv_var[$cadapar[0]]))
                 {
                     $cadapar[0] = $this->sc_conv_var[$cadapar[0]];
                 }
                 elseif (isset($this->sc_conv_var[strtolower($cadapar[0])]))
                 {
                     $cadapar[0] = $this->sc_conv_var[strtolower($cadapar[0])];
                 }
                 nm_limpa_str_form_webservicefe($cadapar[1]);
                 if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                 $Tmp_par = $cadapar[0];
                 $this->$Tmp_par = $cadapar[1];
             }
             $ix++;
          }
          if (isset($this->NM_where_filter_form))
          {
              $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['where_filter_form'] = $this->NM_where_filter_form;
              unset($_SESSION['sc_session'][$script_case_init]['form_webservicefe']['total']);
          }
          if (isset($this->sc_redir_atualiz))
          {
              $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['sc_redir_atualiz'] = $this->sc_redir_atualiz;
          }
          if (isset($this->sc_redir_insert))
          {
              $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['sc_redir_insert'] = $this->sc_redir_insert;
          }
      } 
      elseif (isset($script_case_init) && !empty($script_case_init) && isset($_SESSION['sc_session'][$script_case_init]['form_webservicefe']['parms']))
      {
          if ((!isset($this->nmgp_opcao) || ($this->nmgp_opcao != "incluir" && $this->nmgp_opcao != "alterar" && $this->nmgp_opcao != "excluir" && $this->nmgp_opcao != "novo" && $this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form")) && (!isset($this->NM_ajax_opcao) || $this->NM_ajax_opcao == ""))
          {
              $todox = str_replace("?#?@?@?", "?#?@ ?@?", $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['parms']);
              $todo  = explode("?@?", $todox);
              $ix = 0;
              while (!empty($todo[$ix]))
              {
                 $cadapar = explode("?#?", $todo[$ix]);
                 if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                 {
                     $cadapar[0] = substr($cadapar[0], 11);
                     $cadapar[1] = $_SESSION[$cadapar[1]];
                 }
                 if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                 $Tmp_par = $cadapar[0];
                 $this->$Tmp_par = $cadapar[1];
                 $ix++;
              }
          }
      } 

      if (isset($this->nm_run_menu) && $this->nm_run_menu == 1)
      { 
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['nm_run_menu'] = 1;
      } 
      if (!$this->NM_ajax_flag && 'autocomp_' == substr($this->NM_ajax_opcao, 0, 9))
      {
          $this->NM_ajax_flag = true;
      }

      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      if (isset($this->nm_evt_ret_edit) && '' != $this->nm_evt_ret_edit)
      {
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup']     = true;
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup_cb']  = $this->nm_evt_ret_edit;
          $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup_row'] = isset($this->nm_evt_ret_row) ? $this->nm_evt_ret_row : '';
      }
      if (isset($_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup']) && $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup'])
      {
          $this->lig_edit_lookup     = true;
          $this->lig_edit_lookup_cb  = $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup_cb'];
          $this->lig_edit_lookup_row = $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['lig_edit_lookup_row'];
      }
      if (!$this->Ini)
      { 
          $this->Ini = new form_webservicefe_ini(); 
          $this->Ini->init();
          $this->nm_data = new nm_data("es");
          $this->app_is_initializing = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['initialize'];
      } 
      else 
      { 
         $this->nm_data = new nm_data("es");
      } 
      $_SESSION['sc_session'][$script_case_init]['form_webservicefe']['upload_field_info'] = array();

      unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue']);
      $this->Change_Menu = false;
      $run_iframe = (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe']) && ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "R")) ? true : false;
      if (!$run_iframe && isset($_SESSION['scriptcase']['menu_atual']) && !$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_call'] && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_outra_jan']) || !$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_outra_jan']))
      {
          $this->sc_init_menu = "x";
          if (isset($_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['form_webservicefe']))
          {
              $this->sc_init_menu = $_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['form_webservicefe'];
          }
          elseif (isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']]))
          {
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']] as $init => $resto)
              {
                  if ($this->Ini->sc_page == $init)
                  {
                      $this->sc_init_menu = $init;
                      break;
                  }
              }
          }
          if ($this->Ini->sc_page == $this->sc_init_menu && !isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_webservicefe']))
          {
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_webservicefe']['link'] = $this->Ini->sc_protocolo . $this->Ini->server . $this->Ini->path_link . "" . SC_dir_app_name('form_webservicefe') . "/";
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_webservicefe']['label'] = "WebService Proveedor documentos electrónicos";
               $this->Change_Menu = true;
          }
          elseif ($this->Ini->sc_page == $this->sc_init_menu)
          {
              $achou = false;
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu] as $apl => $parms)
              {
                  if ($apl == "form_webservicefe")
                  {
                      $achou = true;
                  }
                  elseif ($achou)
                  {
                      unset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu][$apl]);
                      $this->Change_Menu = true;
                  }
              }
          }
      }
      if (!function_exists("nmButtonOutput"))
      {
          include_once($this->Ini->path_lib_php . "nm_gp_config_btn.php");
      }
      include("../_lib/css/" . $this->Ini->str_schema_all . "_form.php");
      $this->Ini->Str_btn_form = (isset($_SESSION['scriptcase']['str_button_all'])) ? $_SESSION['scriptcase']['str_button_all'] : "scriptcase9_BlueBerry";
      $_SESSION['scriptcase']['str_button_all'] = $this->Ini->Str_btn_form;
      include($this->Ini->path_btn . $this->Ini->Str_btn_form . '/' . $this->Ini->Str_btn_form . $_SESSION['scriptcase']['reg_conf']['css_dir'] . '.php');
      $_SESSION['scriptcase']['css_form_help'] = '../_lib/css/' . $this->Ini->str_schema_all . "_form.css";
      $_SESSION['scriptcase']['css_form_help_dir'] = '../_lib/css/' . $this->Ini->str_schema_all . "_form" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css";
      $this->Db = $this->Ini->Db; 
      $this->Ini->str_google_fonts = isset($str_google_fonts)?$str_google_fonts:'';
      $this->Ini->Img_sep_form    = "/" . trim($str_toolbar_separator);
      $this->Ini->Color_bg_ajax   = "" == trim($str_ajax_bg)         ? "#000" : $str_ajax_bg;
      $this->Ini->Border_c_ajax   = "" == trim($str_ajax_border_c)   ? ""     : $str_ajax_border_c;
      $this->Ini->Border_s_ajax   = "" == trim($str_ajax_border_s)   ? ""     : $str_ajax_border_s;
      $this->Ini->Border_w_ajax   = "" == trim($str_ajax_border_w)   ? ""     : $str_ajax_border_w;
      $this->Ini->Block_img_exp   = "" == trim($str_block_exp)       ? ""     : $str_block_exp;
      $this->Ini->Block_img_col   = "" == trim($str_block_col)       ? ""     : $str_block_col;
      $this->Ini->Msg_ico_title   = "" == trim($str_msg_ico_title)   ? ""     : $str_msg_ico_title;
      $this->Ini->Msg_ico_body    = "" == trim($str_msg_ico_body)    ? ""     : $str_msg_ico_body;
      $this->Ini->Err_ico_title   = "" == trim($str_err_ico_title)   ? ""     : $str_err_ico_title;
      $this->Ini->Err_ico_body    = "" == trim($str_err_ico_body)    ? ""     : $str_err_ico_body;
      $this->Ini->Cal_ico_back    = "" == trim($str_cal_ico_back)    ? ""     : $str_cal_ico_back;
      $this->Ini->Cal_ico_for     = "" == trim($str_cal_ico_for)     ? ""     : $str_cal_ico_for;
      $this->Ini->Cal_ico_close   = "" == trim($str_cal_ico_close)   ? ""     : $str_cal_ico_close;
      $this->Ini->Tab_space       = "" == trim($str_tab_space)       ? ""     : $str_tab_space;
      $this->Ini->Bubble_tail     = "" == trim($str_bubble_tail)     ? ""     : $str_bubble_tail;
      $this->Ini->Label_sort_pos  = "" == trim($str_label_sort_pos)  ? ""     : $str_label_sort_pos;
      $this->Ini->Label_sort      = "" == trim($str_label_sort)      ? ""     : $str_label_sort;
      $this->Ini->Label_sort_asc  = "" == trim($str_label_sort_asc)  ? ""     : $str_label_sort_asc;
      $this->Ini->Label_sort_desc = "" == trim($str_label_sort_desc) ? ""     : $str_label_sort_desc;
      $this->Ini->Img_status_ok       = "" == trim($str_img_status_ok)   ? ""     : $str_img_status_ok;
      $this->Ini->Img_status_err      = "" == trim($str_img_status_err)  ? ""     : $str_img_status_err;
      $this->Ini->Css_status          = "scFormInputError";
      $this->Ini->Css_status_pwd_box  = "scFormInputErrorPwdBox";
      $this->Ini->Css_status_pwd_text = "scFormInputErrorPwdText";
      $this->Ini->Error_icon_span = "" == trim($str_error_icon_span) ? false  : "message" == $str_error_icon_span;
      $this->Ini->Img_qs_search        = "" == trim($img_qs_search)        ? "scriptcase__NM__qs_lupa.png"  : $img_qs_search;
      $this->Ini->Img_qs_clean         = "" == trim($img_qs_clean)         ? "scriptcase__NM__qs_close.png" : $img_qs_clean;
      $this->Ini->Str_qs_image_padding = "" == trim($str_qs_image_padding) ? "0"                            : $str_qs_image_padding;
      $this->Ini->App_div_tree_img_col = trim($app_div_str_tree_col);
      $this->Ini->App_div_tree_img_exp = trim($app_div_str_tree_exp);
      $this->Ini->form_table_width     = isset($str_form_table_width) && '' != trim($str_form_table_width) ? $str_form_table_width : '';

        $this->classes_100perc_fields['table'] = '';
        $this->classes_100perc_fields['input'] = '';
        $this->classes_100perc_fields['span_input'] = '';
        $this->classes_100perc_fields['span_select'] = '';
        $this->classes_100perc_fields['style_category'] = '';
        $this->classes_100perc_fields['keep_field_size'] = true;



      $_SESSION['scriptcase']['error_icon']['form_webservicefe']  = "<img src=\"" . $this->Ini->path_icones . "/scriptcase__NM__btn__NM__scriptcase9_Rhino__NM__nm_scriptcase9_Rhino_error.png\" style=\"border-width: 0px\" align=\"top\">&nbsp;";
      $_SESSION['scriptcase']['error_close']['form_webservicefe'] = "<td>" . nmButtonOutput($this->arr_buttons, "berrm_clse", "document.getElementById('id_error_display_fixed').style.display = 'none'; document.getElementById('id_error_message_fixed').innerHTML = ''; return false", "document.getElementById('id_error_display_fixed').style.display = 'none'; document.getElementById('id_error_message_fixed').innerHTML = ''; return false", "", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "") . "</td>";

      $this->Embutida_proc = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_proc']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_proc'] : $this->Embutida_proc;
      $this->Embutida_form = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_form']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_form'] : $this->Embutida_form;
      $this->Embutida_call = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_call']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_call'] : $this->Embutida_call;

       $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['table_refresh'] = false;

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit'])
      {
          $this->Grid_editavel = ('on' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit']) ? true : false;
      }
      if (isset($this->Grid_editavel) && $this->Grid_editavel)
      {
          $this->Embutida_form  = true;
          $this->Embutida_ronly = true;
      }
      $this->Embutida_multi = false;
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_multi']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_multi'])
      {
          $this->Grid_editavel  = false;
          $this->Embutida_form  = false;
          $this->Embutida_ronly = false;
          $this->Embutida_multi = true;
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_tp_pag']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_tp_pag'])
      {
          $this->form_paginacao = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_tp_pag'];
      }

      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_form']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_form'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_form'] = $this->Embutida_form;
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit'] = $this->Grid_editavel ? 'on' : 'off';
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_grid_edit'] = $this->Embutida_call;
      }

      $this->Ini->cor_grid_par = $this->Ini->cor_grid_impar;
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nmgp_url_saida  = $nm_url_saida;
      $this->nmgp_form_show  = "on";
      $this->nmgp_form_empty = false;
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_valida.php", "C", "NM_Valida") ; 
      $teste_validade = new NM_Valida ;

      $this->loadFieldConfig();

      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['first_time'])
      {
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['new']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto']);
      }
      $this->NM_cancel_return_new = (isset($this->NM_cancel_return_new) && $this->NM_cancel_return_new == 1) ? "1" : "";
      $this->NM_cancel_insert_new = ((isset($this->NM_cancel_insert_new) && $this->NM_cancel_insert_new == 1) || $this->NM_cancel_return_new == 1) ? "document.F5.action='" . $nm_url_saida . "';" : "";
      if (isset($this->NM_btn_insert) && '' != $this->NM_btn_insert && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert']))
      {
          if ('N' == $this->NM_btn_insert)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert'] = 'on';
          }
      }
      if (isset($this->NM_btn_new) && 'N' == $this->NM_btn_new)
      {
          $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['new'] = 'off';
      }
      if (isset($this->NM_btn_update) && '' != $this->NM_btn_update && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['update']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['update']))
      {
          if ('N' == $this->NM_btn_update)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update'] = 'on';
          }
      }
      if (isset($this->NM_btn_delete) && '' != $this->NM_btn_delete && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['delete']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['delete']))
      {
          if ('N' == $this->NM_btn_delete)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete'] = 'on';
          }
      }
      if (isset($this->NM_btn_navega) && '' != $this->NM_btn_navega)
      {
          if ('N' == $this->NM_btn_navega)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first']     = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back']      = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last']      = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch'] = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto']      = 'off';
              $this->Nav_permite_ava = false;
              $this->Nav_permite_ret = false;
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first']     = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back']      = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last']      = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch'] = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto']      = 'on';
          }
      }

      $this->nmgp_botoes['cancel'] = "on";
      $this->nmgp_botoes['exit'] = "on";
      $this->nmgp_botoes['new']  = "off";
      $this->nmgp_botoes['copy'] = "off";
      $this->nmgp_botoes['insert'] = "off";
      $this->nmgp_botoes['update'] = "on";
      $this->nmgp_botoes['delete'] = "off";
      $this->nmgp_botoes['first'] = "off";
      $this->nmgp_botoes['back'] = "off";
      $this->nmgp_botoes['forward'] = "off";
      $this->nmgp_botoes['last'] = "off";
      $this->nmgp_botoes['summary'] = "off";
      $this->nmgp_botoes['navpage'] = "off";
      $this->nmgp_botoes['goto'] = "off";
      $this->nmgp_botoes['qtline'] = "off";
      $this->nmgp_botoes['reload'] = "off";
      if (isset($this->NM_btn_cancel) && 'N' == $this->NM_btn_cancel)
      {
          $this->nmgp_botoes['cancel'] = "off";
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_orig'] = " where idwebservicefe='1'";
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_pesq']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_pesq'] = " where idwebservicefe='1'";
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_pesq_filtro'] = "";
      }
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_pesq_filtro'];
      if ($this->NM_ajax_flag && 'event_' == substr($this->NM_ajax_opcao, 0, 6)) {
          $this->nmgp_botoes = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['buttonStatus'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['iframe_filtro']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['iframe_filtro'] == "S")
      {
          $this->nmgp_botoes['exit'] = "off";
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['btn_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
          {
              $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
          }
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['insert'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['new']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['new'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['new'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['update'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['delete'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first'] != '')
      {
          $this->nmgp_botoes['first'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['first'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back'] != '')
      {
          $this->nmgp_botoes['back'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['back'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward'] != '')
      {
          $this->nmgp_botoes['forward'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['forward'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last'] != '')
      {
          $this->nmgp_botoes['last'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['last'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch'] != '')
      {
          $this->nmgp_botoes['qsearch'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['qsearch'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch'] != '')
      {
          $this->nmgp_botoes['dynsearch'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['dynsearch'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary'] != '')
      {
          $this->nmgp_botoes['summary'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['summary'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage'] != '')
      {
          $this->nmgp_botoes['navpage'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['navpage'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto'] != '')
      {
          $this->nmgp_botoes['goto'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_webservicefe']['goto'];
      }

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_insert']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_insert'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_update']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_update'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_delete']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_delete'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav'] != '')
      {
          $this->nmgp_botoes['first']   = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['back']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['forward'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['last']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['embutida_liga_form_btn_nav'];
      }

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['under_dashboard']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['under_dashboard'] && !$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['maximized']) {
          $tmpDashboardApp = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['dashboard_app'];
          if (isset($_SESSION['scriptcase']['dashboard_toolbar'][$tmpDashboardApp]['form_webservicefe'])) {
              $tmpDashboardButtons = $_SESSION['scriptcase']['dashboard_toolbar'][$tmpDashboardApp]['form_webservicefe'];

              $this->nmgp_botoes['update']     = $tmpDashboardButtons['form_update']    ? 'on' : 'off';
              $this->nmgp_botoes['new']        = $tmpDashboardButtons['form_insert']    ? 'on' : 'off';
              $this->nmgp_botoes['insert']     = $tmpDashboardButtons['form_insert']    ? 'on' : 'off';
              $this->nmgp_botoes['delete']     = $tmpDashboardButtons['form_delete']    ? 'on' : 'off';
              $this->nmgp_botoes['copy']       = $tmpDashboardButtons['form_copy']      ? 'on' : 'off';
              $this->nmgp_botoes['first']      = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['back']       = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['last']       = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['forward']    = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['navpage']    = $tmpDashboardButtons['form_navpage']   ? 'on' : 'off';
              $this->nmgp_botoes['goto']       = $tmpDashboardButtons['form_goto']      ? 'on' : 'off';
              $this->nmgp_botoes['qtline']     = $tmpDashboardButtons['form_lineqty']   ? 'on' : 'off';
              $this->nmgp_botoes['summary']    = $tmpDashboardButtons['form_summary']   ? 'on' : 'off';
              $this->nmgp_botoes['qsearch']    = $tmpDashboardButtons['form_qsearch']   ? 'on' : 'off';
              $this->nmgp_botoes['dynsearch']  = $tmpDashboardButtons['form_dynsearch'] ? 'on' : 'off';
              $this->nmgp_botoes['reload']     = $tmpDashboardButtons['form_reload']    ? 'on' : 'off';
          }
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert']) && $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['insert'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['update']) && $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['update'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['delete']) && $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['delete'];
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->nmgp_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
              $this->NM_ajax_info['fieldDisplay'][$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_readonly']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_readonly']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['field_readonly'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->nmgp_cmp_readonly[$NM_cada_field] = "on";
              $this->NM_ajax_info['readOnly'][$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['exit']) && $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['exit'] != '')
      {
          $_SESSION['scriptcase']['sc_url_saida'][$this->Ini->sc_page]       = $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['exit'];
          $_SESSION['scriptcase']['sc_force_url_saida'][$this->Ini->sc_page] = true;
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form']))
      {
          $this->nmgp_dados_form = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form'];
          if (!isset($this->idwebservicefe)){$this->idwebservicefe = $this->nmgp_dados_form['idwebservicefe'];} 
      }
      $glo_senha_protect = (isset($_SESSION['scriptcase']['glo_senha_protect'])) ? $_SESSION['scriptcase']['glo_senha_protect'] : "S";
      $this->aba_iframe = false;
      if (isset($_SESSION['scriptcase']['sc_aba_iframe']))
      {
          foreach ($_SESSION['scriptcase']['sc_aba_iframe'] as $aba => $apls_aba)
          {
              if (in_array("form_webservicefe", $apls_aba))
              {
                  $this->aba_iframe = true;
                  break;
              }
          }
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['iframe_menu'] && (!isset($_SESSION['scriptcase']['menu_mobile']) || empty($_SESSION['scriptcase']['menu_mobile'])))
      {
          $this->aba_iframe = true;
      }
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_limpa.php", "F", "nm_limpa_valor") ; 
      $this->Ini->sc_Include($this->Ini->path_libs . "/nm_gc.php", "F", "nm_gc") ; 
      $_SESSION['scriptcase']['sc_tab_meses']['int'] = array(
                                      $this->Ini->Nm_lang['lang_mnth_janu'],
                                      $this->Ini->Nm_lang['lang_mnth_febr'],
                                      $this->Ini->Nm_lang['lang_mnth_marc'],
                                      $this->Ini->Nm_lang['lang_mnth_apri'],
                                      $this->Ini->Nm_lang['lang_mnth_mayy'],
                                      $this->Ini->Nm_lang['lang_mnth_june'],
                                      $this->Ini->Nm_lang['lang_mnth_july'],
                                      $this->Ini->Nm_lang['lang_mnth_augu'],
                                      $this->Ini->Nm_lang['lang_mnth_sept'],
                                      $this->Ini->Nm_lang['lang_mnth_octo'],
                                      $this->Ini->Nm_lang['lang_mnth_nove'],
                                      $this->Ini->Nm_lang['lang_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_meses']['abr'] = array(
                                      $this->Ini->Nm_lang['lang_shrt_mnth_janu'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_febr'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_marc'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_apri'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_mayy'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_june'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_july'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_augu'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_sept'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_octo'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_nove'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_dias']['int'] = array(
                                      $this->Ini->Nm_lang['lang_days_sund'],
                                      $this->Ini->Nm_lang['lang_days_mond'],
                                      $this->Ini->Nm_lang['lang_days_tued'],
                                      $this->Ini->Nm_lang['lang_days_wend'],
                                      $this->Ini->Nm_lang['lang_days_thud'],
                                      $this->Ini->Nm_lang['lang_days_frid'],
                                      $this->Ini->Nm_lang['lang_days_satd']);
      $_SESSION['scriptcase']['sc_tab_dias']['abr'] = array(
                                      $this->Ini->Nm_lang['lang_shrt_days_sund'],
                                      $this->Ini->Nm_lang['lang_shrt_days_mond'],
                                      $this->Ini->Nm_lang['lang_shrt_days_tued'],
                                      $this->Ini->Nm_lang['lang_shrt_days_wend'],
                                      $this->Ini->Nm_lang['lang_shrt_days_thud'],
                                      $this->Ini->Nm_lang['lang_shrt_days_frid'],
                                      $this->Ini->Nm_lang['lang_shrt_days_satd']);
      nm_gc($this->Ini->path_libs);
      $this->Ini->Gd_missing  = true;
      if(function_exists("getProdVersion"))
      {
         $_SESSION['scriptcase']['sc_prod_Version'] = str_replace(".", "", getProdVersion($this->Ini->path_libs));
         if(function_exists("gd_info"))
         {
            $this->Ini->Gd_missing = false;
         }
      }
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_trata_img.php", "C", "nm_trata_img") ; 

      if (is_file($this->Ini->path_aplicacao . 'form_webservicefe_help.txt'))
      {
          $arr_link_webhelp = file($this->Ini->path_aplicacao . 'form_webservicefe_help.txt');
          if ($arr_link_webhelp)
          {
              foreach ($arr_link_webhelp as $str_link_webhelp)
              {
                  $str_link_webhelp = trim($str_link_webhelp);
                  if ('form:' == substr($str_link_webhelp, 0, 5))
                  {
                      $arr_link_parts = explode(':', $str_link_webhelp);
                      if ('' != $arr_link_parts[1] && is_file($this->Ini->root . $this->Ini->path_help . $arr_link_parts[1]))
                      {
                          $this->url_webhelp = $this->Ini->path_help . $arr_link_parts[1];
                      }
                  }
              }
          }
      }

      if (is_dir($this->Ini->path_aplicacao . 'img'))
      {
          $Res_dir_img = @opendir($this->Ini->path_aplicacao . 'img');
          if ($Res_dir_img)
          {
              while (FALSE !== ($Str_arquivo = @readdir($Res_dir_img))) 
              {
                 if (@is_file($this->Ini->path_aplicacao . 'img/' . $Str_arquivo) && '.' != $Str_arquivo && '..' != $this->Ini->path_aplicacao . 'img/' . $Str_arquivo)
                 {
                     @unlink($this->Ini->path_aplicacao . 'img/' . $Str_arquivo);
                 }
              }
          }
          @closedir($Res_dir_img);
          rmdir($this->Ini->path_aplicacao . 'img');
      }

      if ($this->Embutida_proc)
      { 
          require_once($this->Ini->path_embutida . 'form_webservicefe/form_webservicefe_erro.class.php');
      }
      else
      { 
          require_once($this->Ini->path_aplicacao . "form_webservicefe_erro.class.php"); 
      }
      $this->Erro      = new form_webservicefe_erro();
      $this->Erro->Ini = $this->Ini;
      $this->proc_fast_search = false;
      if ($nm_opc_lookup != "lookup" && $nm_opc_php != "formphp")
      { 
         if (empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao']))
         { 
             if ($this->idwebservicefe != "")   
             { 
                 $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao'] = "igual" ;  
             }   
         }   
      } 
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao']) && empty($this->nmgp_refresh_fields))
      {
          $this->nmgp_opcao = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao'];  
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao'] = "" ;  
          if ($this->nmgp_opcao == "edit_novo")  
          {
             $this->nmgp_opcao = "novo";
             $this->nm_flag_saida_novo = "S";
          }
      } 
      $this->nm_Start_new = false;
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['start'] == 'new')
      {
          $this->nmgp_opcao = "novo";
          $this->nm_Start_new = true;
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao'] = "novo";
          unset($_SESSION['scriptcase']['sc_apl_conf']['form_webservicefe']['start']);
      }
      if ($this->nmgp_opcao == "igual")  
      {
          $this->nmgp_opc_ant = $this->nmgp_opcao;
      } 
      else
      {
          $this->nmgp_opc_ant = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opc_ant'];
      } 
      if ($this->nmgp_opcao == "recarga" || $this->nmgp_opcao == "muda_form")  
      {
          $this->nmgp_botoes = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['botoes'];
          $this->Nav_permite_ret = 0 != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['inicio'];
          $this->Nav_permite_ava = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total'] != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['final'];
      }
      else
      {
      }
      $this->nm_flag_iframe = false;
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form'])) 
      {
         $this->nmgp_dados_form = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form'];
      }
      if ($this->nmgp_opcao == "edit_novo")  
      {
          $this->nmgp_opcao = "novo";
          $this->nm_flag_saida_novo = "S";
      }
//
      $this->NM_case_insensitive = false;
      $this->sc_evento = $this->nmgp_opcao;
            if ('ajax_check_file' == $this->nmgp_opcao ){
                 ob_start(); 
                 include_once("../_lib/lib/php/nm_api.php"); 
            switch( $_POST['rsargs'] ){
               default:
                   echo 0;exit;
               break;
               }

            $out1_img_cache = $_SESSION['scriptcase']['form_webservicefe']['glo_nm_path_imag_temp'] . $file_name;
            $orig_img = $_SESSION['scriptcase']['form_webservicefe']['glo_nm_path_imag_temp']. '/sc_'.md5(date('YmdHis').basename($_POST['AjaxCheckImg'])).'.gif';
            copy($__file_download, $_SERVER['DOCUMENT_ROOT'].$orig_img);
            echo $orig_img . '_@@NM@@_';

            if(file_exists($out1_img_cache)){
                echo $out1_img_cache;
                exit;
            }
            copy($__file_download, $_SERVER['DOCUMENT_ROOT'].$out1_img_cache);
            $sc_obj_img = new nm_trata_img($_SERVER['DOCUMENT_ROOT'].$out1_img_cache, true);

            if(!empty($img_width) && !empty($img_height)){
                $sc_obj_img->setWidth($img_width);
                $sc_obj_img->setHeight($img_height);
            }
                $sc_obj_img->setManterAspecto(true);
            $sc_obj_img->createImg($_SERVER['DOCUMENT_ROOT'].$out1_img_cache);
            echo $out1_img_cache;
               exit;
            }
      if (isset($this->proveedor)) { $this->nm_limpa_alfa($this->proveedor); }
      if (isset($this->servidor1)) { $this->nm_limpa_alfa($this->servidor1); }
      if (isset($this->servidor2)) { $this->nm_limpa_alfa($this->servidor2); }
      if (isset($this->tokenempresa)) { $this->nm_limpa_alfa($this->tokenempresa); }
      if (isset($this->tokenpassword)) { $this->nm_limpa_alfa($this->tokenpassword); }
      if (isset($this->servidor_prueba1)) { $this->nm_limpa_alfa($this->servidor_prueba1); }
      if (isset($this->servidor_prueba2)) { $this->nm_limpa_alfa($this->servidor_prueba2); }
      if (isset($this->token_prueba)) { $this->nm_limpa_alfa($this->token_prueba); }
      if (isset($this->password_prueba)) { $this->nm_limpa_alfa($this->password_prueba); }
      if (isset($this->enviar_dian)) { $this->nm_limpa_alfa($this->enviar_dian); }
      if (isset($this->enviar_cliente)) { $this->nm_limpa_alfa($this->enviar_cliente); }
      if (isset($this->servidor3)) { $this->nm_limpa_alfa($this->servidor3); }
      if (isset($this->servidor_prueba3)) { $this->nm_limpa_alfa($this->servidor_prueba3); }
      if (isset($this->url_api_pdfs)) { $this->nm_limpa_alfa($this->url_api_pdfs); }
      if (isset($this->url_api_sendmail)) { $this->nm_limpa_alfa($this->url_api_sendmail); }
      if (isset($this->copia_factura_a)) { $this->nm_limpa_alfa($this->copia_factura_a); }
      if (isset($this->plantilla_pordefecto)) { $this->nm_limpa_alfa($this->plantilla_pordefecto); }
      if (isset($this->proveedor_anterior)) { $this->nm_limpa_alfa($this->proveedor_anterior); }
      if (isset($this->servidor_anterior1)) { $this->nm_limpa_alfa($this->servidor_anterior1); }
      if (isset($this->servidor_anterior2)) { $this->nm_limpa_alfa($this->servidor_anterior2); }
      if (isset($this->servidor_anterior3)) { $this->nm_limpa_alfa($this->servidor_anterior3); }
      if (isset($this->token_anterior)) { $this->nm_limpa_alfa($this->token_anterior); }
      if (isset($this->password_anterior)) { $this->nm_limpa_alfa($this->password_anterior); }
      if (isset($this->servidor4)) { $this->nm_limpa_alfa($this->servidor4); }
      if (isset($this->servidor5)) { $this->nm_limpa_alfa($this->servidor5); }
      $Campos_Crit       = "";
      $Campos_erro       = "";
      $Campos_Falta      = array();
      $Campos_Erros      = array();
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          =  substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opc_edit'] = true;  
     if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_select'])) 
     {
        $this->nmgp_dados_select = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_select'];
     }
   }

   function loadFieldConfig()
   {
      $this->field_config = array();
      //-- idwebservicefe
      $this->field_config['idwebservicefe']               = array();
      $this->field_config['idwebservicefe']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['idwebservicefe']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['idwebservicefe']['symbol_dec'] = '';
      $this->field_config['idwebservicefe']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['idwebservicefe']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
   }

   function controle()
   {
        global $nm_url_saida, $teste_validade, 
               $glo_senha_protect, $nm_apl_dependente, $nm_form_submit, $sc_check_excl, $nm_opc_form_php, $nm_call_php, $nm_opc_lookup;


      $this->ini_controle();

      if ('' != $_SESSION['scriptcase']['change_regional_old'])
      {
          $_SESSION['scriptcase']['str_conf_reg'] = $_SESSION['scriptcase']['change_regional_old'];
          $this->Ini->regionalDefault($_SESSION['scriptcase']['str_conf_reg']);
          $this->loadFieldConfig();
          $this->nm_tira_formatacao();

          $_SESSION['scriptcase']['str_conf_reg'] = $_SESSION['scriptcase']['change_regional_new'];
          $this->Ini->regionalDefault($_SESSION['scriptcase']['str_conf_reg']);
          $this->loadFieldConfig();
          $guarda_formatado = $this->formatado;
          $this->nm_formatar_campos();
          $this->formatado = $guarda_formatado;

          $_SESSION['scriptcase']['change_regional_old'] = '';
          $_SESSION['scriptcase']['change_regional_new'] = '';
      }

      if ($nm_form_submit == 1 && ($this->nmgp_opcao == 'inicio' || $this->nmgp_opcao == 'igual'))
      {
          $this->nm_tira_formatacao();
      }
      if (!$this->NM_ajax_flag || 'alterar' != $this->nmgp_opcao || 'submit_form' != $this->NM_ajax_opcao)
      {
      }
//
//-----> 
//
      if ($this->NM_ajax_flag && 'validate_' == substr($this->NM_ajax_opcao, 0, 9))
      {
          if ('validate_proveedor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'proveedor');
          }
          if ('validate_modo' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'modo');
          }
          if ('validate_servidor1' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor1');
          }
          if ('validate_servidor2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor2');
          }
          if ('validate_servidor3' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor3');
          }
          if ('validate_servidor4' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor4');
          }
          if ('validate_servidor5' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor5');
          }
          if ('validate_tokenempresa' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'tokenempresa');
          }
          if ('validate_tokenpassword' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'tokenpassword');
          }
          if ('validate_url_api_pdfs' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'url_api_pdfs');
          }
          if ('validate_url_api_sendmail' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'url_api_sendmail');
          }
          if ('validate_servidor_prueba1' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_prueba1');
          }
          if ('validate_servidor_prueba2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_prueba2');
          }
          if ('validate_servidor_prueba3' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_prueba3');
          }
          if ('validate_token_prueba' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'token_prueba');
          }
          if ('validate_password_prueba' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'password_prueba');
          }
          if ('validate_enviar_dian' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'enviar_dian');
          }
          if ('validate_enviar_cliente' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'enviar_cliente');
          }
          if ('validate_proveedor_anterior' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'proveedor_anterior');
          }
          if ('validate_servidor_anterior1' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_anterior1');
          }
          if ('validate_servidor_anterior2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_anterior2');
          }
          if ('validate_servidor_anterior3' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'servidor_anterior3');
          }
          if ('validate_token_anterior' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'token_anterior');
          }
          if ('validate_password_anterior' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'password_anterior');
          }
          if ('validate_envio_credenciales' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'envio_credenciales');
          }
          if ('validate_copia_factura_a' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'copia_factura_a');
          }
          if ('validate_plantillas_correo' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'plantillas_correo');
          }
          if ('validate_plantilla_pordefecto' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'plantilla_pordefecto');
          }
          form_webservicefe_pack_ajax_response();
          exit;
      }
      if ($this->NM_ajax_flag && 'event_' == substr($this->NM_ajax_opcao, 0, 6))
      {
          $this->nm_tira_formatacao();
          if ('event_proveedor_anterior_onchange' == $this->NM_ajax_opcao)
          {
              $this->proveedor_anterior_onChange();
          }
          if ('event_proveedor_onchange' == $this->NM_ajax_opcao)
          {
              $this->proveedor_onChange();
          }
          form_webservicefe_pack_ajax_response();
          exit;
      }
      if (isset($this->sc_inline_call) && 'Y' == $this->sc_inline_call)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['inline_form_seq'] = $this->sc_seq_row;
          $this->nm_tira_formatacao();
      }
      if ($this->nmgp_opcao == "recarga" || $this->nmgp_opcao == "recarga_mobile" || $this->nmgp_opcao == "muda_form") 
      {
          if (is_array($this->enviar_dian))
          {
              $x = 0; 
              $this->enviar_dian_1 = $this->enviar_dian;
              $this->enviar_dian = ""; 
              if ($this->enviar_dian_1 != "") 
              { 
                  foreach ($this->enviar_dian_1 as $dados_enviar_dian_1 ) 
                  { 
                      if ($x != 0)
                      { 
                          $this->enviar_dian .= ";";
                      } 
                      $this->enviar_dian .= $dados_enviar_dian_1;
                      $x++ ; 
                  } 
              } 
          } 
          if (is_array($this->enviar_cliente))
          {
              $x = 0; 
              $this->enviar_cliente_1 = $this->enviar_cliente;
              $this->enviar_cliente = ""; 
              if ($this->enviar_cliente_1 != "") 
              { 
                  foreach ($this->enviar_cliente_1 as $dados_enviar_cliente_1 ) 
                  { 
                      if ($x != 0)
                      { 
                          $this->enviar_cliente .= ";";
                      } 
                      $this->enviar_cliente .= $dados_enviar_cliente_1;
                      $x++ ; 
                  } 
              } 
          } 
          if (is_array($this->envio_credenciales))
          {
              $x = 0; 
              $this->envio_credenciales_1 = $this->envio_credenciales;
              $this->envio_credenciales = ""; 
              if ($this->envio_credenciales_1 != "") 
              { 
                  foreach ($this->envio_credenciales_1 as $dados_envio_credenciales_1 ) 
                  { 
                      if ($x != 0)
                      { 
                          $this->envio_credenciales .= ";";
                      } 
                      $this->envio_credenciales .= $dados_envio_credenciales_1;
                      $x++ ; 
                  } 
              } 
          } 
          if (is_array($this->plantillas_correo))
          {
              $x = 0; 
              $this->plantillas_correo_1 = $this->plantillas_correo;
              $this->plantillas_correo = ""; 
              if ($this->plantillas_correo_1 != "") 
              { 
                  foreach ($this->plantillas_correo_1 as $dados_plantillas_correo_1 ) 
                  { 
                      if ($x != 0)
                      { 
                          $this->plantillas_correo .= ";";
                      } 
                      $this->plantillas_correo .= $dados_plantillas_correo_1;
                      $x++ ; 
                  } 
              } 
          } 
          $this->nm_tira_formatacao();
          $nm_sc_sv_opcao = $this->nmgp_opcao; 
          $this->nmgp_opcao = "nada"; 
          $this->nm_acessa_banco();
          if ($this->NM_ajax_flag)
          {
              $this->ajax_return_values();
              form_webservicefe_pack_ajax_response();
              exit;
          }
          $this->nm_formatar_campos();
          $this->nmgp_opcao = $nm_sc_sv_opcao; 
          $this->nm_gera_html();
          $this->NM_close_db(); 
          $this->nmgp_opcao = ""; 
          exit; 
      }
      if ($this->nmgp_opcao == "incluir" || $this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "excluir") 
      {
          $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros) ; 
          $_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'off';
          if ($Campos_Crit != "") 
          {
              $Campos_Crit = $this->Ini->Nm_lang['lang_errm_flds'] . ' ' . $Campos_Crit ; 
          }
          if ($Campos_Crit != "" || !empty($Campos_Falta) || $this->Campos_Mens_erro != "")
          {
              if ($this->NM_ajax_flag)
              {
                  form_webservicefe_pack_ajax_response();
                  exit;
              }
              $campos_erro = $this->Formata_Erros($Campos_Crit, $Campos_Falta, $Campos_Erros, 4);
              $this->Campos_Mens_erro = ""; 
              $this->Erro->mensagem(__FILE__, __LINE__, "critica", $campos_erro); 
              $this->nmgp_opc_ant = $this->nmgp_opcao ; 
              if ($this->nmgp_opcao == "incluir" && $nm_apl_dependente == 1) 
              { 
                  $this->nm_flag_saida_novo = "S";; 
              }
              if ($this->nmgp_opcao == "incluir") 
              { 
                  $GLOBALS["erro_incl"] = 1; 
              }
              $this->nmgp_opcao = "nada" ; 
          }
      }
      elseif (isset($nm_form_submit) && 1 == $nm_form_submit && $this->nmgp_opcao != "menu_link" && $this->nmgp_opcao != "recarga_mobile")
      {
      }
//
      if ($this->nmgp_opcao != "nada")
      {
          $this->nm_acessa_banco();
      }
      else
      {
           if ($this->nmgp_opc_ant == "incluir") 
           { 
               $this->nm_proc_onload(false);
           }
           else
           { 
              $this->nm_guardar_campos();
           }
      }
      if ($this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form" && !$this->Apl_com_erro)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['recarga'] = $this->nmgp_opcao;
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_insert']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_insert'] == "ok")
          {
              if ($this->sc_evento == "insert" || ($this->nmgp_opc_ant == "novo" && $this->nmgp_opcao == "novo" && $this->sc_evento == "novo"))
              {
                  $this->NM_close_db(); 
                  $this->nmgp_redireciona(2); 
              }
          }
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_atualiz']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_atualiz'] == "ok")
          {
              if ($this->sc_evento == "update")
              {
                  $this->NM_close_db(); 
                  $this->nmgp_redireciona(2); 
              }
              if ($this->sc_evento == "delete")
              {
                  $this->NM_close_db(); 
                  $this->nmgp_redireciona(2); 
              }
          }
      }
      if ($this->NM_ajax_flag && 'navigate_form' == $this->NM_ajax_opcao)
      {
          $this->ajax_return_values();
          $this->ajax_add_parameters();
          form_webservicefe_pack_ajax_response();
          exit;
      }
      $this->nm_formatar_campos();
      if ($this->NM_ajax_flag)
      {
          $this->NM_ajax_info['result'] = 'OK';
          if ('alterar' == $this->NM_ajax_info['param']['nmgp_opcao'])
          {
              $this->NM_ajax_info['msgDisplay'] = NM_charset_to_utf8($this->Ini->Nm_lang['lang_othr_ajax_frmu']);
          }
          form_webservicefe_pack_ajax_response();
          exit;
      }
      $this->nm_gera_html();
      $this->NM_close_db(); 
      $this->nmgp_opcao = ""; 
      if ($this->Change_Menu)
      {
          $apl_menu  = $_SESSION['scriptcase']['menu_atual'];
          $Arr_rastro = array();
          if (isset($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) && count($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) > 1)
          {
              foreach ($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu] as $menu => $apls)
              {
                 $Arr_rastro[] = "'<a href=\"" . $apls['link'] . "?script_case_init=" . $this->sc_init_menu . "\" target=\"#NMIframe#\">" . $apls['label'] . "</a>'";
              }
              $ult_apl = count($Arr_rastro) - 1;
              unset($Arr_rastro[$ult_apl]);
              $rastro = implode(",", $Arr_rastro);
?>
  <script type="text/javascript">
     link_atual = new Array (<?php echo $rastro ?>);
     parent.writeFastMenu(link_atual);
  </script>
<?php
          }
          else
          {
?>
  <script type="text/javascript">
     parent.clearFastMenu();
  </script>
<?php
          }
      }
   }
  function html_export_print($nm_arquivo_html, $nmgp_password)
  {
      $Html_password = "";
          $Arq_base  = $this->Ini->root . $this->Ini->path_imag_temp . $nm_arquivo_html;
          $Parm_pass = ($Html_password != "") ? " -p" : "";
          $Zip_name = "sc_prt_" . date("YmdHis") . "_" . rand(0, 1000) . "form_webservicefe.zip";
          $Arq_htm = $this->Ini->path_imag_temp . "/" . $Zip_name;
          $Arq_zip = $this->Ini->root . $Arq_htm;
          $Zip_f     = (FALSE !== strpos($Arq_zip, ' ')) ? " \"" . $Arq_zip . "\"" :  $Arq_zip;
          $Arq_input = (FALSE !== strpos($Arq_base, ' ')) ? " \"" . $Arq_base . "\"" :  $Arq_base;
           if (is_file($Arq_zip)) {
               unlink($Arq_zip);
           }
           $str_zip = "";
           if (FALSE !== strpos(strtolower(php_uname()), 'windows')) 
           {
               chdir($this->Ini->path_third . "/zip/windows");
               $str_zip = "zip.exe " . strtoupper($Parm_pass) . " -j " . $Html_password . " " . $Zip_f . " " . $Arq_input;
           }
           elseif (FALSE !== strpos(strtolower(php_uname()), 'linux')) 
           {
                if (FALSE !== strpos(strtolower(php_uname()), 'i686')) 
                {
                    chdir($this->Ini->path_third . "/zip/linux-i386/bin");
                }
                else
                {
                    chdir($this->Ini->path_third . "/zip/linux-amd64/bin");
                }
               $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $Arq_input;
           }
           elseif (FALSE !== strpos(strtolower(php_uname()), 'darwin'))
           {
               chdir($this->Ini->path_third . "/zip/mac/bin");
               $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $Arq_input;
           }
           if (!empty($str_zip)) {
               exec($str_zip);
           }
           // ----- ZIP log
           $fp = @fopen(trim(str_replace(array(".zip",'"'), array(".log",""), $Zip_f)), 'w');
           if ($fp)
           {
               @fwrite($fp, $str_zip . "\r\n\r\n");
               @fclose($fp);
           }
           foreach ($this->Ini->Img_export_zip as $cada_img_zip)
           {
               $str_zip      = "";
              $cada_img_zip = '"' . $cada_img_zip . '"';
               if (FALSE !== strpos(strtolower(php_uname()), 'windows')) 
               {
                   $str_zip = "zip.exe " . strtoupper($Parm_pass) . " -j -u " . $Html_password . " " . $Zip_f . " " . $cada_img_zip;
               }
               elseif (FALSE !== strpos(strtolower(php_uname()), 'linux')) 
               {
                   $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $cada_img_zip;
               }
               elseif (FALSE !== strpos(strtolower(php_uname()), 'darwin'))
               {
                   $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $cada_img_zip;
               }
               if (!empty($str_zip)) {
                   exec($str_zip);
               }
               // ----- ZIP log
               $fp = @fopen(trim(str_replace(array(".zip",'"'), array(".log",""), $Zip_f)), 'a');
               if ($fp)
               {
                   @fwrite($fp, $str_zip . "\r\n\r\n");
                   @fclose($fp);
               }
           }
           if (is_file($Arq_zip)) {
               unlink($Arq_base);
           } 
          $path_doc_md5 = md5($Arq_htm);
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe'][$path_doc_md5][0] = $Arq_htm;
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe'][$path_doc_md5][1] = $Zip_name;
?>
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo strip_tags("WebService Proveedor documentos electrónicos") ?></TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php

if (isset($_SESSION['scriptcase']['device_mobile']) && $_SESSION['scriptcase']['device_mobile'] && $_SESSION['scriptcase']['display_mobile'])
{
?>
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
}

?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_form . '/' . $this->Ini->Str_btn_form ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->path_prod; ?>/third/font-awesome/css/all.min.css" /> 
  <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
</HEAD>
<BODY class="scExportPage">
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: top">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">PRINT</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
   <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

   <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

   <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo  $this->form_encode_input($Arq_htm) ?>" target="_self" style="display: none"> 
</form>
<form name="Fdown" method="get" action="form_webservicefe_download.php" target="_self" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="form_webservicefe"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<form name="F0" method=post action="./" target="_self" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nmgp_opcao" value="<?php echo $this->nmgp_opcao ?>"> 
</form> 
         </BODY>
         </HTML>
<?php
          exit;
  }
//
//--------------------------------------------------------------------------------------
   function NM_has_trans()
   {
       return !in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access);
   }
//
//--------------------------------------------------------------------------------------
   function NM_commit_db()
   {
       if ($this->Ini->sc_tem_trans_banco && !$this->Embutida_proc)
       { 
           $this->Db->CommitTrans(); 
           $this->Ini->sc_tem_trans_banco = false;
       } 
   }
//
//--------------------------------------------------------------------------------------
   function NM_rollback_db()
   {
       if ($this->Ini->sc_tem_trans_banco && !$this->Embutida_proc)
       { 
           $this->Db->RollbackTrans(); 
           $this->Ini->sc_tem_trans_banco = false;
       } 
   }
//
//--------------------------------------------------------------------------------------
   function NM_close_db()
   {
       if ($this->Db && !$this->Embutida_proc)
       { 
           $this->Db->Close(); 
       } 
   }
//
//--------------------------------------------------------------------------------------
   function Formata_Erros($Campos_Crit, $Campos_Falta, $Campos_Erros, $mode = 3) 
   {
       switch ($mode)
       {
           case 1:
               $campos_erro = array();
               if (!empty($Campos_Crit))
               {
                   $campos_erro[] = $Campos_Crit;
               }
               if (!empty($Campos_Falta))
               {
                   $campos_erro[] = $this->Formata_Campos_Falta($Campos_Falta);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_erro[] = $this->Campos_Mens_erro;
               }
               return implode('<br />', $campos_erro);
               break;

           case 2:
               $campos_erro = array();
               if (!empty($Campos_Crit))
               {
                   $campos_erro[] = $Campos_Crit;
               }
               if (!empty($Campos_Falta))
               {
                   $campos_erro[] = $this->Formata_Campos_Falta($Campos_Falta, true);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_erro[] = $this->Campos_Mens_erro;
               }
               return implode('<br />', $campos_erro);
               break;

           case 3:
               $campos_erro = array();
               if (!empty($Campos_Erros))
               {
                   $campos_erro[] = $this->Formata_Campos_Erros($Campos_Erros);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_mens_erro = str_replace(array('<br />', '<br>', '<BR />'), array('<BR>', '<BR>', '<BR>'), $this->Campos_Mens_erro);
                   $campos_mens_erro = explode('<BR>', $campos_mens_erro);
                   foreach ($campos_mens_erro as $msg_erro)
                   {
                       if ('' != $msg_erro && !in_array($msg_erro, $campos_erro))
                       {
                           $campos_erro[] = $msg_erro;
                       }
                   }
               }
               return implode('<br />', $campos_erro);
               break;

           case 4:
               $campos_erro = array();
               if (!empty($Campos_Erros))
               {
                   $campos_erro[] = $this->Formata_Campos_Erros_SweetAlert($Campos_Erros);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_mens_erro = str_replace(array('<br />', '<br>', '<BR />'), array('<BR>', '<BR>', '<BR>'), $this->Campos_Mens_erro);
                   $campos_mens_erro = explode('<BR>', $campos_mens_erro);
                   foreach ($campos_mens_erro as $msg_erro)
                   {
                       if ('' != $msg_erro && !in_array($msg_erro, $campos_erro))
                       {
                           $campos_erro[] = $msg_erro;
                       }
                   }
               }
               return implode('<br />', $campos_erro);
               break;
       }
   }

   function Formata_Campos_Falta($Campos_Falta, $table = false) 
   {
       $Campos_Falta = array_unique($Campos_Falta);

       if (!$table)
       {
           return $this->Ini->Nm_lang['lang_errm_reqd'] . ' ' . implode('; ', $Campos_Falta);
       }

       $aCols  = array();
       $iTotal = sizeof($Campos_Falta);
       $iCols  = 6 > $iTotal ? 1 : (11 > $iTotal ? 2 : (16 > $iTotal ? 3 : 4));
       $iItems = ceil($iTotal / $iCols);
       $iNowC  = 0;
       $iNowI  = 0;

       foreach ($Campos_Falta as $campo)
       {
           $aCols[$iNowC][] = $campo;
           if ($iItems == ++$iNowI)
           {
               $iNowC++;
               $iNowI = 0;
           }
       }

       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';
       $sError .= '<tr>';
       $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Ini->Nm_lang['lang_errm_reqd'] . '</td>';
       foreach ($aCols as $aCol)
       {
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', $aCol) . '</td>';
       }
       $sError .= '</tr>';
       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Crit($Campos_Crit, $table = false) 
   {
       $Campos_Crit = array_unique($Campos_Crit);

       if (!$table)
       {
           return $this->Ini->Nm_lang['lang_errm_flds'] . ' ' . implode('; ', $Campos_Crit);
       }

       $aCols  = array();
       $iTotal = sizeof($Campos_Crit);
       $iCols  = 6 > $iTotal ? 1 : (11 > $iTotal ? 2 : (16 > $iTotal ? 3 : 4));
       $iItems = ceil($iTotal / $iCols);
       $iNowC  = 0;
       $iNowI  = 0;

       foreach ($Campos_Crit as $campo)
       {
           $aCols[$iNowC][] = $campo;
           if ($iItems == ++$iNowI)
           {
               $iNowC++;
               $iNowI = 0;
           }
       }

       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';
       $sError .= '<tr>';
       $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Ini->Nm_lang['lang_errm_flds'] . '</td>';
       foreach ($aCols as $aCol)
       {
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', $aCol) . '</td>';
       }
       $sError .= '</tr>';
       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Erros($Campos_Erros) 
   {
       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';

       foreach ($Campos_Erros as $campo => $erros)
       {
           $sError .= '<tr>';
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Recupera_Nome_Campo($campo) . ':</td>';
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', array_unique($erros)) . '</td>';
           $sError .= '</tr>';
       }

       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Erros_SweetAlert($Campos_Erros) 
   {
       $sError  = '';

       foreach ($Campos_Erros as $campo => $erros)
       {
           $sError .= $this->Recupera_Nome_Campo($campo) . ': ' . implode('<br />', array_unique($erros)) . '<br />';
       }

       return $sError;
   }

   function Recupera_Nome_Campo($campo) 
   {
       switch($campo)
       {
           case 'proveedor':
               return "Proveedor";
               break;
           case 'modo':
               return "Modo";
               break;
           case 'servidor1':
               return "Servidor 1";
               break;
           case 'servidor2':
               return "Servidor 2";
               break;
           case 'servidor3':
               return "Servidor 3";
               break;
           case 'servidor4':
               return "Servidor 4";
               break;
           case 'servidor5':
               return "Servidor 5";
               break;
           case 'tokenempresa':
               return "Token Empresa";
               break;
           case 'tokenpassword':
               return "Token Password";
               break;
           case 'url_api_pdfs':
               return "URL API PDFs";
               break;
           case 'url_api_sendmail':
               return "URL API SENDMAIL";
               break;
           case 'servidor_prueba1':
               return "Servidor Prueba 1";
               break;
           case 'servidor_prueba2':
               return "Servidor Prueba 2";
               break;
           case 'servidor_prueba3':
               return "Servidor Prueba 3";
               break;
           case 'token_prueba':
               return "Token Prueba";
               break;
           case 'password_prueba':
               return "Password Prueba";
               break;
           case 'enviar_dian':
               return "Enviar Dian";
               break;
           case 'enviar_cliente':
               return "Enviar Cliente";
               break;
           case 'proveedor_anterior':
               return "Proveedor Anterior";
               break;
           case 'servidor_anterior1':
               return "Servidor Anterior 1";
               break;
           case 'servidor_anterior2':
               return "Servidor Anterior 2";
               break;
           case 'servidor_anterior3':
               return "Servidor Anterior 3";
               break;
           case 'token_anterior':
               return "Token Anterior";
               break;
           case 'password_anterior':
               return "Password Anterior";
               break;
           case 'envio_credenciales':
               return "Envío Credenciales";
               break;
           case 'copia_factura_a':
               return "Enviar copia de factura electrónica a";
               break;
           case 'plantillas_correo':
               return "Activar el uso de plantillas de correo";
               break;
           case 'plantilla_pordefecto':
               return "Plantilla Por Defecto";
               break;
           case 'idwebservicefe':
               return "Idwebservicefe";
               break;
       }

       return $campo;
   }

   function dateDefaultFormat()
   {
       if (isset($this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_format']))
       {
           $sDate = str_replace('yyyy', 'Y', $this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_format']);
           $sDate = str_replace('mm',   'm', $sDate);
           $sDate = str_replace('dd',   'd', $sDate);
           return substr(chunk_split($sDate, 1, $this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_sep']), 0, -1);
       }
       elseif ('en_us' == $this->Ini->str_lang)
       {
           return 'm/d/Y';
       }
       else
       {
           return 'd/m/Y';
       }
   } // dateDefaultFormat

//
//--------------------------------------------------------------------------------------
   function Valida_campos(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros, $filtro = '') 
   {
     global $nm_browser, $teste_validade;
     if (is_array($filtro) && empty($filtro)) {
         $filtro = '';
     }
//---------------------------------------------------------
     $this->sc_force_zero = array();

     if (!is_array($filtro) && '' == $filtro && isset($this->nm_form_submit) && '1' == $this->nm_form_submit && $this->scCsrfGetToken() != $this->csrf_token)
     {
          $this->Campos_Mens_erro .= (empty($this->Campos_Mens_erro)) ? "" : "<br />";
          $this->Campos_Mens_erro .= "CSRF: " . $this->Ini->Nm_lang['lang_errm_ajax_csrf'];
          if ($this->NM_ajax_flag)
          {
              if (!isset($this->NM_ajax_info['errList']['geral_form_webservicefe']) || !is_array($this->NM_ajax_info['errList']['geral_form_webservicefe']))
              {
                  $this->NM_ajax_info['errList']['geral_form_webservicefe'] = array();
              }
              $this->NM_ajax_info['errList']['geral_form_webservicefe'][] = "CSRF: " . $this->Ini->Nm_lang['lang_errm_ajax_csrf'];
          }
     }
      if ((!is_array($filtro) && ('' == $filtro || 'proveedor' == $filtro)) || (is_array($filtro) && in_array('proveedor', $filtro)))
        $this->ValidateField_proveedor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'modo' == $filtro)) || (is_array($filtro) && in_array('modo', $filtro)))
        $this->ValidateField_modo($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor1' == $filtro)) || (is_array($filtro) && in_array('servidor1', $filtro)))
        $this->ValidateField_servidor1($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor2' == $filtro)) || (is_array($filtro) && in_array('servidor2', $filtro)))
        $this->ValidateField_servidor2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor3' == $filtro)) || (is_array($filtro) && in_array('servidor3', $filtro)))
        $this->ValidateField_servidor3($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor4' == $filtro)) || (is_array($filtro) && in_array('servidor4', $filtro)))
        $this->ValidateField_servidor4($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor5' == $filtro)) || (is_array($filtro) && in_array('servidor5', $filtro)))
        $this->ValidateField_servidor5($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'tokenempresa' == $filtro)) || (is_array($filtro) && in_array('tokenempresa', $filtro)))
        $this->ValidateField_tokenempresa($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'tokenpassword' == $filtro)) || (is_array($filtro) && in_array('tokenpassword', $filtro)))
        $this->ValidateField_tokenpassword($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'url_api_pdfs' == $filtro)) || (is_array($filtro) && in_array('url_api_pdfs', $filtro)))
        $this->ValidateField_url_api_pdfs($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'url_api_sendmail' == $filtro)) || (is_array($filtro) && in_array('url_api_sendmail', $filtro)))
        $this->ValidateField_url_api_sendmail($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_prueba1' == $filtro)) || (is_array($filtro) && in_array('servidor_prueba1', $filtro)))
        $this->ValidateField_servidor_prueba1($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_prueba2' == $filtro)) || (is_array($filtro) && in_array('servidor_prueba2', $filtro)))
        $this->ValidateField_servidor_prueba2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_prueba3' == $filtro)) || (is_array($filtro) && in_array('servidor_prueba3', $filtro)))
        $this->ValidateField_servidor_prueba3($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'token_prueba' == $filtro)) || (is_array($filtro) && in_array('token_prueba', $filtro)))
        $this->ValidateField_token_prueba($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'password_prueba' == $filtro)) || (is_array($filtro) && in_array('password_prueba', $filtro)))
        $this->ValidateField_password_prueba($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'enviar_dian' == $filtro)) || (is_array($filtro) && in_array('enviar_dian', $filtro)))
        $this->ValidateField_enviar_dian($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'enviar_cliente' == $filtro)) || (is_array($filtro) && in_array('enviar_cliente', $filtro)))
        $this->ValidateField_enviar_cliente($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'proveedor_anterior' == $filtro)) || (is_array($filtro) && in_array('proveedor_anterior', $filtro)))
        $this->ValidateField_proveedor_anterior($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_anterior1' == $filtro)) || (is_array($filtro) && in_array('servidor_anterior1', $filtro)))
        $this->ValidateField_servidor_anterior1($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_anterior2' == $filtro)) || (is_array($filtro) && in_array('servidor_anterior2', $filtro)))
        $this->ValidateField_servidor_anterior2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'servidor_anterior3' == $filtro)) || (is_array($filtro) && in_array('servidor_anterior3', $filtro)))
        $this->ValidateField_servidor_anterior3($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'token_anterior' == $filtro)) || (is_array($filtro) && in_array('token_anterior', $filtro)))
        $this->ValidateField_token_anterior($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'password_anterior' == $filtro)) || (is_array($filtro) && in_array('password_anterior', $filtro)))
        $this->ValidateField_password_anterior($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'envio_credenciales' == $filtro)) || (is_array($filtro) && in_array('envio_credenciales', $filtro)))
        $this->ValidateField_envio_credenciales($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'copia_factura_a' == $filtro)) || (is_array($filtro) && in_array('copia_factura_a', $filtro)))
        $this->ValidateField_copia_factura_a($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'plantillas_correo' == $filtro)) || (is_array($filtro) && in_array('plantillas_correo', $filtro)))
        $this->ValidateField_plantillas_correo($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'plantilla_pordefecto' == $filtro)) || (is_array($filtro) && in_array('plantilla_pordefecto', $filtro)))
        $this->ValidateField_plantilla_pordefecto($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if (!empty($Campos_Crit) || !empty($Campos_Falta) || !empty($this->Campos_Mens_erro))
      {
          if (!empty($this->sc_force_zero))
          {
              foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
              {
                  eval('$this->' . $sc_force_zero_field . ' = "";');
                  unset($this->sc_force_zero[$i_force_zero]);
              }
          }
      }
   }

    function ValidateField_proveedor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->proveedor == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'proveedor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_proveedor

    function ValidateField_modo(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->modo == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'modo';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_modo

    function ValidateField_servidor1(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor1) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor 1 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor1']))
              {
                  $Campos_Erros['servidor1'] = array();
              }
              $Campos_Erros['servidor1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor1']) || !is_array($this->NM_ajax_info['errList']['servidor1']))
              {
                  $this->NM_ajax_info['errList']['servidor1'] = array();
              }
              $this->NM_ajax_info['errList']['servidor1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor1';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor1

    function ValidateField_servidor2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor2) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor 2 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor2']))
              {
                  $Campos_Erros['servidor2'] = array();
              }
              $Campos_Erros['servidor2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor2']) || !is_array($this->NM_ajax_info['errList']['servidor2']))
              {
                  $this->NM_ajax_info['errList']['servidor2'] = array();
              }
              $this->NM_ajax_info['errList']['servidor2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor2

    function ValidateField_servidor3(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor3) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor 3 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor3']))
              {
                  $Campos_Erros['servidor3'] = array();
              }
              $Campos_Erros['servidor3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor3']) || !is_array($this->NM_ajax_info['errList']['servidor3']))
              {
                  $this->NM_ajax_info['errList']['servidor3'] = array();
              }
              $this->NM_ajax_info['errList']['servidor3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor3';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor3

    function ValidateField_servidor4(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor4) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor 4 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor4']))
              {
                  $Campos_Erros['servidor4'] = array();
              }
              $Campos_Erros['servidor4'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor4']) || !is_array($this->NM_ajax_info['errList']['servidor4']))
              {
                  $this->NM_ajax_info['errList']['servidor4'] = array();
              }
              $this->NM_ajax_info['errList']['servidor4'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor4';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor4

    function ValidateField_servidor5(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor5) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor 5 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor5']))
              {
                  $Campos_Erros['servidor5'] = array();
              }
              $Campos_Erros['servidor5'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor5']) || !is_array($this->NM_ajax_info['errList']['servidor5']))
              {
                  $this->NM_ajax_info['errList']['servidor5'] = array();
              }
              $this->NM_ajax_info['errList']['servidor5'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor5';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor5

    function ValidateField_tokenempresa(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->tokenempresa) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Token Empresa " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['tokenempresa']))
              {
                  $Campos_Erros['tokenempresa'] = array();
              }
              $Campos_Erros['tokenempresa'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['tokenempresa']) || !is_array($this->NM_ajax_info['errList']['tokenempresa']))
              {
                  $this->NM_ajax_info['errList']['tokenempresa'] = array();
              }
              $this->NM_ajax_info['errList']['tokenempresa'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'tokenempresa';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_tokenempresa

    function ValidateField_tokenpassword(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->tokenpassword) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Token Password " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['tokenpassword']))
              {
                  $Campos_Erros['tokenpassword'] = array();
              }
              $Campos_Erros['tokenpassword'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['tokenpassword']) || !is_array($this->NM_ajax_info['errList']['tokenpassword']))
              {
                  $this->NM_ajax_info['errList']['tokenpassword'] = array();
              }
              $this->NM_ajax_info['errList']['tokenpassword'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'tokenpassword';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_tokenpassword

    function ValidateField_url_api_pdfs(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->url_api_pdfs) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "URL API PDFs " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['url_api_pdfs']))
              {
                  $Campos_Erros['url_api_pdfs'] = array();
              }
              $Campos_Erros['url_api_pdfs'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['url_api_pdfs']) || !is_array($this->NM_ajax_info['errList']['url_api_pdfs']))
              {
                  $this->NM_ajax_info['errList']['url_api_pdfs'] = array();
              }
              $this->NM_ajax_info['errList']['url_api_pdfs'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'url_api_pdfs';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_url_api_pdfs

    function ValidateField_url_api_sendmail(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->url_api_sendmail) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "URL API SENDMAIL " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['url_api_sendmail']))
              {
                  $Campos_Erros['url_api_sendmail'] = array();
              }
              $Campos_Erros['url_api_sendmail'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['url_api_sendmail']) || !is_array($this->NM_ajax_info['errList']['url_api_sendmail']))
              {
                  $this->NM_ajax_info['errList']['url_api_sendmail'] = array();
              }
              $this->NM_ajax_info['errList']['url_api_sendmail'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'url_api_sendmail';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_url_api_sendmail

    function ValidateField_servidor_prueba1(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_prueba1) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Prueba 1 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_prueba1']))
              {
                  $Campos_Erros['servidor_prueba1'] = array();
              }
              $Campos_Erros['servidor_prueba1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_prueba1']) || !is_array($this->NM_ajax_info['errList']['servidor_prueba1']))
              {
                  $this->NM_ajax_info['errList']['servidor_prueba1'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_prueba1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_prueba1';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_prueba1

    function ValidateField_servidor_prueba2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_prueba2) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Prueba 2 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_prueba2']))
              {
                  $Campos_Erros['servidor_prueba2'] = array();
              }
              $Campos_Erros['servidor_prueba2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_prueba2']) || !is_array($this->NM_ajax_info['errList']['servidor_prueba2']))
              {
                  $this->NM_ajax_info['errList']['servidor_prueba2'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_prueba2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_prueba2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_prueba2

    function ValidateField_servidor_prueba3(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_prueba3) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Prueba 3 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_prueba3']))
              {
                  $Campos_Erros['servidor_prueba3'] = array();
              }
              $Campos_Erros['servidor_prueba3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_prueba3']) || !is_array($this->NM_ajax_info['errList']['servidor_prueba3']))
              {
                  $this->NM_ajax_info['errList']['servidor_prueba3'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_prueba3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_prueba3';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_prueba3

    function ValidateField_token_prueba(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->token_prueba) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Token Prueba " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['token_prueba']))
              {
                  $Campos_Erros['token_prueba'] = array();
              }
              $Campos_Erros['token_prueba'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['token_prueba']) || !is_array($this->NM_ajax_info['errList']['token_prueba']))
              {
                  $this->NM_ajax_info['errList']['token_prueba'] = array();
              }
              $this->NM_ajax_info['errList']['token_prueba'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'token_prueba';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_token_prueba

    function ValidateField_password_prueba(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->password_prueba) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Password Prueba " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['password_prueba']))
              {
                  $Campos_Erros['password_prueba'] = array();
              }
              $Campos_Erros['password_prueba'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['password_prueba']) || !is_array($this->NM_ajax_info['errList']['password_prueba']))
              {
                  $this->NM_ajax_info['errList']['password_prueba'] = array();
              }
              $this->NM_ajax_info['errList']['password_prueba'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'password_prueba';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_password_prueba

    function ValidateField_enviar_dian(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->enviar_dian == "" && $this->nmgp_opcao != "excluir")
      { 
          $this->enviar_dian = "0";
      } 
      else 
      { 
          if (is_array($this->enviar_dian))
          {
              $x = 0; 
              $this->enviar_dian_1 = array(); 
              foreach ($this->enviar_dian as $ind => $dados_enviar_dian_1 ) 
              {
                  if ($dados_enviar_dian_1 != "") 
                  {
                      $this->enviar_dian_1[] = $dados_enviar_dian_1;
                  } 
              } 
              $this->enviar_dian = ""; 
              foreach ($this->enviar_dian_1 as $dados_enviar_dian_1 ) 
              { 
                   if ($x != 0)
                   { 
                       $this->enviar_dian .= ";";
                   } 
                   $this->enviar_dian .= $dados_enviar_dian_1;
                   $x++ ; 
              } 
          } 
      } 
      if ($this->enviar_dian === "" || is_null($this->enviar_dian))  
      { 
          $this->enviar_dian = 0;
          $this->sc_force_zero[] = 'enviar_dian';
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'enviar_dian';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_enviar_dian

    function ValidateField_enviar_cliente(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->enviar_cliente == "" && $this->nmgp_opcao != "excluir")
      { 
          $this->enviar_cliente = "0";
      } 
      else 
      { 
          if (is_array($this->enviar_cliente))
          {
              $x = 0; 
              $this->enviar_cliente_1 = array(); 
              foreach ($this->enviar_cliente as $ind => $dados_enviar_cliente_1 ) 
              {
                  if ($dados_enviar_cliente_1 != "") 
                  {
                      $this->enviar_cliente_1[] = $dados_enviar_cliente_1;
                  } 
              } 
              $this->enviar_cliente = ""; 
              foreach ($this->enviar_cliente_1 as $dados_enviar_cliente_1 ) 
              { 
                   if ($x != 0)
                   { 
                       $this->enviar_cliente .= ";";
                   } 
                   $this->enviar_cliente .= $dados_enviar_cliente_1;
                   $x++ ; 
              } 
          } 
      } 
      if ($this->enviar_cliente === "" || is_null($this->enviar_cliente))  
      { 
          $this->enviar_cliente = 0;
          $this->sc_force_zero[] = 'enviar_cliente';
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'enviar_cliente';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_enviar_cliente

    function ValidateField_proveedor_anterior(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->proveedor_anterior == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'proveedor_anterior';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_proveedor_anterior

    function ValidateField_servidor_anterior1(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_anterior1) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Anterior 1 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_anterior1']))
              {
                  $Campos_Erros['servidor_anterior1'] = array();
              }
              $Campos_Erros['servidor_anterior1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_anterior1']) || !is_array($this->NM_ajax_info['errList']['servidor_anterior1']))
              {
                  $this->NM_ajax_info['errList']['servidor_anterior1'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_anterior1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_anterior1';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_anterior1

    function ValidateField_servidor_anterior2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_anterior2) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Anterior 2 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_anterior2']))
              {
                  $Campos_Erros['servidor_anterior2'] = array();
              }
              $Campos_Erros['servidor_anterior2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_anterior2']) || !is_array($this->NM_ajax_info['errList']['servidor_anterior2']))
              {
                  $this->NM_ajax_info['errList']['servidor_anterior2'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_anterior2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_anterior2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_anterior2

    function ValidateField_servidor_anterior3(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->servidor_anterior3) > 300) 
          { 
              $hasError = true;
              $Campos_Crit .= "Servidor Anterior 3 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['servidor_anterior3']))
              {
                  $Campos_Erros['servidor_anterior3'] = array();
              }
              $Campos_Erros['servidor_anterior3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['servidor_anterior3']) || !is_array($this->NM_ajax_info['errList']['servidor_anterior3']))
              {
                  $this->NM_ajax_info['errList']['servidor_anterior3'] = array();
              }
              $this->NM_ajax_info['errList']['servidor_anterior3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 300 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'servidor_anterior3';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_servidor_anterior3

    function ValidateField_token_anterior(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->token_anterior) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Token Anterior " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['token_anterior']))
              {
                  $Campos_Erros['token_anterior'] = array();
              }
              $Campos_Erros['token_anterior'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['token_anterior']) || !is_array($this->NM_ajax_info['errList']['token_anterior']))
              {
                  $this->NM_ajax_info['errList']['token_anterior'] = array();
              }
              $this->NM_ajax_info['errList']['token_anterior'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'token_anterior';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_token_anterior

    function ValidateField_password_anterior(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->password_anterior) > 150) 
          { 
              $hasError = true;
              $Campos_Crit .= "Password Anterior " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['password_anterior']))
              {
                  $Campos_Erros['password_anterior'] = array();
              }
              $Campos_Erros['password_anterior'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['password_anterior']) || !is_array($this->NM_ajax_info['errList']['password_anterior']))
              {
                  $this->NM_ajax_info['errList']['password_anterior'] = array();
              }
              $this->NM_ajax_info['errList']['password_anterior'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 150 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'password_anterior';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_password_anterior

    function ValidateField_envio_credenciales(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->envio_credenciales == "" && $this->nmgp_opcao != "excluir")
      { 
          $this->envio_credenciales = "NO";
      } 
      else 
      { 
          if (is_array($this->envio_credenciales))
          {
              $x = 0; 
              $this->envio_credenciales_1 = array(); 
              foreach ($this->envio_credenciales as $ind => $dados_envio_credenciales_1 ) 
              {
                  if ($dados_envio_credenciales_1 != "") 
                  {
                      $this->envio_credenciales_1[] = $dados_envio_credenciales_1;
                  } 
              } 
              $this->envio_credenciales = ""; 
              foreach ($this->envio_credenciales_1 as $dados_envio_credenciales_1 ) 
              { 
                   if ($x != 0)
                   { 
                       $this->envio_credenciales .= ";";
                   } 
                   $this->envio_credenciales .= $dados_envio_credenciales_1;
                   $x++ ; 
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'envio_credenciales';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_envio_credenciales

    function ValidateField_copia_factura_a(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->copia_factura_a) != "")  
          { 
              if ($teste_validade->Email($this->copia_factura_a) == false)  
              { 
                  $hasError = true;
                      $Campos_Crit .= "Enviar copia de factura electrónica a; " ; 
                  if (!isset($Campos_Erros['copia_factura_a']))
                  {
                      $Campos_Erros['copia_factura_a'] = array();
                  }
                  $Campos_Erros['copia_factura_a'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                      if (!isset($this->NM_ajax_info['errList']['copia_factura_a']) || !is_array($this->NM_ajax_info['errList']['copia_factura_a']))
                      {
                          $this->NM_ajax_info['errList']['copia_factura_a'] = array();
                      }
                      $this->NM_ajax_info['errList']['copia_factura_a'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'copia_factura_a';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_copia_factura_a

    function ValidateField_plantillas_correo(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->plantillas_correo == "" && $this->nmgp_opcao != "excluir")
      { 
          $this->plantillas_correo = "NO";
      } 
      else 
      { 
          if (is_array($this->plantillas_correo))
          {
              $x = 0; 
              $this->plantillas_correo_1 = array(); 
              foreach ($this->plantillas_correo as $ind => $dados_plantillas_correo_1 ) 
              {
                  if ($dados_plantillas_correo_1 != "") 
                  {
                      $this->plantillas_correo_1[] = $dados_plantillas_correo_1;
                  } 
              } 
              $this->plantillas_correo = ""; 
              foreach ($this->plantillas_correo_1 as $dados_plantillas_correo_1 ) 
              { 
                   if ($x != 0)
                   { 
                       $this->plantillas_correo .= ";";
                   } 
                   $this->plantillas_correo .= $dados_plantillas_correo_1;
                   $x++ ; 
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'plantillas_correo';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_plantillas_correo

    function ValidateField_plantilla_pordefecto(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->plantilla_pordefecto) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']) && !in_array($this->plantilla_pordefecto, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['plantilla_pordefecto']))
                   {
                       $Campos_Erros['plantilla_pordefecto'] = array();
                   }
                   $Campos_Erros['plantilla_pordefecto'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['plantilla_pordefecto']) || !is_array($this->NM_ajax_info['errList']['plantilla_pordefecto']))
                   {
                       $this->NM_ajax_info['errList']['plantilla_pordefecto'] = array();
                   }
                   $this->NM_ajax_info['errList']['plantilla_pordefecto'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'plantilla_pordefecto';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_plantilla_pordefecto

    function removeDuplicateDttmError($aErrDate, &$aErrTime)
    {
        if (empty($aErrDate) || empty($aErrTime))
        {
            return;
        }

        foreach ($aErrDate as $sErrDate)
        {
            foreach ($aErrTime as $iErrTime => $sErrTime)
            {
                if ($sErrDate == $sErrTime)
                {
                    unset($aErrTime[$iErrTime]);
                }
            }
        }
    } // removeDuplicateDttmError

   function nm_guardar_campos()
   {
    global
           $sc_seq_vert;
    $this->nmgp_dados_form['proveedor'] = $this->proveedor;
    $this->nmgp_dados_form['modo'] = $this->modo;
    $this->nmgp_dados_form['servidor1'] = $this->servidor1;
    $this->nmgp_dados_form['servidor2'] = $this->servidor2;
    $this->nmgp_dados_form['servidor3'] = $this->servidor3;
    $this->nmgp_dados_form['servidor4'] = $this->servidor4;
    $this->nmgp_dados_form['servidor5'] = $this->servidor5;
    $this->nmgp_dados_form['tokenempresa'] = $this->tokenempresa;
    $this->nmgp_dados_form['tokenpassword'] = $this->tokenpassword;
    $this->nmgp_dados_form['url_api_pdfs'] = $this->url_api_pdfs;
    $this->nmgp_dados_form['url_api_sendmail'] = $this->url_api_sendmail;
    $this->nmgp_dados_form['servidor_prueba1'] = $this->servidor_prueba1;
    $this->nmgp_dados_form['servidor_prueba2'] = $this->servidor_prueba2;
    $this->nmgp_dados_form['servidor_prueba3'] = $this->servidor_prueba3;
    $this->nmgp_dados_form['token_prueba'] = $this->token_prueba;
    $this->nmgp_dados_form['password_prueba'] = $this->password_prueba;
    $this->nmgp_dados_form['enviar_dian'] = $this->enviar_dian;
    $this->nmgp_dados_form['enviar_cliente'] = $this->enviar_cliente;
    $this->nmgp_dados_form['proveedor_anterior'] = $this->proveedor_anterior;
    $this->nmgp_dados_form['servidor_anterior1'] = $this->servidor_anterior1;
    $this->nmgp_dados_form['servidor_anterior2'] = $this->servidor_anterior2;
    $this->nmgp_dados_form['servidor_anterior3'] = $this->servidor_anterior3;
    $this->nmgp_dados_form['token_anterior'] = $this->token_anterior;
    $this->nmgp_dados_form['password_anterior'] = $this->password_anterior;
    $this->nmgp_dados_form['envio_credenciales'] = $this->envio_credenciales;
    $this->nmgp_dados_form['copia_factura_a'] = $this->copia_factura_a;
    $this->nmgp_dados_form['plantillas_correo'] = $this->plantillas_correo;
    $this->nmgp_dados_form['plantilla_pordefecto'] = $this->plantilla_pordefecto;
    $this->nmgp_dados_form['idwebservicefe'] = $this->idwebservicefe;
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form'] = $this->nmgp_dados_form;
   }
   function nm_tira_formatacao()
   {
      global $nm_form_submit;
         $this->Before_unformat = array();
         $this->formatado = false;
      $this->Before_unformat['idwebservicefe'] = $this->idwebservicefe;
      nm_limpa_numero($this->idwebservicefe, $this->field_config['idwebservicefe']['symbol_grp']) ; 
   }
   function sc_add_currency(&$value, $symbol, $pos)
   {
       if ('' == $value)
       {
           return;
       }
       $value = (1 == $pos || 3 == $pos) ? $symbol . ' ' . $value : $value . ' ' . $symbol;
   }
   function sc_remove_currency(&$value, $symbol_dec, $symbol_tho, $symbol_mon)
   {
       $value = preg_replace('~&#x0*([0-9a-f]+);~i', '', $value);
       $sNew  = str_replace($symbol_mon, '', $value);
       if ($sNew != $value)
       {
           $value = str_replace(' ', '', $sNew);
           return;
       }
       $aTest = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', $symbol_dec, $symbol_tho);
       $sNew  = '';
       for ($i = 0; $i < strlen($value); $i++)
       {
           if ($this->sc_test_currency_char($value[$i], $aTest))
           {
               $sNew .= $value[$i];
           }
       }
       $value = $sNew;
   }
   function sc_test_currency_char($char, $test)
   {
       $found = false;
       foreach ($test as $test_char)
       {
           if ($char === $test_char)
           {
               $found = true;
           }
       }
       return $found;
   }
   function nm_clear_val($Nome_Campo)
   {
      if ($Nome_Campo == "idwebservicefe")
      {
          nm_limpa_numero($this->idwebservicefe, $this->field_config['idwebservicefe']['symbol_grp']) ; 
      }
   }
   function nm_formatar_campos($format_fields = array())
   {
      global $nm_form_submit;
     if (isset($this->formatado) && $this->formatado)
     {
         return;
     }
     $this->formatado = true;
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";

      if (false !== strpos($nm_mask, '9') || false !== strpos($nm_mask, 'a') || false !== strpos($nm_mask, '*'))
      {
          $new_campo = '';
          $a_mask_ord  = array();
          $i_mask_size = -1;

          foreach (explode(';', $nm_mask) as $str_mask)
          {
              $a_mask_ord[ $this->nm_conta_mask_chars($str_mask) ] = $str_mask;
          }
          ksort($a_mask_ord);

          foreach ($a_mask_ord as $i_size => $s_mask)
          {
              if (-1 == $i_mask_size)
              {
                  $i_mask_size = $i_size;
              }
              elseif (strlen($nm_campo) >= $i_size && strlen($nm_campo) > $i_mask_size)
              {
                  $i_mask_size = $i_size;
              }
          }
          $nm_mask = $a_mask_ord[$i_mask_size];

          for ($i = 0; $i < strlen($nm_mask); $i++)
          {
              $test_mask = substr($nm_mask, $i, 1);
              
              if ('9' == $test_mask || 'a' == $test_mask || '*' == $test_mask)
              {
                  $new_campo .= substr($nm_campo, 0, 1);
                  $nm_campo   = substr($nm_campo, 1);
              }
              else
              {
                  $new_campo .= $test_mask;
              }
          }

                  $nm_campo = $new_campo;

          return;
      }

      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont1 < $cont2 && $tam_campo <= $cont2 && $tam_campo > $cont1)
              {
                  $trab_mask = $ver_duas[1];
              }
              elseif ($cont1 > $cont2 && $tam_campo <= $cont2)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conta_mask_chars($sMask)
   {
       $iLength = 0;

       for ($i = 0; $i < strlen($sMask); $i++)
       {
           if (in_array($sMask[$i], array('9', 'a', '*')))
           {
               $iLength++;
           }
       }

       return $iLength;
   }
   function nm_tira_mask(&$nm_campo, $nm_mask, $nm_chars = '')
   { 
      $mask_dados = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $tam_mask   = strlen($nm_mask);
      $trab_saida = "";

      if (false !== strpos($nm_mask, '9') || false !== strpos($nm_mask, 'a') || false !== strpos($nm_mask, '*'))
      {
          $raw_campo = $this->sc_clear_mask($nm_campo, $nm_chars);
          $raw_mask  = $this->sc_clear_mask($nm_mask, $nm_chars);
          $new_campo = '';

          $test_mask = substr($raw_mask, 0, 1);
          $raw_mask  = substr($raw_mask, 1);

          while ('' != $raw_campo)
          {
              $test_val  = substr($raw_campo, 0, 1);
              $raw_campo = substr($raw_campo, 1);
              $ord       = ord($test_val);
              $found     = false;

              switch ($test_mask)
              {
                  case '9':
                      if (48 <= $ord && 57 >= $ord)
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;

                  case 'a':
                      if ((65 <= $ord && 90 >= $ord) || (97 <= $ord && 122 >= $ord))
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;

                  case '*':
                      if ((48 <= $ord && 57 >= $ord) || (65 <= $ord && 90 >= $ord) || (97 <= $ord && 122 >= $ord))
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;
              }

              if ($found)
              {
                  $test_mask = substr($raw_mask, 0, 1);
                  $raw_mask  = substr($raw_mask, 1);
              }
          }

          $nm_campo = $new_campo;

          return;
      }

      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          for ($x=0; $x < strlen($mask_dados); $x++)
          {
              if (is_numeric(substr($mask_dados, $x, 1)))
              {
                  $trab_saida .= substr($mask_dados, $x, 1);
              }
          }
          $nm_campo = $trab_saida;
          return;
      }
      if ($tam_mask > $tam_campo)
      {
         $mask_desfaz = "";
         for ($mask_ind = 0; $tam_mask > $tam_campo; $mask_ind++)
         {
              $mask_char = substr($trab_mask, $mask_ind, 1);
              if ($mask_char == "z")
              {
                  $tam_mask--;
              }
              else
              {
                  $mask_desfaz .= $mask_char;
              }
              if ($mask_ind == $tam_campo)
              {
                  $tam_mask = $tam_campo;
              }
         }
         $trab_mask = $mask_desfaz . substr($trab_mask, $mask_ind);
      }
      $mask_saida = "";
      for ($mask_ind = strlen($trab_mask); $mask_ind > 0; $mask_ind--)
      {
          $mask_char = substr($trab_mask, $mask_ind - 1, 1);
          if ($mask_char == "x" || $mask_char == "z")
          {
              if ($tam_campo > 0)
              {
                  $mask_saida = substr($mask_dados, $tam_campo - 1, 1) . $mask_saida;
              }
          }
          else
          {
              if ($mask_char != substr($mask_dados, $tam_campo - 1, 1) && $tam_campo > 0)
              {
                  $mask_saida = substr($mask_dados, $tam_campo - 1, 1) . $mask_saida;
                  $mask_ind--;
              }
          }
          $tam_campo--;
      }
      if ($tam_campo > 0)
      {
         $mask_saida = substr($mask_dados, 0, $tam_campo) . $mask_saida;
      }
      $nm_campo = $mask_saida;
   }

   function sc_clear_mask($value, $chars)
   {
       $new = '';

       for ($i = 0; $i < strlen($value); $i++)
       {
           if (false === strpos($chars, $value[$i]))
           {
               $new .= $value[$i];
           }
       }

       return $new;
   }
//
   function nm_limpa_alfa(&$str)
   {
       if (get_magic_quotes_gpc())
       {
           if (is_array($str))
           {
               $x = 0;
               foreach ($str as $cada_str)
               {
                   $str[$x] = stripslashes($str[$x]);
                   $x++;
               }
           }
           else
           {
               $str = stripslashes($str);
           }
       }
   }
   function nm_conv_data_db($dt_in, $form_in, $form_out, $replaces = array())
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT") {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT") {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "SC_FORMAT_REGION") {
           $this->nm_data->SetaData($dt_in, strtoupper($form_in));
           $prep_out  = (strpos(strtolower($form_in), "dd") !== false) ? "dd" : "";
           $prep_out .= (strpos(strtolower($form_in), "mm") !== false) ? "mm" : "";
           $prep_out .= (strpos(strtolower($form_in), "aa") !== false) ? "aaaa" : "";
           $prep_out .= (strpos(strtolower($form_in), "yy") !== false) ? "aaaa" : "";
           return $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", $prep_out));
       }
       else {
           nm_conv_form_data($dt_out, $form_in, $form_out, $replaces);
           return $dt_out;
       }
   }

   function returnWhere($aCond, $sOp = 'AND')
   {
       $aWhere = array();
       foreach ($aCond as $sCond)
       {
           $this->handleWhereCond($sCond);
           if ('' != $sCond)
           {
               $aWhere[] = $sCond;
           }
       }
       if (empty($aWhere))
       {
           return '';
       }
       else
       {
           return ' WHERE (' . implode(') ' . $sOp . ' (', $aWhere) . ')';
       }
   } // returnWhere

   function handleWhereCond(&$sCond)
   {
       $sCond = trim($sCond);
       if ('where' == strtolower(substr($sCond, 0, 5)))
       {
           $sCond = trim(substr($sCond, 5));
       }
   } // handleWhereCond

   function ajax_return_values()
   {
          $this->ajax_return_values_proveedor();
          $this->ajax_return_values_modo();
          $this->ajax_return_values_servidor1();
          $this->ajax_return_values_servidor2();
          $this->ajax_return_values_servidor3();
          $this->ajax_return_values_servidor4();
          $this->ajax_return_values_servidor5();
          $this->ajax_return_values_tokenempresa();
          $this->ajax_return_values_tokenpassword();
          $this->ajax_return_values_url_api_pdfs();
          $this->ajax_return_values_url_api_sendmail();
          $this->ajax_return_values_servidor_prueba1();
          $this->ajax_return_values_servidor_prueba2();
          $this->ajax_return_values_servidor_prueba3();
          $this->ajax_return_values_token_prueba();
          $this->ajax_return_values_password_prueba();
          $this->ajax_return_values_enviar_dian();
          $this->ajax_return_values_enviar_cliente();
          $this->ajax_return_values_proveedor_anterior();
          $this->ajax_return_values_servidor_anterior1();
          $this->ajax_return_values_servidor_anterior2();
          $this->ajax_return_values_servidor_anterior3();
          $this->ajax_return_values_token_anterior();
          $this->ajax_return_values_password_anterior();
          $this->ajax_return_values_envio_credenciales();
          $this->ajax_return_values_copia_factura_a();
          $this->ajax_return_values_plantillas_correo();
          $this->ajax_return_values_plantilla_pordefecto();
          if ('navigate_form' == $this->NM_ajax_opcao)
          {
              $this->NM_ajax_info['clearUpload']      = 'S';
              $this->NM_ajax_info['navStatus']['ret'] = $this->Nav_permite_ret ? 'S' : 'N';
              $this->NM_ajax_info['navStatus']['ava'] = $this->Nav_permite_ava ? 'S' : 'N';
              $this->NM_ajax_info['fldList']['idwebservicefe']['keyVal'] = form_webservicefe_pack_protect_string($this->nmgp_dados_form['idwebservicefe']);
          }
   } // ajax_return_values

          //----- proveedor
   function ajax_return_values_proveedor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("proveedor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->proveedor);
              $aLookup = array();
              $this->_tmp_lookup_proveedor = $this->proveedor;

$aLookup[] = array(form_webservicefe_pack_protect_string('FACILWEB') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("FACILWEB")));
$aLookup[] = array(form_webservicefe_pack_protect_string('DATAICO') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("DATAICO")));
$aLookup[] = array(form_webservicefe_pack_protect_string('CADENA S. A.') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("CADENA S. A.")));
$aLookup[] = array(form_webservicefe_pack_protect_string('THE FACTORY HKA') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("THE FACTORY HKA")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor'][] = 'FACILWEB';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor'][] = 'DATAICO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor'][] = 'CADENA S. A.';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor'][] = 'THE FACTORY HKA';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"proveedor\"";
          if (isset($this->NM_ajax_info['select_html']['proveedor']) && !empty($this->NM_ajax_info['select_html']['proveedor']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['proveedor']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->proveedor == $sValue)
                  {
                      $this->_tmp_lookup_proveedor = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['proveedor'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['proveedor']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['proveedor']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['proveedor']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['proveedor']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['proveedor']['labList'] = $aLabel;
          }
   }

          //----- modo
   function ajax_return_values_modo($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("modo", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->modo);
              $aLookup = array();
              $this->_tmp_lookup_modo = $this->modo;

$aLookup[] = array(form_webservicefe_pack_protect_string('PRUEBAS') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("PRUEBAS")));
$aLookup[] = array(form_webservicefe_pack_protect_string('PRODUCCION') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("PRODUCCIÓN")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_modo'][] = 'PRUEBAS';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_modo'][] = 'PRODUCCION';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"modo\"";
          if (isset($this->NM_ajax_info['select_html']['modo']) && !empty($this->NM_ajax_info['select_html']['modo']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['modo']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->modo == $sValue)
                  {
                      $this->_tmp_lookup_modo = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['modo'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['modo']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['modo']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['modo']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['modo']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['modo']['labList'] = $aLabel;
          }
   }

          //----- servidor1
   function ajax_return_values_servidor1($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor1", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor1);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor1'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor2
   function ajax_return_values_servidor2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor3
   function ajax_return_values_servidor3($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor3", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor3);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor3'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor4
   function ajax_return_values_servidor4($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor4", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor4);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor4'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor5
   function ajax_return_values_servidor5($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor5", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor5);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor5'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- tokenempresa
   function ajax_return_values_tokenempresa($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("tokenempresa", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->tokenempresa);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['tokenempresa'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- tokenpassword
   function ajax_return_values_tokenpassword($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("tokenpassword", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->tokenpassword);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['tokenpassword'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- url_api_pdfs
   function ajax_return_values_url_api_pdfs($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("url_api_pdfs", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->url_api_pdfs);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['url_api_pdfs'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- url_api_sendmail
   function ajax_return_values_url_api_sendmail($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("url_api_sendmail", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->url_api_sendmail);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['url_api_sendmail'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor_prueba1
   function ajax_return_values_servidor_prueba1($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_prueba1", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_prueba1);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_prueba1'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor_prueba2
   function ajax_return_values_servidor_prueba2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_prueba2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_prueba2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_prueba2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor_prueba3
   function ajax_return_values_servidor_prueba3($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_prueba3", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_prueba3);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_prueba3'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- token_prueba
   function ajax_return_values_token_prueba($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("token_prueba", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->token_prueba);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['token_prueba'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- password_prueba
   function ajax_return_values_password_prueba($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("password_prueba", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->password_prueba);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['password_prueba'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- enviar_dian
   function ajax_return_values_enviar_dian($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("enviar_dian", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->enviar_dian);
              $aLookup = array();
              $this->_tmp_lookup_enviar_dian = $this->enviar_dian;

$aLookup[] = array(form_webservicefe_pack_protect_string('1') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_enviar_dian'][] = '1';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['enviar_dian']) && !empty($this->NM_ajax_info['select_html']['enviar_dian']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['enviar_dian']);
          }
          $this->NM_ajax_info['fldList']['enviar_dian'] = array(
                       'row'    => '',
               'type'    => 'checkbox',
               'switch'  => false,
               'valList' => explode(';', $sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
               'optClass' => 'sc-ui-checkbox-enviar_dian',
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['enviar_dian']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['enviar_dian']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['enviar_dian']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['enviar_dian']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['enviar_dian']['labList'] = $aLabel;
          }
   }

          //----- enviar_cliente
   function ajax_return_values_enviar_cliente($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("enviar_cliente", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->enviar_cliente);
              $aLookup = array();
              $this->_tmp_lookup_enviar_cliente = $this->enviar_cliente;

$aLookup[] = array(form_webservicefe_pack_protect_string('1') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_enviar_cliente'][] = '1';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['enviar_cliente']) && !empty($this->NM_ajax_info['select_html']['enviar_cliente']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['enviar_cliente']);
          }
          $this->NM_ajax_info['fldList']['enviar_cliente'] = array(
                       'row'    => '',
               'type'    => 'checkbox',
               'switch'  => false,
               'valList' => explode(';', $sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
               'optClass' => 'sc-ui-checkbox-enviar_cliente',
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['enviar_cliente']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['enviar_cliente']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['enviar_cliente']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['enviar_cliente']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['enviar_cliente']['labList'] = $aLabel;
          }
   }

          //----- proveedor_anterior
   function ajax_return_values_proveedor_anterior($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("proveedor_anterior", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->proveedor_anterior);
              $aLookup = array();
              $this->_tmp_lookup_proveedor_anterior = $this->proveedor_anterior;

$aLookup[] = array(form_webservicefe_pack_protect_string('FACILWEB') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("FACILWEB")));
$aLookup[] = array(form_webservicefe_pack_protect_string('DATAICO') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("DATAICO")));
$aLookup[] = array(form_webservicefe_pack_protect_string('CADENA S. A.') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("CADENA S. A.")));
$aLookup[] = array(form_webservicefe_pack_protect_string('THE FACTORY HKA') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("THE FACTORY HKA")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor_anterior'][] = 'FACILWEB';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor_anterior'][] = 'DATAICO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor_anterior'][] = 'CADENA S. A.';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_proveedor_anterior'][] = 'THE FACTORY HKA';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"proveedor_anterior\"";
          if (isset($this->NM_ajax_info['select_html']['proveedor_anterior']) && !empty($this->NM_ajax_info['select_html']['proveedor_anterior']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['proveedor_anterior']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->proveedor_anterior == $sValue)
                  {
                      $this->_tmp_lookup_proveedor_anterior = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['proveedor_anterior'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['proveedor_anterior']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['proveedor_anterior']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['proveedor_anterior']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['proveedor_anterior']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['proveedor_anterior']['labList'] = $aLabel;
          }
   }

          //----- servidor_anterior1
   function ajax_return_values_servidor_anterior1($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_anterior1", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_anterior1);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_anterior1'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor_anterior2
   function ajax_return_values_servidor_anterior2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_anterior2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_anterior2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_anterior2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- servidor_anterior3
   function ajax_return_values_servidor_anterior3($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("servidor_anterior3", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->servidor_anterior3);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['servidor_anterior3'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- token_anterior
   function ajax_return_values_token_anterior($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("token_anterior", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->token_anterior);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['token_anterior'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- password_anterior
   function ajax_return_values_password_anterior($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("password_anterior", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->password_anterior);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['password_anterior'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- envio_credenciales
   function ajax_return_values_envio_credenciales($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("envio_credenciales", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->envio_credenciales);
              $aLookup = array();
              $this->_tmp_lookup_envio_credenciales = $this->envio_credenciales;

$aLookup[] = array(form_webservicefe_pack_protect_string('SI') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_envio_credenciales'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['envio_credenciales']) && !empty($this->NM_ajax_info['select_html']['envio_credenciales']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['envio_credenciales']);
          }
          $this->NM_ajax_info['fldList']['envio_credenciales'] = array(
                       'row'    => '',
               'type'    => 'checkbox',
               'switch'  => true,
               'valList' => explode(';', $sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
               'optClass' => 'sc-ui-checkbox-envio_credenciales',
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['envio_credenciales']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['envio_credenciales']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['envio_credenciales']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['envio_credenciales']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['envio_credenciales']['labList'] = $aLabel;
          }
   }

          //----- copia_factura_a
   function ajax_return_values_copia_factura_a($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("copia_factura_a", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->copia_factura_a);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['copia_factura_a'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- plantillas_correo
   function ajax_return_values_plantillas_correo($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("plantillas_correo", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->plantillas_correo);
              $aLookup = array();
              $this->_tmp_lookup_plantillas_correo = $this->plantillas_correo;

$aLookup[] = array(form_webservicefe_pack_protect_string('SI') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantillas_correo'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['plantillas_correo']) && !empty($this->NM_ajax_info['select_html']['plantillas_correo']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['plantillas_correo']);
          }
          $this->NM_ajax_info['fldList']['plantillas_correo'] = array(
                       'row'    => '',
               'type'    => 'checkbox',
               'switch'  => true,
               'valList' => explode(';', $sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
               'optClass' => 'sc-ui-checkbox-plantillas_correo',
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['plantillas_correo']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['plantillas_correo']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['plantillas_correo']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['plantillas_correo']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['plantillas_correo']['labList'] = $aLabel;
          }
   }

          //----- plantilla_pordefecto
   function ajax_return_values_plantilla_pordefecto($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("plantilla_pordefecto", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->plantilla_pordefecto);
              $aLookup = array();
              $this->_tmp_lookup_plantilla_pordefecto = $this->plantilla_pordefecto;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array(); 
}
$aLookup[] = array(form_webservicefe_pack_protect_string('') => str_replace('<', '&lt;',form_webservicefe_pack_protect_string('Seleccione')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   $enviar_dian_val_str = "";
   if (!empty($this->enviar_dian))
   {
       if (is_array($this->enviar_dian))
       {
           $Tmp_array = $this->enviar_dian;
       }
       else
       {
           $Tmp_array = explode(";", $this->enviar_dian);
       }
       $enviar_dian_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $enviar_dian_val_str)
          {
             $enviar_dian_val_str .= ", ";
          }
          $enviar_dian_val_str .= $Tmp_val_cmp;
       }
   }
   $enviar_cliente_val_str = "";
   if (!empty($this->enviar_cliente))
   {
       if (is_array($this->enviar_cliente))
       {
           $Tmp_array = $this->enviar_cliente;
       }
       else
       {
           $Tmp_array = explode(";", $this->enviar_cliente);
       }
       $enviar_cliente_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $enviar_cliente_val_str)
          {
             $enviar_cliente_val_str .= ", ";
          }
          $enviar_cliente_val_str .= $Tmp_val_cmp;
       }
   }
   $envio_credenciales_val_str = "''";
   if (!empty($this->envio_credenciales))
   {
       if (is_array($this->envio_credenciales))
       {
           $Tmp_array = $this->envio_credenciales;
       }
       else
       {
           $Tmp_array = explode(";", $this->envio_credenciales);
       }
       $envio_credenciales_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $envio_credenciales_val_str)
          {
             $envio_credenciales_val_str .= ", ";
          }
          $envio_credenciales_val_str .= "'$Tmp_val_cmp'";
       }
   }
   $plantillas_correo_val_str = "''";
   if (!empty($this->plantillas_correo))
   {
       if (is_array($this->plantillas_correo))
       {
           $Tmp_array = $this->plantillas_correo;
       }
       else
       {
           $Tmp_array = explode(";", $this->plantillas_correo);
       }
       $plantillas_correo_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $plantillas_correo_val_str)
          {
             $plantillas_correo_val_str .= ", ";
          }
          $plantillas_correo_val_str .= "'$Tmp_val_cmp'";
       }
   }
   $nm_comando = "SELECT id, descripcion  FROM plantillas_correo_propio  ORDER BY descripcion";
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_webservicefe_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_webservicefe_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"plantilla_pordefecto\"";
          if (isset($this->NM_ajax_info['select_html']['plantilla_pordefecto']) && !empty($this->NM_ajax_info['select_html']['plantilla_pordefecto']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['plantilla_pordefecto']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->plantilla_pordefecto == $sValue)
                  {
                      $this->_tmp_lookup_plantilla_pordefecto = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['plantilla_pordefecto'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['plantilla_pordefecto']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['plantilla_pordefecto']['valList'][$i] = form_webservicefe_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['plantilla_pordefecto']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['plantilla_pordefecto']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['plantilla_pordefecto']['labList'] = $aLabel;
          }
   }

    function fetchUniqueUploadName($originalName, $uploadDir, $fieldName)
    {
        $originalName = trim($originalName);
        if ('' == $originalName)
        {
            return $originalName;
        }
        if (!@is_dir($uploadDir))
        {
            return $originalName;
        }
        if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName]))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName] = array();
            $resDir = @opendir($uploadDir);
            if (!$resDir)
            {
                return $originalName;
            }
            while (false !== ($fileName = @readdir($resDir)))
            {
                if (@is_file($uploadDir . $fileName))
                {
                    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName][] = $fileName;
                }
            }
            @closedir($resDir);
        }
        if (!in_array($originalName, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName]))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName][] = $originalName;
            return $originalName;
        }
        else
        {
            $newName = $this->fetchFileNextName($originalName, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName]);
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['upload_dir'][$fieldName][] = $newName;
            return $newName;
        }
    } // fetchUniqueUploadName

    function fetchFileNextName($uniqueName, $uniqueList)
    {
        $aPathinfo     = pathinfo($uniqueName);
        $fileExtension = $aPathinfo['extension'];
        $fileName      = $aPathinfo['filename'];
        $foundName     = false;
        $nameIt        = 1;
        if ('' != $fileExtension)
        {
            $fileExtension = '.' . $fileExtension;
        }
        while (!$foundName)
        {
            $testName = $fileName . '(' . $nameIt . ')' . $fileExtension;
            if (in_array($testName, $uniqueList))
            {
                $nameIt++;
            }
            else
            {
                $foundName = true;
                return $testName;
            }
        }
    } // fetchFileNextName

   function ajax_add_parameters()
   {
   } // ajax_add_parameters
  function nm_proc_onload($bFormat = true)
  {
      if (!$this->NM_ajax_flag || !isset($this->nmgp_refresh_fields)) {
      $_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_proveedor = $this->proveedor;
}
  $vsql = "SELECT servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba FROM webservicefe_proveedores where proveedor='".$this->proveedor ."'";
 
      $nm_select = $vsql; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vDatos = array();
      $this->vdatos = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vDatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vdatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vDatos = false;
          $this->vDatos_erro = $this->Db->ErrorMsg();
          $this->vdatos = false;
          $this->vdatos_erro = $this->Db->ErrorMsg();
      } 
;
if(isset($this->vdatos[0][0]))
{
}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_proveedor != $this->proveedor || (isset($bFlagRead_proveedor) && $bFlagRead_proveedor)))
    {
        $this->ajax_return_values_proveedor(true);
    }
}
$_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'off'; 
      }
      $this->nm_guardar_campos();
      if ($bFormat) $this->nm_formatar_campos();
  }
//
//----------------------------------------------------
//-----> 
//----------------------------------------------------
//----------- 


   function temRegistros($sWhere)
   {
       if ('' == $sWhere)
       {
           return false;
       }
       $nmgp_sel_count = 'SELECT COUNT(*) AS countTest FROM ' . $this->Ini->nm_tabela . ' WHERE ' . $sWhere;
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_sel_count; 
       $rsc = $this->Db->Execute($nmgp_sel_count); 
       if ($rsc === false && !$rsc->EOF)
       {
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg());
           exit; 
       }
       $iTotal = $rsc->fields[0];
       $rsc->Close();
       return 0 < $iTotal;
   } // temRegistros

   function deletaRegistros($sWhere)
   {
       if ('' == $sWhere)
       {
           return false;
       }
       $nmgp_sel_count = 'DELETE FROM ' . $this->Ini->nm_tabela . ' WHERE ' . $sWhere;
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_sel_count; 
       $rsc = $this->Db->Execute($nmgp_sel_count); 
       $bResult = $rsc;
       $rsc->Close();
       return $bResult == true;
   } // deletaRegistros
    function handleDbErrorMessage(&$dbErrorMessage, $dbErrorCode)
    {
        if (1267 == $dbErrorCode) {
            $dbErrorMessage = $this->Ini->Nm_lang['lang_errm_db_invalid_collation'];
        }
    }


   function nm_acessa_banco() 
   { 
      global  $nm_form_submit, $teste_validade, $sc_where;
 
      $NM_val_null = array();
      $NM_val_form = array();
      $this->sc_erro_insert = "";
      $this->sc_erro_update = "";
      $this->sc_erro_delete = "";
      if (!empty($this->sc_force_zero))
      {
          foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
          {
              eval('if ($this->' . $sc_force_zero_field . ' == 0) {$this->' . $sc_force_zero_field . ' = "";}');
          }
      }
      $this->sc_force_zero = array();
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $salva_opcao = $this->nmgp_opcao; 
      if ($this->sc_evento != "novo" && $this->sc_evento != "incluir") 
      { 
          $this->sc_evento = ""; 
      } 
      if (!in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) && !$this->Ini->sc_tem_trans_banco && in_array($this->nmgp_opcao, array('excluir', 'incluir', 'alterar')))
      { 
          $this->Ini->sc_tem_trans_banco = $this->Db->BeginTrans(); 
      } 
      $NM_val_form['proveedor'] = $this->proveedor;
      $NM_val_form['modo'] = $this->modo;
      $NM_val_form['servidor1'] = $this->servidor1;
      $NM_val_form['servidor2'] = $this->servidor2;
      $NM_val_form['servidor3'] = $this->servidor3;
      $NM_val_form['servidor4'] = $this->servidor4;
      $NM_val_form['servidor5'] = $this->servidor5;
      $NM_val_form['tokenempresa'] = $this->tokenempresa;
      $NM_val_form['tokenpassword'] = $this->tokenpassword;
      $NM_val_form['url_api_pdfs'] = $this->url_api_pdfs;
      $NM_val_form['url_api_sendmail'] = $this->url_api_sendmail;
      $NM_val_form['servidor_prueba1'] = $this->servidor_prueba1;
      $NM_val_form['servidor_prueba2'] = $this->servidor_prueba2;
      $NM_val_form['servidor_prueba3'] = $this->servidor_prueba3;
      $NM_val_form['token_prueba'] = $this->token_prueba;
      $NM_val_form['password_prueba'] = $this->password_prueba;
      $NM_val_form['enviar_dian'] = $this->enviar_dian;
      $NM_val_form['enviar_cliente'] = $this->enviar_cliente;
      $NM_val_form['proveedor_anterior'] = $this->proveedor_anterior;
      $NM_val_form['servidor_anterior1'] = $this->servidor_anterior1;
      $NM_val_form['servidor_anterior2'] = $this->servidor_anterior2;
      $NM_val_form['servidor_anterior3'] = $this->servidor_anterior3;
      $NM_val_form['token_anterior'] = $this->token_anterior;
      $NM_val_form['password_anterior'] = $this->password_anterior;
      $NM_val_form['envio_credenciales'] = $this->envio_credenciales;
      $NM_val_form['copia_factura_a'] = $this->copia_factura_a;
      $NM_val_form['plantillas_correo'] = $this->plantillas_correo;
      $NM_val_form['plantilla_pordefecto'] = $this->plantilla_pordefecto;
      $NM_val_form['idwebservicefe'] = $this->idwebservicefe;
      if ($this->idwebservicefe === "" || is_null($this->idwebservicefe))  
      { 
          $this->idwebservicefe = 0;
      } 
      if ($this->enviar_dian === "" || is_null($this->enviar_dian))  
      { 
          $this->enviar_dian = 0;
          $this->sc_force_zero[] = 'enviar_dian';
      } 
      if ($this->enviar_cliente === "" || is_null($this->enviar_cliente))  
      { 
          $this->enviar_cliente = 0;
          $this->sc_force_zero[] = 'enviar_cliente';
      } 
      if ($this->plantilla_pordefecto === "" || is_null($this->plantilla_pordefecto))  
      { 
          $this->plantilla_pordefecto = 0;
          $this->sc_force_zero[] = 'plantilla_pordefecto';
      } 
      $nm_bases_lob_geral = array_merge($this->Ini->nm_bases_oracle, $this->Ini->nm_bases_ibase, $this->Ini->nm_bases_informix, $this->Ini->nm_bases_mysql, $this->Ini->nm_bases_access, $this->Ini->nm_bases_sqlite, array('pdo_ibm'), array('pdo_sqlsrv'));
      if ($this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "incluir") 
      {
          $this->proveedor_before_qstr = $this->proveedor;
          $this->proveedor = substr($this->Db->qstr($this->proveedor), 1, -1); 
          if ($this->proveedor == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->proveedor = "null"; 
              $NM_val_null[] = "proveedor";
          } 
          $this->servidor1_before_qstr = $this->servidor1;
          $this->servidor1 = substr($this->Db->qstr($this->servidor1), 1, -1); 
          if ($this->servidor1 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor1 = "null"; 
              $NM_val_null[] = "servidor1";
          } 
          $this->servidor2_before_qstr = $this->servidor2;
          $this->servidor2 = substr($this->Db->qstr($this->servidor2), 1, -1); 
          if ($this->servidor2 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor2 = "null"; 
              $NM_val_null[] = "servidor2";
          } 
          $this->tokenempresa_before_qstr = $this->tokenempresa;
          $this->tokenempresa = substr($this->Db->qstr($this->tokenempresa), 1, -1); 
          if ($this->tokenempresa == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->tokenempresa = "null"; 
              $NM_val_null[] = "tokenempresa";
          } 
          $this->tokenpassword_before_qstr = $this->tokenpassword;
          $this->tokenpassword = substr($this->Db->qstr($this->tokenpassword), 1, -1); 
          if ($this->tokenpassword == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->tokenpassword = "null"; 
              $NM_val_null[] = "tokenpassword";
          } 
          $this->servidor_prueba1_before_qstr = $this->servidor_prueba1;
          $this->servidor_prueba1 = substr($this->Db->qstr($this->servidor_prueba1), 1, -1); 
          if ($this->servidor_prueba1 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_prueba1 = "null"; 
              $NM_val_null[] = "servidor_prueba1";
          } 
          $this->servidor_prueba2_before_qstr = $this->servidor_prueba2;
          $this->servidor_prueba2 = substr($this->Db->qstr($this->servidor_prueba2), 1, -1); 
          if ($this->servidor_prueba2 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_prueba2 = "null"; 
              $NM_val_null[] = "servidor_prueba2";
          } 
          $this->token_prueba_before_qstr = $this->token_prueba;
          $this->token_prueba = substr($this->Db->qstr($this->token_prueba), 1, -1); 
          if ($this->token_prueba == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->token_prueba = "null"; 
              $NM_val_null[] = "token_prueba";
          } 
          $this->password_prueba_before_qstr = $this->password_prueba;
          $this->password_prueba = substr($this->Db->qstr($this->password_prueba), 1, -1); 
          if ($this->password_prueba == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->password_prueba = "null"; 
              $NM_val_null[] = "password_prueba";
          } 
          if ($this->modo == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->modo = "null"; 
              $NM_val_null[] = "modo";
          } 
          $this->servidor3_before_qstr = $this->servidor3;
          $this->servidor3 = substr($this->Db->qstr($this->servidor3), 1, -1); 
          if ($this->servidor3 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor3 = "null"; 
              $NM_val_null[] = "servidor3";
          } 
          $this->servidor_prueba3_before_qstr = $this->servidor_prueba3;
          $this->servidor_prueba3 = substr($this->Db->qstr($this->servidor_prueba3), 1, -1); 
          if ($this->servidor_prueba3 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_prueba3 = "null"; 
              $NM_val_null[] = "servidor_prueba3";
          } 
          $this->url_api_pdfs_before_qstr = $this->url_api_pdfs;
          $this->url_api_pdfs = substr($this->Db->qstr($this->url_api_pdfs), 1, -1); 
          if ($this->url_api_pdfs == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->url_api_pdfs = "null"; 
              $NM_val_null[] = "url_api_pdfs";
          } 
          $this->url_api_sendmail_before_qstr = $this->url_api_sendmail;
          $this->url_api_sendmail = substr($this->Db->qstr($this->url_api_sendmail), 1, -1); 
          if ($this->url_api_sendmail == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->url_api_sendmail = "null"; 
              $NM_val_null[] = "url_api_sendmail";
          } 
          if ($this->envio_credenciales == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->envio_credenciales = "null"; 
              $NM_val_null[] = "envio_credenciales";
          } 
          if ($this->plantillas_correo == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->plantillas_correo = "null"; 
              $NM_val_null[] = "plantillas_correo";
          } 
          $this->copia_factura_a_before_qstr = $this->copia_factura_a;
          $this->copia_factura_a = substr($this->Db->qstr($this->copia_factura_a), 1, -1); 
          if ($this->copia_factura_a == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->copia_factura_a = "null"; 
              $NM_val_null[] = "copia_factura_a";
          } 
          $this->proveedor_anterior_before_qstr = $this->proveedor_anterior;
          $this->proveedor_anterior = substr($this->Db->qstr($this->proveedor_anterior), 1, -1); 
          if ($this->proveedor_anterior == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->proveedor_anterior = "null"; 
              $NM_val_null[] = "proveedor_anterior";
          } 
          $this->servidor_anterior1_before_qstr = $this->servidor_anterior1;
          $this->servidor_anterior1 = substr($this->Db->qstr($this->servidor_anterior1), 1, -1); 
          if ($this->servidor_anterior1 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_anterior1 = "null"; 
              $NM_val_null[] = "servidor_anterior1";
          } 
          $this->servidor_anterior2_before_qstr = $this->servidor_anterior2;
          $this->servidor_anterior2 = substr($this->Db->qstr($this->servidor_anterior2), 1, -1); 
          if ($this->servidor_anterior2 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_anterior2 = "null"; 
              $NM_val_null[] = "servidor_anterior2";
          } 
          $this->servidor_anterior3_before_qstr = $this->servidor_anterior3;
          $this->servidor_anterior3 = substr($this->Db->qstr($this->servidor_anterior3), 1, -1); 
          if ($this->servidor_anterior3 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor_anterior3 = "null"; 
              $NM_val_null[] = "servidor_anterior3";
          } 
          $this->token_anterior_before_qstr = $this->token_anterior;
          $this->token_anterior = substr($this->Db->qstr($this->token_anterior), 1, -1); 
          if ($this->token_anterior == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->token_anterior = "null"; 
              $NM_val_null[] = "token_anterior";
          } 
          $this->password_anterior_before_qstr = $this->password_anterior;
          $this->password_anterior = substr($this->Db->qstr($this->password_anterior), 1, -1); 
          if ($this->password_anterior == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->password_anterior = "null"; 
              $NM_val_null[] = "password_anterior";
          } 
          $this->servidor4_before_qstr = $this->servidor4;
          $this->servidor4 = substr($this->Db->qstr($this->servidor4), 1, -1); 
          if ($this->servidor4 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor4 = "null"; 
              $NM_val_null[] = "servidor4";
          } 
          $this->servidor5_before_qstr = $this->servidor5;
          $this->servidor5 = substr($this->Db->qstr($this->servidor5), 1, -1); 
          if ($this->servidor5 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->servidor5 = "null"; 
              $NM_val_null[] = "servidor5";
          } 
      }
      if ($this->nmgp_opcao == "alterar") 
      {
          $SC_fields_update = array(); 
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          else  
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              if ($this->NM_ajax_flag)
              {
                 form_webservicefe_pack_ajax_response();
              }
              exit; 
          }  
          $bUpdateOk = true;
          $tmp_result = (int) $rs1->fields[0]; 
          if ($tmp_result != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "critica", $this->Ini->Nm_lang['lang_errm_nfnd']); 
              $this->nmgp_opcao = "nada"; 
              $bUpdateOk = false;
              $this->sc_evento = 'update';
          } 
          $aUpdateOk = array();
          $bUpdateOk = $bUpdateOk && empty($aUpdateOk);
          if ($bUpdateOk)
          { 
              $rs1->Close(); 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              else 
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "proveedor = '$this->proveedor', servidor1 = '$this->servidor1', servidor2 = '$this->servidor2', tokenempresa = '$this->tokenempresa', tokenpassword = '$this->tokenpassword', servidor_prueba1 = '$this->servidor_prueba1', servidor_prueba2 = '$this->servidor_prueba2', token_prueba = '$this->token_prueba', password_prueba = '$this->password_prueba', modo = '$this->modo', enviar_dian = $this->enviar_dian, enviar_cliente = $this->enviar_cliente, servidor3 = '$this->servidor3', servidor_prueba3 = '$this->servidor_prueba3', url_api_pdfs = '$this->url_api_pdfs', url_api_sendmail = '$this->url_api_sendmail', envio_credenciales = '$this->envio_credenciales', plantillas_correo = '$this->plantillas_correo', copia_factura_a = '$this->copia_factura_a', plantilla_pordefecto = $this->plantilla_pordefecto, proveedor_anterior = '$this->proveedor_anterior', servidor_anterior1 = '$this->servidor_anterior1', servidor_anterior2 = '$this->servidor_anterior2', servidor_anterior3 = '$this->servidor_anterior3', token_anterior = '$this->token_anterior', password_anterior = '$this->password_anterior', servidor4 = '$this->servidor4', servidor5 = '$this->servidor5'"; 
              } 
              $aDoNotUpdate = array();
              $comando .= implode(",", $SC_fields_update);  
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              {
                  $comando .= " WHERE idwebservicefe = $this->idwebservicefe ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $comando .= " WHERE idwebservicefe = $this->idwebservicefe ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $comando .= " WHERE idwebservicefe = $this->idwebservicefe ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $comando .= " WHERE idwebservicefe = $this->idwebservicefe ";  
              }  
              else  
              {
                  $comando .= " WHERE idwebservicefe = $this->idwebservicefe ";  
              }  
              $comando = str_replace("N'null'", "null", $comando) ; 
              $comando = str_replace("'null'", "null", $comando) ; 
              $comando = str_replace("#null#", "null", $comando) ; 
              $comando = str_replace($this->Ini->date_delim . "null" . $this->Ini->date_delim1, "null", $comando) ; 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                $comando = str_replace("EXTEND('', YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND('', YEAR TO DAY)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO DAY)", "null", $comando) ; 
              }  
              $useUpdateProcedure = false;
              if (!empty($SC_fields_update) || $useUpdateProcedure)
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = $comando; 
                  $rs = $this->Db->Execute($comando);  
                  if ($rs === false) 
                  { 
                      if (FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "MAIL SENT") && FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "WARNING"))
                      {
                          $dbErrorMessage = $this->Db->ErrorMsg();
                          $dbErrorCode = $this->Db->ErrorNo();
                          $this->handleDbErrorMessage($dbErrorMessage, $dbErrorCode);
                          $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_updt'], $dbErrorMessage, true);
                          if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler']) 
                          { 
                              $this->sc_erro_update = $dbErrorMessage;
                              $this->NM_rollback_db(); 
                              if ($this->NM_ajax_flag)
                              {
                                  form_webservicefe_pack_ajax_response();
                              }
                              exit;  
                          }   
                      }   
                  }   
              }   
              $this->proveedor = $this->proveedor_before_qstr;
              $this->servidor1 = $this->servidor1_before_qstr;
              $this->servidor2 = $this->servidor2_before_qstr;
              $this->tokenempresa = $this->tokenempresa_before_qstr;
              $this->tokenpassword = $this->tokenpassword_before_qstr;
              $this->servidor_prueba1 = $this->servidor_prueba1_before_qstr;
              $this->servidor_prueba2 = $this->servidor_prueba2_before_qstr;
              $this->token_prueba = $this->token_prueba_before_qstr;
              $this->password_prueba = $this->password_prueba_before_qstr;
              $this->servidor3 = $this->servidor3_before_qstr;
              $this->servidor_prueba3 = $this->servidor_prueba3_before_qstr;
              $this->url_api_pdfs = $this->url_api_pdfs_before_qstr;
              $this->url_api_sendmail = $this->url_api_sendmail_before_qstr;
              $this->copia_factura_a = $this->copia_factura_a_before_qstr;
              $this->proveedor_anterior = $this->proveedor_anterior_before_qstr;
              $this->servidor_anterior1 = $this->servidor_anterior1_before_qstr;
              $this->servidor_anterior2 = $this->servidor_anterior2_before_qstr;
              $this->servidor_anterior3 = $this->servidor_anterior3_before_qstr;
              $this->token_anterior = $this->token_anterior_before_qstr;
              $this->password_anterior = $this->password_anterior_before_qstr;
              $this->servidor4 = $this->servidor4_before_qstr;
              $this->servidor5 = $this->servidor5_before_qstr;
              if (in_array(strtolower($this->Ini->nm_tpbanco), $nm_bases_lob_geral))
              { 
              }   
              $this->sc_evento = "update"; 
              $this->nmgp_opcao = "igual"; 
              $this->nm_flag_iframe = true;
              if ($this->lig_edit_lookup)
              {
                  $this->lig_edit_lookup_call = true;
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['db_changed'] = true;
              if ($this->NM_ajax_flag) {
                  $this->NM_ajax_info['clearUpload'] = 'S';
              }


              if     (isset($NM_val_form) && isset($NM_val_form['proveedor'])) { $this->proveedor = $NM_val_form['proveedor']; }
              elseif (isset($this->proveedor)) { $this->nm_limpa_alfa($this->proveedor); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor1'])) { $this->servidor1 = $NM_val_form['servidor1']; }
              elseif (isset($this->servidor1)) { $this->nm_limpa_alfa($this->servidor1); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor2'])) { $this->servidor2 = $NM_val_form['servidor2']; }
              elseif (isset($this->servidor2)) { $this->nm_limpa_alfa($this->servidor2); }
              if     (isset($NM_val_form) && isset($NM_val_form['tokenempresa'])) { $this->tokenempresa = $NM_val_form['tokenempresa']; }
              elseif (isset($this->tokenempresa)) { $this->nm_limpa_alfa($this->tokenempresa); }
              if     (isset($NM_val_form) && isset($NM_val_form['tokenpassword'])) { $this->tokenpassword = $NM_val_form['tokenpassword']; }
              elseif (isset($this->tokenpassword)) { $this->nm_limpa_alfa($this->tokenpassword); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_prueba1'])) { $this->servidor_prueba1 = $NM_val_form['servidor_prueba1']; }
              elseif (isset($this->servidor_prueba1)) { $this->nm_limpa_alfa($this->servidor_prueba1); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_prueba2'])) { $this->servidor_prueba2 = $NM_val_form['servidor_prueba2']; }
              elseif (isset($this->servidor_prueba2)) { $this->nm_limpa_alfa($this->servidor_prueba2); }
              if     (isset($NM_val_form) && isset($NM_val_form['token_prueba'])) { $this->token_prueba = $NM_val_form['token_prueba']; }
              elseif (isset($this->token_prueba)) { $this->nm_limpa_alfa($this->token_prueba); }
              if     (isset($NM_val_form) && isset($NM_val_form['password_prueba'])) { $this->password_prueba = $NM_val_form['password_prueba']; }
              elseif (isset($this->password_prueba)) { $this->nm_limpa_alfa($this->password_prueba); }
              if     (isset($NM_val_form) && isset($NM_val_form['enviar_dian'])) { $this->enviar_dian = $NM_val_form['enviar_dian']; }
              elseif (isset($this->enviar_dian)) { $this->nm_limpa_alfa($this->enviar_dian); }
              if     (isset($NM_val_form) && isset($NM_val_form['enviar_cliente'])) { $this->enviar_cliente = $NM_val_form['enviar_cliente']; }
              elseif (isset($this->enviar_cliente)) { $this->nm_limpa_alfa($this->enviar_cliente); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor3'])) { $this->servidor3 = $NM_val_form['servidor3']; }
              elseif (isset($this->servidor3)) { $this->nm_limpa_alfa($this->servidor3); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_prueba3'])) { $this->servidor_prueba3 = $NM_val_form['servidor_prueba3']; }
              elseif (isset($this->servidor_prueba3)) { $this->nm_limpa_alfa($this->servidor_prueba3); }
              if     (isset($NM_val_form) && isset($NM_val_form['url_api_pdfs'])) { $this->url_api_pdfs = $NM_val_form['url_api_pdfs']; }
              elseif (isset($this->url_api_pdfs)) { $this->nm_limpa_alfa($this->url_api_pdfs); }
              if     (isset($NM_val_form) && isset($NM_val_form['url_api_sendmail'])) { $this->url_api_sendmail = $NM_val_form['url_api_sendmail']; }
              elseif (isset($this->url_api_sendmail)) { $this->nm_limpa_alfa($this->url_api_sendmail); }
              if     (isset($NM_val_form) && isset($NM_val_form['copia_factura_a'])) { $this->copia_factura_a = $NM_val_form['copia_factura_a']; }
              elseif (isset($this->copia_factura_a)) { $this->nm_limpa_alfa($this->copia_factura_a); }
              if     (isset($NM_val_form) && isset($NM_val_form['plantilla_pordefecto'])) { $this->plantilla_pordefecto = $NM_val_form['plantilla_pordefecto']; }
              elseif (isset($this->plantilla_pordefecto)) { $this->nm_limpa_alfa($this->plantilla_pordefecto); }
              if     (isset($NM_val_form) && isset($NM_val_form['proveedor_anterior'])) { $this->proveedor_anterior = $NM_val_form['proveedor_anterior']; }
              elseif (isset($this->proveedor_anterior)) { $this->nm_limpa_alfa($this->proveedor_anterior); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_anterior1'])) { $this->servidor_anterior1 = $NM_val_form['servidor_anterior1']; }
              elseif (isset($this->servidor_anterior1)) { $this->nm_limpa_alfa($this->servidor_anterior1); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_anterior2'])) { $this->servidor_anterior2 = $NM_val_form['servidor_anterior2']; }
              elseif (isset($this->servidor_anterior2)) { $this->nm_limpa_alfa($this->servidor_anterior2); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor_anterior3'])) { $this->servidor_anterior3 = $NM_val_form['servidor_anterior3']; }
              elseif (isset($this->servidor_anterior3)) { $this->nm_limpa_alfa($this->servidor_anterior3); }
              if     (isset($NM_val_form) && isset($NM_val_form['token_anterior'])) { $this->token_anterior = $NM_val_form['token_anterior']; }
              elseif (isset($this->token_anterior)) { $this->nm_limpa_alfa($this->token_anterior); }
              if     (isset($NM_val_form) && isset($NM_val_form['password_anterior'])) { $this->password_anterior = $NM_val_form['password_anterior']; }
              elseif (isset($this->password_anterior)) { $this->nm_limpa_alfa($this->password_anterior); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor4'])) { $this->servidor4 = $NM_val_form['servidor4']; }
              elseif (isset($this->servidor4)) { $this->nm_limpa_alfa($this->servidor4); }
              if     (isset($NM_val_form) && isset($NM_val_form['servidor5'])) { $this->servidor5 = $NM_val_form['servidor5']; }
              elseif (isset($this->servidor5)) { $this->nm_limpa_alfa($this->servidor5); }

              $this->nm_formatar_campos();
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
              }

              $aOldRefresh               = $this->nmgp_refresh_fields;
              $this->nmgp_refresh_fields = array_diff(array('proveedor', 'modo', 'servidor1', 'servidor2', 'servidor3', 'servidor4', 'servidor5', 'tokenempresa', 'tokenpassword', 'url_api_pdfs', 'url_api_sendmail', 'servidor_prueba1', 'servidor_prueba2', 'servidor_prueba3', 'token_prueba', 'password_prueba', 'enviar_dian', 'enviar_cliente', 'proveedor_anterior', 'servidor_anterior1', 'servidor_anterior2', 'servidor_anterior3', 'token_anterior', 'password_anterior', 'envio_credenciales', 'copia_factura_a', 'plantillas_correo', 'plantilla_pordefecto'), $aDoNotUpdate);
              $this->ajax_return_values();
              $this->nmgp_refresh_fields = $aOldRefresh;

              $this->nm_tira_formatacao();
          }  
      }  
      if ($this->nmgp_opcao == "incluir") 
      { 
          $NM_cmp_auto = "";
          $NM_seq_auto = "";
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
          { 
              $NM_seq_auto = "NULL, ";
              $NM_cmp_auto = "idwebservicefe, ";
          } 
          $bInsertOk = true;
          $aInsertOk = array(); 
          $bInsertOk = $bInsertOk && empty($aInsertOk);
          if (!isset($_POST['nmgp_ins_valid']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['insert_validation'] != $_POST['nmgp_ins_valid'])
          {
              $bInsertOk = false;
              $this->Erro->mensagem(__FILE__, __LINE__, 'security', $this->Ini->Nm_lang['lang_errm_inst_vald']);
              if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler'])
              {
                  $this->nmgp_opcao = 'refresh_insert';
                  if ($this->NM_ajax_flag)
                  {
                      form_webservicefe_pack_ajax_response();
                      exit;
                  }
              }
          }
          if ($bInsertOk)
          { 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              { 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES ('$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              { 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
              { 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              elseif ($this->Ini->nm_tpbanco == 'pdo_ibm')
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              else
              {
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5) VALUES (" . $NM_seq_auto . "'$this->proveedor', '$this->servidor1', '$this->servidor2', '$this->tokenempresa', '$this->tokenpassword', '$this->servidor_prueba1', '$this->servidor_prueba2', '$this->token_prueba', '$this->password_prueba', '$this->modo', $this->enviar_dian, $this->enviar_cliente, '$this->servidor3', '$this->servidor_prueba3', '$this->url_api_pdfs', '$this->url_api_sendmail', '$this->envio_credenciales', '$this->plantillas_correo', '$this->copia_factura_a', $this->plantilla_pordefecto, '$this->proveedor_anterior', '$this->servidor_anterior1', '$this->servidor_anterior2', '$this->servidor_anterior3', '$this->token_anterior', '$this->password_anterior', '$this->servidor4', '$this->servidor5')"; 
              }
              $comando = str_replace("N'null'", "null", $comando) ; 
              $comando = str_replace("'null'", "null", $comando) ; 
              $comando = str_replace("#null#", "null", $comando) ; 
              $comando = str_replace($this->Ini->date_delim . "null" . $this->Ini->date_delim1, "null", $comando) ; 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                $comando = str_replace("EXTEND('', YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND('', YEAR TO DAY)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO DAY)", "null", $comando) ; 
              }  
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $comando; 
              $rs = $this->Db->Execute($comando); 
              if ($rs === false)  
              { 
                  if (FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "MAIL SENT") && FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "WARNING"))
                  {
                      $dbErrorMessage = $this->Db->ErrorMsg();
                      $dbErrorCode = $this->Db->ErrorNo();
                      $this->handleDbErrorMessage($dbErrorMessage, $dbErrorCode);
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_inst'], $dbErrorMessage, true);
                      if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler'])
                      { 
                          $this->sc_erro_insert = $dbErrorMessage;
                          $this->nmgp_opcao     = 'refresh_insert';
                          $this->NM_rollback_db(); 
                          if ($this->NM_ajax_flag)
                          {
                              form_webservicefe_pack_ajax_response();
                              exit; 
                          }
                      }  
                  }  
              }  
              if ('refresh_insert' != $this->nmgp_opcao)
              {
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select @@identity"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      $this->NM_rollback_db(); 
                      if ($this->NM_ajax_flag)
                      {
                          form_webservicefe_pack_ajax_response();
                      }
                      exit; 
                  } 
                  $this->idwebservicefe =  $rsy->fields[0];
                 $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select last_insert_id()"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SELECT dbinfo('sqlca.sqlerrd1') FROM " . $this->Ini->nm_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select .currval from dual"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
              { 
                  $str_tabela = "SYSIBM.SYSDUMMY1"; 
                  if($this->Ini->nm_con_use_schema == "N") 
                  { 
                          $str_tabela = "SYSDUMMY1"; 
                  } 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SELECT IDENTITY_VAL_LOCAL() FROM " . $str_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select CURRVAL('')"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select gen_id(, 0) from " . $this->Ini->nm_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select last_insert_rowid()"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idwebservicefe = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              $this->proveedor = $this->proveedor_before_qstr;
              $this->servidor1 = $this->servidor1_before_qstr;
              $this->servidor2 = $this->servidor2_before_qstr;
              $this->tokenempresa = $this->tokenempresa_before_qstr;
              $this->tokenpassword = $this->tokenpassword_before_qstr;
              $this->servidor_prueba1 = $this->servidor_prueba1_before_qstr;
              $this->servidor_prueba2 = $this->servidor_prueba2_before_qstr;
              $this->token_prueba = $this->token_prueba_before_qstr;
              $this->password_prueba = $this->password_prueba_before_qstr;
              $this->servidor3 = $this->servidor3_before_qstr;
              $this->servidor_prueba3 = $this->servidor_prueba3_before_qstr;
              $this->url_api_pdfs = $this->url_api_pdfs_before_qstr;
              $this->url_api_sendmail = $this->url_api_sendmail_before_qstr;
              $this->copia_factura_a = $this->copia_factura_a_before_qstr;
              $this->proveedor_anterior = $this->proveedor_anterior_before_qstr;
              $this->servidor_anterior1 = $this->servidor_anterior1_before_qstr;
              $this->servidor_anterior2 = $this->servidor_anterior2_before_qstr;
              $this->servidor_anterior3 = $this->servidor_anterior3_before_qstr;
              $this->token_anterior = $this->token_anterior_before_qstr;
              $this->password_anterior = $this->password_anterior_before_qstr;
              $this->servidor4 = $this->servidor4_before_qstr;
              $this->servidor5 = $this->servidor5_before_qstr;
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['db_changed'] = true;

              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']))
              {
                  unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']);
              }

              $this->sc_evento = "insert"; 
              $this->proveedor = $this->proveedor_before_qstr;
              $this->servidor1 = $this->servidor1_before_qstr;
              $this->servidor2 = $this->servidor2_before_qstr;
              $this->tokenempresa = $this->tokenempresa_before_qstr;
              $this->tokenpassword = $this->tokenpassword_before_qstr;
              $this->servidor_prueba1 = $this->servidor_prueba1_before_qstr;
              $this->servidor_prueba2 = $this->servidor_prueba2_before_qstr;
              $this->token_prueba = $this->token_prueba_before_qstr;
              $this->password_prueba = $this->password_prueba_before_qstr;
              $this->servidor3 = $this->servidor3_before_qstr;
              $this->servidor_prueba3 = $this->servidor_prueba3_before_qstr;
              $this->url_api_pdfs = $this->url_api_pdfs_before_qstr;
              $this->url_api_sendmail = $this->url_api_sendmail_before_qstr;
              $this->copia_factura_a = $this->copia_factura_a_before_qstr;
              $this->proveedor_anterior = $this->proveedor_anterior_before_qstr;
              $this->servidor_anterior1 = $this->servidor_anterior1_before_qstr;
              $this->servidor_anterior2 = $this->servidor_anterior2_before_qstr;
              $this->servidor_anterior3 = $this->servidor_anterior3_before_qstr;
              $this->token_anterior = $this->token_anterior_before_qstr;
              $this->password_anterior = $this->password_anterior_before_qstr;
              $this->servidor4 = $this->servidor4_before_qstr;
              $this->servidor5 = $this->servidor5_before_qstr;
              if (empty($this->sc_erro_insert)) {
                  $this->record_insert_ok = true;
              } 
              if ('refresh_insert' != $this->nmgp_opcao && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_insert']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_redir_insert'] != "S"))
              {
              $this->nmgp_opcao = "novo"; 
              if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "R")
              { 
                   $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['return_edit'] = "new";
              } 
              }
              $this->nm_flag_iframe = true;
          } 
          if ($this->lig_edit_lookup)
          {
              $this->lig_edit_lookup_call = true;
          }
      } 
      if ($this->nmgp_opcao == "excluir") 
      { 
          $this->idwebservicefe = substr($this->Db->qstr($this->idwebservicefe), 1, -1); 

          $bDelecaoOk = true;
          $sMsgErro   = '';

          if ($bDelecaoOk)
          {

          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          else  
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              exit; 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              exit; 
          }  
          $tmp_result = (int) $rs1->fields[0]; 
          if ($tmp_result != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "critica", $this->Ini->Nm_lang['lang_errm_dele_nfnd']); 
              $this->nmgp_opcao = "nada"; 
              $this->sc_evento = 'delete';
          } 
          else 
          { 
              $rs1->Close(); 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
              }  
              else  
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idwebservicefe = $this->idwebservicefe "); 
              }  
              if ($rs === false) 
              { 
                  $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dele'], $this->Db->ErrorMsg(), true); 
                  if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler']) 
                  { 
                      $this->sc_erro_delete = $this->Db->ErrorMsg();  
                      $this->NM_rollback_db(); 
                      if ($this->NM_ajax_flag)
                      {
                          form_webservicefe_pack_ajax_response();
                          exit; 
                      }
                  } 
              } 
              $this->sc_evento = "delete"; 
              if (empty($this->sc_erro_delete)) {
                  $this->record_delete_ok = true;
              }
              $this->nmgp_opcao = "avanca"; 
              $this->nm_flag_iframe = true;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start']--; 
              if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] < 0)
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] = 0; 
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['db_changed'] = true;

              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']))
              {
                  unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']);
              }

              if ($this->lig_edit_lookup)
              {
                  $this->lig_edit_lookup_call = true;
              }
          }

          }
          else
          {
              $this->sc_evento = "delete"; 
              $this->nmgp_opcao = "igual"; 
              $this->Erro->mensagem(__FILE__, __LINE__, "critica", $sMsgErro); 
          }

      }  
      if (!empty($this->sc_force_zero))
      {
          foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
          {
              eval('if ($this->' . $sc_force_zero_field . ' == 0) {$this->' . $sc_force_zero_field . ' = "";}');
          }
      }
      $this->sc_force_zero = array();
      if (!empty($NM_val_null))
      {
          foreach ($NM_val_null as $i_val_null => $sc_val_null_field)
          {
              eval('$this->' . $sc_val_null_field . ' = "";');
          }
      }
      if ($salva_opcao == "incluir" && $GLOBALS["erro_incl"] != 1) 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['parms'] = "idwebservicefe?#?$this->idwebservicefe?@?"; 
      }
      $this->NM_commit_db(); 
      if ($this->sc_evento != "insert" && $this->sc_evento != "update" && $this->sc_evento != "delete")
      { 
          $this->idwebservicefe = null === $this->idwebservicefe ? null : substr($this->Db->qstr($this->idwebservicefe), 1, -1); 
      } 
      if (isset($this->NM_where_filter))
      {
          $this->NM_where_filter = str_replace("@percent@", "%", $this->NM_where_filter);
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'] = trim($this->NM_where_filter);
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']))
          {
              unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']);
          }
      }
      $sc_where_filter = '';
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form'])
      {
          $sc_where_filter = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'] && $sc_where_filter != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'])
      {
          if (empty($sc_where_filter))
          {
              $sc_where_filter = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'];
          }
          else
          {
              $sc_where_filter .= " and (" . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'] . ")";
          }
      }
      if ($this->nmgp_opcao != "novo" && $this->nmgp_opcao != "nada" && $this->nmgp_opcao != "inicio")
      { 
          $this->nmgp_opcao = "igual"; 
      } 
      $GLOBALS["NM_ERRO_IBASE"] = 0;  
//---------- 
      if ($this->nmgp_opcao != "novo" && $this->nmgp_opcao != "nada" && $this->nmgp_opcao != "refresh_insert") 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['parms'] = ""; 
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 1;  
          } 
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
          { 
              $nmgp_select = "SELECT idwebservicefe, proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5 from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          { 
              $nmgp_select = "SELECT idwebservicefe, proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5 from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          { 
              $nmgp_select = "SELECT idwebservicefe, proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5 from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $nmgp_select = "SELECT idwebservicefe, proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5 from " . $this->Ini->nm_tabela ; 
          } 
          else 
          { 
              $nmgp_select = "SELECT idwebservicefe, proveedor, servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba, modo, enviar_dian, enviar_cliente, servidor3, servidor_prueba3, url_api_pdfs, url_api_sendmail, envio_credenciales, plantillas_correo, copia_factura_a, plantilla_pordefecto, proveedor_anterior, servidor_anterior1, servidor_anterior2, servidor_anterior3, token_anterior, password_anterior, servidor4, servidor5 from " . $this->Ini->nm_tabela ; 
          } 
          $aWhere = array();
          $aWhere[] = "idwebservicefe='1'";
          $aWhere[] = $sc_where_filter;
          if ($this->nmgp_opcao == "igual" || (($_SESSION['sc_session'][$this->Ini->sc_page]['form_adm_clientes']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_adm_clientes']['run_iframe'] == "R") && ($this->sc_evento == "insert" || $this->sc_evento == "update")) )
          { 
              if (!empty($sc_where))
              {
                  if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
                  {
                     $aWhere[] = "idwebservicefe = $this->idwebservicefe"; 
                  }  
                  elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                  {
                     $aWhere[] = "idwebservicefe = $this->idwebservicefe"; 
                  }  
                  elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                  {
                     $aWhere[] = "idwebservicefe = $this->idwebservicefe"; 
                  }  
                  elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
                  {
                     $aWhere[] = "idwebservicefe = $this->idwebservicefe"; 
                  }  
                  else  
                  {
                     $aWhere[] = "idwebservicefe = $this->idwebservicefe"; 
                  }
              } 
          } 
          $nmgp_select .= $this->returnWhere($aWhere) . ' ';
          $sc_order_by = "";
          $sc_order_by = "idwebservicefe";
          $sc_order_by = str_replace("order by ", "", $sc_order_by);
          $sc_order_by = str_replace("ORDER BY ", "", trim($sc_order_by));
          if (!empty($sc_order_by))
          {
              $nmgp_select .= " order by $sc_order_by "; 
          }
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "R")
          {
              if ($this->sc_evento == "update")
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['select'] = $nmgp_select;
                  $this->nm_gera_html();
              } 
              elseif (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['select']))
              { 
                  $nmgp_select = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['select'];
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['select'] = ""; 
              } 
          } 
          if ($this->nmgp_opcao == "igual") 
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
              $rs = $this->Db->Execute($nmgp_select) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start']) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start']) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start']) ; 
          } 
          else  
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
              $rs = $this->Db->Execute($nmgp_select) ; 
              if (!$rs === false && !$rs->EOF) 
              { 
                  $rs->Move($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start']) ;  
              } 
          } 
          if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          if ($rs === false && $GLOBALS["NM_ERRO_IBASE"] == 1) 
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_nfnd_extr'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          if ($rs->EOF) 
          { 
              if (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter']))
              {
                  $this->nmgp_form_empty        = true;
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['first']   = "off";
                  $this->NM_ajax_info['buttonDisplay']['back']    = $this->nmgp_botoes['back']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['forward'] = $this->nmgp_botoes['forward'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['last']    = $this->nmgp_botoes['last']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['update']  = $this->nmgp_botoes['update']  = "off";
                  $this->NM_ajax_info['buttonDisplay']['delete']  = $this->nmgp_botoes['delete']  = "off";
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['insert']  = "off";
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter'] = true;
                  return; 
              }
              if ($this->nmgp_botoes['insert'] != "on")
              {
                  $this->nmgp_form_empty        = true;
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['first']   = "off";
                  $this->NM_ajax_info['buttonDisplay']['back']    = $this->nmgp_botoes['back']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['forward'] = $this->nmgp_botoes['forward'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['last']    = $this->nmgp_botoes['last']    = "off";
              }
              $this->NM_ajax_info['buttonDisplay']['update'] = $this->nmgp_botoes['update'] = "off";
              $this->NM_ajax_info['buttonDisplay']['delete'] = $this->nmgp_botoes['delete'] = "off";
              return; 
          } 
          if ($rs === false && $GLOBALS["NM_ERRO_IBASE"] == 1) 
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_nfnd_extr'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          if ($this->nmgp_opcao != "novo") 
          { 
              $this->idwebservicefe = $rs->fields[0] ; 
              $this->nmgp_dados_select['idwebservicefe'] = $this->idwebservicefe;
              $this->proveedor = $rs->fields[1] ; 
              $this->nmgp_dados_select['proveedor'] = $this->proveedor;
              $this->servidor1 = $rs->fields[2] ; 
              $this->nmgp_dados_select['servidor1'] = $this->servidor1;
              $this->servidor2 = $rs->fields[3] ; 
              $this->nmgp_dados_select['servidor2'] = $this->servidor2;
              $this->tokenempresa = $rs->fields[4] ; 
              $this->nmgp_dados_select['tokenempresa'] = $this->tokenempresa;
              $this->tokenpassword = $rs->fields[5] ; 
              $this->nmgp_dados_select['tokenpassword'] = $this->tokenpassword;
              $this->servidor_prueba1 = $rs->fields[6] ; 
              $this->nmgp_dados_select['servidor_prueba1'] = $this->servidor_prueba1;
              $this->servidor_prueba2 = $rs->fields[7] ; 
              $this->nmgp_dados_select['servidor_prueba2'] = $this->servidor_prueba2;
              $this->token_prueba = $rs->fields[8] ; 
              $this->nmgp_dados_select['token_prueba'] = $this->token_prueba;
              $this->password_prueba = $rs->fields[9] ; 
              $this->nmgp_dados_select['password_prueba'] = $this->password_prueba;
              $this->modo = $rs->fields[10] ; 
              $this->nmgp_dados_select['modo'] = $this->modo;
              $this->enviar_dian = $rs->fields[11] ; 
              $this->nmgp_dados_select['enviar_dian'] = $this->enviar_dian;
              $this->enviar_cliente = $rs->fields[12] ; 
              $this->nmgp_dados_select['enviar_cliente'] = $this->enviar_cliente;
              $this->servidor3 = $rs->fields[13] ; 
              $this->nmgp_dados_select['servidor3'] = $this->servidor3;
              $this->servidor_prueba3 = $rs->fields[14] ; 
              $this->nmgp_dados_select['servidor_prueba3'] = $this->servidor_prueba3;
              $this->url_api_pdfs = $rs->fields[15] ; 
              $this->nmgp_dados_select['url_api_pdfs'] = $this->url_api_pdfs;
              $this->url_api_sendmail = $rs->fields[16] ; 
              $this->nmgp_dados_select['url_api_sendmail'] = $this->url_api_sendmail;
              $this->envio_credenciales = $rs->fields[17] ; 
              $this->nmgp_dados_select['envio_credenciales'] = $this->envio_credenciales;
              $this->plantillas_correo = $rs->fields[18] ; 
              $this->nmgp_dados_select['plantillas_correo'] = $this->plantillas_correo;
              $this->copia_factura_a = $rs->fields[19] ; 
              $this->nmgp_dados_select['copia_factura_a'] = $this->copia_factura_a;
              $this->plantilla_pordefecto = $rs->fields[20] ; 
              $this->nmgp_dados_select['plantilla_pordefecto'] = $this->plantilla_pordefecto;
              $this->proveedor_anterior = $rs->fields[21] ; 
              $this->nmgp_dados_select['proveedor_anterior'] = $this->proveedor_anterior;
              $this->servidor_anterior1 = $rs->fields[22] ; 
              $this->nmgp_dados_select['servidor_anterior1'] = $this->servidor_anterior1;
              $this->servidor_anterior2 = $rs->fields[23] ; 
              $this->nmgp_dados_select['servidor_anterior2'] = $this->servidor_anterior2;
              $this->servidor_anterior3 = $rs->fields[24] ; 
              $this->nmgp_dados_select['servidor_anterior3'] = $this->servidor_anterior3;
              $this->token_anterior = $rs->fields[25] ; 
              $this->nmgp_dados_select['token_anterior'] = $this->token_anterior;
              $this->password_anterior = $rs->fields[26] ; 
              $this->nmgp_dados_select['password_anterior'] = $this->password_anterior;
              $this->servidor4 = $rs->fields[27] ; 
              $this->nmgp_dados_select['servidor4'] = $this->servidor4;
              $this->servidor5 = $rs->fields[28] ; 
              $this->nmgp_dados_select['servidor5'] = $this->servidor5;
          $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->idwebservicefe = (string)$this->idwebservicefe; 
              $this->enviar_dian = (string)$this->enviar_dian; 
              $this->enviar_cliente = (string)$this->enviar_cliente; 
              $this->plantilla_pordefecto = (string)$this->plantilla_pordefecto; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['parms'] = "idwebservicefe?#?$this->idwebservicefe?@?";
          } 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_select'] = $this->nmgp_dados_select;
          if (!$this->NM_ajax_flag || 'backup_line' != $this->NM_ajax_opcao)
          {
              $this->Nav_permite_ret = 0 != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'];
              $this->Nav_permite_ava = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['reg_start'] < $qt_geral_reg_form_webservicefe;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opcao']   = '';
          }
      } 
      if ($this->nmgp_opcao == "novo" || $this->nmgp_opcao == "refresh_insert") 
      { 
          $this->sc_evento_old = $this->sc_evento;
          $this->sc_evento = "novo";
          if ('refresh_insert' == $this->nmgp_opcao)
          {
              $this->nmgp_opcao = 'novo';
          }
          else
          {
              $this->nm_formatar_campos();
              $this->idwebservicefe = "";  
              $this->nmgp_dados_form["idwebservicefe"] = $this->idwebservicefe;
              $this->proveedor = "";  
              $this->nmgp_dados_form["proveedor"] = $this->proveedor;
              $this->servidor1 = "";  
              $this->nmgp_dados_form["servidor1"] = $this->servidor1;
              $this->servidor2 = "";  
              $this->nmgp_dados_form["servidor2"] = $this->servidor2;
              $this->tokenempresa = "";  
              $this->nmgp_dados_form["tokenempresa"] = $this->tokenempresa;
              $this->tokenpassword = "";  
              $this->nmgp_dados_form["tokenpassword"] = $this->tokenpassword;
              $this->servidor_prueba1 = "";  
              $this->nmgp_dados_form["servidor_prueba1"] = $this->servidor_prueba1;
              $this->servidor_prueba2 = "";  
              $this->nmgp_dados_form["servidor_prueba2"] = $this->servidor_prueba2;
              $this->token_prueba = "";  
              $this->nmgp_dados_form["token_prueba"] = $this->token_prueba;
              $this->password_prueba = "";  
              $this->nmgp_dados_form["password_prueba"] = $this->password_prueba;
              $this->modo = "";  
              $this->nmgp_dados_form["modo"] = $this->modo;
              $this->enviar_dian = "0";  
              $this->nmgp_dados_form["enviar_dian"] = $this->enviar_dian;
              $this->enviar_cliente = "0";  
              $this->nmgp_dados_form["enviar_cliente"] = $this->enviar_cliente;
              $this->servidor3 = "";  
              $this->nmgp_dados_form["servidor3"] = $this->servidor3;
              $this->servidor_prueba3 = "";  
              $this->nmgp_dados_form["servidor_prueba3"] = $this->servidor_prueba3;
              $this->url_api_pdfs = "";  
              $this->nmgp_dados_form["url_api_pdfs"] = $this->url_api_pdfs;
              $this->url_api_sendmail = "";  
              $this->nmgp_dados_form["url_api_sendmail"] = $this->url_api_sendmail;
              $this->envio_credenciales = "NO";  
              $this->nmgp_dados_form["envio_credenciales"] = $this->envio_credenciales;
              $this->plantillas_correo = "NO";  
              $this->nmgp_dados_form["plantillas_correo"] = $this->plantillas_correo;
              $this->copia_factura_a = "";  
              $this->nmgp_dados_form["copia_factura_a"] = $this->copia_factura_a;
              $this->plantilla_pordefecto = "";  
              $this->nmgp_dados_form["plantilla_pordefecto"] = $this->plantilla_pordefecto;
              $this->proveedor_anterior = "";  
              $this->nmgp_dados_form["proveedor_anterior"] = $this->proveedor_anterior;
              $this->servidor_anterior1 = "";  
              $this->nmgp_dados_form["servidor_anterior1"] = $this->servidor_anterior1;
              $this->servidor_anterior2 = "";  
              $this->nmgp_dados_form["servidor_anterior2"] = $this->servidor_anterior2;
              $this->servidor_anterior3 = "";  
              $this->nmgp_dados_form["servidor_anterior3"] = $this->servidor_anterior3;
              $this->token_anterior = "";  
              $this->nmgp_dados_form["token_anterior"] = $this->token_anterior;
              $this->password_anterior = "";  
              $this->nmgp_dados_form["password_anterior"] = $this->password_anterior;
              $this->servidor4 = "";  
              $this->nmgp_dados_form["servidor4"] = $this->servidor4;
              $this->servidor5 = "";  
              $this->nmgp_dados_form["servidor5"] = $this->servidor5;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dados_form'] = $this->nmgp_dados_form;
              $this->formatado = false;
          }
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
      }  
//
//
//-- 
      if ($this->nmgp_opcao != "novo") 
      {
      }
      if (!isset($this->nmgp_refresh_fields)) 
      { 
          $this->nm_proc_onload();
      }
  }
        function initializeRecordState() {
                $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'] = array();
        }

        function storeRecordState($sc_seq_vert = 0) {
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'])) {
                        $this->initializeRecordState();
                }
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert])) {
                        $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert] = array();
                }

                $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert]['buttons'] = array(
                        'delete' => $this->nmgp_botoes['delete'],
                        'update' => $this->nmgp_botoes['update']
                );
        }

        function loadRecordState($sc_seq_vert = 0) {
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state']) || !isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert])) {
                        return;
                }

                if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert]['buttons']['delete'])) {
                        $this->nmgp_botoes['delete'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert]['buttons']['delete'];
                }
                if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert]['buttons']['update'])) {
                        $this->nmgp_botoes['update'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['record_state'][$sc_seq_vert]['buttons']['update'];
                }
        }

//
function proveedor_anterior_onChange()
{
$_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'on';
  
$original_proveedor_anterior = $this->proveedor_anterior;
$original_servidor_anterior1 = $this->servidor_anterior1;
$original_servidor_anterior2 = $this->servidor_anterior2;
$original_servidor_anterior3 = $this->servidor_anterior3;

if(!empty($this->proveedor_anterior ))
{
	$vsql = "SELECT servidor1, servidor2, servidor3 FROM webservicefe_proveedores where proveedor='".$this->proveedor_anterior ."'";
	 
      $nm_select = $vsql; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vDatos = array();
      $this->vdatos = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vDatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vdatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vDatos = false;
          $this->vDatos_erro = $this->Db->ErrorMsg();
          $this->vdatos = false;
          $this->vdatos_erro = $this->Db->ErrorMsg();
      } 
;
	if(isset($this->vdatos[0][0]))
	{
		$this->servidor_anterior1  = $this->vdatos[0][0];
		$this->servidor_anterior2  = $this->vdatos[0][1];
		$this->servidor_anterior3  = $this->vdatos[0][2];
	}
}
else
{
	$this->servidor_anterior1  = "";
	$this->servidor_anterior2  = "";
	$this->servidor_anterior3  = "";
}

$modificado_proveedor_anterior = $this->proveedor_anterior;
$modificado_servidor_anterior1 = $this->servidor_anterior1;
$modificado_servidor_anterior2 = $this->servidor_anterior2;
$modificado_servidor_anterior3 = $this->servidor_anterior3;
$this->nm_formatar_campos('proveedor_anterior', 'servidor_anterior1', 'servidor_anterior2', 'servidor_anterior3');
if ($original_proveedor_anterior !== $modificado_proveedor_anterior || isset($this->nmgp_cmp_readonly['proveedor_anterior']) || (isset($bFlagRead_proveedor_anterior) && $bFlagRead_proveedor_anterior))
{
    $this->ajax_return_values_proveedor_anterior(true);
}
if ($original_servidor_anterior1 !== $modificado_servidor_anterior1 || isset($this->nmgp_cmp_readonly['servidor_anterior1']) || (isset($bFlagRead_servidor_anterior1) && $bFlagRead_servidor_anterior1))
{
    $this->ajax_return_values_servidor_anterior1(true);
}
if ($original_servidor_anterior2 !== $modificado_servidor_anterior2 || isset($this->nmgp_cmp_readonly['servidor_anterior2']) || (isset($bFlagRead_servidor_anterior2) && $bFlagRead_servidor_anterior2))
{
    $this->ajax_return_values_servidor_anterior2(true);
}
if ($original_servidor_anterior3 !== $modificado_servidor_anterior3 || isset($this->nmgp_cmp_readonly['servidor_anterior3']) || (isset($bFlagRead_servidor_anterior3) && $bFlagRead_servidor_anterior3))
{
    $this->ajax_return_values_servidor_anterior3(true);
}
$this->NM_ajax_info['event_field'] = 'proveedor';
form_webservicefe_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'off';
}
function proveedor_onChange()
{
$_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'on';
  
$original_proveedor = $this->proveedor;
$original_servidor1 = $this->servidor1;
$original_servidor2 = $this->servidor2;
$original_servidor3 = $this->servidor3;
$original_tokenempresa = $this->tokenempresa;
$original_tokenpassword = $this->tokenpassword;
$original_servidor_prueba1 = $this->servidor_prueba1;
$original_servidor_prueba2 = $this->servidor_prueba2;
$original_servidor_prueba3 = $this->servidor_prueba3;
$original_token_prueba = $this->token_prueba;
$original_password_prueba = $this->password_prueba;
$original_url_api_pdfs = $this->url_api_pdfs;
$original_url_api_sendmail = $this->url_api_sendmail;

$vsql = "SELECT servidor1, servidor2, tokenempresa, tokenpassword, servidor_prueba1, servidor_prueba2, token_prueba, password_prueba,servidor3,servidor_prueba3, url_api_pdfs, url_api_sendmail FROM webservicefe_proveedores where proveedor='".$this->proveedor ."'";
 
      $nm_select = $vsql; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vDatos = array();
      $this->vdatos = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vDatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vdatos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vDatos = false;
          $this->vDatos_erro = $this->Db->ErrorMsg();
          $this->vdatos = false;
          $this->vdatos_erro = $this->Db->ErrorMsg();
      } 
;
if(isset($this->vdatos[0][0]))
{
	$this->servidor1  = $this->vdatos[0][0];
	$this->servidor2  = $this->vdatos[0][1];
	$this->servidor3  = $this->vdatos[0][8];
	$this->servidor_prueba1  = $this->vdatos[0][4];
	$this->servidor_prueba2  = $this->vdatos[0][5];
	$this->servidor_prueba3  = $this->vdatos[0][9];
	$this->url_api_pdfs      = $this->vdatos[0][10];
	$this->url_api_sendmail  = $this->vdatos[0][11];
}


$modificado_proveedor = $this->proveedor;
$modificado_servidor1 = $this->servidor1;
$modificado_servidor2 = $this->servidor2;
$modificado_servidor3 = $this->servidor3;
$modificado_tokenempresa = $this->tokenempresa;
$modificado_tokenpassword = $this->tokenpassword;
$modificado_servidor_prueba1 = $this->servidor_prueba1;
$modificado_servidor_prueba2 = $this->servidor_prueba2;
$modificado_servidor_prueba3 = $this->servidor_prueba3;
$modificado_token_prueba = $this->token_prueba;
$modificado_password_prueba = $this->password_prueba;
$modificado_url_api_pdfs = $this->url_api_pdfs;
$modificado_url_api_sendmail = $this->url_api_sendmail;
$this->nm_formatar_campos('proveedor', 'servidor1', 'servidor2', 'servidor3', 'tokenempresa', 'tokenpassword', 'servidor_prueba1', 'servidor_prueba2', 'servidor_prueba3', 'token_prueba', 'password_prueba', 'url_api_pdfs', 'url_api_sendmail');
if ($original_proveedor !== $modificado_proveedor || isset($this->nmgp_cmp_readonly['proveedor']) || (isset($bFlagRead_proveedor) && $bFlagRead_proveedor))
{
    $this->ajax_return_values_proveedor(true);
}
if ($original_servidor1 !== $modificado_servidor1 || isset($this->nmgp_cmp_readonly['servidor1']) || (isset($bFlagRead_servidor1) && $bFlagRead_servidor1))
{
    $this->ajax_return_values_servidor1(true);
}
if ($original_servidor2 !== $modificado_servidor2 || isset($this->nmgp_cmp_readonly['servidor2']) || (isset($bFlagRead_servidor2) && $bFlagRead_servidor2))
{
    $this->ajax_return_values_servidor2(true);
}
if ($original_servidor3 !== $modificado_servidor3 || isset($this->nmgp_cmp_readonly['servidor3']) || (isset($bFlagRead_servidor3) && $bFlagRead_servidor3))
{
    $this->ajax_return_values_servidor3(true);
}
if ($original_tokenempresa !== $modificado_tokenempresa || isset($this->nmgp_cmp_readonly['tokenempresa']) || (isset($bFlagRead_tokenempresa) && $bFlagRead_tokenempresa))
{
    $this->ajax_return_values_tokenempresa(true);
}
if ($original_tokenpassword !== $modificado_tokenpassword || isset($this->nmgp_cmp_readonly['tokenpassword']) || (isset($bFlagRead_tokenpassword) && $bFlagRead_tokenpassword))
{
    $this->ajax_return_values_tokenpassword(true);
}
if ($original_servidor_prueba1 !== $modificado_servidor_prueba1 || isset($this->nmgp_cmp_readonly['servidor_prueba1']) || (isset($bFlagRead_servidor_prueba1) && $bFlagRead_servidor_prueba1))
{
    $this->ajax_return_values_servidor_prueba1(true);
}
if ($original_servidor_prueba2 !== $modificado_servidor_prueba2 || isset($this->nmgp_cmp_readonly['servidor_prueba2']) || (isset($bFlagRead_servidor_prueba2) && $bFlagRead_servidor_prueba2))
{
    $this->ajax_return_values_servidor_prueba2(true);
}
if ($original_servidor_prueba3 !== $modificado_servidor_prueba3 || isset($this->nmgp_cmp_readonly['servidor_prueba3']) || (isset($bFlagRead_servidor_prueba3) && $bFlagRead_servidor_prueba3))
{
    $this->ajax_return_values_servidor_prueba3(true);
}
if ($original_token_prueba !== $modificado_token_prueba || isset($this->nmgp_cmp_readonly['token_prueba']) || (isset($bFlagRead_token_prueba) && $bFlagRead_token_prueba))
{
    $this->ajax_return_values_token_prueba(true);
}
if ($original_password_prueba !== $modificado_password_prueba || isset($this->nmgp_cmp_readonly['password_prueba']) || (isset($bFlagRead_password_prueba) && $bFlagRead_password_prueba))
{
    $this->ajax_return_values_password_prueba(true);
}
if ($original_url_api_pdfs !== $modificado_url_api_pdfs || isset($this->nmgp_cmp_readonly['url_api_pdfs']) || (isset($bFlagRead_url_api_pdfs) && $bFlagRead_url_api_pdfs))
{
    $this->ajax_return_values_url_api_pdfs(true);
}
if ($original_url_api_sendmail !== $modificado_url_api_sendmail || isset($this->nmgp_cmp_readonly['url_api_sendmail']) || (isset($bFlagRead_url_api_sendmail) && $bFlagRead_url_api_sendmail))
{
    $this->ajax_return_values_url_api_sendmail(true);
}
$this->NM_ajax_info['event_field'] = 'proveedor';
form_webservicefe_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_webservicefe']['contr_erro'] = 'off';
}
//
 function nm_gera_html()
 {
    global
           $nm_url_saida, $nmgp_url_saida, $nm_saida_global, $nm_apl_dependente, $glo_subst, $sc_check_excl, $sc_check_incl, $nmgp_num_form, $NM_run_iframe;
     if ($this->Embutida_proc)
     {
         return;
     }
     if ($this->nmgp_form_show == 'off')
     {
         exit;
     }
      if (isset($NM_run_iframe) && $NM_run_iframe == 1)
      {
          $this->nmgp_botoes['exit'] = "off";
      }
     $HTTP_REFERER = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : ""; 
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['botoes'] = $this->nmgp_botoes;
     if ($this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form")
     {
         $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opc_ant'] = $this->nmgp_opcao;
     }
     else
     {
         $this->nmgp_opcao = $this->nmgp_opc_ant;
     }
     if (!empty($this->Campos_Mens_erro)) 
     {
         $this->Erro->mensagem(__FILE__, __LINE__, "critica", $this->Campos_Mens_erro); 
         $this->Campos_Mens_erro = "";
     }
     if (($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "R") && $this->nm_flag_iframe && empty($this->nm_todas_criticas))
     {
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe_ajax']))
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'] = array("edit", "");
          }
          else
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'] .= "&nmgp_opcao=edit";
          }
          if ($this->sc_evento == "insert" && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F")
          {
              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe_ajax']))
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'] = array("edit", "fim");
              }
              else
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'] .= "&rec=fim";
              }
          }
          $this->NM_close_db(); 
          $sJsParent = '';
          if ($this->NM_ajax_flag && isset($this->NM_ajax_info['param']['buffer_output']) && $this->NM_ajax_info['param']['buffer_output'])
          {
              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe_ajax']))
              {
                  $this->NM_ajax_info['ajaxJavascript'][] = array("parent.ajax_navigate", $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit']);
              }
              else
              {
                  $sJsParent .= 'parent';
                  $this->NM_ajax_info['redir']['metodo'] = 'location';
                  $this->NM_ajax_info['redir']['action'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'];
                  $this->NM_ajax_info['redir']['target'] = $sJsParent;
              }
              form_webservicefe_pack_ajax_response();
              exit;
          }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">

         <html><body>
         <script type="text/javascript">
<?php
    
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe_ajax']))
    {
        $opc = ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['run_iframe'] == "F" && $this->sc_evento == "insert") ? "fim" : "";
        echo "parent.ajax_navigate('edit', '" .$opc . "');";
    }
    else
    {
        echo $sJsParent . "parent.location = '" . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['retorno_edit'] . "';";
    }
?>
         </script>
         </body></html>
<?php
         exit;
     }
        $this->initFormPages();
        include_once("form_webservicefe_form0.php");
        include_once("form_webservicefe_form1.php");
        include_once("form_webservicefe_form2.php");
        include_once("form_webservicefe_form3.php");
        $this->hideFormPages();
 }

        function initFormPages() {
                $this->Ini->nm_page_names = array(
                        'Pag1' => '0',
                        'Pag2' => '1',
                        'Pag4' => '2',
                        'Pag3' => '3',
                );

                $this->Ini->nm_page_blocks = array(
                        'Pag1' => array(
                                0 => 'on',
                        ),
                        'Pag2' => array(
                                1 => 'on',
                        ),
                        'Pag4' => array(
                                2 => 'on',
                        ),
                        'Pag3' => array(
                                3 => 'on',
                        ),
                );

                $this->Ini->nm_block_page = array(
                        0 => 'Pag1',
                        1 => 'Pag2',
                        2 => 'Pag4',
                        3 => 'Pag3',
                );

                if (!empty($this->Ini->nm_hidden_blocos)) {
                        foreach ($this->Ini->nm_hidden_blocos as $blockNo => $blockStatus) {
                                if ('off' == $blockStatus) {
                                        $this->Ini->nm_page_blocks[ $this->Ini->nm_block_page[$blockNo] ][$blockNo] = 'off';
                                }
                        }
                }

                foreach ($this->Ini->nm_page_blocks as $pageName => $pageBlocks) {
                        $hasDisplayedBlock = false;

                        foreach ($pageBlocks as $blockNo => $blockStatus) {
                                if ('on' == $blockStatus) {
                                        $hasDisplayedBlock = true;
                                }
                        }

                        if (!$hasDisplayedBlock) {
                                $this->Ini->nm_hidden_pages[$pageName] = 'off';
                        }
                }
        } // initFormPages

        function hideFormPages() {
                if (!empty($this->Ini->nm_hidden_pages)) {
?>
<script type="text/javascript">
$(function() {
        scResetPagesDisplay();
<?php
                        foreach ($this->Ini->nm_hidden_pages as $pageName => $pageStatus) {
                                if ('off' == $pageStatus) {
?>
        scHidePage("<?php echo $this->Ini->nm_page_names[$pageName]; ?>");
<?php
                                }
                        }
?>
        scCheckNoPageSelected();
});
</script>
<?php
                }
        } // hideFormPages

    function form_format_readonly($field, $value)
    {
        $result = $value;

        $this->form_highlight_search($result, $field, $value);

        return $result;
    }

    function form_highlight_search(&$result, $field, $value)
    {
        if ($this->proc_fast_search) {
            $this->form_highlight_search_quicksearch($result, $field, $value);
        }
    }

    function form_highlight_search_quicksearch(&$result, $field, $value)
    {
        $searchOk = false;
        if ('SC_all_Cmp' == $this->nmgp_fast_search && in_array($field, array("idwebservicefe", "proveedor", "servidor1", "servidor2", "tokenempresa", "tokenpassword"))) {
            $searchOk = true;
        }
        elseif ($field == $this->nmgp_fast_search && in_array($field, array(""))) {
            $searchOk = true;
        }

        if (!$searchOk || '' == $this->nmgp_arg_fast_search) {
            return;
        }

        $htmlIni = '<div class="highlight" style="background-color: #fafaca; display: inline-block">';
        $htmlFim = '</div>';

        if ('qp' == $this->nmgp_cond_fast_search) {
            $keywords = preg_quote($this->nmgp_arg_fast_search, '/');
            $result = preg_replace('/'. $keywords .'/i', $htmlIni . '$0' . $htmlFim, $result);
        } elseif ('eq' == $this->nmgp_cond_fast_search) {
            if (strcasecmp($this->nmgp_arg_fast_search, $value) == 0) {
                $result = $htmlIni. $result .$htmlFim;
            }
        }
    }


    function form_encode_input($string)
    {
        if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['table_refresh']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['table_refresh'])
        {
            return NM_encode_input(NM_encode_input($string));
        }
        else
        {
            return NM_encode_input($string);
        }
    } // form_encode_input


    function scCsrfGetToken()
    {
        if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['csrf_token']))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['csrf_token'] = $this->scCsrfGenerateToken();
        }

        return $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['csrf_token'];
    }

    function scCsrfGenerateToken()
    {
        $aSources = array(
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            '1234567890',
            '!@$*()-_[]{},.;:'
        );

        $sRandom = '';

        $aSourcesSizes = array();
        $iSourceSize   = sizeof($aSources) - 1;
        for ($i = 0; $i <= $iSourceSize; $i++)
        {
            $aSourcesSizes[$i] = strlen($aSources[$i]) - 1;
        }

        for ($i = 0; $i < 64; $i++)
        {
            $iSource = $this->scCsrfRandom(0, $iSourceSize);
            $sRandom .= substr($aSources[$iSource], $this->scCsrfRandom(0, $aSourcesSizes[$iSource]), 1);
        }

        return $sRandom;
    }

    function scCsrfRandom($iMin, $iMax)
    {
        return mt_rand($iMin, $iMax);
    }

        function addUrlParam($url, $param, $value) {
                $urlParts  = explode('?', $url);
                $urlParams = isset($urlParts[1]) ? explode('&', $urlParts[1]) : array();
                $objParams = array();
                foreach ($urlParams as $paramInfo) {
                        $paramParts = explode('=', $paramInfo);
                        $objParams[ $paramParts[0] ] = isset($paramParts[1]) ? $paramParts[1] : '';
                }
                $objParams[$param] = $value;
                $urlParams = array();
                foreach ($objParams as $paramName => $paramValue) {
                        $urlParams[] = $paramName . '=' . $paramValue;
                }
                return $urlParts[0] . '?' . implode('&', $urlParams);
        }
 function allowedCharsCharset($charlist)
 {
     if ($_SESSION['scriptcase']['charset'] != 'UTF-8')
     {
         $charlist = NM_conv_charset($charlist, $_SESSION['scriptcase']['charset'], 'UTF-8');
     }
     return str_replace("'", "\'", $charlist);
 }

function sc_file_size($file, $format = false)
{
    if ('' == $file) {
        return '';
    }
    if (!@is_file($file)) {
        return '';
    }
    $fileSize = @filesize($file);
    if ($format) {
        $suffix = '';
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' KB';
        }
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' MB';
        }
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' GB';
        }
        $fileSize = $fileSize . $suffix;
    }
    return $fileSize;
}


 function new_date_format($type, $field)
 {
     $new_date_format_out = '';

     if ('DT' == $type)
     {
         $date_format  = $this->field_config[$field]['date_format'];
         $date_sep     = $this->field_config[$field]['date_sep'];
         $date_display = $this->field_config[$field]['date_display'];
         $time_format  = '';
         $time_sep     = '';
         $time_display = '';
     }
     elseif ('DH' == $type)
     {
         $date_format  = false !== strpos($this->field_config[$field]['date_format'] , ';') ? substr($this->field_config[$field]['date_format'] , 0, strpos($this->field_config[$field]['date_format'] , ';')) : $this->field_config[$field]['date_format'];
         $date_sep     = $this->field_config[$field]['date_sep'];
         $date_display = false !== strpos($this->field_config[$field]['date_display'], ';') ? substr($this->field_config[$field]['date_display'], 0, strpos($this->field_config[$field]['date_display'], ';')) : $this->field_config[$field]['date_display'];
         $time_format  = false !== strpos($this->field_config[$field]['date_format'] , ';') ? substr($this->field_config[$field]['date_format'] , strpos($this->field_config[$field]['date_format'] , ';') + 1) : '';
         $time_sep     = $this->field_config[$field]['time_sep'];
         $time_display = false !== strpos($this->field_config[$field]['date_display'], ';') ? substr($this->field_config[$field]['date_display'], strpos($this->field_config[$field]['date_display'], ';') + 1) : '';
     }
     elseif ('HH' == $type)
     {
         $date_format  = '';
         $date_sep     = '';
         $date_display = '';
         $time_format  = $this->field_config[$field]['date_format'];
         $time_sep     = $this->field_config[$field]['time_sep'];
         $time_display = $this->field_config[$field]['date_display'];
     }

     if ('DT' == $type || 'DH' == $type)
     {
         $date_array = array();
         $date_index = 0;
         $date_ult   = '';
         for ($i = 0; $i < strlen($date_format); $i++)
         {
             $char = strtolower(substr($date_format, $i, 1));
             if (in_array($char, array('d', 'm', 'y', 'a')))
             {
                 if ('a' == $char)
                 {
                     $char = 'y';
                 }
                 if ($char == $date_ult)
                 {
                     $date_array[$date_index] .= $char;
                 }
                 else
                 {
                     if ('' != $date_ult)
                     {
                         $date_index++;
                     }
                     $date_array[$date_index] = $char;
                 }
             }
             $date_ult = $char;
         }

         $disp_array = array();
         $date_index = 0;
         $date_ult   = '';
         for ($i = 0; $i < strlen($date_display); $i++)
         {
             $char = strtolower(substr($date_display, $i, 1));
             if (in_array($char, array('d', 'm', 'y', 'a')))
             {
                 if ('a' == $char)
                 {
                     $char = 'y';
                 }
                 if ($char == $date_ult)
                 {
                     $disp_array[$date_index] .= $char;
                 }
                 else
                 {
                     if ('' != $date_ult)
                     {
                         $date_index++;
                     }
                     $disp_array[$date_index] = $char;
                 }
             }
             $date_ult = $char;
         }

         $date_final = array();
         foreach ($date_array as $date_part)
         {
             if (in_array($date_part, $disp_array))
             {
                 $date_final[] = $date_part;
             }
         }

         $date_format = implode($date_sep, $date_final);
     }
     if ('HH' == $type || 'DH' == $type)
     {
         $time_array = array();
         $time_index = 0;
         $time_ult   = '';
         for ($i = 0; $i < strlen($time_format); $i++)
         {
             $char = strtolower(substr($time_format, $i, 1));
             if (in_array($char, array('h', 'i', 's')))
             {
                 if ($char == $time_ult)
                 {
                     $time_array[$time_index] .= $char;
                 }
                 else
                 {
                     if ('' != $time_ult)
                     {
                         $time_index++;
                     }
                     $time_array[$time_index] = $char;
                 }
             }
             $time_ult = $char;
         }

         $disp_array = array();
         $time_index = 0;
         $time_ult   = '';
         for ($i = 0; $i < strlen($time_display); $i++)
         {
             $char = strtolower(substr($time_display, $i, 1));
             if (in_array($char, array('h', 'i', 's')))
             {
                 if ($char == $time_ult)
                 {
                     $disp_array[$time_index] .= $char;
                 }
                 else
                 {
                     if ('' != $time_ult)
                     {
                         $time_index++;
                     }
                     $disp_array[$time_index] = $char;
                 }
             }
             $time_ult = $char;
         }

         $time_final = array();
         foreach ($time_array as $time_part)
         {
             if (in_array($time_part, $disp_array))
             {
                 $time_final[] = $time_part;
             }
         }

         $time_format = implode($time_sep, $time_final);
     }

     if ('DT' == $type)
     {
         $old_date_format = $date_format;
     }
     elseif ('DH' == $type)
     {
         $old_date_format = $date_format . ';' . $time_format;
     }
     elseif ('HH' == $type)
     {
         $old_date_format = $time_format;
     }

     for ($i = 0; $i < strlen($old_date_format); $i++)
     {
         $char = substr($old_date_format, $i, 1);
         if ('/' == $char)
         {
             $new_date_format_out .= $date_sep;
         }
         elseif (':' == $char)
         {
             $new_date_format_out .= $time_sep;
         }
         else
         {
             $new_date_format_out .= $char;
         }
     }

     $this->field_config[$field]['date_format'] = $new_date_format_out;
     if ('DH' == $type)
     {
         $new_date_format_out                                  = explode(';', $new_date_format_out);
         $this->field_config[$field]['date_format_js']        = $new_date_format_out[0];
         $this->field_config[$field . '_hora']['date_format'] = $new_date_format_out[1];
         $this->field_config[$field . '_hora']['time_sep']    = $this->field_config[$field]['time_sep'];
     }
 } // new_date_format

   function Form_lookup_proveedor()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "FACILWEB?#?FACILWEB?#?N?@?";
       $nmgp_def_dados .= "DATAICO?#?DATAICO?#?N?@?";
       $nmgp_def_dados .= "CADENA S. A.?#?CADENA S. A.?#?N?@?";
       $nmgp_def_dados .= "THE FACTORY HKA?#?THE FACTORY HKA?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_modo()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "PRUEBAS?#?PRUEBAS?#?N?@?";
       $nmgp_def_dados .= "PRODUCCIÓN?#?PRODUCCION?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_enviar_dian()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?1?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_enviar_cliente()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?1?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_proveedor_anterior()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "FACILWEB?#?FACILWEB?#??@?";
       $nmgp_def_dados .= "DATAICO?#?DATAICO?#??@?";
       $nmgp_def_dados .= "CADENA S. A.?#?CADENA S. A.?#??@?";
       $nmgp_def_dados .= "THE FACTORY HKA?#?THE FACTORY HKA?#??@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_envio_credenciales()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_plantillas_correo()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_plantilla_pordefecto()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'] = array(); 
    }
   $enviar_dian_val_str = "";
   if (!empty($this->enviar_dian))
   {
       if (is_array($this->enviar_dian))
       {
           $Tmp_array = $this->enviar_dian;
       }
       else
       {
           $Tmp_array = explode(";", $this->enviar_dian);
       }
       $enviar_dian_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $enviar_dian_val_str)
          {
             $enviar_dian_val_str .= ", ";
          }
          $enviar_dian_val_str .= $Tmp_val_cmp;
       }
   }
   $enviar_cliente_val_str = "";
   if (!empty($this->enviar_cliente))
   {
       if (is_array($this->enviar_cliente))
       {
           $Tmp_array = $this->enviar_cliente;
       }
       else
       {
           $Tmp_array = explode(";", $this->enviar_cliente);
       }
       $enviar_cliente_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $enviar_cliente_val_str)
          {
             $enviar_cliente_val_str .= ", ";
          }
          $enviar_cliente_val_str .= $Tmp_val_cmp;
       }
   }
   $envio_credenciales_val_str = "''";
   if (!empty($this->envio_credenciales))
   {
       if (is_array($this->envio_credenciales))
       {
           $Tmp_array = $this->envio_credenciales;
       }
       else
       {
           $Tmp_array = explode(";", $this->envio_credenciales);
       }
       $envio_credenciales_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $envio_credenciales_val_str)
          {
             $envio_credenciales_val_str .= ", ";
          }
          $envio_credenciales_val_str .= "'$Tmp_val_cmp'";
       }
   }
   $plantillas_correo_val_str = "''";
   if (!empty($this->plantillas_correo))
   {
       if (is_array($this->plantillas_correo))
       {
           $Tmp_array = $this->plantillas_correo;
       }
       else
       {
           $Tmp_array = explode(";", $this->plantillas_correo);
       }
       $plantillas_correo_val_str = "";
       foreach ($Tmp_array as $Tmp_val_cmp)
       {
          if ("" != $plantillas_correo_val_str)
          {
             $plantillas_correo_val_str .= ", ";
          }
          $plantillas_correo_val_str .= "'$Tmp_val_cmp'";
       }
   }
   $nm_comando = "SELECT id, descripcion  FROM plantillas_correo_propio  ORDER BY descripcion";
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['Lookup_plantilla_pordefecto'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function SC_fast_search($in_fields, $arg_search, $data_search)
   {
      $fields = (strpos($in_fields, "SC_all_Cmp") !== false) ? array("SC_all_Cmp") : explode(";", $in_fields);
      $this->NM_case_insensitive = false;
      if (empty($data_search)) 
      {
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter']);
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total']);
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['fast_search']);
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal'])) 
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal'];
          }
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter'])
          {
              unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter']);
              $this->NM_ajax_info['empty_filter'] = 'ok';
              form_webservicefe_pack_ajax_response();
              exit;
          }
          return;
      }
      $comando = "";
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($data_search))
      {
          $data_search = NM_conv_charset($data_search, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
      $sv_data = $data_search;
      foreach ($fields as $field) {
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "idwebservicefe", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_proveedor($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "proveedor", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "servidor1", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "servidor2", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "tokenempresa", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "tokenpassword", $arg_search, $data_search);
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal']) && !empty($comando)) 
      {
          $comando = $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_detal'] . " and (" .  $comando . ")";
      }
      if (empty($comando)) 
      {
          $comando = " 1 <> 1 "; 
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form'])
      {
          $sc_where = " where " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter_form'] . " and (idwebservicefe='1') and (" . $comando . ")";
      }
      else
      {
          $sc_where = " where idwebservicefe='1' and (" . $comando . ")";
      }
      $nmgp_select = "SELECT count(*) AS countTest from " . $this->Ini->nm_tabela . $sc_where; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
      $rt = $this->Db->Execute($nmgp_select) ; 
      if ($rt === false && !$rt->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
      { 
          $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
          exit ; 
      }  
      $qt_geral_reg_form_webservicefe = isset($rt->fields[0]) ? $rt->fields[0] - 1 : 0; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['total'] = $qt_geral_reg_form_webservicefe;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['where_filter'] = $comando;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['fast_search'][0] = $field;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['fast_search'][1] = $arg_search;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['fast_search'][2] = $sv_data;
      $rt->Close(); 
      if (isset($rt->fields[0]) && $rt->fields[0] > 0 &&  isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter'])
      {
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter']);
          $this->NM_ajax_info['empty_filter'] = 'ok';
          form_webservicefe_pack_ajax_response();
          exit;
      }
      elseif (!isset($rt->fields[0]) || $rt->fields[0] == 0)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['empty_filter'] = true;
          $this->NM_ajax_info['empty_filter'] = 'ok';
          form_webservicefe_pack_ajax_response();
          exit;
      }
   }
   function SC_monta_condicao(&$comando, $nome, $condicao, $campo, $tp_campo="")
   {
      $nm_aspas   = "'";
      $nm_aspas1  = "'";
      $nm_numeric = array();
      $Nm_datas   = array();
      $nm_esp_postgres = array();
      $campo_join = strtolower(str_replace(".", "_", $nome));
      $nm_ini_lower = "";
      $nm_fim_lower = "";
      $nm_numeric[] = "idwebservicefe";$nm_numeric[] = "enviar_dian";$nm_numeric[] = "enviar_cliente";$nm_numeric[] = "plantilla_pordefecto";
      if (in_array($campo_join, $nm_numeric))
      {
         if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['decimal_db'] == ".")
         {
             $nm_aspas  = "";
             $nm_aspas1 = "";
         }
         if (is_array($campo))
         {
             foreach ($campo as $Ind => $Cmp)
             {
                if (!is_numeric($Cmp))
                {
                    return;
                }
                if ($Cmp == "")
                {
                    $campo[$Ind] = 0;
                }
             }
         }
         else
         {
             if (!is_numeric($campo))
             {
                 return;
             }
             if ($campo == "")
             {
                $campo = 0;
             }
         }
      }
         if (in_array($campo_join, $nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && (strtoupper($condicao) == "II" || strtoupper($condicao) == "QP" || strtoupper($condicao) == "NP"))
         {
             $nome      = "CAST ($nome AS TEXT)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
         if (in_array($campo_join, $nm_esp_postgres) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
         {
             $nome      = "CAST ($nome AS TEXT)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
         if (in_array($campo_join, $nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase) && (strtoupper($condicao) == "II" || strtoupper($condicao) == "QP" || strtoupper($condicao) == "NP"))
         {
             $nome      = "CAST ($nome AS VARCHAR)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
         $comando .= (!empty($comando) ? " or " : "");
         if (is_array($campo))
         {
             $prep = "";
             foreach ($campo as $Ind => $Cmp)
             {
                 $prep .= (!empty($prep)) ? "," : "";
                 $Cmp   = substr($this->Db->qstr($Cmp), 1, -1);
                 $prep .= $nm_ini_lower . $nm_aspas . $Cmp . $nm_aspas1 . $nm_fim_lower;
             }
             $prep .= (empty($prep)) ? $nm_aspas . $nm_aspas1 : "";
             $comando .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $prep . ")";
             return;
         }
         $campo  = substr($this->Db->qstr($campo), 1, -1);
         $cond_tst = strtoupper($condicao);
         if ($cond_tst == "II" || $cond_tst == "QP" || $cond_tst == "NP")
         {
             if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && $this->NM_case_insensitive)
             {
                 $op_like      = " ilike ";
                 $nm_ini_lower = "";
                 $nm_fim_lower = "";
             }
             else
             {
                 $op_like = " like ";
             }
         }
         switch ($cond_tst)
         {
            case "EQ":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " = " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "II":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_like . $nm_ini_lower . "'" . $campo . "%'" . $nm_fim_lower;
            break;
            case "QP":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_like . $nm_ini_lower . "'%" . $campo . "%'" . $nm_fim_lower;
            break;
            case "NP":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " not" . $op_like . $nm_ini_lower . "'%" . $campo . "%'" . $nm_fim_lower;
            break;
            case "DF":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "GT":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " > " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "GE":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " >= " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "LT":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " < " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "LE":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <= " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
         }
   }
   function SC_lookup_proveedor($condicao, $campo)
   {
       $data_look = array();
       $campo  = substr($this->Db->qstr($campo), 1, -1);
       $data_look['FACILWEB'] = "FACILWEB";
       $data_look['DATAICO'] = "DATAICO";
       $data_look['CADENA S. A.'] = "CADENA S. A.";
       $data_look['THE FACTORY HKA'] = "THE FACTORY HKA";
       $result = array();
       foreach ($data_look as $chave => $label) 
       {
           if ($condicao == "eq" && $campo == $label)
           {
               $result[] = $chave;
           }
           if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
           {
               $result[] = $chave;
           }
           if ($condicao == "qp" && strstr($label, $campo))
           {
               $result[] = $chave;
           }
           if ($condicao == "np" && !strstr($label, $campo))
           {
               $result[] = $chave;
           }
           if ($condicao == "df" && $campo != $label)
           {
               $result[] = $chave;
           }
           if ($condicao == "gt" && $label > $campo )
           {
               $result[] = $chave;
           }
           if ($condicao == "ge" && $label >= $campo)
            {
               $result[] = $chave;
           }
           if ($condicao == "lt" && $label < $campo)
           {
               $result[] = $chave;
           }
           if ($condicao == "le" && $label <= $campo)
           {
               $result[] = $chave;
           }
          
       }
       return $result;
   }
function nmgp_redireciona($tipo=0)
{
   global $nm_apl_dependente;
   if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno']) && $_SESSION['scriptcase']['sc_tp_saida'] != "D" && $nm_apl_dependente != 1) 
   {
       $nmgp_saida_form = $_SESSION['scriptcase']['nm_sc_retorno'];
   }
   else
   {
       $nmgp_saida_form = $_SESSION['scriptcase']['sc_url_saida'][$this->Ini->sc_page];
   }
   if ($tipo == 2)
   {
       $nmgp_saida_form = "form_webservicefe_fim.php";
   }
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['redir']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['redir'] == 'redir')
   {
       unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']);
   }
   unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['opc_ant']);
   if ($tipo == 2 && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['nm_run_menu']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['nm_run_menu'] == 1)
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['nm_run_menu'] = 2;
       $nmgp_saida_form = "form_webservicefe_fim.php";
   }
   $diretorio = explode("/", $nmgp_saida_form);
   $cont = count($diretorio);
   $apl = $diretorio[$cont - 1];
   $apl = str_replace(".php", "", $apl);
   $pos = strpos($apl, "?");
   if ($pos !== false)
   {
       $apl = substr($apl, 0, $pos);
   }
   if ($tipo != 1 && $tipo != 2)
   {
       unset($_SESSION['sc_session'][$this->Ini->sc_page][$apl]['where_orig']);
   }
   if ($this->NM_ajax_flag)
   {
       $sTarget = '_self';
       $this->NM_ajax_info['redir']['metodo']              = 'post';
       $this->NM_ajax_info['redir']['action']              = $nmgp_saida_form;
       $this->NM_ajax_info['redir']['target']              = $sTarget;
       $this->NM_ajax_info['redir']['script_case_init']    = $this->Ini->sc_page;
       if (0 == $tipo)
       {
           $this->NM_ajax_info['redir']['nmgp_url_saida'] = $this->nm_location;
       }
       form_webservicefe_pack_ajax_response();
       exit;
   }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">

   <HTML>
   <HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php

   if (isset($_SESSION['scriptcase']['device_mobile']) && $_SESSION['scriptcase']['device_mobile'] && $_SESSION['scriptcase']['display_mobile'])
   {
?>
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
   }

?>
    <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
    <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
    <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
    <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
    <META http-equiv="Pragma" content="no-cache"/>
    <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
   </HEAD>
   <BODY>
   <FORM name="form_ok" method="POST" action="<?php echo $this->form_encode_input($nmgp_saida_form); ?>" target="_self">
<?php
   if ($tipo == 0)
   {
?>
     <INPUT type="hidden" name="nmgp_url_saida" value="<?php echo $this->form_encode_input($this->nm_location); ?>"> 
<?php
   }
?>
     <INPUT type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
   </FORM>
   <SCRIPT type="text/javascript">
      bLigEditLookupCall = <?php if ($this->lig_edit_lookup_call) { ?>true<?php } else { ?>false<?php } ?>;
      function scLigEditLookupCall()
      {
<?php
   if ($this->lig_edit_lookup && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_modal']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['sc_modal'])
   {
?>
        parent.<?php echo $this->lig_edit_lookup_cb; ?>(<?php echo $this->lig_edit_lookup_row; ?>);
<?php
   }
   elseif ($this->lig_edit_lookup)
   {
?>
        opener.<?php echo $this->lig_edit_lookup_cb; ?>(<?php echo $this->lig_edit_lookup_row; ?>);
<?php
   }
?>
      }
      if (bLigEditLookupCall)
      {
        scLigEditLookupCall();
      }
<?php
if ($tipo == 2 && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue']))
{
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['under_dashboard']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['under_dashboard']) {
?>
var dbParentFrame = $(parent.document).find("[name='<?php echo $_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['dashboard_info']['parent_widget']; ?>']");
if (dbParentFrame && dbParentFrame[0] && dbParentFrame[0].contentWindow.scAjaxDetailValue)
{
<?php
        foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue'] as $cmp_master => $val_master)
        {
?>
    dbParentFrame[0].contentWindow.scAjaxDetailValue('<?php echo $cmp_master ?>', '<?php echo $val_master ?>');
<?php
        }
        unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue']);
?>
}
<?php
    }
    else {
?>
if (parent && parent.scAjaxDetailValue)
{
<?php
        foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue'] as $cmp_master => $val_master)
        {
?>
    parent.scAjaxDetailValue('<?php echo $cmp_master ?>', '<?php echo $val_master ?>');
<?php
        }
        unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_webservicefe']['masterValue']);
?>
}
<?php
    }
}
?>
      document.form_ok.submit();
   </SCRIPT>
   </BODY>
   </HTML>
<?php
  exit;
}
    function getButtonIds($buttonName) {
        switch ($buttonName) {
            case "update":
                return array("sc_b_upd_t.sc-unique-btn-1");
                break;
            case "help":
                return array("sc_b_hlp_t");
                break;
            case "exit":
                return array("sc_b_sai_t.sc-unique-btn-2", "sc_b_sai_t.sc-unique-btn-4", "sc_b_sai_t.sc-unique-btn-3");
                break;
        }

        return array($buttonName);
    } // getButtonIds

}
?>
