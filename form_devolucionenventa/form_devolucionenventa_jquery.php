
function scJQGeneralAdd() {
  scLoadScInput('input:text.sc-js-input');
  scLoadScInput('input:password.sc-js-input');
  scLoadScInput('input:checkbox.sc-js-input');
  scLoadScInput('input:radio.sc-js-input');
  scLoadScInput('select.sc-js-input');
  scLoadScInput('textarea.sc-js-input');

} // scJQGeneralAdd

function scFocusField(sField) {
  var $oField = $('#id_sc_field_' + sField);

  if (0 == $oField.length) {
    $oField = $('input[name=' + sField + ']');
  }

  if (0 == $oField.length && document.F1.elements[sField]) {
    $oField = $(document.F1.elements[sField]);
  }

  if ($("#id_ac_" + sField).length > 0) {
    if ($oField.hasClass("select2-hidden-accessible")) {
      if (false == scSetFocusOnField($oField)) {
        setTimeout(function() { scSetFocusOnField($oField); }, 500);
      }
    }
    else {
      if (false == scSetFocusOnField($oField)) {
        if (false == scSetFocusOnField($("#id_ac_" + sField))) {
          setTimeout(function() { scSetFocusOnField($("#id_ac_" + sField)); }, 500);
        }
      }
      else {
        setTimeout(function() { scSetFocusOnField($oField); }, 500);
      }
    }
  }
  else {
    setTimeout(function() { scSetFocusOnField($oField); }, 500);
  }
} // scFocusField

function scSetFocusOnField($oField) {
  if ($oField.length > 0 && $oField[0].offsetHeight > 0 && $oField[0].offsetWidth > 0 && !$oField[0].disabled) {
    $oField[0].focus();
    return true;
  }
  return false;
} // scSetFocusOnField

function scEventControl_init(iSeqRow) {
  scEventControl_data["numerodev" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["fecha" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["resolucion" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["numfacven" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["costopro" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["fechafactura" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["vdesc" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["vparc" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["viva" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["vunit" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["observa" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["actualiza" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
  scEventControl_data["detalle" + iSeqRow] = {"blur": false, "change": false, "autocomp": false, "original": "", "calculated": ""};
}

function scEventControl_active(iSeqRow) {
  if (scEventControl_data["numerodev" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["numerodev" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["fecha" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["fecha" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["resolucion" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["resolucion" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["numfacven" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["numfacven" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["costopro" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["costopro" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["fechafactura" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["fechafactura" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["vdesc" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["vdesc" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["vparc" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["vparc" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["viva" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["viva" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["vunit" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["vunit" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["observa" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["observa" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["actualiza" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["actualiza" + iSeqRow]["change"]) {
    return true;
  }
  if (scEventControl_data["detalle" + iSeqRow]["blur"]) {
    return true;
  }
  if (scEventControl_data["detalle" + iSeqRow]["change"]) {
    return true;
  }
  return false;
} // scEventControl_active

function scEventControl_onFocus(oField, iSeq) {
  var fieldId, fieldName;
  fieldId = $(oField).attr("id");
  fieldName = fieldId.substr(12);
  scEventControl_data[fieldName]["blur"] = true;
  if ("resolucion" + iSeq == fieldName) {
    scEventControl_data[fieldName]["blur"] = false;
  }
  if ("numfacven" + iSeq == fieldName) {
    scEventControl_data[fieldName]["blur"] = false;
  }
  if ("idpro" + iSeq == fieldName) {
    scEventControl_data[fieldName]["blur"] = false;
  }
  if ("idpro" + iSeq == fieldName) {
    scEventControl_data[fieldName]["change"]   = true;
    scEventControl_data[fieldName]["original"] = $(oField).val();
    scEventControl_data[fieldName]["calculated"] = $(oField).val();
    return;
  }
  if ("numfacven" + iSeq == fieldName) {
    scEventControl_data[fieldName]["change"]   = true;
    scEventControl_data[fieldName]["original"] = $(oField).val();
    scEventControl_data[fieldName]["calculated"] = $(oField).val();
    return;
  }
  if ("resolucion" + iSeq == fieldName) {
    scEventControl_data[fieldName]["change"]   = true;
    scEventControl_data[fieldName]["original"] = $(oField).val();
    scEventControl_data[fieldName]["calculated"] = $(oField).val();
    return;
  }
  scEventControl_data[fieldName]["change"] = false;
} // scEventControl_onFocus

function scEventControl_onBlur(sFieldName) {
  scEventControl_data[sFieldName]["blur"] = false;
  if (scEventControl_data[sFieldName]["change"]) {
        if (scEventControl_data[sFieldName]["original"] == $("#id_sc_field_" + sFieldName).val() || scEventControl_data[sFieldName]["calculated"] == $("#id_sc_field_" + sFieldName).val()) {
          scEventControl_data[sFieldName]["change"] = false;
        }
  }
} // scEventControl_onBlur

function scEventControl_onChange(sFieldName) {
  scEventControl_data[sFieldName]["change"] = false;
} // scEventControl_onChange

function scEventControl_onAutocomp(sFieldName) {
  scEventControl_data[sFieldName]["autocomp"] = false;
} // scEventControl_onChange

var scEventControl_data = {};

function scJQEventsAdd(iSeqRow) {
  $('#id_sc_field_iddevol' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_iddevol_onchange(this, iSeqRow) });
  $('#id_sc_field_numerodev' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_numerodev_onblur(this, iSeqRow) })
                                       .bind('change', function() { sc_form_devolucionenventa_numerodev_onchange(this, iSeqRow) })
                                       .bind('focus', function() { sc_form_devolucionenventa_numerodev_onfocus(this, iSeqRow) });
  $('#id_sc_field_fecha' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_fecha_onblur(this, iSeqRow) })
                                   .bind('change', function() { sc_form_devolucionenventa_fecha_onchange(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_devolucionenventa_fecha_onfocus(this, iSeqRow) });
  $('#id_sc_field_numfacven' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_numfacven_onblur(this, iSeqRow) })
                                       .bind('change', function() { sc_form_devolucionenventa_numfacven_onchange(this, iSeqRow) })
                                       .bind('focus', function() { sc_form_devolucionenventa_numfacven_onfocus(this, iSeqRow) });
  $('#id_sc_field_idinven' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_idinven_onchange(this, iSeqRow) });
  $('#id_sc_field_idpro' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_idpro_onchange(this, iSeqRow) });
  $('#id_sc_field_cantidad' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_cantidad_onchange(this, iSeqRow) });
  $('#id_sc_field_unimay' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_unimay_onchange(this, iSeqRow) });
  $('#id_sc_field_fac' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_fac_onchange(this, iSeqRow) });
  $('#id_sc_field_vunit' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_vunit_onblur(this, iSeqRow) })
                                   .bind('change', function() { sc_form_devolucionenventa_vunit_onchange(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_devolucionenventa_vunit_onfocus(this, iSeqRow) });
  $('#id_sc_field_vparc' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_vparc_onblur(this, iSeqRow) })
                                   .bind('change', function() { sc_form_devolucionenventa_vparc_onchange(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_devolucionenventa_vparc_onfocus(this, iSeqRow) });
  $('#id_sc_field_viva' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_viva_onblur(this, iSeqRow) })
                                  .bind('change', function() { sc_form_devolucionenventa_viva_onchange(this, iSeqRow) })
                                  .bind('focus', function() { sc_form_devolucionenventa_viva_onfocus(this, iSeqRow) });
  $('#id_sc_field_vdesc' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_vdesc_onblur(this, iSeqRow) })
                                   .bind('change', function() { sc_form_devolucionenventa_vdesc_onchange(this, iSeqRow) })
                                   .bind('focus', function() { sc_form_devolucionenventa_vdesc_onfocus(this, iSeqRow) });
  $('#id_sc_field_idbod' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_idbod_onchange(this, iSeqRow) });
  $('#id_sc_field_costopro' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_costopro_onblur(this, iSeqRow) })
                                      .bind('change', function() { sc_form_devolucionenventa_costopro_onchange(this, iSeqRow) })
                                      .bind('click', function() { sc_form_devolucionenventa_costopro_onclick(this, iSeqRow) })
                                      .bind('focus', function() { sc_form_devolucionenventa_costopro_onfocus(this, iSeqRow) });
  $('#id_sc_field_observa' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_observa_onblur(this, iSeqRow) })
                                     .bind('change', function() { sc_form_devolucionenventa_observa_onchange(this, iSeqRow) })
                                     .bind('focus', function() { sc_form_devolucionenventa_observa_onfocus(this, iSeqRow) });
  $('#id_sc_field_resolucion' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_resolucion_onblur(this, iSeqRow) })
                                        .bind('change', function() { sc_form_devolucionenventa_resolucion_onchange(this, iSeqRow) })
                                        .bind('focus', function() { sc_form_devolucionenventa_resolucion_onfocus(this, iSeqRow) });
  $('#id_sc_field_fechafactura' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_fechafactura_onblur(this, iSeqRow) })
                                          .bind('change', function() { sc_form_devolucionenventa_fechafactura_onchange(this, iSeqRow) })
                                          .bind('focus', function() { sc_form_devolucionenventa_fechafactura_onfocus(this, iSeqRow) });
  $('#id_sc_field_canres' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_canres_onchange(this, iSeqRow) });
  $('#id_sc_field_iddetalle' + iSeqRow).bind('change', function() { sc_form_devolucionenventa_iddetalle_onchange(this, iSeqRow) });
  $('#id_sc_field_detalle' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_detalle_onblur(this, iSeqRow) })
                                     .bind('change', function() { sc_form_devolucionenventa_detalle_onchange(this, iSeqRow) })
                                     .bind('focus', function() { sc_form_devolucionenventa_detalle_onfocus(this, iSeqRow) });
  $('#id_sc_field_actualiza' + iSeqRow).bind('blur', function() { sc_form_devolucionenventa_actualiza_onblur(this, iSeqRow) })
                                       .bind('change', function() { sc_form_devolucionenventa_actualiza_onchange(this, iSeqRow) })
                                       .bind('click', function() { sc_form_devolucionenventa_actualiza_onclick(this, iSeqRow) })
                                       .bind('focus', function() { sc_form_devolucionenventa_actualiza_onfocus(this, iSeqRow) });
  $('.sc-ui-checkbox-actualiza' + iSeqRow).on('click', function() { scMarkFormAsChanged(); });
} // scJQEventsAdd

function sc_form_devolucionenventa_iddevol_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_numerodev_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_numerodev();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_numerodev_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_numerodev_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_fecha_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_fecha();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_fecha_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_fecha_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_numfacven_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_numfacven();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_numfacven_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
  do_ajax_form_devolucionenventa_event_numfacven_onchange();
}

function sc_form_devolucionenventa_numfacven_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_idinven_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_idpro_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_cantidad_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_unimay_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_fac_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_vunit_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_vunit();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_vunit_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_vunit_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_vparc_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_vparc();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_vparc_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_vparc_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_viva_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_viva();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_viva_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_viva_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_vdesc_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_vdesc();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_vdesc_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_vdesc_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_idbod_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_costopro_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_costopro();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_costopro_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_costopro_onclick(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_event_costopro_onclick();
}

function sc_form_devolucionenventa_costopro_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_observa_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_observa();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_observa_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_observa_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_resolucion_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_resolucion();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_resolucion_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
  do_ajax_form_devolucionenventa_event_resolucion_onchange();
}

function sc_form_devolucionenventa_resolucion_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_fechafactura_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_fechafactura();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_fechafactura_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_fechafactura_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_canres_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_iddetalle_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_detalle_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_detalle();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_detalle_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_detalle_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function sc_form_devolucionenventa_actualiza_onblur(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_validate_actualiza();
  scCssBlur(oThis);
}

function sc_form_devolucionenventa_actualiza_onchange(oThis, iSeqRow) {
  scMarkFormAsChanged();
}

function sc_form_devolucionenventa_actualiza_onclick(oThis, iSeqRow) {
  do_ajax_form_devolucionenventa_event_actualiza_onclick();
}

function sc_form_devolucionenventa_actualiza_onfocus(oThis, iSeqRow) {
  scEventControl_onFocus(oThis, iSeqRow);
  scCssFocus(oThis);
}

function displayChange_block(block, status) {
	if ("0" == block) {
		displayChange_block_0(status);
	}
	if ("1" == block) {
		displayChange_block_1(status);
	}
	if ("2" == block) {
		displayChange_block_2(status);
	}
}

function displayChange_block_0(status) {
	displayChange_field("numerodev", "", status);
	displayChange_field("fecha", "", status);
	displayChange_field("resolucion", "", status);
	displayChange_field("numfacven", "", status);
	displayChange_field("costopro", "", status);
	displayChange_field("fechafactura", "", status);
}

function displayChange_block_1(status) {
	displayChange_field("vdesc", "", status);
	displayChange_field("vparc", "", status);
	displayChange_field("viva", "", status);
	displayChange_field("vunit", "", status);
	displayChange_field("observa", "", status);
	displayChange_field("actualiza", "", status);
}

function displayChange_block_2(status) {
	displayChange_field("detalle", "", status);
}

function displayChange_row(row, status) {
	displayChange_field_numerodev(row, status);
	displayChange_field_fecha(row, status);
	displayChange_field_resolucion(row, status);
	displayChange_field_numfacven(row, status);
	displayChange_field_costopro(row, status);
	displayChange_field_fechafactura(row, status);
	displayChange_field_vdesc(row, status);
	displayChange_field_vparc(row, status);
	displayChange_field_viva(row, status);
	displayChange_field_vunit(row, status);
	displayChange_field_observa(row, status);
	displayChange_field_actualiza(row, status);
	displayChange_field_detalle(row, status);
}

function displayChange_field(field, row, status) {
	if ("numerodev" == field) {
		displayChange_field_numerodev(row, status);
	}
	if ("fecha" == field) {
		displayChange_field_fecha(row, status);
	}
	if ("resolucion" == field) {
		displayChange_field_resolucion(row, status);
	}
	if ("numfacven" == field) {
		displayChange_field_numfacven(row, status);
	}
	if ("costopro" == field) {
		displayChange_field_costopro(row, status);
	}
	if ("fechafactura" == field) {
		displayChange_field_fechafactura(row, status);
	}
	if ("vdesc" == field) {
		displayChange_field_vdesc(row, status);
	}
	if ("vparc" == field) {
		displayChange_field_vparc(row, status);
	}
	if ("viva" == field) {
		displayChange_field_viva(row, status);
	}
	if ("vunit" == field) {
		displayChange_field_vunit(row, status);
	}
	if ("observa" == field) {
		displayChange_field_observa(row, status);
	}
	if ("actualiza" == field) {
		displayChange_field_actualiza(row, status);
	}
	if ("detalle" == field) {
		displayChange_field_detalle(row, status);
	}
}

function displayChange_field_numerodev(row, status) {
}

function displayChange_field_fecha(row, status) {
}

function displayChange_field_resolucion(row, status) {
	if ("on" == status) {
		if ("all" == row) {
			var fieldList = $(".css_resolucion__obj");
			for (var i = 0; i < fieldList.length; i++) {
				$($(fieldList[i]).attr("id")).select2("destroy");
			}
		}
		else {
			$("#id_sc_field_resolucion" + row).select2("destroy");
		}
		scJQSelect2Add(row, "resolucion");
	}
}

function displayChange_field_numfacven(row, status) {
	if ("on" == status) {
		if ("all" == row) {
			var fieldList = $(".css_numfacven__obj");
			for (var i = 0; i < fieldList.length; i++) {
				$($(fieldList[i]).attr("id")).select2("destroy");
			}
		}
		else {
			$("#id_sc_field_numfacven" + row).select2("destroy");
		}
		scJQSelect2Add(row, "numfacven");
	}
}

function displayChange_field_costopro(row, status) {
}

function displayChange_field_fechafactura(row, status) {
}

function displayChange_field_vdesc(row, status) {
}

function displayChange_field_vparc(row, status) {
}

function displayChange_field_viva(row, status) {
}

function displayChange_field_vunit(row, status) {
}

function displayChange_field_observa(row, status) {
}

function displayChange_field_actualiza(row, status) {
}

function displayChange_field_detalle(row, status) {
	if ("on" == status && typeof $("#nmsc_iframe_liga_grid_corregir_devoluciones")[0].contentWindow.scRecreateSelect2 === "function") {
		$("#nmsc_iframe_liga_grid_corregir_devoluciones")[0].contentWindow.scRecreateSelect2();
	}
}

function scRecreateSelect2() {
	displayChange_field_resolucion("all", "on");
	displayChange_field_numfacven("all", "on");
}
function scResetPagesDisplay() {
	$(".sc-form-page").show();
}

function scHidePage(pageNo) {
	$("#id_form_devolucionenventa_form" + pageNo).hide();
}

function scCheckNoPageSelected() {
	if (!$(".sc-form-page").filter(".scTabActive").filter(":visible").length) {
		var inactiveTabs = $(".sc-form-page").filter(".scTabInactive").filter(":visible");
		if (inactiveTabs.length) {
			var tabNo = $(inactiveTabs[0]).attr("id").substr(30);
		}
	}
}
function scJQUploadAdd(iSeqRow) {
} // scJQUploadAdd

var api_cache_requests = [];
function ajax_check_file(img_name, field  ,t, p, p_cache, iSeqRow, hasRun, img_before){
    setTimeout(function(){
        if(img_name == '') return;
        iSeqRow= iSeqRow !== undefined && iSeqRow !== null ? iSeqRow : '';
        var hasVar = p.indexOf('_@NM@_') > -1 || p_cache.indexOf('_@NM@_') > -1 ? true : false;

        p = p.split('_@NM@_');
        $.each(p, function(i,v){
            try{
                p[i] = $('[name='+v+iSeqRow+']').val();
            }
            catch(err){
                p[i] = v;
            }
        });
        p = p.join('');

        p_cache = p_cache.split('_@NM@_');
        $.each(p_cache, function(i,v){
            try{
                p_cache[i] = $('[name='+v+iSeqRow+']').val();
            }
            catch(err){
                p_cache[i] = v;
            }
        });
        p_cache = p_cache.join('');

        img_before = img_before !== undefined ? img_before : $(t).attr('src');
        var str_key_cache = '<?php echo $this->Ini->sc_page; ?>' + img_name+field+p+p_cache;
        if(api_cache_requests[ str_key_cache ] !== undefined && api_cache_requests[ str_key_cache ] !== null){
            if(api_cache_requests[ str_key_cache ] != false){
                do_ajax_check_file(api_cache_requests[ str_key_cache ], field  ,t, iSeqRow);
            }
            return;
        }
        //scAjaxProcOn();
        $(t).attr('src', '<?php echo $this->Ini->path_icones ?>/scriptcase__NM__ajax_load.gif');
        api_cache_requests[ str_key_cache ] = false;
        var rs =$.ajax({
                    type: "POST",
                    url: 'index.php?script_case_init=<?php echo $this->Ini->sc_page; ?>',
                    async: true,
                    data:'nmgp_opcao=ajax_check_file&AjaxCheckImg=' + encodeURI(img_name) +'&rsargs='+ field + '&p=' + p + '&p_cache=' + p_cache,
                    success: function (rs) {
                        if(rs.indexOf('</span>') != -1){
                            rs = rs.substr(rs.indexOf('</span>') + 7);
                        }
                        if(rs.indexOf('/') != -1 && rs.indexOf('/') != 0){
                            rs = rs.substr(rs.indexOf('/'));
                        }
                        rs = sc_trim(rs);

                        // if(rs == 0 && hasVar && hasRun === undefined){
                        //     delete window.api_cache_requests[ str_key_cache ];
                        //     ajax_check_file(img_name, field  ,t, p, p_cache, iSeqRow, 1, img_before);
                        //     return;
                        // }
                        window.api_cache_requests[ str_key_cache ] = rs;
                        do_ajax_check_file(rs, field  ,t, iSeqRow)
                        if(rs == 0){
                            delete window.api_cache_requests[ str_key_cache ];

                           // $(t).attr('src',img_before);
                            do_ajax_check_file(img_before+'_@@NM@@_' + img_before, field  ,t, iSeqRow)

                        }


                    }
        });
    },100);
}

function do_ajax_check_file(rs, field  ,t, iSeqRow){
    if (rs != 0) {
        rs_split = rs.split('_@@NM@@_');
        rs_orig = rs_split[0];
        rs2 = rs_split[1];
        try{
            if(!$(t).is('img')){

                if($('#id_read_on_'+field+iSeqRow).length > 0 ){
                                    var usa_read_only = false;

                switch(field){

                }
                     if(usa_read_only && $('a',$('#id_read_on_'+field+iSeqRow)).length == 0){
                         $(t).html("<a href=\"javascript:nm_mostra_doc('0', '"+rs2+"', 'form_devolucionenventa')\">"+$('#id_read_on_'+field+iSeqRow).text()+"</a>");
                     }
                }
                if($('#id_ajax_doc_'+field+iSeqRow+' a').length > 0){
                    var target = $('#id_ajax_doc_'+field+iSeqRow+' a').attr('href').split(',');
                    target[1] = "'"+rs2+"'";
                    $('#id_ajax_doc_'+field+iSeqRow+' a').attr('href', target.join(','));
                }else{
                    var target = $(t).attr('href').split(',');
                     target[1] = "'"+rs2+"'";
                     $(t).attr('href', target.join(','));
                }
            }else{
                $(t).attr('src', rs2);
                $(t).css('display', '');
                if($('#id_ajax_doc_'+field+iSeqRow+' a').length > 0){
                    var target = $('#id_ajax_doc_'+field+iSeqRow+' a').attr('href').split(',');
                    target[1] = "'"+rs2+"'";
                    $(t).attr('href', target.join(','));
                }else{
                     var t_link = $(t).parent('a');
                     var target = $(t_link).attr('href').split(',');
                     target[0] = "javascript:nm_mostra_img('"+rs_orig+"'";
                     $(t_link).attr('href', target.join(','));
                }

            }
            eval("window.var_ajax_img_"+field+iSeqRow+" = '"+rs_orig+"';");

        } catch(err){
                        eval("window.var_ajax_img_"+field+iSeqRow+" = '"+rs_orig+"';");

        }
    }
   /* hasFalseCacheRequest = false;
    $.each(api_cache_requests, function(i,v){
        if(v == false){
            hasFalseCacheRequest = true;
        }
    });
    if(hasFalseCacheRequest == false){
        scAjaxProcOff();
    }*/
}

$(document).ready(function(){
});function scJQPasswordToggleAdd(seqRow) {
  $(".sc-ui-pwd-toggle-icon" + seqRow).on("click", function() {
    var fieldName = $(this).attr("id").substr(17), fieldObj = $("#id_sc_field_" + fieldName), fieldFA = $("#id_pwd_fa_" + fieldName);
    if ("text" == fieldObj.attr("type")) {
      fieldObj.attr("type", "password");
      fieldFA.attr("class", "fa fa-eye sc-ui-pwd-eye");
    } else {
      fieldObj.attr("type", "text");
      fieldFA.attr("class", "fa fa-eye-slash sc-ui-pwd-eye");
    }
  });
} // scJQPasswordToggleAdd

function scJQSelect2Add(seqRow, specificField) {
  if (null == specificField || "resolucion" == specificField) {
    scJQSelect2Add_resolucion(seqRow);
  }
  if (null == specificField || "numfacven" == specificField) {
    scJQSelect2Add_numfacven(seqRow);
  }
  if (null == specificField || "idpro" == specificField) {
    scJQSelect2Add_idpro(seqRow);
  }
} // scJQSelect2Add

function scJQSelect2Add_resolucion(seqRow) {
  var elemSelector = "all" == seqRow ? ".css_resolucion_obj" : "#id_sc_field_resolucion" + seqRow;
  $(elemSelector).select2(
    {
      containerCssClass: 'css_resolucion_obj',
      dropdownCssClass: 'css_resolucion_obj',
      language: {
        noResults: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_notfound'] ?>";
        },
        searching: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_searching'] ?>";
        }
      }
    }
  );
} // scJQSelect2Add

function scJQSelect2Add_numfacven(seqRow) {
  var elemSelector = "all" == seqRow ? ".css_numfacven_obj" : "#id_sc_field_numfacven" + seqRow;
  $(elemSelector).select2(
    {
      containerCssClass: 'css_numfacven_obj',
      dropdownCssClass: 'css_numfacven_obj',
      language: {
        noResults: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_notfound'] ?>";
        },
        searching: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_searching'] ?>";
        }
      }
    }
  );
} // scJQSelect2Add

function scJQSelect2Add_idpro(seqRow) {
  var elemSelector = "all" == seqRow ? ".css_idpro_obj" : "#id_sc_field_idpro" + seqRow;
  $(elemSelector).select2(
    {
      containerCssClass: 'css_idpro_obj',
      dropdownCssClass: 'css_idpro_obj',
      language: {
        noResults: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_notfound'] ?>";
        },
        searching: function() {
          return "<?php echo $this->Ini->Nm_lang['lang_autocomp_searching'] ?>";
        }
      }
    }
  );
} // scJQSelect2Add


function scJQElementsAdd(iLine) {
  scJQEventsAdd(iLine);
  scEventControl_init(iLine);
  scJQUploadAdd(iLine);
  scJQPasswordToggleAdd(iLine);
  scJQSelect2Add(iLine);
  setTimeout(function () { if ('function' == typeof displayChange_field_resolucion) { displayChange_field_resolucion(iLine, "on"); } }, 150);
  setTimeout(function () { if ('function' == typeof displayChange_field_numfacven) { displayChange_field_numfacven(iLine, "on"); } }, 150);
  setTimeout(function () { if ('function' == typeof displayChange_field_idpro) { displayChange_field_idpro(iLine, "on"); } }, 150);
} // scJQElementsAdd

function scGetFileExtension(fileName)
{
    fileNameParts = fileName.split(".");

    if (1 === fileNameParts.length || (2 === fileNameParts.length && "" == fileNameParts[0])) {
        return "";
    }

    return fileNameParts.pop().toLowerCase();
}

function scFormatExtensionSizeErrorMsg(errorMsg)
{
    var msgInfo = errorMsg.split("||"), returnMsg = "";

    if ("err_size" == msgInfo[0]) {
        returnMsg = "<?php echo $this->Ini->Nm_lang['lang_errm_file_size'] ?>. <?php echo $this->Ini->Nm_lang['lang_errm_file_size_extension'] ?>".replace("{SC_EXTENSION}", msgInfo[1]).replace("{SC_LIMIT}", msgInfo[2]);
    } else if ("err_extension" == msgInfo[0]) {
        returnMsg = "<?php echo $this->Ini->Nm_lang['lang_errm_file_invl'] ?>";
    }

    return returnMsg;
}

