<?php

class pdfreport_recibocaja_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function __construct()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      if ($this->Ini->sc_export_ajax)
      {
          $this->Arr_result['file_export']  = NM_charset_to_utf8($this->Rtf_f);
          $this->Arr_result['title_export'] = NM_charset_to_utf8($this->Tit_doc);
          $Temp = ob_get_clean();
          if ($Temp !== false && trim($Temp) != "")
          {
              $this->Arr_result['htmOutput'] = NM_charset_to_utf8($Temp);
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($this->Arr_result);
          exit;
      }
      else
      {
          $this->progress_bar_end();
      }
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      if (isset($GLOBALS['nmgp_parms']) && !empty($GLOBALS['nmgp_parms'])) 
      { 
          $GLOBALS['nmgp_parms'] = str_replace("@aspass@", "'", $GLOBALS['nmgp_parms']);
          $todox = str_replace("?#?@?@?", "?#?@ ?@?", $GLOBALS["nmgp_parms"]);
          $todo  = explode("?@?", $todox);
          foreach ($todo as $param)
          {
               $cadapar = explode("?#?", $param);
               if (1 < sizeof($cadapar))
               {
                   if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                   {
                       $cadapar[0] = substr($cadapar[0], 11);
                       $cadapar[1] = $_SESSION[$cadapar[1]];
                   }
                   if (isset($GLOBALS['sc_conv_var'][$cadapar[0]]))
                   {
                       $cadapar[0] = $GLOBALS['sc_conv_var'][$cadapar[0]];
                   }
                   elseif (isset($GLOBALS['sc_conv_var'][strtolower($cadapar[0])]))
                   {
                       $cadapar[0] = $GLOBALS['sc_conv_var'][strtolower($cadapar[0])];
                   }
                   nm_limpa_str_pdfreport_recibocaja($cadapar[1]);
                   nm_protect_num_pdfreport_recibocaja($cadapar[0], $cadapar[1]);
                   if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                   $Tmp_par   = $cadapar[0];
                   $$Tmp_par = $cadapar[1];
                   if ($Tmp_par == "nmgp_opcao")
                   {
                       $_SESSION['sc_session'][$script_case_init]['pdfreport_recibocaja']['opcao'] = $cadapar[1];
                   }
               }
          }
      }
      if (isset($par_numero)) 
      {
          $_SESSION['par_numero'] = $par_numero;
          nm_limpa_str_pdfreport_recibocaja($_SESSION["par_numero"]);
      }
      if (isset($empresa)) 
      {
          $_SESSION['empresa'] = $empresa;
          nm_limpa_str_pdfreport_recibocaja($_SESSION["empresa"]);
      }
      if (isset($nit)) 
      {
          $_SESSION['nit'] = $nit;
          nm_limpa_str_pdfreport_recibocaja($_SESSION["nit"]);
      }
      if (isset($direccion)) 
      {
          $_SESSION['direccion'] = $direccion;
          nm_limpa_str_pdfreport_recibocaja($_SESSION["direccion"]);
      }
      if (isset($tele)) 
      {
          $_SESSION['tele'] = $tele;
          nm_limpa_str_pdfreport_recibocaja($_SESSION["tele"]);
      }
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      require_once($this->Ini->path_aplicacao . "pdfreport_recibocaja_total.class.php"); 
      $this->Tot      = new pdfreport_recibocaja_total($this->Ini->sc_page);
      $this->prep_modulos("Tot");
      $Gb_geral = "quebra_geral_" . $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['SC_Ind_Groupby'];
      if (method_exists($this->Tot,$Gb_geral))
      {
          $this->Tot->$Gb_geral();
          $this->count_ger = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['tot_geral'][1];
      }
      if (!$this->Ini->sc_export_ajax) {
          require_once($this->Ini->path_lib_php . "/sc_progress_bar.php");
          $this->pb = new scProgressBar();
          $this->pb->setRoot($this->Ini->root);
          $this->pb->setDir($_SESSION['scriptcase']['pdfreport_recibocaja']['glo_nm_path_imag_temp'] . "/");
          $this->pb->setProgressbarMd5($_GET['pbmd5']);
          $this->pb->initialize();
          $this->pb->setReturnUrl("./");
          $this->pb->setReturnOption('volta_grid');
          $this->pb->setTotalSteps($this->count_ger);
      }
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_pdfreport_recibocaja";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "pdfreport_recibocaja.rtf";
   }
   //---- 
   function prep_modulos($modulo)
   {
      $this->$modulo->Ini    = $this->Ini;
      $this->$modulo->Db     = $this->Db;
      $this->$modulo->Erro   = $this->Erro;
      $this->$modulo->Lookup = $this->Lookup;
   }


   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->obser = $Busca_temp['obser']; 
          $tmp_pos = strpos($this->obser, "##@@");
          if ($tmp_pos !== false && !is_array($this->obser))
          {
              $this->obser = substr($this->obser, 0, $tmp_pos);
          }
          $this->idrecibo = $Busca_temp['idrecibo']; 
          $tmp_pos = strpos($this->idrecibo, "##@@");
          if ($tmp_pos !== false && !is_array($this->idrecibo))
          {
              $this->idrecibo = substr($this->idrecibo, 0, $tmp_pos);
          }
          $this->idrecibo_2 = $Busca_temp['idrecibo_input_2']; 
          $this->nurecibo = $Busca_temp['nurecibo']; 
          $tmp_pos = strpos($this->nurecibo, "##@@");
          if ($tmp_pos !== false && !is_array($this->nurecibo))
          {
              $this->nurecibo = substr($this->nurecibo, 0, $tmp_pos);
          }
          $this->nurecibo_2 = $Busca_temp['nurecibo_input_2']; 
          $this->nufac = $Busca_temp['nufac']; 
          $tmp_pos = strpos($this->nufac, "##@@");
          if ($tmp_pos !== false && !is_array($this->nufac))
          {
              $this->nufac = substr($this->nufac, 0, $tmp_pos);
          }
          $this->nufac_2 = $Busca_temp['nufac_input_2']; 
          $this->cliente = $Busca_temp['cliente']; 
          $tmp_pos = strpos($this->cliente, "##@@");
          if ($tmp_pos !== false && !is_array($this->cliente))
          {
              $this->cliente = substr($this->cliente, 0, $tmp_pos);
          }
          $this->cliente_2 = $Busca_temp['cliente_input_2']; 
      } 
      $this->nm_where_dinamico = "";
      $_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'on';
if (!isset($_SESSION['tele'])) {$_SESSION['tele'] = "";}
if (!isset($this->sc_temp_tele)) {$this->sc_temp_tele = (isset($_SESSION['tele'])) ? $_SESSION['tele'] : "";}
if (!isset($_SESSION['direccion'])) {$_SESSION['direccion'] = "";}
if (!isset($this->sc_temp_direccion)) {$this->sc_temp_direccion = (isset($_SESSION['direccion'])) ? $_SESSION['direccion'] : "";}
if (!isset($_SESSION['nit'])) {$_SESSION['nit'] = "";}
if (!isset($this->sc_temp_nit)) {$this->sc_temp_nit = (isset($_SESSION['nit'])) ? $_SESSION['nit'] : "";}
if (!isset($_SESSION['empresa'])) {$_SESSION['empresa'] = "";}
if (!isset($this->sc_temp_empresa)) {$this->sc_temp_empresa = (isset($_SESSION['empresa'])) ? $_SESSION['empresa'] : "";}
  $_SESSION['pdfreport_recibocaja']['empresa_nombre']=$this->sc_temp_empresa;
$_SESSION['pdfreport_recibocaja']['nit']=$this->sc_temp_nit;
$_SESSION['pdfreport_recibocaja']['direccion1']=$this->sc_temp_direccion;
$_SESSION['pdfreport_recibocaja']['telefono1']=$this->sc_temp_tele;
if (isset($this->sc_temp_empresa)) {$_SESSION['empresa'] = $this->sc_temp_empresa;}
if (isset($this->sc_temp_nit)) {$_SESSION['nit'] = $this->sc_temp_nit;}
if (isset($this->sc_temp_direccion)) {$_SESSION['direccion'] = $this->sc_temp_direccion;}
if (isset($this->sc_temp_tele)) {$_SESSION['tele'] = $this->sc_temp_tele;}
$_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'off'; 
      if  (!empty($this->nm_where_dinamico)) 
      {   
          $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_pesq'] .= $this->nm_where_dinamico;
      }   
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name']))
      {
          $Pos = strrpos($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name'], ".");
          if ($Pos === false) {
              $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name'] .= ".rtf";
          }
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_name']);
      }
      $this->arr_export = array('label' => array(), 'lines' => array());
      $this->arr_span   = array();

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['idrecibo'])) ? $this->New_label['idrecibo'] : "Idrecibo"; 
          if ($Cada_col == "idrecibo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nurecibo'])) ? $this->New_label['nurecibo'] : "Nurecibo"; 
          if ($Cada_col == "nurecibo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nufac'])) ? $this->New_label['nufac'] : "Nufac"; 
          if ($Cada_col == "nufac" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['cliente'])) ? $this->New_label['cliente'] : "Cliente"; 
          if ($Cada_col == "cliente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecharecibo'])) ? $this->New_label['fecharecibo'] : "Fecharecibo"; 
          if ($Cada_col == "fecharecibo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['monto'])) ? $this->New_label['monto'] : "Monto"; 
          if ($Cada_col == "monto" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['son'])) ? $this->New_label['son'] : "Son"; 
          if ($Cada_col == "son" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['saldofac'])) ? $this->New_label['saldofac'] : "Saldofac"; 
          if ($Cada_col == "saldofac" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['formapago'])) ? $this->New_label['formapago'] : "Formapago"; 
          if ($Cada_col == "formapago" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['numcheque'])) ? $this->New_label['numcheque'] : "Numcheque"; 
          if ($Cada_col == "numcheque" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['banco'])) ? $this->New_label['banco'] : "Banco"; 
          if ($Cada_col == "banco" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['numtarjeta'])) ? $this->New_label['numtarjeta'] : "Numtarjeta"; 
          if ($Cada_col == "numtarjeta" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nombreobanco'])) ? $this->New_label['nombreobanco'] : "Nombreobanco"; 
          if ($Cada_col == "nombreobanco" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['obser'])) ? $this->New_label['obser'] : "Obser"; 
          if ($Cada_col == "obser" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['concepto'])) ? $this->New_label['concepto'] : "Concepto"; 
          if ($Cada_col == "concepto" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['resolucion'])) ? $this->New_label['resolucion'] : "Resolucion"; 
          if ($Cada_col == "resolucion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['rete'])) ? $this->New_label['rete'] : "Rete"; 
          if ($Cada_col == "rete" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['descu'])) ? $this->New_label['descu'] : "Descu"; 
          if ($Cada_col == "descu" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['porc_rete'])) ? $this->New_label['porc_rete'] : "Porc Rete"; 
          if ($Cada_col == "porc_rete" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['val_ica'])) ? $this->New_label['val_ica'] : "Val Ica"; 
          if ($Cada_col == "val_ica" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['por_ica'])) ? $this->New_label['por_ica'] : "Por Ica"; 
          if ($Cada_col == "por_ica" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['por_retiva'])) ? $this->New_label['por_retiva'] : "Por Retiva"; 
          if ($Cada_col == "por_retiva" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['val_retiva'])) ? $this->New_label['val_retiva'] : "Val Retiva"; 
          if ($Cada_col == "val_retiva" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos'])) ? $this->New_label['detallepagos'] : "detallepagos"; 
          if ($Cada_col == "detallepagos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_banco'])) ? $this->New_label['detallepagos_banco'] : "Banco"; 
          if ($Cada_col == "detallepagos_banco" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_fechacob'])) ? $this->New_label['detallepagos_fechacob'] : "Fechacob"; 
          if ($Cada_col == "detallepagos_fechacob" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_iddetall'])) ? $this->New_label['detallepagos_iddetall'] : "Iddetall"; 
          if ($Cada_col == "detallepagos_iddetall" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_idfact'])) ? $this->New_label['detallepagos_idfact'] : "Idfact"; 
          if ($Cada_col == "detallepagos_idfact" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_idfp'])) ? $this->New_label['detallepagos_idfp'] : "Idfp"; 
          if ($Cada_col == "detallepagos_idfp" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_idrc'])) ? $this->New_label['detallepagos_idrc'] : "Idrc"; 
          if ($Cada_col == "detallepagos_idrc" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_monto'])) ? $this->New_label['detallepagos_monto'] : "Monto"; 
          if ($Cada_col == "detallepagos_monto" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['detallepagos_numcheque'])) ? $this->New_label['detallepagos_numcheque'] : "Numcheque"; 
          if ($Cada_col == "detallepagos_numcheque" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $SC_Label = NM_charset_to_utf8($SC_Label);
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $nmgp_select_count = "SELECT count(*) AS countTest from " . $this->Ini->nm_tabela; 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, str_replace (convert(char(10),fecharecibo,102), '.', '-') + ' ' + convert(char(8),fecharecibo,20), monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, fecharecibo, monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, convert(char(23),fecharecibo,121), monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, fecharecibo, monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, EXTEND(fecharecibo, YEAR TO DAY), monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT idrecibo, nurecibo, nufac, cliente, fecharecibo, monto, son, saldofac, formapago, numcheque, banco, numtarjeta, nombreobanco, obser, concepto, resolucion, rete, descu, porc_rete, val_ica, por_ica, por_retiva, val_retiva from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_pesq'];
      $nmgp_select_count .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select_count;
      $rt = $this->Db->Execute($nmgp_select_count);
      if ($rt === false && !$rt->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }
      $this->count_ger = $rt->fields[0];
      $rt->Close();
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }
      $this->SC_seq_register = 0;
      $PB_tot = (isset($this->count_ger) && $this->count_ger > 0) ? "/" . $this->count_ger : "";
      while (!$rs->EOF)
      {
         $this->SC_seq_register++;
         if (!$this->Ini->sc_export_ajax) {
             $Mens_bar = NM_charset_to_utf8($this->Ini->Nm_lang['lang_othr_prcs']);
             $this->pb->setProgressbarMessage($Mens_bar . ": " . $this->SC_seq_register . $PB_tot);
             $this->pb->addSteps(1);
         }
         $this->Texto_tag .= "<tr>\r\n";
         $this->idrecibo = $rs->fields[0] ;  
         $this->idrecibo = (string)$this->idrecibo;
         $this->nurecibo = $rs->fields[1] ;  
         $this->nurecibo = (string)$this->nurecibo;
         $this->nufac = $rs->fields[2] ;  
         $this->nufac = (string)$this->nufac;
         $this->cliente = $rs->fields[3] ;  
         $this->cliente = (string)$this->cliente;
         $this->fecharecibo = $rs->fields[4] ;  
         $this->monto = $rs->fields[5] ;  
         $this->monto =  str_replace(",", ".", $this->monto);
         $this->monto = (strpos(strtolower($this->monto), "e")) ? (float)$this->monto : $this->monto; 
         $this->monto = (string)$this->monto;
         $this->son = $rs->fields[6] ;  
         $this->saldofac = $rs->fields[7] ;  
         $this->saldofac =  str_replace(",", ".", $this->saldofac);
         $this->saldofac = (strpos(strtolower($this->saldofac), "e")) ? (float)$this->saldofac : $this->saldofac; 
         $this->saldofac = (string)$this->saldofac;
         $this->formapago = $rs->fields[8] ;  
         $this->numcheque = $rs->fields[9] ;  
         $this->banco = $rs->fields[10] ;  
         $this->numtarjeta = $rs->fields[11] ;  
         $this->nombreobanco = $rs->fields[12] ;  
         $this->obser = $rs->fields[13] ;  
         $this->concepto = $rs->fields[14] ;  
         $this->resolucion = $rs->fields[15] ;  
         $this->resolucion = (string)$this->resolucion;
         $this->rete = $rs->fields[16] ;  
         $this->rete =  str_replace(",", ".", $this->rete);
         $this->rete = (strpos(strtolower($this->rete), "e")) ? (float)$this->rete : $this->rete; 
         $this->rete = (string)$this->rete;
         $this->descu = $rs->fields[17] ;  
         $this->descu =  str_replace(",", ".", $this->descu);
         $this->descu = (strpos(strtolower($this->descu), "e")) ? (float)$this->descu : $this->descu; 
         $this->descu = (string)$this->descu;
         $this->porc_rete = $rs->fields[18] ;  
         $this->porc_rete = (strpos(strtolower($this->porc_rete), "e")) ? (float)$this->porc_rete : $this->porc_rete; 
         $this->porc_rete = (string)$this->porc_rete;
         $this->val_ica = $rs->fields[19] ;  
         $this->val_ica =  str_replace(",", ".", $this->val_ica);
         $this->val_ica = (strpos(strtolower($this->val_ica), "e")) ? (float)$this->val_ica : $this->val_ica; 
         $this->val_ica = (string)$this->val_ica;
         $this->por_ica = $rs->fields[20] ;  
         $this->por_ica = (strpos(strtolower($this->por_ica), "e")) ? (float)$this->por_ica : $this->por_ica; 
         $this->por_ica = (string)$this->por_ica;
         $this->por_retiva = $rs->fields[21] ;  
         $this->por_retiva = (strpos(strtolower($this->por_retiva), "e")) ? (float)$this->por_retiva : $this->por_retiva; 
         $this->por_retiva = (string)$this->por_retiva;
         $this->val_retiva = $rs->fields[22] ;  
         $this->val_retiva =  str_replace(",", ".", $this->val_retiva);
         $this->val_retiva = (strpos(strtolower($this->val_retiva), "e")) ? (float)$this->val_retiva : $this->val_retiva; 
         $this->val_retiva = (string)$this->val_retiva;
         //----- lookup - cliente
         $this->look_cliente = $this->cliente; 
         $this->Lookup->lookup_cliente($this->look_cliente, $this->cliente) ; 
         $this->look_cliente = ($this->look_cliente == "&nbsp;") ? "" : $this->look_cliente; 
         //----- lookup - resolucion
         $this->look_resolucion = $this->resolucion; 
         $this->Lookup->lookup_resolucion($this->look_resolucion, $this->resolucion) ; 
         $this->look_resolucion = ($this->look_resolucion == "&nbsp;") ? "" : $this->look_resolucion; 
         $this->sc_proc_grid = true; 
         //----- lookup - detallepagos_idfp
         $this->Lookup->lookup_detallepagos_idfp($this->detallepagos_idfp, $this->detallepagos_idfp, $this->array_detallepagos_idfp); 
         $this->detallepagos_idfp = str_replace("<br>", " ", $this->detallepagos_idfp); 
         $this->detallepagos_idfp = ($this->detallepagos_idfp == "&nbsp;") ? "" : $this->detallepagos_idfp; 
         $_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'on';
  $this->fFormapago();
$this->rete =-$this->rete ;
$this->val_ica =-$this->val_ica ;
$this->val_retiva =-$this->val_retiva ;
$this->descu =-$this->descu ;
$_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'off'; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";
      if(isset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['field_order']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['field_order'] = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['field_order'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['field_order']);
      }
      if(isset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['usr_cmp_sel']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['usr_cmp_sel'] = $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['usr_cmp_sel'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['export_sel_columns']['usr_cmp_sel']);
      }
      $rs->Close();
   }
   //----- idrecibo
   function NM_export_idrecibo()
   {
             nmgp_Form_Num_Val($this->idrecibo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->idrecibo = NM_charset_to_utf8($this->idrecibo);
         $this->idrecibo = str_replace('<', '&lt;', $this->idrecibo);
         $this->idrecibo = str_replace('>', '&gt;', $this->idrecibo);
         $this->Texto_tag .= "<td>" . $this->idrecibo . "</td>\r\n";
   }
   //----- nurecibo
   function NM_export_nurecibo()
   {
             nmgp_Form_Num_Val($this->nurecibo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->nurecibo = NM_charset_to_utf8($this->nurecibo);
         $this->nurecibo = str_replace('<', '&lt;', $this->nurecibo);
         $this->nurecibo = str_replace('>', '&gt;', $this->nurecibo);
         $this->Texto_tag .= "<td>" . $this->nurecibo . "</td>\r\n";
   }
   //----- nufac
   function NM_export_nufac()
   {
             nmgp_Form_Num_Val($this->nufac, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->nufac = NM_charset_to_utf8($this->nufac);
         $this->nufac = str_replace('<', '&lt;', $this->nufac);
         $this->nufac = str_replace('>', '&gt;', $this->nufac);
         $this->Texto_tag .= "<td>" . $this->nufac . "</td>\r\n";
   }
   //----- cliente
   function NM_export_cliente()
   {
         nmgp_Form_Num_Val($this->look_cliente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->look_cliente = NM_charset_to_utf8($this->look_cliente);
         $this->look_cliente = str_replace('<', '&lt;', $this->look_cliente);
         $this->look_cliente = str_replace('>', '&gt;', $this->look_cliente);
         $this->Texto_tag .= "<td>" . $this->look_cliente . "</td>\r\n";
   }
   //----- fecharecibo
   function NM_export_fecharecibo()
   {
             $conteudo_x =  $this->fecharecibo;
             nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
             if (is_numeric($conteudo_x) && strlen($conteudo_x) > 0) 
             { 
                 $this->nm_data->SetaData($this->fecharecibo, "YYYY-MM-DD  ");
                 $this->fecharecibo = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
             } 
         $this->fecharecibo = NM_charset_to_utf8($this->fecharecibo);
         $this->fecharecibo = str_replace('<', '&lt;', $this->fecharecibo);
         $this->fecharecibo = str_replace('>', '&gt;', $this->fecharecibo);
         $this->Texto_tag .= "<td>" . $this->fecharecibo . "</td>\r\n";
   }
   //----- monto
   function NM_export_monto()
   {
             nmgp_Form_Num_Val($this->monto, $_SESSION['scriptcase']['reg_conf']['grup_val'], $_SESSION['scriptcase']['reg_conf']['dec_val'], "0", "S", "2", $_SESSION['scriptcase']['reg_conf']['monet_simb'], "V:" . $_SESSION['scriptcase']['reg_conf']['monet_f_pos'] . ":" . $_SESSION['scriptcase']['reg_conf']['monet_f_neg'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit']) ; 
         $this->monto = NM_charset_to_utf8($this->monto);
         $this->monto = str_replace('<', '&lt;', $this->monto);
         $this->monto = str_replace('>', '&gt;', $this->monto);
         $this->Texto_tag .= "<td>" . $this->monto . "</td>\r\n";
   }
   //----- son
   function NM_export_son()
   {
         $this->son = html_entity_decode($this->son, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->son = strip_tags($this->son);
         $this->son = NM_charset_to_utf8($this->son);
         $this->son = str_replace('<', '&lt;', $this->son);
         $this->son = str_replace('>', '&gt;', $this->son);
         $this->Texto_tag .= "<td>" . $this->son . "</td>\r\n";
   }
   //----- saldofac
   function NM_export_saldofac()
   {
             nmgp_Form_Num_Val($this->saldofac, $_SESSION['scriptcase']['reg_conf']['grup_val'], $_SESSION['scriptcase']['reg_conf']['dec_val'], "0", "S", "2", $_SESSION['scriptcase']['reg_conf']['monet_simb'], "V:" . $_SESSION['scriptcase']['reg_conf']['monet_f_pos'] . ":" . $_SESSION['scriptcase']['reg_conf']['monet_f_neg'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit']) ; 
         $this->saldofac = NM_charset_to_utf8($this->saldofac);
         $this->saldofac = str_replace('<', '&lt;', $this->saldofac);
         $this->saldofac = str_replace('>', '&gt;', $this->saldofac);
         $this->Texto_tag .= "<td>" . $this->saldofac . "</td>\r\n";
   }
   //----- formapago
   function NM_export_formapago()
   {
         $this->formapago = html_entity_decode($this->formapago, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->formapago = strip_tags($this->formapago);
         $this->formapago = NM_charset_to_utf8($this->formapago);
         $this->formapago = str_replace('<', '&lt;', $this->formapago);
         $this->formapago = str_replace('>', '&gt;', $this->formapago);
         $this->Texto_tag .= "<td>" . $this->formapago . "</td>\r\n";
   }
   //----- numcheque
   function NM_export_numcheque()
   {
         $this->numcheque = html_entity_decode($this->numcheque, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->numcheque = strip_tags($this->numcheque);
         $this->numcheque = NM_charset_to_utf8($this->numcheque);
         $this->numcheque = str_replace('<', '&lt;', $this->numcheque);
         $this->numcheque = str_replace('>', '&gt;', $this->numcheque);
         $this->Texto_tag .= "<td>" . $this->numcheque . "</td>\r\n";
   }
   //----- banco
   function NM_export_banco()
   {
         $this->banco = html_entity_decode($this->banco, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->banco = strip_tags($this->banco);
         $this->banco = NM_charset_to_utf8($this->banco);
         $this->banco = str_replace('<', '&lt;', $this->banco);
         $this->banco = str_replace('>', '&gt;', $this->banco);
         $this->Texto_tag .= "<td>" . $this->banco . "</td>\r\n";
   }
   //----- numtarjeta
   function NM_export_numtarjeta()
   {
         $this->numtarjeta = html_entity_decode($this->numtarjeta, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->numtarjeta = strip_tags($this->numtarjeta);
         $this->numtarjeta = NM_charset_to_utf8($this->numtarjeta);
         $this->numtarjeta = str_replace('<', '&lt;', $this->numtarjeta);
         $this->numtarjeta = str_replace('>', '&gt;', $this->numtarjeta);
         $this->Texto_tag .= "<td>" . $this->numtarjeta . "</td>\r\n";
   }
   //----- nombreobanco
   function NM_export_nombreobanco()
   {
         $this->nombreobanco = html_entity_decode($this->nombreobanco, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->nombreobanco = strip_tags($this->nombreobanco);
         $this->nombreobanco = NM_charset_to_utf8($this->nombreobanco);
         $this->nombreobanco = str_replace('<', '&lt;', $this->nombreobanco);
         $this->nombreobanco = str_replace('>', '&gt;', $this->nombreobanco);
         $this->Texto_tag .= "<td>" . $this->nombreobanco . "</td>\r\n";
   }
   //----- obser
   function NM_export_obser()
   {
             if ($this->obser !== "&nbsp;") 
             { 
                 $this->obser = sc_strtoupper($this->obser); 
             } 
         $this->obser = html_entity_decode($this->obser, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->obser = strip_tags($this->obser);
         $this->obser = NM_charset_to_utf8($this->obser);
         $this->obser = str_replace('<', '&lt;', $this->obser);
         $this->obser = str_replace('>', '&gt;', $this->obser);
         $this->Texto_tag .= "<td>" . $this->obser . "</td>\r\n";
   }
   //----- concepto
   function NM_export_concepto()
   {
         $this->concepto = html_entity_decode($this->concepto, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->concepto = strip_tags($this->concepto);
         $this->concepto = NM_charset_to_utf8($this->concepto);
         $this->concepto = str_replace('<', '&lt;', $this->concepto);
         $this->concepto = str_replace('>', '&gt;', $this->concepto);
         $this->Texto_tag .= "<td>" . $this->concepto . "</td>\r\n";
   }
   //----- resolucion
   function NM_export_resolucion()
   {
         nmgp_Form_Num_Val($this->look_resolucion, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->look_resolucion = NM_charset_to_utf8($this->look_resolucion);
         $this->look_resolucion = str_replace('<', '&lt;', $this->look_resolucion);
         $this->look_resolucion = str_replace('>', '&gt;', $this->look_resolucion);
         $this->Texto_tag .= "<td>" . $this->look_resolucion . "</td>\r\n";
   }
   //----- rete
   function NM_export_rete()
   {
             nmgp_Form_Num_Val($this->rete, ".", ",", "0", "S", "2", "$", "V:3:13", "-"); 
         $this->rete = NM_charset_to_utf8($this->rete);
         $this->rete = str_replace('<', '&lt;', $this->rete);
         $this->rete = str_replace('>', '&gt;', $this->rete);
         $this->Texto_tag .= "<td>" . $this->rete . "</td>\r\n";
   }
   //----- descu
   function NM_export_descu()
   {
             nmgp_Form_Num_Val($this->descu, $_SESSION['scriptcase']['reg_conf']['grup_val'], $_SESSION['scriptcase']['reg_conf']['dec_val'], "0", "S", "2", $_SESSION['scriptcase']['reg_conf']['monet_simb'], "V:" . $_SESSION['scriptcase']['reg_conf']['monet_f_pos'] . ":" . $_SESSION['scriptcase']['reg_conf']['monet_f_neg'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit']) ; 
         $this->descu = NM_charset_to_utf8($this->descu);
         $this->descu = str_replace('<', '&lt;', $this->descu);
         $this->descu = str_replace('>', '&gt;', $this->descu);
         $this->Texto_tag .= "<td>" . $this->descu . "</td>\r\n";
   }
   //----- porc_rete
   function NM_export_porc_rete()
   {
             nmgp_Form_Num_Val($this->porc_rete, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "2", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->porc_rete = NM_charset_to_utf8($this->porc_rete);
         $this->porc_rete = str_replace('<', '&lt;', $this->porc_rete);
         $this->porc_rete = str_replace('>', '&gt;', $this->porc_rete);
         $this->Texto_tag .= "<td>" . $this->porc_rete . "</td>\r\n";
   }
   //----- val_ica
   function NM_export_val_ica()
   {
             nmgp_Form_Num_Val($this->val_ica, ".", ",", "0", "S", "2", "$", "V:3:3", "-"); 
         $this->val_ica = NM_charset_to_utf8($this->val_ica);
         $this->val_ica = str_replace('<', '&lt;', $this->val_ica);
         $this->val_ica = str_replace('>', '&gt;', $this->val_ica);
         $this->Texto_tag .= "<td>" . $this->val_ica . "</td>\r\n";
   }
   //----- por_ica
   function NM_export_por_ica()
   {
             nmgp_Form_Num_Val($this->por_ica, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "2", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->por_ica = NM_charset_to_utf8($this->por_ica);
         $this->por_ica = str_replace('<', '&lt;', $this->por_ica);
         $this->por_ica = str_replace('>', '&gt;', $this->por_ica);
         $this->Texto_tag .= "<td>" . $this->por_ica . "</td>\r\n";
   }
   //----- por_retiva
   function NM_export_por_retiva()
   {
             nmgp_Form_Num_Val($this->por_retiva, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "2", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->por_retiva = NM_charset_to_utf8($this->por_retiva);
         $this->por_retiva = str_replace('<', '&lt;', $this->por_retiva);
         $this->por_retiva = str_replace('>', '&gt;', $this->por_retiva);
         $this->Texto_tag .= "<td>" . $this->por_retiva . "</td>\r\n";
   }
   //----- val_retiva
   function NM_export_val_retiva()
   {
             nmgp_Form_Num_Val($this->val_retiva, ".", ",", "0", "S", "2", "$", "V:3:3", "-"); 
         $this->val_retiva = NM_charset_to_utf8($this->val_retiva);
         $this->val_retiva = str_replace('<', '&lt;', $this->val_retiva);
         $this->val_retiva = str_replace('>', '&gt;', $this->val_retiva);
         $this->Texto_tag .= "<td>" . $this->val_retiva . "</td>\r\n";
   }
   //----- detallepagos
   function NM_export_detallepagos()
   {
         $this->detallepagos = NM_charset_to_utf8($this->detallepagos);
         $this->detallepagos = str_replace('<', '&lt;', $this->detallepagos);
         $this->detallepagos = str_replace('>', '&gt;', $this->detallepagos);
         $this->Texto_tag .= "<td>" . $this->detallepagos . "</td>\r\n";
   }
   //----- detallepagos_banco
   function NM_export_detallepagos_banco()
   {
         $this->detallepagos_banco = html_entity_decode($this->detallepagos_banco, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->detallepagos_banco = strip_tags($this->detallepagos_banco);
         $this->detallepagos_banco = NM_charset_to_utf8($this->detallepagos_banco);
         $this->detallepagos_banco = str_replace('<', '&lt;', $this->detallepagos_banco);
         $this->detallepagos_banco = str_replace('>', '&gt;', $this->detallepagos_banco);
         $this->Texto_tag .= "<td>" . $this->detallepagos_banco . "</td>\r\n";
   }
   //----- detallepagos_fechacob
   function NM_export_detallepagos_fechacob()
   {
             $conteudo_x =  $this->detallepagos_fechacob;
             nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
             if (is_numeric($conteudo_x) && strlen($conteudo_x) > 0) 
             { 
                 $this->nm_data->SetaData($this->detallepagos_fechacob, "YYYY-MM-DD  ");
                 $this->detallepagos_fechacob = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
             } 
         $this->detallepagos_fechacob = NM_charset_to_utf8($this->detallepagos_fechacob);
         $this->detallepagos_fechacob = str_replace('<', '&lt;', $this->detallepagos_fechacob);
         $this->detallepagos_fechacob = str_replace('>', '&gt;', $this->detallepagos_fechacob);
         $this->Texto_tag .= "<td>" . $this->detallepagos_fechacob . "</td>\r\n";
   }
   //----- detallepagos_iddetall
   function NM_export_detallepagos_iddetall()
   {
             nmgp_Form_Num_Val($this->detallepagos_iddetall, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->detallepagos_iddetall = NM_charset_to_utf8($this->detallepagos_iddetall);
         $this->detallepagos_iddetall = str_replace('<', '&lt;', $this->detallepagos_iddetall);
         $this->detallepagos_iddetall = str_replace('>', '&gt;', $this->detallepagos_iddetall);
         $this->Texto_tag .= "<td>" . $this->detallepagos_iddetall . "</td>\r\n";
   }
   //----- detallepagos_idfact
   function NM_export_detallepagos_idfact()
   {
             nmgp_Form_Num_Val($this->detallepagos_idfact, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->detallepagos_idfact = NM_charset_to_utf8($this->detallepagos_idfact);
         $this->detallepagos_idfact = str_replace('<', '&lt;', $this->detallepagos_idfact);
         $this->detallepagos_idfact = str_replace('>', '&gt;', $this->detallepagos_idfact);
         $this->Texto_tag .= "<td>" . $this->detallepagos_idfact . "</td>\r\n";
   }
   //----- detallepagos_idfp
   function NM_export_detallepagos_idfp()
   {
         nmgp_Form_Num_Val($this->detallepagos_idfp, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->detallepagos_idfp = NM_charset_to_utf8($this->detallepagos_idfp);
         $this->detallepagos_idfp = str_replace('<', '&lt;', $this->detallepagos_idfp);
         $this->detallepagos_idfp = str_replace('>', '&gt;', $this->detallepagos_idfp);
         $this->Texto_tag .= "<td>" . $this->detallepagos_idfp . "</td>\r\n";
   }
   //----- detallepagos_idrc
   function NM_export_detallepagos_idrc()
   {
             nmgp_Form_Num_Val($this->detallepagos_idrc, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         $this->detallepagos_idrc = NM_charset_to_utf8($this->detallepagos_idrc);
         $this->detallepagos_idrc = str_replace('<', '&lt;', $this->detallepagos_idrc);
         $this->detallepagos_idrc = str_replace('>', '&gt;', $this->detallepagos_idrc);
         $this->Texto_tag .= "<td>" . $this->detallepagos_idrc . "</td>\r\n";
   }
   //----- detallepagos_monto
   function NM_export_detallepagos_monto()
   {
             nmgp_Form_Num_Val($this->detallepagos_monto, $_SESSION['scriptcase']['reg_conf']['grup_val'], $_SESSION['scriptcase']['reg_conf']['dec_val'], "0", "S", "2", $_SESSION['scriptcase']['reg_conf']['monet_simb'], "V:" . $_SESSION['scriptcase']['reg_conf']['monet_f_pos'] . ":" . $_SESSION['scriptcase']['reg_conf']['monet_f_neg'], $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit']) ; 
         $this->detallepagos_monto = NM_charset_to_utf8($this->detallepagos_monto);
         $this->detallepagos_monto = str_replace('<', '&lt;', $this->detallepagos_monto);
         $this->detallepagos_monto = str_replace('>', '&gt;', $this->detallepagos_monto);
         $this->Texto_tag .= "<td>" . $this->detallepagos_monto . "</td>\r\n";
   }
   //----- detallepagos_numcheque
   function NM_export_detallepagos_numcheque()
   {
         $this->detallepagos_numcheque = html_entity_decode($this->detallepagos_numcheque, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->detallepagos_numcheque = strip_tags($this->detallepagos_numcheque);
         $this->detallepagos_numcheque = NM_charset_to_utf8($this->detallepagos_numcheque);
         $this->detallepagos_numcheque = str_replace('<', '&lt;', $this->detallepagos_numcheque);
         $this->detallepagos_numcheque = str_replace('>', '&gt;', $this->detallepagos_numcheque);
         $this->Texto_tag .= "<td>" . $this->detallepagos_numcheque . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $this->Rtf_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $rtf_f       = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT") {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT") {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "SC_FORMAT_REGION") {
           $this->nm_data->SetaData($dt_in, strtoupper($form_in));
           $prep_out  = (strpos(strtolower($form_in), "dd") !== false) ? "dd" : "";
           $prep_out .= (strpos(strtolower($form_in), "mm") !== false) ? "mm" : "";
           $prep_out .= (strpos(strtolower($form_in), "aa") !== false) ? "aaaa" : "";
           $prep_out .= (strpos(strtolower($form_in), "yy") !== false) ? "aaaa" : "";
           return $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", $prep_out));
       }
       else {
           nm_conv_form_data($dt_out, $form_in, $form_out);
           return $dt_out;
       }
   }
   function progress_bar_end()
   {
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja'][$path_doc_md5][1] = $this->Tit_doc;
      $Mens_bar = $this->Ini->Nm_lang['lang_othr_file_msge'];
      if ($_SESSION['scriptcase']['charset'] != "UTF-8") {
          $Mens_bar = sc_convert_encoding($Mens_bar, "UTF-8", $_SESSION['scriptcase']['charset']);
      }
      $this->pb->setProgressbarMessage($Mens_bar);
      $this->pb->setDownloadLink($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $this->pb->setDownloadMd5($path_doc_md5);
      $this->pb->completed();
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['pdfreport_recibocaja'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_chart_titl'] ?> - recibocaja :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
 <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <?php
 if(isset($this->Ini->str_google_fonts) && !empty($this->Ini->str_google_fonts))
 {
 ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->str_google_fonts ?>" />
 <?php
 }
 ?>
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="pdfreport_recibocaja_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="pdfreport_recibocaja"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $str_highlight_ini = "";
      $str_highlight_fim = "";
      if(substr($nm_campo, 0, 23) == '<div class="highlight">' && substr($nm_campo, -6) == '</div>')
      {
           $str_highlight_ini = substr($nm_campo, 0, 23);
           $str_highlight_fim = substr($nm_campo, -6);

           $trab_campo = substr($nm_campo, 23, -6);
           $tam_campo  = strlen($trab_campo);
      }      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $str_highlight_ini . $trab_saida . $str_highlight_ini;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $str_highlight_ini . $trab_saida . $str_highlight_ini;
   } 
function fFormapago()
{
$_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'on';
  
$cad=$this->formapago ; 
	$existe=strpos ($cad, 'MIXT');
	if($existe !== false)
		{
		$this->formapago ='FORMA DE PAGO MULTIPLE';
		}



$_SESSION['scriptcase']['pdfreport_recibocaja']['contr_erro'] = 'off';
}
}

?>
