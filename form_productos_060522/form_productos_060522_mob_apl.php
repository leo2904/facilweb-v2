<?php
//
class form_productos_060522_mob_apl
{
   var $has_where_params = false;
   var $NM_is_redirected = false;
   var $NM_non_ajax_info = false;
   var $formatado = false;
   var $use_100perc_fields = false;
   var $classes_100perc_fields = array();
   var $NM_ajax_flag    = false;
   var $NM_ajax_opcao   = '';
   var $NM_ajax_retorno = '';
   var $NM_ajax_info    = array('result'            => '',
                                'param'             => array(),
                                'autoComp'          => '',
                                'rsSize'            => '',
                                'msgDisplay'        => '',
                                'errList'           => array(),
                                'fldList'           => array(),
                                'varList'           => array(),
                                'focus'             => '',
                                'navStatus'         => array(),
                                'navSummary'        => array(),
                                'navPage'           => array(),
                                'redir'             => array(),
                                'blockDisplay'      => array(),
                                'fieldDisplay'      => array(),
                                'fieldLabel'        => array(),
                                'readOnly'          => array(),
                                'btnVars'           => array(),
                                'ajaxAlert'         => array(),
                                'ajaxMessage'       => array(),
                                'ajaxJavascript'    => array(),
                                'buttonDisplay'     => array(),
                                'buttonDisplayVert' => array(),
                                'calendarReload'    => false,
                                'quickSearchRes'    => false,
                                'displayMsg'        => false,
                                'displayMsgTxt'     => '',
                                'dyn_search'        => array(),
                                'empty_filter'      => '',
                                'event_field'       => '',
                                'fieldsWithErrors'  => array(),
                               );
   var $NM_ajax_force_values = false;
   var $Nav_permite_ava     = true;
   var $Nav_permite_ret     = true;
   var $Apl_com_erro        = false;
   var $app_is_initializing = false;
   var $Ini;
   var $Erro;
   var $Db;
   var $idprod;
   var $codigobar;
   var $codigoprod;
   var $nompro;
   var $unidmaymen;
   var $unimay;
   var $unimen;
   var $costomay;
   var $costomen;
   var $recmayamen;
   var $preciomen;
   var $preciomen2;
   var $preciomen3;
   var $precio2;
   var $preciomay;
   var $preciofull;
   var $stockmay;
   var $stockmen;
   var $idgrup;
   var $idgrup_1;
   var $idpro1;
   var $idpro2;
   var $idiva;
   var $idiva_1;
   var $otro;
   var $otro_1;
   var $otro2;
   var $colores;
   var $colores_1;
   var $tallas;
   var $tallas_1;
   var $sabores;
   var $sabores_1;
   var $imagenprod;
   var $imagenprod_scfile_name;
   var $imagenprod_ul_name;
   var $imagenprod_scfile_type;
   var $imagenprod_ul_type;
   var $imagenprod_limpa;
   var $imagenprod_salva;
   var $out_imagenprod;
   var $imconsumo;
   var $escombo;
   var $idcombo;
   var $precio_editable;
   var $precio_editable_1;
   var $cod_cuenta;
   var $cod_cuenta_1;
   var $fecha_vencimiento;
   var $fecha_vencimiento_1;
   var $fecha_fab;
   var $lote;
   var $lote_1;
   var $serial_codbarras;
   var $serial_codbarras_1;
   var $maneja_tcs_lfs;
   var $control_costo;
   var $por_preciominimo;
   var $id_marca;
   var $id_marca_1;
   var $id_linea;
   var $id_linea_1;
   var $ultima_compra;
   var $n_ultcompra;
   var $ultima_venta;
   var $n_ultventa;
   var $codigobar2;
   var $codigobar3;
   var $nube;
   var $existencia;
   var $multiple_escala;
   var $en_base_a;
   var $activo;
   var $tipo_producto;
   var $tipo_producto_1;
   var $costo_prom;
   var $imagen;
   var $imagen_scfile_name;
   var $imagen_ul_name;
   var $imagen_scfile_type;
   var $imagen_ul_type;
   var $imagen_limpa;
   var $imagen_salva;
   var $out_imagen;
   var $out1_imagen;
   var $ubicacion;
   var $para_registro_fe;
   var $para_registro_fe_1;
   var $sugerido_mayor;
   var $sugerido_menor;
   var $confcolor;
   var $conftalla;
   var $relleno;
   var $sabor;
   var $u_menor;
   var $unidad_;
   var $unidad_ma;
   var $nm_data;
   var $nmgp_opcao;
   var $nmgp_opc_ant;
   var $sc_evento;
   var $sc_insert_on;
   var $nmgp_clone;
   var $nmgp_return_img = array();
   var $nmgp_dados_form = array();
   var $nmgp_dados_select = array();
   var $nm_location;
   var $nm_flag_iframe;
   var $nm_flag_saida_novo;
   var $nmgp_botoes = array();
   var $nmgp_url_saida;
   var $nmgp_form_show;
   var $nmgp_form_empty;
   var $nmgp_cmp_readonly = array();
   var $nmgp_cmp_hidden = array();
   var $form_paginacao = 'parcial';
   var $lig_edit_lookup      = false;
   var $lig_edit_lookup_call = false;
   var $lig_edit_lookup_cb   = '';
   var $lig_edit_lookup_row  = '';
   var $is_calendar_app = false;
   var $Embutida_call  = false;
   var $Embutida_ronly = false;
   var $Embutida_proc  = false;
   var $Embutida_form  = false;
   var $Grid_editavel  = false;
   var $url_webhelp = '';
   var $nm_todas_criticas;
   var $Campos_Mens_erro;
   var $nm_new_label = array();
   var $record_insert_ok = false;
   var $record_delete_ok = false;
//
//----- 
   function ini_controle()
   {
        global $nm_url_saida, $teste_validade, $script_case_init, 
               $glo_senha_protect, $nm_apl_dependente, $nm_form_submit, $sc_check_excl, $nm_opc_form_php, $nm_call_php, $nm_opc_lookup;


      if ($this->NM_ajax_flag)
      {
          if (isset($this->NM_ajax_info['param']['activo']))
          {
              $this->activo = $this->NM_ajax_info['param']['activo'];
          }
          if (isset($this->NM_ajax_info['param']['cod_cuenta']))
          {
              $this->cod_cuenta = $this->NM_ajax_info['param']['cod_cuenta'];
          }
          if (isset($this->NM_ajax_info['param']['codigobar']))
          {
              $this->codigobar = $this->NM_ajax_info['param']['codigobar'];
          }
          if (isset($this->NM_ajax_info['param']['codigobar2']))
          {
              $this->codigobar2 = $this->NM_ajax_info['param']['codigobar2'];
          }
          if (isset($this->NM_ajax_info['param']['codigobar3']))
          {
              $this->codigobar3 = $this->NM_ajax_info['param']['codigobar3'];
          }
          if (isset($this->NM_ajax_info['param']['codigoprod']))
          {
              $this->codigoprod = $this->NM_ajax_info['param']['codigoprod'];
          }
          if (isset($this->NM_ajax_info['param']['colores']))
          {
              $this->colores = $this->NM_ajax_info['param']['colores'];
          }
          if (isset($this->NM_ajax_info['param']['confcolor']))
          {
              $this->confcolor = $this->NM_ajax_info['param']['confcolor'];
          }
          if (isset($this->NM_ajax_info['param']['conftalla']))
          {
              $this->conftalla = $this->NM_ajax_info['param']['conftalla'];
          }
          if (isset($this->NM_ajax_info['param']['control_costo']))
          {
              $this->control_costo = $this->NM_ajax_info['param']['control_costo'];
          }
          if (isset($this->NM_ajax_info['param']['costo_prom']))
          {
              $this->costo_prom = $this->NM_ajax_info['param']['costo_prom'];
          }
          if (isset($this->NM_ajax_info['param']['costomen']))
          {
              $this->costomen = $this->NM_ajax_info['param']['costomen'];
          }
          if (isset($this->NM_ajax_info['param']['csrf_token']))
          {
              $this->csrf_token = $this->NM_ajax_info['param']['csrf_token'];
          }
          if (isset($this->NM_ajax_info['param']['en_base_a']))
          {
              $this->en_base_a = $this->NM_ajax_info['param']['en_base_a'];
          }
          if (isset($this->NM_ajax_info['param']['existencia']))
          {
              $this->existencia = $this->NM_ajax_info['param']['existencia'];
          }
          if (isset($this->NM_ajax_info['param']['fecha_vencimiento']))
          {
              $this->fecha_vencimiento = $this->NM_ajax_info['param']['fecha_vencimiento'];
          }
          if (isset($this->NM_ajax_info['param']['id_linea']))
          {
              $this->id_linea = $this->NM_ajax_info['param']['id_linea'];
          }
          if (isset($this->NM_ajax_info['param']['id_marca']))
          {
              $this->id_marca = $this->NM_ajax_info['param']['id_marca'];
          }
          if (isset($this->NM_ajax_info['param']['idgrup']))
          {
              $this->idgrup = $this->NM_ajax_info['param']['idgrup'];
          }
          if (isset($this->NM_ajax_info['param']['idiva']))
          {
              $this->idiva = $this->NM_ajax_info['param']['idiva'];
          }
          if (isset($this->NM_ajax_info['param']['idpro1']))
          {
              $this->idpro1 = $this->NM_ajax_info['param']['idpro1'];
          }
          if (isset($this->NM_ajax_info['param']['idpro2']))
          {
              $this->idpro2 = $this->NM_ajax_info['param']['idpro2'];
          }
          if (isset($this->NM_ajax_info['param']['idprod']))
          {
              $this->idprod = $this->NM_ajax_info['param']['idprod'];
          }
          if (isset($this->NM_ajax_info['param']['imagen']))
          {
              $this->imagen = $this->NM_ajax_info['param']['imagen'];
          }
          if (isset($this->NM_ajax_info['param']['imagen_limpa']))
          {
              $this->imagen_limpa = $this->NM_ajax_info['param']['imagen_limpa'];
          }
          if (isset($this->NM_ajax_info['param']['imagen_salva']))
          {
              $this->imagen_salva = $this->NM_ajax_info['param']['imagen_salva'];
          }
          if (isset($this->NM_ajax_info['param']['imagen_ul_name']))
          {
              $this->imagen_ul_name = $this->NM_ajax_info['param']['imagen_ul_name'];
          }
          if (isset($this->NM_ajax_info['param']['imagen_ul_type']))
          {
              $this->imagen_ul_type = $this->NM_ajax_info['param']['imagen_ul_type'];
          }
          if (isset($this->NM_ajax_info['param']['lote']))
          {
              $this->lote = $this->NM_ajax_info['param']['lote'];
          }
          if (isset($this->NM_ajax_info['param']['maneja_tcs_lfs']))
          {
              $this->maneja_tcs_lfs = $this->NM_ajax_info['param']['maneja_tcs_lfs'];
          }
          if (isset($this->NM_ajax_info['param']['multiple_escala']))
          {
              $this->multiple_escala = $this->NM_ajax_info['param']['multiple_escala'];
          }
          if (isset($this->NM_ajax_info['param']['nm_form_submit']))
          {
              $this->nm_form_submit = $this->NM_ajax_info['param']['nm_form_submit'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_ancora']))
          {
              $this->nmgp_ancora = $this->NM_ajax_info['param']['nmgp_ancora'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_arg_dyn_search']))
          {
              $this->nmgp_arg_dyn_search = $this->NM_ajax_info['param']['nmgp_arg_dyn_search'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_num_form']))
          {
              $this->nmgp_num_form = $this->NM_ajax_info['param']['nmgp_num_form'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_opcao']))
          {
              $this->nmgp_opcao = $this->NM_ajax_info['param']['nmgp_opcao'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_ordem']))
          {
              $this->nmgp_ordem = $this->NM_ajax_info['param']['nmgp_ordem'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_parms']))
          {
              $this->nmgp_parms = $this->NM_ajax_info['param']['nmgp_parms'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_refresh_fields']))
          {
              $this->nmgp_refresh_fields = $this->NM_ajax_info['param']['nmgp_refresh_fields'];
          }
          if (isset($this->NM_ajax_info['param']['nmgp_url_saida']))
          {
              $this->nmgp_url_saida = $this->NM_ajax_info['param']['nmgp_url_saida'];
          }
          if (isset($this->NM_ajax_info['param']['nompro']))
          {
              $this->nompro = $this->NM_ajax_info['param']['nompro'];
          }
          if (isset($this->NM_ajax_info['param']['otro']))
          {
              $this->otro = $this->NM_ajax_info['param']['otro'];
          }
          if (isset($this->NM_ajax_info['param']['otro2']))
          {
              $this->otro2 = $this->NM_ajax_info['param']['otro2'];
          }
          if (isset($this->NM_ajax_info['param']['para_registro_fe']))
          {
              $this->para_registro_fe = $this->NM_ajax_info['param']['para_registro_fe'];
          }
          if (isset($this->NM_ajax_info['param']['por_preciominimo']))
          {
              $this->por_preciominimo = $this->NM_ajax_info['param']['por_preciominimo'];
          }
          if (isset($this->NM_ajax_info['param']['precio2']))
          {
              $this->precio2 = $this->NM_ajax_info['param']['precio2'];
          }
          if (isset($this->NM_ajax_info['param']['precio_editable']))
          {
              $this->precio_editable = $this->NM_ajax_info['param']['precio_editable'];
          }
          if (isset($this->NM_ajax_info['param']['preciofull']))
          {
              $this->preciofull = $this->NM_ajax_info['param']['preciofull'];
          }
          if (isset($this->NM_ajax_info['param']['preciomay']))
          {
              $this->preciomay = $this->NM_ajax_info['param']['preciomay'];
          }
          if (isset($this->NM_ajax_info['param']['preciomen']))
          {
              $this->preciomen = $this->NM_ajax_info['param']['preciomen'];
          }
          if (isset($this->NM_ajax_info['param']['preciomen2']))
          {
              $this->preciomen2 = $this->NM_ajax_info['param']['preciomen2'];
          }
          if (isset($this->NM_ajax_info['param']['preciomen3']))
          {
              $this->preciomen3 = $this->NM_ajax_info['param']['preciomen3'];
          }
          if (isset($this->NM_ajax_info['param']['recmayamen']))
          {
              $this->recmayamen = $this->NM_ajax_info['param']['recmayamen'];
          }
          if (isset($this->NM_ajax_info['param']['relleno']))
          {
              $this->relleno = $this->NM_ajax_info['param']['relleno'];
          }
          if (isset($this->NM_ajax_info['param']['sabor']))
          {
              $this->sabor = $this->NM_ajax_info['param']['sabor'];
          }
          if (isset($this->NM_ajax_info['param']['sabores']))
          {
              $this->sabores = $this->NM_ajax_info['param']['sabores'];
          }
          if (isset($this->NM_ajax_info['param']['script_case_init']))
          {
              $this->script_case_init = $this->NM_ajax_info['param']['script_case_init'];
          }
          if (isset($this->NM_ajax_info['param']['serial_codbarras']))
          {
              $this->serial_codbarras = $this->NM_ajax_info['param']['serial_codbarras'];
          }
          if (isset($this->NM_ajax_info['param']['stockmen']))
          {
              $this->stockmen = $this->NM_ajax_info['param']['stockmen'];
          }
          if (isset($this->NM_ajax_info['param']['sugerido_mayor']))
          {
              $this->sugerido_mayor = $this->NM_ajax_info['param']['sugerido_mayor'];
          }
          if (isset($this->NM_ajax_info['param']['sugerido_menor']))
          {
              $this->sugerido_menor = $this->NM_ajax_info['param']['sugerido_menor'];
          }
          if (isset($this->NM_ajax_info['param']['tallas']))
          {
              $this->tallas = $this->NM_ajax_info['param']['tallas'];
          }
          if (isset($this->NM_ajax_info['param']['tipo_producto']))
          {
              $this->tipo_producto = $this->NM_ajax_info['param']['tipo_producto'];
          }
          if (isset($this->NM_ajax_info['param']['u_menor']))
          {
              $this->u_menor = $this->NM_ajax_info['param']['u_menor'];
          }
          if (isset($this->NM_ajax_info['param']['ubicacion']))
          {
              $this->ubicacion = $this->NM_ajax_info['param']['ubicacion'];
          }
          if (isset($this->NM_ajax_info['param']['unidad_']))
          {
              $this->unidad_ = $this->NM_ajax_info['param']['unidad_'];
          }
          if (isset($this->NM_ajax_info['param']['unidad_ma']))
          {
              $this->unidad_ma = $this->NM_ajax_info['param']['unidad_ma'];
          }
          if (isset($this->NM_ajax_info['param']['unidmaymen']))
          {
              $this->unidmaymen = $this->NM_ajax_info['param']['unidmaymen'];
          }
          if (isset($this->NM_ajax_info['param']['unimay']))
          {
              $this->unimay = $this->NM_ajax_info['param']['unimay'];
          }
          if (isset($this->NM_ajax_info['param']['unimen']))
          {
              $this->unimen = $this->NM_ajax_info['param']['unimen'];
          }
          if (isset($this->nmgp_refresh_fields))
          {
              $this->nmgp_refresh_fields = explode('_#fld#_', $this->nmgp_refresh_fields);
              $this->nmgp_opcao          = 'recarga';
          }
          if (!isset($this->nmgp_refresh_row))
          {
              $this->nmgp_refresh_row = '';
          }
      }

      $this->sc_conv_var = array();
      if (!empty($_FILES))
      {
          foreach ($_FILES as $nmgp_campo => $nmgp_valores)
          {
               if (isset($this->sc_conv_var[$nmgp_campo]))
               {
                   $nmgp_campo = $this->sc_conv_var[$nmgp_campo];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_campo)]))
               {
                   $nmgp_campo = $this->sc_conv_var[strtolower($nmgp_campo)];
               }
               $tmp_scfile_name     = $nmgp_campo . "_scfile_name";
               $tmp_scfile_type     = $nmgp_campo . "_scfile_type";
               $this->$nmgp_campo = is_array($nmgp_valores['tmp_name']) ? $nmgp_valores['tmp_name'][0] : $nmgp_valores['tmp_name'];
               $this->$tmp_scfile_type   = is_array($nmgp_valores['type'])     ? $nmgp_valores['type'][0]     : $nmgp_valores['type'];
               $this->$tmp_scfile_name   = is_array($nmgp_valores['name'])     ? $nmgp_valores['name'][0]     : $nmgp_valores['name'];
          }
      }
      $Sc_lig_md5 = false;
      if (!empty($_POST))
      {
          foreach ($_POST as $nmgp_var => $nmgp_val)
          {
               if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
               {
                   $nmgp_var = substr($nmgp_var, 11);
                   $nmgp_val = $_SESSION[$nmgp_val];
               }
              if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
              {
                  $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                  if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                  {
                      $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                      $Sc_lig_md5 = true;
                  }
                  else
                  {
                      $_SESSION['sc_session']['SC_parm_violation'] = true;
                  }
              }
               if (isset($this->sc_conv_var[$nmgp_var]))
               {
                   $nmgp_var = $this->sc_conv_var[$nmgp_var];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_var)]))
               {
                   $nmgp_var = $this->sc_conv_var[strtolower($nmgp_var)];
               }
               $nmgp_val = NM_decode_input($nmgp_val);
               $this->$nmgp_var = $nmgp_val;
          }
      }
      if (!empty($_GET))
      {
          foreach ($_GET as $nmgp_var => $nmgp_val)
          {
               if (substr($nmgp_var, 0, 11) == "SC_glo_par_")
               {
                   $nmgp_var = substr($nmgp_var, 11);
                   $nmgp_val = $_SESSION[$nmgp_val];
               }
              if ($nmgp_var == "nmgp_parms" && substr($nmgp_val, 0, 8) == "@SC_par@")
              {
                  $SC_Ind_Val = explode("@SC_par@", $nmgp_val);
                  if (count($SC_Ind_Val) == 4 && isset($_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]]))
                  {
                      $nmgp_val = $_SESSION['sc_session'][$SC_Ind_Val[1]][$SC_Ind_Val[2]]['Lig_Md5'][$SC_Ind_Val[3]];
                      $Sc_lig_md5 = true;
                  }
                  else
                  {
                       $_SESSION['sc_session']['SC_parm_violation'] = true;
                  }
              }
               if (isset($this->sc_conv_var[$nmgp_var]))
               {
                   $nmgp_var = $this->sc_conv_var[$nmgp_var];
               }
               elseif (isset($this->sc_conv_var[strtolower($nmgp_var)]))
               {
                   $nmgp_var = $this->sc_conv_var[strtolower($nmgp_var)];
               }
               $nmgp_val = NM_decode_input($nmgp_val);
               $this->$nmgp_var = $nmgp_val;
          }
      }
      if (isset($SC_lig_apl_orig) && !$Sc_lig_md5 && (!isset($nmgp_parms) || ($nmgp_parms != "SC_null" && substr($nmgp_parms, 0, 8) != "OrScLink")))
      {
          $_SESSION['sc_session']['SC_parm_violation'] = true;
      }
      if (isset($nmgp_parms) && $nmgp_parms == "SC_null")
      {
          $nmgp_parms = "";
      }
      if (isset($this->gnit) && isset($this->NM_contr_var_session) && $this->NM_contr_var_session == "Yes") 
      {
          $_SESSION['gnit'] = $this->gnit;
      }
      if (isset($this->par_idproducto) && isset($this->NM_contr_var_session) && $this->NM_contr_var_session == "Yes") 
      {
          $_SESSION['par_idproducto'] = $this->par_idproducto;
      }
      if (isset($this->gidtercero) && isset($this->NM_contr_var_session) && $this->NM_contr_var_session == "Yes") 
      {
          $_SESSION['gidtercero'] = $this->gidtercero;
      }
      if (isset($this->regimen_emp) && isset($this->NM_contr_var_session) && $this->NM_contr_var_session == "Yes") 
      {
          $_SESSION['regimen_emp'] = $this->regimen_emp;
      }
      if (isset($_POST["gnit"]) && isset($this->gnit)) 
      {
          $_SESSION['gnit'] = $this->gnit;
      }
      if (isset($_POST["par_idproducto"]) && isset($this->par_idproducto)) 
      {
          $_SESSION['par_idproducto'] = $this->par_idproducto;
      }
      if (isset($_POST["gidtercero"]) && isset($this->gidtercero)) 
      {
          $_SESSION['gidtercero'] = $this->gidtercero;
      }
      if (isset($_POST["regimen_emp"]) && isset($this->regimen_emp)) 
      {
          $_SESSION['regimen_emp'] = $this->regimen_emp;
      }
      if (isset($_GET["gnit"]) && isset($this->gnit)) 
      {
          $_SESSION['gnit'] = $this->gnit;
      }
      if (isset($_GET["par_idproducto"]) && isset($this->par_idproducto)) 
      {
          $_SESSION['par_idproducto'] = $this->par_idproducto;
      }
      if (isset($_GET["gidtercero"]) && isset($this->gidtercero)) 
      {
          $_SESSION['gidtercero'] = $this->gidtercero;
      }
      if (isset($_GET["regimen_emp"]) && isset($this->regimen_emp)) 
      {
          $_SESSION['regimen_emp'] = $this->regimen_emp;
      }
      if (isset($this->nmgp_opcao) && $this->nmgp_opcao == "reload_novo") {
          $_POST['nmgp_opcao'] = "novo";
          $this->nmgp_opcao    = "novo";
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['opcao']   = "novo";
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['opc_ant'] = "inicio";
      }
      if (isset($_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['embutida_parms']))
      { 
          $this->nmgp_parms = $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['embutida_parms'];
          unset($_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['embutida_parms']);
      } 
      if (isset($this->nmgp_parms) && !empty($this->nmgp_parms)) 
      { 
          if (isset($_SESSION['nm_aba_bg_color'])) 
          { 
              unset($_SESSION['nm_aba_bg_color']);
          }   
          $nmgp_parms = NM_decode_input($nmgp_parms);
          $nmgp_parms = str_replace("@aspass@", "'", $this->nmgp_parms);
          $nmgp_parms = str_replace("*scout", "?@?", $nmgp_parms);
          $nmgp_parms = str_replace("*scin", "?#?", $nmgp_parms);
          $todox = str_replace("?#?@?@?", "?#?@ ?@?", $nmgp_parms);
          $todo  = explode("?@?", $todox);
          $ix = 0;
          while (!empty($todo[$ix]))
          {
             $cadapar = explode("?#?", $todo[$ix]);
             if (1 < sizeof($cadapar))
             {
                if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                {
                    $cadapar[0] = substr($cadapar[0], 11);
                    $cadapar[1] = $_SESSION[$cadapar[1]];
                }
                 if (isset($this->sc_conv_var[$cadapar[0]]))
                 {
                     $cadapar[0] = $this->sc_conv_var[$cadapar[0]];
                 }
                 elseif (isset($this->sc_conv_var[strtolower($cadapar[0])]))
                 {
                     $cadapar[0] = $this->sc_conv_var[strtolower($cadapar[0])];
                 }
                 nm_limpa_str_form_productos_060522_mob($cadapar[1]);
                 if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                 $Tmp_par = $cadapar[0];
                 $this->$Tmp_par = $cadapar[1];
             }
             $ix++;
          }
          if (isset($this->gnit)) 
          {
              $_SESSION['gnit'] = $this->gnit;
          }
          if (isset($this->par_idproducto)) 
          {
              $_SESSION['par_idproducto'] = $this->par_idproducto;
          }
          if (isset($this->gidtercero)) 
          {
              $_SESSION['gidtercero'] = $this->gidtercero;
          }
          if (isset($this->regimen_emp)) 
          {
              $_SESSION['regimen_emp'] = $this->regimen_emp;
          }
          if (isset($this->NM_where_filter_form))
          {
              $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['where_filter_form'] = $this->NM_where_filter_form;
              unset($_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['total']);
          }
          if (isset($this->sc_redir_atualiz))
          {
              $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['sc_redir_atualiz'] = $this->sc_redir_atualiz;
          }
          if (isset($this->sc_redir_insert))
          {
              $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['sc_redir_insert'] = $this->sc_redir_insert;
          }
          if (isset($this->gnit)) 
          {
              $_SESSION['gnit'] = $this->gnit;
          }
          if (isset($this->par_idproducto)) 
          {
              $_SESSION['par_idproducto'] = $this->par_idproducto;
          }
          if (isset($this->gidtercero)) 
          {
              $_SESSION['gidtercero'] = $this->gidtercero;
          }
          if (isset($this->regimen_emp)) 
          {
              $_SESSION['regimen_emp'] = $this->regimen_emp;
          }
      } 
      elseif (isset($script_case_init) && !empty($script_case_init) && isset($_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['parms']))
      {
          if ((!isset($this->nmgp_opcao) || ($this->nmgp_opcao != "incluir" && $this->nmgp_opcao != "alterar" && $this->nmgp_opcao != "excluir" && $this->nmgp_opcao != "novo" && $this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form")) && (!isset($this->NM_ajax_opcao) || $this->NM_ajax_opcao == ""))
          {
              $todox = str_replace("?#?@?@?", "?#?@ ?@?", $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['parms']);
              $todo  = explode("?@?", $todox);
              $ix = 0;
              while (!empty($todo[$ix]))
              {
                 $cadapar = explode("?#?", $todo[$ix]);
                 if (substr($cadapar[0], 0, 11) == "SC_glo_par_")
                 {
                     $cadapar[0] = substr($cadapar[0], 11);
                     $cadapar[1] = $_SESSION[$cadapar[1]];
                 }
                 if ($cadapar[1] == "@ ") {$cadapar[1] = trim($cadapar[1]); }
                 $Tmp_par = $cadapar[0];
                 $this->$Tmp_par = $cadapar[1];
                 $ix++;
              }
          }
      } 

      if (isset($this->nm_run_menu) && $this->nm_run_menu == 1)
      { 
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['nm_run_menu'] = 1;
      } 
      if (!$this->NM_ajax_flag && 'autocomp_' == substr($this->NM_ajax_opcao, 0, 9))
      {
          $this->NM_ajax_flag = true;
      }

      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      if (isset($this->nm_evt_ret_edit) && '' != $this->nm_evt_ret_edit)
      {
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup']     = true;
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup_cb']  = $this->nm_evt_ret_edit;
          $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup_row'] = isset($this->nm_evt_ret_row) ? $this->nm_evt_ret_row : '';
      }
      if (isset($_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup']) && $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup'])
      {
          $this->lig_edit_lookup     = true;
          $this->lig_edit_lookup_cb  = $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup_cb'];
          $this->lig_edit_lookup_row = $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['lig_edit_lookup_row'];
      }
      if (!$this->Ini)
      { 
          $this->Ini = new form_productos_060522_mob_ini(); 
          $this->Ini->init();
          $this->nm_data = new nm_data("es");
          $this->app_is_initializing = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['initialize'];
          $this->Db = $this->Ini->Db; 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['initialize']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['initialize'])
          {
              $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_confcolor = $this->confcolor;
    $original_conftalla = $this->conftalla;
    $original_idiva = $this->idiva;
    $original_sabor = $this->sabor;
}
if (!isset($this->sc_temp_regimen_emp)) {$this->sc_temp_regimen_emp = (isset($_SESSION['regimen_emp'])) ? $_SESSION['regimen_emp'] : "";}
  if ($this->sc_temp_regimen_emp==0)
	{
	$this->idiva =1;
	$this->nmgp_cmp_hidden["idiva"] = "off"; $this->NM_ajax_info['fieldDisplay']['idiva'] = 'off';
	$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
	$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
	$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
	}
if (isset($this->sc_temp_regimen_emp)) { $_SESSION['regimen_emp'] = $this->sc_temp_regimen_emp;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_confcolor != $this->confcolor || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor)))
    {
        $this->ajax_return_values_confcolor(true);
    }
    if (($original_conftalla != $this->conftalla || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla)))
    {
        $this->ajax_return_values_conftalla(true);
    }
    if (($original_idiva != $this->idiva || (isset($bFlagRead_idiva) && $bFlagRead_idiva)))
    {
        $this->ajax_return_values_idiva(true);
    }
    if (($original_sabor != $this->sabor || (isset($bFlagRead_sabor) && $bFlagRead_sabor)))
    {
        $this->ajax_return_values_sabor(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
          if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522']))
          {
              foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522'] as $I_conf => $Conf_opt)
              {
                  $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob'][$I_conf]  = $Conf_opt;
              }
          }
          }
          $this->Ini->init2();
      } 
      else 
      { 
         $this->nm_data = new nm_data("es");
      } 
      $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['upload_field_info'] = array();

      $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['upload_field_info']['imagen'] = array(
          'app_dir'            => $this->Ini->path_aplicacao,
          'app_name'           => 'form_productos_060522_mob',
          'upload_dir'         => $this->Ini->root . $this->Ini->path_imag_temp . '/',
          'upload_url'         => $this->Ini->path_imag_temp . '/',
          'upload_type'        => 'single',
          'upload_allowed_type'  => '/.+$/i',
          'upload_max_size'  => null,
          'upload_file_height' => '150',
          'upload_file_width'  => '150',
          'upload_file_aspect' => 'S',
          'upload_file_type'   => 'I',
      );

      $_SESSION['sc_session'][$script_case_init]['form_productos_060522_mob']['upload_field_info']['imagenprod'] = array(
          'app_dir'            => $this->Ini->path_aplicacao,
          'app_name'           => 'form_productos_060522_mob',
          'upload_dir'         => $this->Ini->root . $this->Ini->path_imag_temp . '/',
          'upload_url'         => $this->Ini->path_imag_temp . '/',
          'upload_type'        => 'single',
          'upload_allowed_type'  => '/\.(jpg|jpeg|gif|png)$/i',
          'upload_max_size'  => null,
          'upload_file_height' => '0',
          'upload_file_width'  => '0',
          'upload_file_aspect' => 'S',
          'upload_file_type'   => 'I',
      );

      $this->Ini->Init_apl_lig = array();
      $this->List_apl_lig = array('form_escalas_productos'=>array('type'=>'form', 'lab'=>'CREACIÓN MULTIPLES ESCALAS', 'hint'=>'', 'img_on'=>'', 'img_off'=>''));
      if (isset($_SESSION['scriptcase']['menu_atual']) && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_outra_jan']) || !$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_outra_jan'] || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_modal']))
      {
          foreach ($this->List_apl_lig as $apl_name => $Lig_parms)
          {
              if (!isset($_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init'][$apl_name]))
              {
                  $_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init'][$apl_name] = rand(2, 10000);
              }
              $this->Ini->Init_apl_lig[$apl_name]['ini']     = "&script_case_init=" . $_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init'][$apl_name];
              $this->Ini->Init_apl_lig[$apl_name]['type']    = $Lig_parms['type'];
              $this->Ini->Init_apl_lig[$apl_name]['lab']     = $Lig_parms['lab'];
              $this->Ini->Init_apl_lig[$apl_name]['hint']    = $Lig_parms['hint'];
              $this->Ini->Init_apl_lig[$apl_name]['img_on']  = $Lig_parms['img_on'];
              $this->Ini->Init_apl_lig[$apl_name]['img_off'] = $Lig_parms['img_off'];
          }
      }
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue']);
      $this->Change_Menu = false;
      $run_iframe = (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe']) && ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "R")) ? true : false;
      if (!$run_iframe && isset($_SESSION['scriptcase']['menu_atual']) && !$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_call'] && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_outra_jan']) || !$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_outra_jan']))
      {
          $this->sc_init_menu = "x";
          if (isset($_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['form_productos_060522_mob']))
          {
              $this->sc_init_menu = $_SESSION['scriptcase'][$_SESSION['scriptcase']['menu_atual']]['sc_init']['form_productos_060522_mob'];
          }
          elseif (isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']]))
          {
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']] as $init => $resto)
              {
                  if ($this->Ini->sc_page == $init)
                  {
                      $this->sc_init_menu = $init;
                      break;
                  }
              }
          }
          if ($this->Ini->sc_page == $this->sc_init_menu && !isset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_productos_060522_mob']))
          {
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_productos_060522_mob']['link'] = $this->Ini->sc_protocolo . $this->Ini->server . $this->Ini->path_link . "" . SC_dir_app_name('form_productos_060522_mob') . "/";
               $_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu]['form_productos_060522_mob']['label'] = "Editar producto";
               $this->Change_Menu = true;
          }
          elseif ($this->Ini->sc_page == $this->sc_init_menu)
          {
              $achou = false;
              foreach ($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu] as $apl => $parms)
              {
                  if ($apl == "form_productos_060522_mob")
                  {
                      $achou = true;
                  }
                  elseif ($achou)
                  {
                      unset($_SESSION['scriptcase']['menu_apls'][$_SESSION['scriptcase']['menu_atual']][$this->sc_init_menu][$apl]);
                      $this->Change_Menu = true;
                  }
              }
          }
      }
      if (!function_exists("nmButtonOutput"))
      {
          include_once($this->Ini->path_lib_php . "nm_gp_config_btn.php");
      }
      include("../_lib/css/" . $this->Ini->str_schema_all . "_form.php");
      $this->Ini->Str_btn_form    = trim($str_button);
      include($this->Ini->path_btn . $this->Ini->Str_btn_form . '/' . $this->Ini->Str_btn_form . $_SESSION['scriptcase']['reg_conf']['css_dir'] . '.php');
      $_SESSION['scriptcase']['css_form_help'] = '../_lib/css/' . $this->Ini->str_schema_all . "_form.css";
      $_SESSION['scriptcase']['css_form_help_dir'] = '../_lib/css/' . $this->Ini->str_schema_all . "_form" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css";
      $this->Db = $this->Ini->Db; 
      $this->Ini->str_google_fonts = isset($str_google_fonts)?$str_google_fonts:'';
      $this->Ini->Img_sep_form    = "/" . trim($str_toolbar_separator);
      $this->Ini->Color_bg_ajax   = "" == trim($str_ajax_bg)         ? "#000" : $str_ajax_bg;
      $this->Ini->Border_c_ajax   = "" == trim($str_ajax_border_c)   ? ""     : $str_ajax_border_c;
      $this->Ini->Border_s_ajax   = "" == trim($str_ajax_border_s)   ? ""     : $str_ajax_border_s;
      $this->Ini->Border_w_ajax   = "" == trim($str_ajax_border_w)   ? ""     : $str_ajax_border_w;
      $this->Ini->Block_img_exp   = "" == trim($str_block_exp)       ? ""     : $str_block_exp;
      $this->Ini->Block_img_col   = "" == trim($str_block_col)       ? ""     : $str_block_col;
      $this->Ini->Msg_ico_title   = "" == trim($str_msg_ico_title)   ? ""     : $str_msg_ico_title;
      $this->Ini->Msg_ico_body    = "" == trim($str_msg_ico_body)    ? ""     : $str_msg_ico_body;
      $this->Ini->Err_ico_title   = "" == trim($str_err_ico_title)   ? ""     : $str_err_ico_title;
      $this->Ini->Err_ico_body    = "" == trim($str_err_ico_body)    ? ""     : $str_err_ico_body;
      $this->Ini->Cal_ico_back    = "" == trim($str_cal_ico_back)    ? ""     : $str_cal_ico_back;
      $this->Ini->Cal_ico_for     = "" == trim($str_cal_ico_for)     ? ""     : $str_cal_ico_for;
      $this->Ini->Cal_ico_close   = "" == trim($str_cal_ico_close)   ? ""     : $str_cal_ico_close;
      $this->Ini->Tab_space       = "" == trim($str_tab_space)       ? ""     : $str_tab_space;
      $this->Ini->Bubble_tail     = "" == trim($str_bubble_tail)     ? ""     : $str_bubble_tail;
      $this->Ini->Label_sort_pos  = "" == trim($str_label_sort_pos)  ? ""     : $str_label_sort_pos;
      $this->Ini->Label_sort      = "" == trim($str_label_sort)      ? ""     : $str_label_sort;
      $this->Ini->Label_sort_asc  = "" == trim($str_label_sort_asc)  ? ""     : $str_label_sort_asc;
      $this->Ini->Label_sort_desc = "" == trim($str_label_sort_desc) ? ""     : $str_label_sort_desc;
      $this->Ini->Img_status_ok       = "" == trim($str_img_status_ok)   ? ""     : $str_img_status_ok;
      $this->Ini->Img_status_err      = "" == trim($str_img_status_err)  ? ""     : $str_img_status_err;
      $this->Ini->Css_status          = "scFormInputError";
      $this->Ini->Css_status_pwd_box  = "scFormInputErrorPwdBox";
      $this->Ini->Css_status_pwd_text = "scFormInputErrorPwdText";
      $this->Ini->Error_icon_span = "" == trim($str_error_icon_span) ? false  : "message" == $str_error_icon_span;
      $this->Ini->Img_qs_search        = "" == trim($img_qs_search)        ? "scriptcase__NM__qs_lupa.png"  : $img_qs_search;
      $this->Ini->Img_qs_clean         = "" == trim($img_qs_clean)         ? "scriptcase__NM__qs_close.png" : $img_qs_clean;
      $this->Ini->Str_qs_image_padding = "" == trim($str_qs_image_padding) ? "0"                            : $str_qs_image_padding;
      $this->Ini->App_div_tree_img_col = trim($app_div_str_tree_col);
      $this->Ini->App_div_tree_img_exp = trim($app_div_str_tree_exp);
      $this->Ini->form_table_width     = isset($str_form_table_width) && '' != trim($str_form_table_width) ? $str_form_table_width : '';

        $this->classes_100perc_fields['table'] = '';
        $this->classes_100perc_fields['input'] = '';
        $this->classes_100perc_fields['span_input'] = '';
        $this->classes_100perc_fields['span_select'] = '';
        $this->classes_100perc_fields['style_category'] = '';
        $this->classes_100perc_fields['keep_field_size'] = true;


      $this->arr_buttons['sc_btn_0']['hint']             = "Abre formulario para crear los colores";
      $this->arr_buttons['sc_btn_0']['type']             = "button";
      $this->arr_buttons['sc_btn_0']['value']            = "Crear Colores";
      $this->arr_buttons['sc_btn_0']['display']          = "only_text";
      $this->arr_buttons['sc_btn_0']['display_position'] = "text_right";
      $this->arr_buttons['sc_btn_0']['style']            = "default";
      $this->arr_buttons['sc_btn_0']['image']            = "";

      $this->arr_buttons['sc_btn_1']['hint']             = "Abre formulario para crear las tallas...";
      $this->arr_buttons['sc_btn_1']['type']             = "button";
      $this->arr_buttons['sc_btn_1']['value']            = "Crear tallas o tamaños";
      $this->arr_buttons['sc_btn_1']['display']          = "only_text";
      $this->arr_buttons['sc_btn_1']['display_position'] = "text_right";
      $this->arr_buttons['sc_btn_1']['style']            = "default";
      $this->arr_buttons['sc_btn_1']['image']            = "";

      $this->arr_buttons['sc_btn_2']['hint']             = "Abre formulario para crear los sabores...";
      $this->arr_buttons['sc_btn_2']['type']             = "button";
      $this->arr_buttons['sc_btn_2']['value']            = "Crear sabores";
      $this->arr_buttons['sc_btn_2']['display']          = "only_text";
      $this->arr_buttons['sc_btn_2']['display_position'] = "text_right";
      $this->arr_buttons['sc_btn_2']['style']            = "default";
      $this->arr_buttons['sc_btn_2']['image']            = "";

      $this->arr_buttons['escalas']['hint']             = "Crear escalas Adicionales para el producto";
      $this->arr_buttons['escalas']['type']             = "button";
      $this->arr_buttons['escalas']['value']            = "Adicionar Escalas";
      $this->arr_buttons['escalas']['display']          = "only_text";
      $this->arr_buttons['escalas']['display_position'] = "text_right";
      $this->arr_buttons['escalas']['style']            = "default";
      $this->arr_buttons['escalas']['image']            = "";

      $this->arr_buttons['recalcular']['hint']             = "Recalcula existencia del producto";
      $this->arr_buttons['recalcular']['type']             = "button";
      $this->arr_buttons['recalcular']['value']            = "Recalcular Existencia";
      $this->arr_buttons['recalcular']['display']          = "only_text";
      $this->arr_buttons['recalcular']['display_position'] = "text_right";
      $this->arr_buttons['recalcular']['style']            = "default";
      $this->arr_buttons['recalcular']['image']            = "";


      $_SESSION['scriptcase']['error_icon']['form_productos_060522_mob']  = "<img src=\"" . $this->Ini->path_icones . "/scriptcase__NM__btn__NM__scriptcase9_Rhino__NM__nm_scriptcase9_Rhino_error.png\" style=\"border-width: 0px\" align=\"top\">&nbsp;";
      $_SESSION['scriptcase']['error_close']['form_productos_060522_mob'] = "<td>" . nmButtonOutput($this->arr_buttons, "berrm_clse", "document.getElementById('id_error_display_fixed').style.display = 'none'; document.getElementById('id_error_message_fixed').innerHTML = ''; return false", "document.getElementById('id_error_display_fixed').style.display = 'none'; document.getElementById('id_error_message_fixed').innerHTML = ''; return false", "", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "") . "</td>";

      $this->Embutida_proc = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_proc']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_proc'] : $this->Embutida_proc;
      $this->Embutida_form = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_form']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_form'] : $this->Embutida_form;
      $this->Embutida_call = isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_call']) ? $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_call'] : $this->Embutida_call;

       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['table_refresh'] = false;

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit'])
      {
          $this->Grid_editavel = ('on' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit']) ? true : false;
      }
      if (isset($this->Grid_editavel) && $this->Grid_editavel)
      {
          $this->Embutida_form  = true;
          $this->Embutida_ronly = true;
      }
      $this->Embutida_multi = false;
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_multi']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_multi'])
      {
          $this->Grid_editavel  = false;
          $this->Embutida_form  = false;
          $this->Embutida_ronly = false;
          $this->Embutida_multi = true;
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_tp_pag']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_tp_pag'])
      {
          $this->form_paginacao = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_tp_pag'];
      }

      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_form']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_form'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_form'] = $this->Embutida_form;
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit'] = $this->Grid_editavel ? 'on' : 'off';
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit']) || '' == $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit'])
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_grid_edit'] = $this->Embutida_call;
      }

      $this->Ini->cor_grid_par = $this->Ini->cor_grid_impar;
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nmgp_url_saida  = $nm_url_saida;
      $this->nmgp_form_show  = "on";
      $this->nmgp_form_empty = false;
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_valida.php", "C", "NM_Valida") ; 
      $teste_validade = new NM_Valida ;

      if (isset($this->NM_ajax_info['param']['imagen_ul_name']) && '' != $this->NM_ajax_info['param']['imagen_ul_name'])
      {
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_field_ul_name'][$this->imagen_ul_name]))
          {
              $this->NM_ajax_info['param']['imagen_ul_name'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_field_ul_name'][$this->imagen_ul_name];
          }
          $this->imagen = $this->Ini->root . $this->Ini->path_imag_temp . '/' . $this->NM_ajax_info['param']['imagen_ul_name'];
          $this->imagen_scfile_name = substr($this->NM_ajax_info['param']['imagen_ul_name'], 12);
          $this->imagen_scfile_type = $this->NM_ajax_info['param']['imagen_ul_type'];
          $this->imagen_ul_name = $this->NM_ajax_info['param']['imagen_ul_name'];
          $this->imagen_ul_type = $this->NM_ajax_info['param']['imagen_ul_type'];
      }
      elseif (isset($this->imagen_ul_name) && '' != $this->imagen_ul_name)
      {
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_field_ul_name'][$this->imagen_ul_name]))
          {
              $this->imagen_ul_name = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_field_ul_name'][$this->imagen_ul_name];
          }
          $this->imagen = $this->Ini->root . $this->Ini->path_imag_temp . '/' . $this->imagen_ul_name;
          $this->imagen_scfile_name = substr($this->imagen_ul_name, 12);
          $this->imagen_scfile_type = $this->imagen_ul_type;
      }

      $this->loadFieldConfig();

      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['first_time'])
      {
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['new']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage']);
          unset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto']);
      }
      $this->NM_cancel_return_new = (isset($this->NM_cancel_return_new) && $this->NM_cancel_return_new == 1) ? "1" : "";
      $this->NM_cancel_insert_new = ((isset($this->NM_cancel_insert_new) && $this->NM_cancel_insert_new == 1) || $this->NM_cancel_return_new == 1) ? "document.F5.action='" . $nm_url_saida . "';" : "";
      if (isset($this->NM_btn_insert) && '' != $this->NM_btn_insert && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert']))
      {
          if ('N' == $this->NM_btn_insert)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'] = 'on';
          }
      }
      if (isset($this->NM_btn_new) && 'N' == $this->NM_btn_new)
      {
          $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['new'] = 'off';
      }
      if (isset($this->NM_btn_update) && '' != $this->NM_btn_update && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['update']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['update']))
      {
          if ('N' == $this->NM_btn_update)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update'] = 'on';
          }
      }
      if (isset($this->NM_btn_delete) && '' != $this->NM_btn_delete && (!isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['delete']) || '' == $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['delete']))
      {
          if ('N' == $this->NM_btn_delete)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete'] = 'off';
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete'] = 'on';
          }
      }
      if (isset($this->NM_btn_navega) && '' != $this->NM_btn_navega)
      {
          if ('N' == $this->NM_btn_navega)
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first']     = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back']      = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last']      = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch'] = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage']   = 'off';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto']      = 'off';
              $this->Nav_permite_ava = false;
              $this->Nav_permite_ret = false;
          }
          else
          {
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first']     = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back']      = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last']      = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch'] = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage']   = 'on';
              $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto']      = 'on';
          }
      }

      $this->nmgp_botoes['cancel'] = "on";
      $this->nmgp_botoes['exit'] = "on";
      $this->nmgp_botoes['new'] = "on";
      $this->nmgp_botoes['insert'] = "on";
      $this->nmgp_botoes['copy'] = "on";
      $this->nmgp_botoes['update'] = "on";
      $this->nmgp_botoes['delete'] = "on";
      $this->nmgp_botoes['first'] = "on";
      $this->nmgp_botoes['back'] = "on";
      $this->nmgp_botoes['forward'] = "on";
      $this->nmgp_botoes['last'] = "on";
      $this->nmgp_botoes['summary'] = "on";
      $this->nmgp_botoes['navpage'] = "on";
      $this->nmgp_botoes['goto'] = "on";
      $this->nmgp_botoes['qtline'] = "off";
      $this->nmgp_botoes['reload'] = "on";
      $this->nmgp_botoes['sc_btn_0'] = "on";
      $this->nmgp_botoes['sc_btn_1'] = "on";
      $this->nmgp_botoes['sc_btn_2'] = "on";
      $this->nmgp_botoes['escalas'] = "on";
      $this->nmgp_botoes['recalcular'] = "on";
      if (isset($this->NM_btn_cancel) && 'N' == $this->NM_btn_cancel)
      {
          $this->nmgp_botoes['cancel'] = "off";
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_orig'] = "";
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_pesq']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_pesq'] = "";
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_pesq_filtro'] = "";
      }
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_pesq_filtro'];
      if ($this->NM_ajax_flag && 'event_' == substr($this->NM_ajax_opcao, 0, 6)) {
          $this->nmgp_botoes = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['buttonStatus'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['iframe_filtro']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['iframe_filtro'] == "S")
      {
          $this->nmgp_botoes['exit'] = "off";
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['btn_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
          {
              $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
          }
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'];
          $this->nmgp_botoes['copy']   = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['insert'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['new']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['new'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['new'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['update'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['delete'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first'] != '')
      {
          $this->nmgp_botoes['first'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['first'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back'] != '')
      {
          $this->nmgp_botoes['back'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['back'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward'] != '')
      {
          $this->nmgp_botoes['forward'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['forward'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last'] != '')
      {
          $this->nmgp_botoes['last'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['last'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch'] != '')
      {
          $this->nmgp_botoes['qsearch'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['qsearch'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch'] != '')
      {
          $this->nmgp_botoes['dynsearch'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['dynsearch'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary'] != '')
      {
          $this->nmgp_botoes['summary'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['summary'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage'] != '')
      {
          $this->nmgp_botoes['navpage'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['navpage'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto']) && $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto'] != '')
      {
          $this->nmgp_botoes['goto'] = $_SESSION['scriptcase']['sc_apl_conf_lig']['form_productos_060522_mob']['goto'];
      }

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_insert']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_insert'];
          $this->nmgp_botoes['copy']   = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_insert'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_update']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_update'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_delete']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_delete'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav'] != '')
      {
          $this->nmgp_botoes['first']   = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['back']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['forward'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav'];
          $this->nmgp_botoes['last']    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['embutida_liga_form_btn_nav'];
      }

      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['under_dashboard']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['under_dashboard'] && !$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['maximized']) {
          $tmpDashboardApp = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['dashboard_app'];
          if (isset($_SESSION['scriptcase']['dashboard_toolbar'][$tmpDashboardApp]['form_productos_060522_mob'])) {
              $tmpDashboardButtons = $_SESSION['scriptcase']['dashboard_toolbar'][$tmpDashboardApp]['form_productos_060522_mob'];

              $this->nmgp_botoes['update']     = $tmpDashboardButtons['form_update']    ? 'on' : 'off';
              $this->nmgp_botoes['new']        = $tmpDashboardButtons['form_insert']    ? 'on' : 'off';
              $this->nmgp_botoes['insert']     = $tmpDashboardButtons['form_insert']    ? 'on' : 'off';
              $this->nmgp_botoes['delete']     = $tmpDashboardButtons['form_delete']    ? 'on' : 'off';
              $this->nmgp_botoes['copy']       = $tmpDashboardButtons['form_copy']      ? 'on' : 'off';
              $this->nmgp_botoes['first']      = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['back']       = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['last']       = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['forward']    = $tmpDashboardButtons['form_navigate']  ? 'on' : 'off';
              $this->nmgp_botoes['navpage']    = $tmpDashboardButtons['form_navpage']   ? 'on' : 'off';
              $this->nmgp_botoes['goto']       = $tmpDashboardButtons['form_goto']      ? 'on' : 'off';
              $this->nmgp_botoes['qtline']     = $tmpDashboardButtons['form_lineqty']   ? 'on' : 'off';
              $this->nmgp_botoes['summary']    = $tmpDashboardButtons['form_summary']   ? 'on' : 'off';
              $this->nmgp_botoes['qsearch']    = $tmpDashboardButtons['form_qsearch']   ? 'on' : 'off';
              $this->nmgp_botoes['dynsearch']  = $tmpDashboardButtons['form_dynsearch'] ? 'on' : 'off';
              $this->nmgp_botoes['reload']     = $tmpDashboardButtons['form_reload']    ? 'on' : 'off';
          }
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert']) && $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert'] != '')
      {
          $this->nmgp_botoes['new']    = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert'];
          $this->nmgp_botoes['insert'] = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert'];
          $this->nmgp_botoes['copy']   = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['insert'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['update']) && $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['update'] != '')
      {
          $this->nmgp_botoes['update'] = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['update'];
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['delete']) && $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['delete'] != '')
      {
          $this->nmgp_botoes['delete'] = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['delete'];
      }

      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->nmgp_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
              $this->NM_ajax_info['fieldDisplay'][$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_readonly']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_readonly']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['field_readonly'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->nmgp_cmp_readonly[$NM_cada_field] = "on";
              $this->NM_ajax_info['readOnly'][$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['exit']) && $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['exit'] != '')
      {
          $_SESSION['scriptcase']['sc_url_saida'][$this->Ini->sc_page]       = $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['exit'];
          $_SESSION['scriptcase']['sc_force_url_saida'][$this->Ini->sc_page] = true;
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']))
      {
          $this->nmgp_dados_form = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form'];
          if (!isset($this->costomay)){$this->costomay = $this->nmgp_dados_form['costomay'];} 
          if (!isset($this->stockmay)){$this->stockmay = $this->nmgp_dados_form['stockmay'];} 
          if ($this->nmgp_opcao == "incluir" && $this->nmgp_dados_form['stockmen'] != "null"){$this->stockmen = $this->nmgp_dados_form['stockmen'];} 
          if (!isset($this->imagenprod)){$this->imagenprod = $this->nmgp_dados_form['imagenprod'];} 
          if (!isset($this->imconsumo)){$this->imconsumo = $this->nmgp_dados_form['imconsumo'];} 
          if (!isset($this->escombo)){$this->escombo = $this->nmgp_dados_form['escombo'];} 
          if (!isset($this->idcombo)){$this->idcombo = $this->nmgp_dados_form['idcombo'];} 
          if (!isset($this->fecha_fab)){$this->fecha_fab = $this->nmgp_dados_form['fecha_fab'];} 
          if (!isset($this->ultima_compra)){$this->ultima_compra = $this->nmgp_dados_form['ultima_compra'];} 
          if (!isset($this->n_ultcompra)){$this->n_ultcompra = $this->nmgp_dados_form['n_ultcompra'];} 
          if (!isset($this->ultima_venta)){$this->ultima_venta = $this->nmgp_dados_form['ultima_venta'];} 
          if (!isset($this->n_ultventa)){$this->n_ultventa = $this->nmgp_dados_form['n_ultventa'];} 
          if (!isset($this->nube)){$this->nube = $this->nmgp_dados_form['nube'];} 
      }
      $glo_senha_protect = (isset($_SESSION['scriptcase']['glo_senha_protect'])) ? $_SESSION['scriptcase']['glo_senha_protect'] : "S";
      $this->aba_iframe = false;
      if (isset($_SESSION['scriptcase']['sc_aba_iframe']))
      {
          foreach ($_SESSION['scriptcase']['sc_aba_iframe'] as $aba => $apls_aba)
          {
              if (in_array("form_productos_060522_mob", $apls_aba))
              {
                  $this->aba_iframe = true;
                  break;
              }
          }
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['iframe_menu'] && (!isset($_SESSION['scriptcase']['menu_mobile']) || empty($_SESSION['scriptcase']['menu_mobile'])))
      {
          $this->aba_iframe = true;
      }
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_limpa.php", "F", "nm_limpa_valor") ; 
      $this->Ini->sc_Include($this->Ini->path_libs . "/nm_gc.php", "F", "nm_gc") ; 
      $_SESSION['scriptcase']['sc_tab_meses']['int'] = array(
                                      $this->Ini->Nm_lang['lang_mnth_janu'],
                                      $this->Ini->Nm_lang['lang_mnth_febr'],
                                      $this->Ini->Nm_lang['lang_mnth_marc'],
                                      $this->Ini->Nm_lang['lang_mnth_apri'],
                                      $this->Ini->Nm_lang['lang_mnth_mayy'],
                                      $this->Ini->Nm_lang['lang_mnth_june'],
                                      $this->Ini->Nm_lang['lang_mnth_july'],
                                      $this->Ini->Nm_lang['lang_mnth_augu'],
                                      $this->Ini->Nm_lang['lang_mnth_sept'],
                                      $this->Ini->Nm_lang['lang_mnth_octo'],
                                      $this->Ini->Nm_lang['lang_mnth_nove'],
                                      $this->Ini->Nm_lang['lang_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_meses']['abr'] = array(
                                      $this->Ini->Nm_lang['lang_shrt_mnth_janu'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_febr'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_marc'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_apri'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_mayy'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_june'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_july'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_augu'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_sept'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_octo'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_nove'],
                                      $this->Ini->Nm_lang['lang_shrt_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_dias']['int'] = array(
                                      $this->Ini->Nm_lang['lang_days_sund'],
                                      $this->Ini->Nm_lang['lang_days_mond'],
                                      $this->Ini->Nm_lang['lang_days_tued'],
                                      $this->Ini->Nm_lang['lang_days_wend'],
                                      $this->Ini->Nm_lang['lang_days_thud'],
                                      $this->Ini->Nm_lang['lang_days_frid'],
                                      $this->Ini->Nm_lang['lang_days_satd']);
      $_SESSION['scriptcase']['sc_tab_dias']['abr'] = array(
                                      $this->Ini->Nm_lang['lang_shrt_days_sund'],
                                      $this->Ini->Nm_lang['lang_shrt_days_mond'],
                                      $this->Ini->Nm_lang['lang_shrt_days_tued'],
                                      $this->Ini->Nm_lang['lang_shrt_days_wend'],
                                      $this->Ini->Nm_lang['lang_shrt_days_thud'],
                                      $this->Ini->Nm_lang['lang_shrt_days_frid'],
                                      $this->Ini->Nm_lang['lang_shrt_days_satd']);
      nm_gc($this->Ini->path_libs);
      $this->Ini->Gd_missing  = true;
      if(function_exists("getProdVersion"))
      {
         $_SESSION['scriptcase']['sc_prod_Version'] = str_replace(".", "", getProdVersion($this->Ini->path_libs));
         if(function_exists("gd_info"))
         {
            $this->Ini->Gd_missing = false;
         }
      }
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_trata_img.php", "C", "nm_trata_img") ; 

      if (is_file($this->Ini->path_aplicacao . 'form_productos_060522_mob_help.txt'))
      {
          $arr_link_webhelp = file($this->Ini->path_aplicacao . 'form_productos_060522_mob_help.txt');
          if ($arr_link_webhelp)
          {
              foreach ($arr_link_webhelp as $str_link_webhelp)
              {
                  $str_link_webhelp = trim($str_link_webhelp);
                  if ('form:' == substr($str_link_webhelp, 0, 5))
                  {
                      $arr_link_parts = explode(':', $str_link_webhelp);
                      if ('' != $arr_link_parts[1] && is_file($this->Ini->root . $this->Ini->path_help . $arr_link_parts[1]))
                      {
                          $this->url_webhelp = $this->Ini->path_help . $arr_link_parts[1];
                      }
                  }
              }
          }
      }

      if (is_dir($this->Ini->path_aplicacao . 'img'))
      {
          $Res_dir_img = @opendir($this->Ini->path_aplicacao . 'img');
          if ($Res_dir_img)
          {
              while (FALSE !== ($Str_arquivo = @readdir($Res_dir_img))) 
              {
                 if (@is_file($this->Ini->path_aplicacao . 'img/' . $Str_arquivo) && '.' != $Str_arquivo && '..' != $this->Ini->path_aplicacao . 'img/' . $Str_arquivo)
                 {
                     @unlink($this->Ini->path_aplicacao . 'img/' . $Str_arquivo);
                 }
              }
          }
          @closedir($Res_dir_img);
          rmdir($this->Ini->path_aplicacao . 'img');
      }

      if ($this->Embutida_proc)
      { 
          require_once($this->Ini->path_embutida . 'form_productos_060522/form_productos_060522_mob_erro.class.php');
      }
      else
      { 
          require_once($this->Ini->path_aplicacao . "form_productos_060522_mob_erro.class.php"); 
      }
      $this->Erro      = new form_productos_060522_mob_erro();
      $this->Erro->Ini = $this->Ini;
      $this->proc_fast_search = false;
      if ($nm_opc_lookup != "lookup" && $nm_opc_php != "formphp")
      { 
         if (empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao']))
         { 
             if ($this->idprod != "")   
             { 
                 $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao'] = "igual" ;  
             }   
         }   
      } 
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao']) && empty($this->nmgp_refresh_fields))
      {
          $this->nmgp_opcao = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao'];  
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao'] = "" ;  
          if ($this->nmgp_opcao == "edit_novo")  
          {
             $this->nmgp_opcao = "novo";
             $this->nm_flag_saida_novo = "S";
          }
      } 
      $this->nm_Start_new = false;
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['start'] == 'new')
      {
          $this->nmgp_opcao = "novo";
          $this->nm_Start_new = true;
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao'] = "novo";
          unset($_SESSION['scriptcase']['sc_apl_conf']['form_productos_060522_mob']['start']);
      }
      if ($this->nmgp_opcao == "igual")  
      {
          $this->nmgp_opc_ant = $this->nmgp_opcao;
      } 
      else
      {
          $this->nmgp_opc_ant = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_ant'];
      } 
      if ($this->nmgp_opcao == "novo")  
      {
          $this->nmgp_botoes['sc_btn_0'] = "on";
          $this->nmgp_botoes['sc_btn_1'] = "on";
          $this->nmgp_botoes['sc_btn_2'] = "on";
          $this->nmgp_botoes['escalas'] = "off";
          $this->nmgp_botoes['recalcular'] = "off";
      }
      elseif ($this->nmgp_opcao == "incluir")  
      {
          $this->nmgp_botoes['sc_btn_0'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes']['sc_btn_0'];
          $this->nmgp_botoes['sc_btn_1'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes']['sc_btn_1'];
          $this->nmgp_botoes['sc_btn_2'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes']['sc_btn_2'];
          $this->nmgp_botoes['escalas'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes']['escalas'];
          $this->nmgp_botoes['recalcular'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes']['recalcular'];
      }
      if ($this->nmgp_opcao == "recarga" || $this->nmgp_opcao == "muda_form")  
      {
          $this->nmgp_botoes = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes'];
          $this->Nav_permite_ret = 0 != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['inicio'];
          $this->Nav_permite_ava = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'] != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['final'];
      }
      else
      {
      }
      $this->nm_flag_iframe = false;
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form'])) 
      {
         $this->nmgp_dados_form = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form'];
      }
      if ($this->nmgp_opcao == "edit_novo")  
      {
          $this->nmgp_opcao = "novo";
          $this->nm_flag_saida_novo = "S";
      }
//
      $this->NM_case_insensitive = false;
      $this->sc_evento = $this->nmgp_opcao;
      $this->sc_insert_on = false;
      if (!isset($this->NM_ajax_flag) || ('validate_' != substr($this->NM_ajax_opcao, 0, 9) && 'add_new_line' != $this->NM_ajax_opcao && 'autocomp_' != substr($this->NM_ajax_opcao, 0, 9)))
      {
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_confcolor = $this->confcolor;
    $original_conftalla = $this->conftalla;
    $original_idiva = $this->idiva;
    $original_para_registro_fe = $this->para_registro_fe;
    $original_sabor = $this->sabor;
}
if (!isset($this->sc_temp_gnit)) {$this->sc_temp_gnit = (isset($_SESSION['gnit'])) ? $_SESSION['gnit'] : "";}
if (!isset($this->sc_temp_regimen_emp)) {$this->sc_temp_regimen_emp = (isset($_SESSION['regimen_emp'])) ? $_SESSION['regimen_emp'] : "";}
  if ($this->sc_temp_regimen_emp==0)
	{
	$this->idiva =1;
	$this->nmgp_cmp_hidden["idiva"] = "off"; $this->NM_ajax_info['fieldDisplay']['idiva'] = 'off';
	$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
	$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
	$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
	}

if($this->sc_temp_gnit!="88261176-7")
{
	$this->nmgp_cmp_hidden["para_registro_fe"] = "off"; $this->NM_ajax_info['fieldDisplay']['para_registro_fe'] = 'off';
}
else
{
	$this->nmgp_cmp_hidden["para_registro_fe"] = "on"; $this->NM_ajax_info['fieldDisplay']['para_registro_fe'] = 'on';
}
if (isset($this->sc_temp_regimen_emp)) { $_SESSION['regimen_emp'] = $this->sc_temp_regimen_emp;}
if (isset($this->sc_temp_gnit)) { $_SESSION['gnit'] = $this->sc_temp_gnit;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_confcolor != $this->confcolor || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor)))
    {
        $this->ajax_return_values_confcolor(true);
    }
    if (($original_conftalla != $this->conftalla || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla)))
    {
        $this->ajax_return_values_conftalla(true);
    }
    if (($original_idiva != $this->idiva || (isset($bFlagRead_idiva) && $bFlagRead_idiva)))
    {
        $this->ajax_return_values_idiva(true);
    }
    if (($original_para_registro_fe != $this->para_registro_fe || (isset($bFlagRead_para_registro_fe) && $bFlagRead_para_registro_fe)))
    {
        $this->ajax_return_values_para_registro_fe(true);
    }
    if (($original_sabor != $this->sabor || (isset($bFlagRead_sabor) && $bFlagRead_sabor)))
    {
        $this->ajax_return_values_sabor(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
      }
            if ('ajax_check_file' == $this->nmgp_opcao ){
                 ob_start(); 
                 include_once("../_lib/lib/php/nm_api.php"); 
            switch( $_POST['rsargs'] ){
               default:
                   echo 0;exit;
               break;
               }

            $out1_img_cache = $_SESSION['scriptcase']['form_productos_060522_mob']['glo_nm_path_imag_temp'] . $file_name;
            $orig_img = $_SESSION['scriptcase']['form_productos_060522_mob']['glo_nm_path_imag_temp']. '/sc_'.md5(date('YmdHis').basename($_POST['AjaxCheckImg'])).'.gif';
            copy($__file_download, $_SERVER['DOCUMENT_ROOT'].$orig_img);
            echo $orig_img . '_@@NM@@_';

            if(file_exists($out1_img_cache)){
                echo $out1_img_cache;
                exit;
            }
            copy($__file_download, $_SERVER['DOCUMENT_ROOT'].$out1_img_cache);
            $sc_obj_img = new nm_trata_img($_SERVER['DOCUMENT_ROOT'].$out1_img_cache, true);

            if(!empty($img_width) && !empty($img_height)){
                $sc_obj_img->setWidth($img_width);
                $sc_obj_img->setHeight($img_height);
            }
                $sc_obj_img->setManterAspecto(true);
            $sc_obj_img->createImg($_SERVER['DOCUMENT_ROOT'].$out1_img_cache);
            echo $out1_img_cache;
               exit;
            }
      if (isset($this->idprod)) { $this->nm_limpa_alfa($this->idprod); }
      if (isset($this->codigobar)) { $this->nm_limpa_alfa($this->codigobar); }
      if (isset($this->codigoprod)) { $this->nm_limpa_alfa($this->codigoprod); }
      if (isset($this->nompro)) { $this->nm_limpa_alfa($this->nompro); }
      if (isset($this->unimay)) { $this->nm_limpa_alfa($this->unimay); }
      if (isset($this->unimen)) { $this->nm_limpa_alfa($this->unimen); }
      if (isset($this->costomen)) { $this->nm_limpa_alfa($this->costomen); }
      if (isset($this->recmayamen)) { $this->nm_limpa_alfa($this->recmayamen); }
      if (isset($this->preciomen)) { $this->nm_limpa_alfa($this->preciomen); }
      if (isset($this->preciomen2)) { $this->nm_limpa_alfa($this->preciomen2); }
      if (isset($this->preciomen3)) { $this->nm_limpa_alfa($this->preciomen3); }
      if (isset($this->precio2)) { $this->nm_limpa_alfa($this->precio2); }
      if (isset($this->preciomay)) { $this->nm_limpa_alfa($this->preciomay); }
      if (isset($this->preciofull)) { $this->nm_limpa_alfa($this->preciofull); }
      if (isset($this->stockmen)) { $this->nm_limpa_alfa($this->stockmen); }
      if (isset($this->idgrup)) { $this->nm_limpa_alfa($this->idgrup); }
      if (isset($this->idpro1)) { $this->nm_limpa_alfa($this->idpro1); }
      if (isset($this->idpro2)) { $this->nm_limpa_alfa($this->idpro2); }
      if (isset($this->idiva)) { $this->nm_limpa_alfa($this->idiva); }
      if (isset($this->otro)) { $this->nm_limpa_alfa($this->otro); }
      if (isset($this->otro2)) { $this->nm_limpa_alfa($this->otro2); }
      if (isset($this->cod_cuenta)) { $this->nm_limpa_alfa($this->cod_cuenta); }
      if (isset($this->por_preciominimo)) { $this->nm_limpa_alfa($this->por_preciominimo); }
      if (isset($this->id_marca)) { $this->nm_limpa_alfa($this->id_marca); }
      if (isset($this->id_linea)) { $this->nm_limpa_alfa($this->id_linea); }
      if (isset($this->codigobar2)) { $this->nm_limpa_alfa($this->codigobar2); }
      if (isset($this->codigobar3)) { $this->nm_limpa_alfa($this->codigobar3); }
      if (isset($this->existencia)) { $this->nm_limpa_alfa($this->existencia); }
      if (isset($this->tipo_producto)) { $this->nm_limpa_alfa($this->tipo_producto); }
      if (isset($this->costo_prom)) { $this->nm_limpa_alfa($this->costo_prom); }
      if (isset($this->ubicacion)) { $this->nm_limpa_alfa($this->ubicacion); }
      if ($nm_opc_form_php == "formphp")
      { 
          if ($nm_call_php == "recalcular")
          { 
              $this->sc_btn_recalcular();
          } 
          $this->NM_close_db(); 
          exit;
      } 
      $Campos_Crit       = "";
      $Campos_erro       = "";
      $Campos_Falta      = array();
      $Campos_Erros      = array();
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          =  substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_edit'] = true;  
     if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select'])) 
     {
        $this->nmgp_dados_select = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select'];
     }
   }

   function loadFieldConfig()
   {
      $this->field_config = array();
      //-- otro2
      $this->field_config['otro2']               = array();
      $this->field_config['otro2']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['otro2']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['otro2']['symbol_dec'] = '';
      $this->field_config['otro2']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['otro2']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- stockmen
      $this->field_config['stockmen']               = array();
      $this->field_config['stockmen']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['stockmen']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['stockmen']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_num'];
      $this->field_config['stockmen']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['stockmen']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- costomen
      $this->field_config['costomen']               = array();
      $this->field_config['costomen']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['costomen']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['costomen']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['costomen']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['costomen']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['costomen']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- costo_prom
      $this->field_config['costo_prom']               = array();
      $this->field_config['costo_prom']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['costo_prom']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['costo_prom']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['costo_prom']['symbol_mon'] = '';
      $this->field_config['costo_prom']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['costo_prom']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- recmayamen
      $this->field_config['recmayamen']               = array();
      $this->field_config['recmayamen']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['recmayamen']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['recmayamen']['symbol_dec'] = '';
      $this->field_config['recmayamen']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['recmayamen']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- existencia
      $this->field_config['existencia']               = array();
      $this->field_config['existencia']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['existencia']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['existencia']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_num'];
      $this->field_config['existencia']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['existencia']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- por_preciominimo
      $this->field_config['por_preciominimo']               = array();
      $this->field_config['por_preciominimo']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['por_preciominimo']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['por_preciominimo']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_num'];
      $this->field_config['por_preciominimo']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['por_preciominimo']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- sugerido_mayor
      $this->field_config['sugerido_mayor']               = array();
      $this->field_config['sugerido_mayor']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['sugerido_mayor']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['sugerido_mayor']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['sugerido_mayor']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['sugerido_mayor']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['sugerido_mayor']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- sugerido_menor
      $this->field_config['sugerido_menor']               = array();
      $this->field_config['sugerido_menor']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['sugerido_menor']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['sugerido_menor']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['sugerido_menor']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['sugerido_menor']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['sugerido_menor']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- preciofull
      $this->field_config['preciofull']               = array();
      $this->field_config['preciofull']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['preciofull']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['preciofull']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['preciofull']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['preciofull']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['preciofull']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- precio2
      $this->field_config['precio2']               = array();
      $this->field_config['precio2']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['precio2']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['precio2']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['precio2']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['precio2']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['precio2']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- preciomay
      $this->field_config['preciomay']               = array();
      $this->field_config['preciomay']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['preciomay']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['preciomay']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['preciomay']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['preciomay']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['preciomay']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- preciomen
      $this->field_config['preciomen']               = array();
      $this->field_config['preciomen']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['preciomen']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['preciomen']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['preciomen']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['preciomen']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['preciomen']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- preciomen2
      $this->field_config['preciomen2']               = array();
      $this->field_config['preciomen2']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['preciomen2']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['preciomen2']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['preciomen2']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['preciomen2']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['preciomen2']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- preciomen3
      $this->field_config['preciomen3']               = array();
      $this->field_config['preciomen3']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['preciomen3']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['preciomen3']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['preciomen3']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['preciomen3']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['preciomen3']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- idprod
      $this->field_config['idprod']               = array();
      $this->field_config['idprod']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['idprod']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['idprod']['symbol_dec'] = '';
      $this->field_config['idprod']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['idprod']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- costomay
      $this->field_config['costomay']               = array();
      $this->field_config['costomay']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_val'];
      $this->field_config['costomay']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['unid_mont_group_digit'];
      $this->field_config['costomay']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_val'];
      $this->field_config['costomay']['symbol_mon'] = $_SESSION['scriptcase']['reg_conf']['monet_simb'];
      $this->field_config['costomay']['format_pos'] = $_SESSION['scriptcase']['reg_conf']['monet_f_pos'];
      $this->field_config['costomay']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['monet_f_neg'];
      //-- stockmay
      $this->field_config['stockmay']               = array();
      $this->field_config['stockmay']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['stockmay']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['stockmay']['symbol_dec'] = $_SESSION['scriptcase']['reg_conf']['dec_num'];
      $this->field_config['stockmay']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['stockmay']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- imconsumo
      $this->field_config['imconsumo']               = array();
      $this->field_config['imconsumo']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['imconsumo']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['imconsumo']['symbol_dec'] = '';
      $this->field_config['imconsumo']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['imconsumo']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- idcombo
      $this->field_config['idcombo']               = array();
      $this->field_config['idcombo']['symbol_grp'] = $_SESSION['scriptcase']['reg_conf']['grup_num'];
      $this->field_config['idcombo']['symbol_fmt'] = $_SESSION['scriptcase']['reg_conf']['num_group_digit'];
      $this->field_config['idcombo']['symbol_dec'] = '';
      $this->field_config['idcombo']['symbol_neg'] = $_SESSION['scriptcase']['reg_conf']['simb_neg'];
      $this->field_config['idcombo']['format_neg'] = $_SESSION['scriptcase']['reg_conf']['neg_num'];
      //-- ultima_compra
      $this->field_config['ultima_compra']                 = array();
      $this->field_config['ultima_compra']['date_format']  = $_SESSION['scriptcase']['reg_conf']['date_format'];
      $this->field_config['ultima_compra']['date_sep']     = $_SESSION['scriptcase']['reg_conf']['date_sep'];
      $this->field_config['ultima_compra']['date_display'] = "ddmmaaaa";
      $this->new_date_format('DT', 'ultima_compra');
      //-- ultima_venta
      $this->field_config['ultima_venta']                 = array();
      $this->field_config['ultima_venta']['date_format']  = $_SESSION['scriptcase']['reg_conf']['date_format'];
      $this->field_config['ultima_venta']['date_sep']     = $_SESSION['scriptcase']['reg_conf']['date_sep'];
      $this->field_config['ultima_venta']['date_display'] = "ddmmaaaa";
      $this->new_date_format('DT', 'ultima_venta');
   }

   function controle()
   {
        global $nm_url_saida, $teste_validade, 
               $glo_senha_protect, $nm_apl_dependente, $nm_form_submit, $sc_check_excl, $nm_opc_form_php, $nm_call_php, $nm_opc_lookup;


      $this->ini_controle();

      if ('' != $_SESSION['scriptcase']['change_regional_old'])
      {
          $_SESSION['scriptcase']['str_conf_reg'] = $_SESSION['scriptcase']['change_regional_old'];
          $this->Ini->regionalDefault($_SESSION['scriptcase']['str_conf_reg']);
          $this->loadFieldConfig();
          $this->nm_tira_formatacao();

          $_SESSION['scriptcase']['str_conf_reg'] = $_SESSION['scriptcase']['change_regional_new'];
          $this->Ini->regionalDefault($_SESSION['scriptcase']['str_conf_reg']);
          $this->loadFieldConfig();
          $guarda_formatado = $this->formatado;
          $this->nm_formatar_campos();
          $this->formatado = $guarda_formatado;

          $_SESSION['scriptcase']['change_regional_old'] = '';
          $_SESSION['scriptcase']['change_regional_new'] = '';
      }

      if ($nm_form_submit == 1 && ($this->nmgp_opcao == 'inicio' || $this->nmgp_opcao == 'igual'))
      {
          $this->nm_tira_formatacao();
      }
      if (!$this->NM_ajax_flag || 'alterar' != $this->nmgp_opcao || 'submit_form' != $this->NM_ajax_opcao)
      {
      }
//
//-----> 
//
      if ($this->NM_ajax_flag && 'validate_' == substr($this->NM_ajax_opcao, 0, 9))
      {
          if ('validate_codigoprod' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'codigoprod');
          }
          if ('validate_codigobar' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'codigobar');
          }
          if ('validate_nompro' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'nompro');
          }
          if ('validate_idgrup' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'idgrup');
          }
          if ('validate_idpro1' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'idpro1');
          }
          if ('validate_tipo_producto' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'tipo_producto');
          }
          if ('validate_idpro2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'idpro2');
          }
          if ('validate_otro' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'otro');
          }
          if ('validate_otro2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'otro2');
          }
          if ('validate_precio_editable' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'precio_editable');
          }
          if ('validate_maneja_tcs_lfs' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'maneja_tcs_lfs');
          }
          if ('validate_stockmen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'stockmen');
          }
          if ('validate_unidmaymen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'unidmaymen');
          }
          if ('validate_unimay' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'unimay');
          }
          if ('validate_unimen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'unimen');
          }
          if ('validate_unidad_ma' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'unidad_ma');
          }
          if ('validate_unidad_' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'unidad_');
          }
          if ('validate_multiple_escala' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'multiple_escala');
          }
          if ('validate_en_base_a' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'en_base_a');
          }
          if ('validate_costomen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'costomen');
          }
          if ('validate_costo_prom' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'costo_prom');
          }
          if ('validate_recmayamen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'recmayamen');
          }
          if ('validate_idiva' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'idiva');
          }
          if ('validate_existencia' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'existencia');
          }
          if ('validate_u_menor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'u_menor');
          }
          if ('validate_ubicacion' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'ubicacion');
          }
          if ('validate_activo' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'activo');
          }
          if ('validate_colores' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'colores');
          }
          if ('validate_confcolor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'confcolor');
          }
          if ('validate_tallas' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'tallas');
          }
          if ('validate_conftalla' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'conftalla');
          }
          if ('validate_sabores' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'sabores');
          }
          if ('validate_sabor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'sabor');
          }
          if ('validate_fecha_vencimiento' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'fecha_vencimiento');
          }
          if ('validate_lote' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'lote');
          }
          if ('validate_serial_codbarras' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'serial_codbarras');
          }
          if ('validate_relleno' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'relleno');
          }
          if ('validate_control_costo' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'control_costo');
          }
          if ('validate_por_preciominimo' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'por_preciominimo');
          }
          if ('validate_sugerido_mayor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'sugerido_mayor');
          }
          if ('validate_sugerido_menor' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'sugerido_menor');
          }
          if ('validate_preciofull' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'preciofull');
          }
          if ('validate_precio2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'precio2');
          }
          if ('validate_preciomay' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'preciomay');
          }
          if ('validate_preciomen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'preciomen');
          }
          if ('validate_preciomen2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'preciomen2');
          }
          if ('validate_preciomen3' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'preciomen3');
          }
          if ('validate_imagen' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'imagen');
          }
          if ('validate_cod_cuenta' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'cod_cuenta');
          }
          if ('validate_idprod' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'idprod');
          }
          if ('validate_id_marca' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'id_marca');
          }
          if ('validate_id_linea' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'id_linea');
          }
          if ('validate_codigobar2' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'codigobar2');
          }
          if ('validate_codigobar3' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'codigobar3');
          }
          if ('validate_para_registro_fe' == $this->NM_ajax_opcao)
          {
              $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros, 'para_registro_fe');
          }
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      if ($this->NM_ajax_flag && 'event_' == substr($this->NM_ajax_opcao, 0, 6))
      {
          $this->nm_tira_formatacao();
          if ('event_codigoprod_onchange' == $this->NM_ajax_opcao)
          {
              $this->codigoprod_onChange();
          }
          if ('event_colores_onchange' == $this->NM_ajax_opcao)
          {
              $this->colores_onChange();
          }
          if ('event_control_costo_onclick' == $this->NM_ajax_opcao)
          {
              $this->control_costo_onClick();
          }
          if ('event_en_base_a_onclick' == $this->NM_ajax_opcao)
          {
              $this->en_base_a_onClick();
          }
          if ('event_fecha_vencimiento_onchange' == $this->NM_ajax_opcao)
          {
              $this->fecha_vencimiento_onChange();
          }
          if ('event_idpro1_onchange' == $this->NM_ajax_opcao)
          {
              $this->idpro1_onChange();
          }
          if ('event_lote_onchange' == $this->NM_ajax_opcao)
          {
              $this->lote_onChange();
          }
          if ('event_maneja_tcs_lfs_onclick' == $this->NM_ajax_opcao)
          {
              $this->maneja_tcs_lfs_onClick();
          }
          if ('event_multiple_escala_onclick' == $this->NM_ajax_opcao)
          {
              $this->multiple_escala_onClick();
          }
          if ('event_otro_onchange' == $this->NM_ajax_opcao)
          {
              $this->otro_onChange();
          }
          if ('event_por_preciominimo_onchange' == $this->NM_ajax_opcao)
          {
              $this->por_preciominimo_onChange();
          }
          if ('event_recmayamen_onchange' == $this->NM_ajax_opcao)
          {
              $this->recmayamen_onChange();
          }
          if ('event_sabores_onchange' == $this->NM_ajax_opcao)
          {
              $this->sabores_onChange();
          }
          if ('event_serial_codbarras_onchange' == $this->NM_ajax_opcao)
          {
              $this->serial_codbarras_onChange();
          }
          if ('event_tallas_onchange' == $this->NM_ajax_opcao)
          {
              $this->tallas_onChange();
          }
          if ('event_unidad__onchange' == $this->NM_ajax_opcao)
          {
              $this->unidad__onChange();
          }
          if ('event_unidad_ma_onchange' == $this->NM_ajax_opcao)
          {
              $this->unidad_ma_onChange();
          }
          if ('event_unidmaymen_onclick' == $this->NM_ajax_opcao)
          {
              $this->unidmaymen_onClick();
          }
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      if ($this->NM_ajax_flag && 'autocomp_' == substr($this->NM_ajax_opcao, 0, 9))
      {
          if ('autocomp_idpro1' == $this->NM_ajax_opcao)
          {
              if (isset($_GET['term'])) {
                  $this->idpro1 = ($_SESSION['scriptcase']['charset'] != "UTF-8") ? NM_utf8_decode(sc_convert_encoding($_GET['term'], $_SESSION['scriptcase']['charset'], 'UTF-8')) : $_GET['term'];
              } else {
                  $this->idpro1 = '';
              }
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND documento + \"- \" + nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idtercero, concat(documento, \"- \",nombres) FROM terceros WHERE (proveedor='SI') AND concat(documento, \"- \",nombres) LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idtercero, documento&\"- \"&nombres FROM terceros WHERE (proveedor='SI') AND documento&\"- \"&nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND documento + \"- \" + nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }
   else
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro1), 1, -1) . "%' ORDER BY documento, nombres";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $AjaxLim = 0;
              $aResponse = array();
              foreach ($aLookup as $sLkpIndex => $aLkpList)
              {
                  $AjaxLim++;
                  if ($AjaxLim > 10)
                  {
                      break;
                  }
                  foreach ($aLkpList as $sLkpIndex => $sLkpValue)
                  {
                      $sLkpIndex = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpIndex);
                      $sLkpValue = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpValue);
                      $aResponse[] = array('text' => $sLkpValue, 'id' => $sLkpIndex);
                  }
              }
              $oJson = new Services_JSON();
              echo $oJson->encode(array('results' => $aResponse));
              exit;
          }
          if ('autocomp_idpro2' == $this->NM_ajax_opcao)
          {
              if (isset($_GET['term'])) {
                  $this->idpro2 = ($_SESSION['scriptcase']['charset'] != "UTF-8") ? NM_utf8_decode(sc_convert_encoding($_GET['term'], $_SESSION['scriptcase']['charset'], 'UTF-8')) : $_GET['term'];
              } else {
                  $this->idpro2 = '';
              }
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND documento + \"- \" + nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idtercero, concat(documento, \"- \",nombres) FROM terceros WHERE (proveedor='SI') AND concat(documento, \"- \",nombres) LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idtercero, documento&\"- \"&nombres FROM terceros WHERE (proveedor='SI') AND documento&\"- \"&nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND documento + \"- \" + nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }
   else
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND documento||\"- \"||nombres LIKE '%" . substr($this->Db->qstr($this->idpro2), 1, -1) . "%' ORDER BY documento, nombres";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $AjaxLim = 0;
              $aResponse = array();
              foreach ($aLookup as $sLkpIndex => $aLkpList)
              {
                  $AjaxLim++;
                  if ($AjaxLim > 10)
                  {
                      break;
                  }
                  foreach ($aLkpList as $sLkpIndex => $sLkpValue)
                  {
                      $sLkpIndex = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpIndex);
                      $sLkpValue = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpValue);
                      $aResponse[] = array('text' => $sLkpValue, 'id' => $sLkpIndex);
                  }
              }
              $oJson = new Services_JSON();
              echo $oJson->encode(array('results' => $aResponse));
              exit;
          }
          if ('autocomp_unidad_' == $this->NM_ajax_opcao)
          {
              if (isset($_GET['term'])) {
                  $this->unidad_ = ($_SESSION['scriptcase']['charset'] != "UTF-8") ? NM_utf8_decode(sc_convert_encoding($_GET['term'], $_SESSION['scriptcase']['charset'], 'UTF-8')) : $_GET['term'];
              } else {
                  $this->unidad_ = '';
              }
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT codigo_um, descripcion_um FROM unidades_medida WHERE descripcion_um LIKE '%" . substr($this->Db->qstr($this->unidad_), 1, -1) . "%' ORDER BY descripcion_um";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $AjaxLim = 0;
              $aResponse = array();
              foreach ($aLookup as $sLkpIndex => $aLkpList)
              {
                  $AjaxLim++;
                  if ($AjaxLim > 10)
                  {
                      break;
                  }
                  foreach ($aLkpList as $sLkpIndex => $sLkpValue)
                  {
                      $sLkpIndex = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpIndex);
                      $sLkpValue = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpValue);
                      $aResponse[] = array('text' => $sLkpValue, 'id' => $sLkpIndex);
                  }
              }
              $oJson = new Services_JSON();
              echo $oJson->encode(array('results' => $aResponse));
              exit;
          }
          if ('autocomp_unidad_ma' == $this->NM_ajax_opcao)
          {
              if (isset($_GET['term'])) {
                  $this->unidad_ma = ($_SESSION['scriptcase']['charset'] != "UTF-8") ? NM_utf8_decode(sc_convert_encoding($_GET['term'], $_SESSION['scriptcase']['charset'], 'UTF-8')) : $_GET['term'];
              } else {
                  $this->unidad_ma = '';
              }
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT codigo_um, descripcion_um FROM unidades_medida WHERE descripcion_um LIKE '%" . substr($this->Db->qstr($this->unidad_ma), 1, -1) . "%' ORDER BY descripcion_um";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $AjaxLim = 0;
              $aResponse = array();
              foreach ($aLookup as $sLkpIndex => $aLkpList)
              {
                  $AjaxLim++;
                  if ($AjaxLim > 10)
                  {
                      break;
                  }
                  foreach ($aLkpList as $sLkpIndex => $sLkpValue)
                  {
                      $sLkpIndex = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpIndex);
                      $sLkpValue = str_replace(array("\r", "\n"), array('', '<br />'), $sLkpValue);
                      $aResponse[] = array('text' => $sLkpValue, 'id' => $sLkpIndex);
                  }
              }
              $oJson = new Services_JSON();
              echo $oJson->encode(array('results' => $aResponse));
              exit;
          }
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      if (isset($this->sc_inline_call) && 'Y' == $this->sc_inline_call)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['inline_form_seq'] = $this->sc_seq_row;
          $this->nm_tira_formatacao();
      }
      if ($this->nmgp_opcao == "recarga" || $this->nmgp_opcao == "recarga_mobile" || $this->nmgp_opcao == "muda_form") 
      {
          $this->upload_img_doc($Campos_Crit, $Campos_Falta, $Campos_Erros) ; 
          $this->nm_tira_formatacao();
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['unimay']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->unimay = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['unimay'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['costomay']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->costomay = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['costomay'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['recmayamen']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->recmayamen = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['recmayamen'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['preciofull']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->preciofull = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['preciofull'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['precio2']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->precio2 = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['precio2'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['preciomay']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->preciomay = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['preciomay'];
          } 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['stockmen']) && !isset($this->nmgp_refresh_fields))
          { 
              $this->stockmen = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']['stockmen'];
          } 
          $nm_sc_sv_opcao = $this->nmgp_opcao; 
          $this->nmgp_opcao = "nada"; 
          $this->nm_acessa_banco();
          if ($this->NM_ajax_flag)
          {
              $this->ajax_return_values();
              form_productos_060522_mob_pack_ajax_response();
              exit;
          }
          $this->nm_formatar_campos();
          $this->nmgp_opcao = $nm_sc_sv_opcao; 
          $this->nm_gera_html();
          $this->NM_close_db(); 
          $this->nmgp_opcao = ""; 
          exit; 
      }
      if ($this->nmgp_opcao == "incluir" || $this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "excluir") 
      {
          $this->Valida_campos($Campos_Crit, $Campos_Falta, $Campos_Erros) ; 
          $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
          if ($Campos_Crit != "") 
          {
              $Campos_Crit = $this->Ini->Nm_lang['lang_errm_flds'] . ' ' . $Campos_Crit ; 
          }
          if ($Campos_Crit != "" || !empty($Campos_Falta) || $this->Campos_Mens_erro != "")
          {
              if ($this->NM_ajax_flag)
              {
                  form_productos_060522_mob_pack_ajax_response();
                  exit;
              }
              $campos_erro = $this->Formata_Erros($Campos_Crit, $Campos_Falta, $Campos_Erros, 4);
              $this->Campos_Mens_erro = ""; 
              $this->Erro->mensagem(__FILE__, __LINE__, "critica", $campos_erro); 
              $this->nmgp_opc_ant = $this->nmgp_opcao ; 
              if ($this->nmgp_opcao == "incluir" && $nm_apl_dependente == 1) 
              { 
                  $this->nm_flag_saida_novo = "S";; 
              }
              if ($this->nmgp_opcao == "incluir") 
              { 
                  $GLOBALS["erro_incl"] = 1; 
              }
              $this->nmgp_opcao = "nada" ; 
          }
      }
      elseif (isset($nm_form_submit) && 1 == $nm_form_submit && $this->nmgp_opcao != "menu_link" && $this->nmgp_opcao != "recarga_mobile")
      {
      }
//
      if ($this->nmgp_opcao != "nada")
      {
          $this->nm_acessa_banco();
      }
      else
      {
           if ($this->nmgp_opc_ant == "incluir") 
           { 
               $this->nm_proc_onload(false);
           }
           else
           { 
              $this->nm_guardar_campos();
           }
      }
      if ($this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form" && !$this->Apl_com_erro)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['recarga'] = $this->nmgp_opcao;
      }
      if ($this->NM_ajax_flag && 'navigate_form' == $this->NM_ajax_opcao)
      {
          $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_confcolor = $this->confcolor;
    $original_conftalla = $this->conftalla;
    $original_idiva = $this->idiva;
    $original_sabor = $this->sabor;
}
if (!isset($this->sc_temp_regimen_emp)) {$this->sc_temp_regimen_emp = (isset($_SESSION['regimen_emp'])) ? $_SESSION['regimen_emp'] : "";}
  if ($this->sc_temp_regimen_emp==0)
	{
	$this->idiva =1;
	$this->nmgp_cmp_hidden["idiva"] = "off"; $this->NM_ajax_info['fieldDisplay']['idiva'] = 'off';
	$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
	$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
	$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
	}
if (isset($this->sc_temp_regimen_emp)) { $_SESSION['regimen_emp'] = $this->sc_temp_regimen_emp;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_confcolor != $this->confcolor || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor)))
    {
        $this->ajax_return_values_confcolor(true);
    }
    if (($original_conftalla != $this->conftalla || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla)))
    {
        $this->ajax_return_values_conftalla(true);
    }
    if (($original_idiva != $this->idiva || (isset($bFlagRead_idiva) && $bFlagRead_idiva)))
    {
        $this->ajax_return_values_idiva(true);
    }
    if (($original_sabor != $this->sabor || (isset($bFlagRead_sabor) && $bFlagRead_sabor)))
    {
        $this->ajax_return_values_sabor(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
          $this->ajax_return_values();
          $this->ajax_add_parameters();
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      $this->nm_formatar_campos();
      if ($this->NM_ajax_flag)
      {
          $this->NM_ajax_info['result'] = 'OK';
          if ('alterar' == $this->NM_ajax_info['param']['nmgp_opcao'])
          {
              $this->NM_ajax_info['msgDisplay'] = NM_charset_to_utf8($this->Ini->Nm_lang['lang_othr_ajax_frmu']);
          }
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      $this->nm_gera_html();
      $this->NM_close_db(); 
      $this->nmgp_opcao = ""; 
      if ($this->Change_Menu)
      {
          $apl_menu  = $_SESSION['scriptcase']['menu_atual'];
          $Arr_rastro = array();
          if (isset($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) && count($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu]) > 1)
          {
              foreach ($_SESSION['scriptcase']['menu_apls'][$apl_menu][$this->sc_init_menu] as $menu => $apls)
              {
                 $Arr_rastro[] = "'<a href=\"" . $apls['link'] . "?script_case_init=" . $this->sc_init_menu . "\" target=\"#NMIframe#\">" . $apls['label'] . "</a>'";
              }
              $ult_apl = count($Arr_rastro) - 1;
              unset($Arr_rastro[$ult_apl]);
              $rastro = implode(",", $Arr_rastro);
?>
  <script type="text/javascript">
     link_atual = new Array (<?php echo $rastro ?>);
     parent.writeFastMenu(link_atual);
  </script>
<?php
          }
          else
          {
?>
  <script type="text/javascript">
     parent.clearFastMenu();
  </script>
<?php
          }
      }
   }
  function html_export_print($nm_arquivo_html, $nmgp_password)
  {
      $Html_password = "";
          $Arq_base  = $this->Ini->root . $this->Ini->path_imag_temp . $nm_arquivo_html;
          $Parm_pass = ($Html_password != "") ? " -p" : "";
          $Zip_name = "sc_prt_" . date("YmdHis") . "_" . rand(0, 1000) . "form_productos_060522_mob.zip";
          $Arq_htm = $this->Ini->path_imag_temp . "/" . $Zip_name;
          $Arq_zip = $this->Ini->root . $Arq_htm;
          $Zip_f     = (FALSE !== strpos($Arq_zip, ' ')) ? " \"" . $Arq_zip . "\"" :  $Arq_zip;
          $Arq_input = (FALSE !== strpos($Arq_base, ' ')) ? " \"" . $Arq_base . "\"" :  $Arq_base;
           if (is_file($Arq_zip)) {
               unlink($Arq_zip);
           }
           $str_zip = "";
           if (FALSE !== strpos(strtolower(php_uname()), 'windows')) 
           {
               chdir($this->Ini->path_third . "/zip/windows");
               $str_zip = "zip.exe " . strtoupper($Parm_pass) . " -j " . $Html_password . " " . $Zip_f . " " . $Arq_input;
           }
           elseif (FALSE !== strpos(strtolower(php_uname()), 'linux')) 
           {
                if (FALSE !== strpos(strtolower(php_uname()), 'i686')) 
                {
                    chdir($this->Ini->path_third . "/zip/linux-i386/bin");
                }
                else
                {
                    chdir($this->Ini->path_third . "/zip/linux-amd64/bin");
                }
               $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $Arq_input;
           }
           elseif (FALSE !== strpos(strtolower(php_uname()), 'darwin'))
           {
               chdir($this->Ini->path_third . "/zip/mac/bin");
               $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $Arq_input;
           }
           if (!empty($str_zip)) {
               exec($str_zip);
           }
           // ----- ZIP log
           $fp = @fopen(trim(str_replace(array(".zip",'"'), array(".log",""), $Zip_f)), 'w');
           if ($fp)
           {
               @fwrite($fp, $str_zip . "\r\n\r\n");
               @fclose($fp);
           }
           foreach ($this->Ini->Img_export_zip as $cada_img_zip)
           {
               $str_zip      = "";
              $cada_img_zip = '"' . $cada_img_zip . '"';
               if (FALSE !== strpos(strtolower(php_uname()), 'windows')) 
               {
                   $str_zip = "zip.exe " . strtoupper($Parm_pass) . " -j -u " . $Html_password . " " . $Zip_f . " " . $cada_img_zip;
               }
               elseif (FALSE !== strpos(strtolower(php_uname()), 'linux')) 
               {
                   $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $cada_img_zip;
               }
               elseif (FALSE !== strpos(strtolower(php_uname()), 'darwin'))
               {
                   $str_zip = "./7za " . $Parm_pass . $Html_password . " a " . $Zip_f . " " . $cada_img_zip;
               }
               if (!empty($str_zip)) {
                   exec($str_zip);
               }
               // ----- ZIP log
               $fp = @fopen(trim(str_replace(array(".zip",'"'), array(".log",""), $Zip_f)), 'a');
               if ($fp)
               {
                   @fwrite($fp, $str_zip . "\r\n\r\n");
                   @fclose($fp);
               }
           }
           if (is_file($Arq_zip)) {
               unlink($Arq_base);
           } 
          $path_doc_md5 = md5($Arq_htm);
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob'][$path_doc_md5][0] = $Arq_htm;
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob'][$path_doc_md5][1] = $Zip_name;
?>
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo strip_tags("Editar producto") ?></TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php

if (isset($_SESSION['scriptcase']['device_mobile']) && $_SESSION['scriptcase']['device_mobile'] && $_SESSION['scriptcase']['display_mobile'])
{
?>
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
}

?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_form . '/' . $this->Ini->Str_btn_form ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->path_prod; ?>/third/font-awesome/css/all.min.css" /> 
  <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
</HEAD>
<BODY class="scExportPage">
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: top">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">PRINT</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
   <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

   <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

   <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "absmiddle", "", "0", $this->Ini->path_botoes, "", "", "", "", "");?>

    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo  $this->form_encode_input($Arq_htm) ?>" target="_self" style="display: none"> 
</form>
<form name="Fdown" method="get" action="form_productos_060522_mob_download.php" target="_self" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="form_productos_060522_mob"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<form name="F0" method=post action="form_productos_060522_mob.php" target="_self" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nmgp_opcao" value="<?php echo $this->nmgp_opcao ?>"> 
</form> 
         </BODY>
         </HTML>
<?php
          exit;
  }
//
//--------------------------------------------------------------------------------------
   function NM_has_trans()
   {
       return !in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access);
   }
//
//--------------------------------------------------------------------------------------
   function NM_commit_db()
   {
       if ($this->Ini->sc_tem_trans_banco && !$this->Embutida_proc)
       { 
           $this->Db->CommitTrans(); 
           $this->Ini->sc_tem_trans_banco = false;
       } 
   }
//
//--------------------------------------------------------------------------------------
   function NM_rollback_db()
   {
       if ($this->Ini->sc_tem_trans_banco && !$this->Embutida_proc)
       { 
           $this->Db->RollbackTrans(); 
           $this->Ini->sc_tem_trans_banco = false;
       } 
   }
//
//--------------------------------------------------------------------------------------
   function NM_close_db()
   {
       if ($this->Db && !$this->Embutida_proc)
       { 
           $this->Db->Close(); 
       } 
   }
   function sc_btn_recalcular() 
   {
        global $nm_url_saida, $teste_validade, 
               $glo_senha_protect, $nm_apl_dependente, $nm_form_submit, $sc_check_excl, $nm_opc_form_php, $nm_call_php, $nm_opc_lookup;
 
     ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">

<html<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
 <head>
    <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php

      if (isset($_SESSION['scriptcase']['device_mobile']) && $_SESSION['scriptcase']['device_mobile'] && $_SESSION['scriptcase']['display_mobile'])
      {
?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
      }

?>
        <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
    <SCRIPT type="text/javascript">
      var sc_pathToTB = '<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/';
      var sc_tbLangClose = "<?php echo html_entity_decode($this->Ini->Nm_lang["lang_tb_close"], ENT_COMPAT, $_SESSION["scriptcase"]["charset"]) ?>";
      var sc_tbLangEsc = "<?php echo html_entity_decode($this->Ini->Nm_lang["lang_tb_esc"], ENT_COMPAT, $_SESSION["scriptcase"]["charset"]) ?>";
      var sc_userSweetAlertDisplayed = false;
    </SCRIPT>
    <SCRIPT type="text/javascript" src="../_lib/lib/js/jquery-3.6.0.min.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/jquery_plugin/malsup-blockui/jquery.blockUI.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/jquery_plugin/thickbox/thickbox-compressed.js"></SCRIPT>
<?php
include_once("form_productos_060522_mob_sajax_js.php");
?>
 <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->path_link ?>_lib/css/<?php echo $this->Ini->str_schema_all ?>_sweetalert.css" />
 <SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/sweetalert/sweetalert2.all.min.js"></SCRIPT>
 <SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/sweetalert/polyfill.min.js"></SCRIPT>
 <script type="text/javascript" src="../_lib/lib/js/frameControl.js"></script>
    <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_form.css" />
    <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_form<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" />
  <?php 
  if(isset($this->Ini->str_google_fonts) && !empty($this->Ini->str_google_fonts)) 
  { 
  ?> 
  <link href="<?php echo $this->Ini->str_google_fonts ?>" rel="stylesheet" /> 
  <?php 
  } 
  ?> 
 </head>
  <body class="scFormPage">
      <table class="scFormTabela" align="center"><tr><td>
<?php
      $varloc_btn_php = array();
      $nmgp_opcao_saida_php = "igual";
      $nmgp_opc_ant_saida_php = "";
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_ant'] == "novo" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_ant'] == "incluir")
      {
          $nmgp_opc_ant_saida_php = "novo";
          $nmgp_opcao_saida_php   = "recarga";
      }
      else
      {
          if (!isset($this->idprod) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod']))
          {
              $varloc_btn_php['idprod'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod'];
          }
          if (!isset($this->unidmaymen) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['unidmaymen']))
          {
              $varloc_btn_php['unidmaymen'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['unidmaymen'];
          }
          if (!isset($this->recmayamen) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen']))
          {
              $varloc_btn_php['recmayamen'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen'];
          }
          if (!isset($this->multiple_escala) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['multiple_escala']))
          {
              $varloc_btn_php['multiple_escala'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['multiple_escala'];
          }
          if (!isset($this->idprod) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod']))
          {
              $varloc_btn_php['idprod'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod'];
          }
          if (!isset($this->idprod) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod']))
          {
              $varloc_btn_php['idprod'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->recmayamen) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen']))
          {
              $varloc_btn_php['recmayamen'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->recmayamen) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen']))
          {
              $varloc_btn_php['recmayamen'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['recmayamen'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->existencia) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia']))
          {
              $varloc_btn_php['existencia'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['existencia'];
          }
          if (!isset($this->idprod) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod']))
          {
              $varloc_btn_php['idprod'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form']['idprod'];
          }
      }
      $nm_f_saida = "form_productos_060522_mob.php";
      nm_limpa_numero($this->otro2, $this->field_config['otro2']['symbol_grp']) ; 
      if (!empty($this->field_config['stockmen']['symbol_dec']))
      {
          nm_limpa_valor($this->stockmen, $this->field_config['stockmen']['symbol_dec'], $this->field_config['stockmen']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['costomen']['symbol_dec']))
      {
          $this->sc_remove_currency($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp'], $this->field_config['costomen']['symbol_mon']); 
          nm_limpa_valor($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['costo_prom']['symbol_dec']))
      {
          $this->sc_remove_currency($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp'], $this->field_config['costo_prom']['symbol_mon']); 
          nm_limpa_valor($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp']) ; 
      }
      nm_limpa_numero($this->recmayamen, $this->field_config['recmayamen']['symbol_grp']) ; 
      if (!empty($this->field_config['existencia']['symbol_dec']))
      {
          nm_limpa_valor($this->existencia, $this->field_config['existencia']['symbol_dec'], $this->field_config['existencia']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['por_preciominimo']['symbol_dec']))
      {
          nm_limpa_valor($this->por_preciominimo, $this->field_config['por_preciominimo']['symbol_dec'], $this->field_config['por_preciominimo']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['sugerido_mayor']['symbol_dec']))
      {
          $this->sc_remove_currency($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp'], $this->field_config['sugerido_mayor']['symbol_mon']); 
          nm_limpa_valor($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['sugerido_menor']['symbol_dec']))
      {
          $this->sc_remove_currency($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp'], $this->field_config['sugerido_menor']['symbol_mon']); 
          nm_limpa_valor($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['preciofull']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp'], $this->field_config['preciofull']['symbol_mon']); 
          nm_limpa_valor($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['precio2']['symbol_dec']))
      {
          $this->sc_remove_currency($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp'], $this->field_config['precio2']['symbol_mon']); 
          nm_limpa_valor($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['preciomay']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp'], $this->field_config['preciomay']['symbol_mon']); 
          nm_limpa_valor($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['preciomen']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp'], $this->field_config['preciomen']['symbol_mon']); 
          nm_limpa_valor($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['preciomen2']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp'], $this->field_config['preciomen2']['symbol_mon']); 
          nm_limpa_valor($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp']) ; 
      }
      if (!empty($this->field_config['preciomen3']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp'], $this->field_config['preciomen3']['symbol_mon']); 
          nm_limpa_valor($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp']) ; 
      }
      nm_limpa_numero($this->idprod, $this->field_config['idprod']['symbol_grp']) ; 
      foreach ($varloc_btn_php as $cmp => $val_cmp)
      {
          $this->$cmp = $val_cmp;
      }
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  $vStock=0;
 
      $nm_select = "select sum(cant_umen) as can_ume from inventario where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->d_pr = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->d_pr[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->d_pr = false;
          $this->d_pr_erro = $this->Db->ErrorMsg();
      } 
     } 
;
if(isset($this->d_pr[0][0]))
	{
	$vStock=$this->d_pr[0][0];
	}

if($this->unidmaymen =='SI' and $this->recmayamen >=1)
	{
	if($this->multiple_escala =='SI')
		{
		 
      $nm_select = "select factor, factor_base, representacion_factor from escalas_productos where id_producto='".$this->idprod ."' order by factor DESC Limit 1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vEscal = array();
      $this->vescal = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vEscal[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vescal[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vEscal = false;
          $this->vEscal_erro = $this->Db->ErrorMsg();
          $this->vescal = false;
          $this->vescal_erro = $this->Db->ErrorMsg();
      } 
     } 
;
		if(isset($this->vescal[0][0]))
			{
			 
      $nm_select = "select sum(cant_umen) as can_ume from inventario where idpro='".$this->idprod ."' group by idpro"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->data = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->data[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->data = false;
          $this->data_erro = $this->Db->ErrorMsg();
      } 
     } 
;
			if(isset($this->data[0][0]) and $this->data[0][0]!=0)
				{
				$this->existencia 	= $this->data[0][0];
				}
			elseif(isset($this->data[0][0]) and $this->data[0][0]==0)
				{
				$this->existencia 	= $this->data[0][0]*$vescala[0][0]*$vescala[0][1];
				}
			}
		else
			{
			$this->existencia =$vStock*$this->recmayamen ;
			}
		}
	else
		{
		$this->existencia =$vStock*$this->recmayamen ;
		}
	}
else
	{
	$this->existencia =$vStock;
	}
echo "LA EXISTENCIA EN MINIMA UNIDAD ES: ",$this->existencia ;

     $nm_select ="update productos set existencia='".$this->existencia ."' where idprod='".$this->idprod ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
    echo ob_get_clean();
?>
      </td></tr><tr><td align="center">
      <form name="FPHP" method="post" 
                        action="<?php echo $nm_f_saida ?>" 
                        target="_self">
      <input type=hidden name="nmgp_opcao" value=""/>
      <input type=hidden name="script_case_init" value="<?php  echo $this->form_encode_input($this->Ini->sc_page); ?>"/>
      <input type=hidden name="idprod" value="<?php echo $this->form_encode_input($this->idprod) ?>"/>
      <input type=hidden name="nmgp_opcao" value="<?php echo $this->form_encode_input($nmgp_opcao_saida_php); ?>"/>
      <input type=hidden name="nmgp_opc_ant" value="<?php echo $this->form_encode_input($nmgp_opc_ant_saida_php); ?>"/>
      <input type=submit name="nmgp_bok" value="<?php echo $this->Ini->Nm_lang['lang_btns_cfrm'] ?>"/>
      </form>
      </td></tr></table>
      </body>
      </html>
<?php
       if (isset($this->redir_modal) && !empty($this->redir_modal))
       {
           echo "<script type=\"text/javascript\">" . $this->redir_modal . "</script>";
           $this->redir_modal = "";
       }
   }
//
//--------------------------------------------------------------------------------------
   function Formata_Erros($Campos_Crit, $Campos_Falta, $Campos_Erros, $mode = 3) 
   {
       switch ($mode)
       {
           case 1:
               $campos_erro = array();
               if (!empty($Campos_Crit))
               {
                   $campos_erro[] = $Campos_Crit;
               }
               if (!empty($Campos_Falta))
               {
                   $campos_erro[] = $this->Formata_Campos_Falta($Campos_Falta);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_erro[] = $this->Campos_Mens_erro;
               }
               return implode('<br />', $campos_erro);
               break;

           case 2:
               $campos_erro = array();
               if (!empty($Campos_Crit))
               {
                   $campos_erro[] = $Campos_Crit;
               }
               if (!empty($Campos_Falta))
               {
                   $campos_erro[] = $this->Formata_Campos_Falta($Campos_Falta, true);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_erro[] = $this->Campos_Mens_erro;
               }
               return implode('<br />', $campos_erro);
               break;

           case 3:
               $campos_erro = array();
               if (!empty($Campos_Erros))
               {
                   $campos_erro[] = $this->Formata_Campos_Erros($Campos_Erros);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_mens_erro = str_replace(array('<br />', '<br>', '<BR />'), array('<BR>', '<BR>', '<BR>'), $this->Campos_Mens_erro);
                   $campos_mens_erro = explode('<BR>', $campos_mens_erro);
                   foreach ($campos_mens_erro as $msg_erro)
                   {
                       if ('' != $msg_erro && !in_array($msg_erro, $campos_erro))
                       {
                           $campos_erro[] = $msg_erro;
                       }
                   }
               }
               return implode('<br />', $campos_erro);
               break;

           case 4:
               $campos_erro = array();
               if (!empty($Campos_Erros))
               {
                   $campos_erro[] = $this->Formata_Campos_Erros_SweetAlert($Campos_Erros);
               }
               if (!empty($this->Campos_Mens_erro))
               {
                   $campos_mens_erro = str_replace(array('<br />', '<br>', '<BR />'), array('<BR>', '<BR>', '<BR>'), $this->Campos_Mens_erro);
                   $campos_mens_erro = explode('<BR>', $campos_mens_erro);
                   foreach ($campos_mens_erro as $msg_erro)
                   {
                       if ('' != $msg_erro && !in_array($msg_erro, $campos_erro))
                       {
                           $campos_erro[] = $msg_erro;
                       }
                   }
               }
               return implode('<br />', $campos_erro);
               break;
       }
   }

   function Formata_Campos_Falta($Campos_Falta, $table = false) 
   {
       $Campos_Falta = array_unique($Campos_Falta);

       if (!$table)
       {
           return $this->Ini->Nm_lang['lang_errm_reqd'] . ' ' . implode('; ', $Campos_Falta);
       }

       $aCols  = array();
       $iTotal = sizeof($Campos_Falta);
       $iCols  = 6 > $iTotal ? 1 : (11 > $iTotal ? 2 : (16 > $iTotal ? 3 : 4));
       $iItems = ceil($iTotal / $iCols);
       $iNowC  = 0;
       $iNowI  = 0;

       foreach ($Campos_Falta as $campo)
       {
           $aCols[$iNowC][] = $campo;
           if ($iItems == ++$iNowI)
           {
               $iNowC++;
               $iNowI = 0;
           }
       }

       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';
       $sError .= '<tr>';
       $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Ini->Nm_lang['lang_errm_reqd'] . '</td>';
       foreach ($aCols as $aCol)
       {
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', $aCol) . '</td>';
       }
       $sError .= '</tr>';
       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Crit($Campos_Crit, $table = false) 
   {
       $Campos_Crit = array_unique($Campos_Crit);

       if (!$table)
       {
           return $this->Ini->Nm_lang['lang_errm_flds'] . ' ' . implode('; ', $Campos_Crit);
       }

       $aCols  = array();
       $iTotal = sizeof($Campos_Crit);
       $iCols  = 6 > $iTotal ? 1 : (11 > $iTotal ? 2 : (16 > $iTotal ? 3 : 4));
       $iItems = ceil($iTotal / $iCols);
       $iNowC  = 0;
       $iNowI  = 0;

       foreach ($Campos_Crit as $campo)
       {
           $aCols[$iNowC][] = $campo;
           if ($iItems == ++$iNowI)
           {
               $iNowC++;
               $iNowI = 0;
           }
       }

       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';
       $sError .= '<tr>';
       $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Ini->Nm_lang['lang_errm_flds'] . '</td>';
       foreach ($aCols as $aCol)
       {
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', $aCol) . '</td>';
       }
       $sError .= '</tr>';
       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Erros($Campos_Erros) 
   {
       $sError  = '<table style="border-collapse: collapse; border-width: 0px">';

       foreach ($Campos_Erros as $campo => $erros)
       {
           $sError .= '<tr>';
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0; vertical-align: top; white-space: nowrap">' . $this->Recupera_Nome_Campo($campo) . ':</td>';
           $sError .= '<td class="scFormErrorMessageFont" style="padding: 0 6px; vertical-align: top; white-space: nowrap">' . implode('<br />', array_unique($erros)) . '</td>';
           $sError .= '</tr>';
       }

       $sError .= '</table>';

       return $sError;
   }

   function Formata_Campos_Erros_SweetAlert($Campos_Erros) 
   {
       $sError  = '';

       foreach ($Campos_Erros as $campo => $erros)
       {
           $sError .= $this->Recupera_Nome_Campo($campo) . ': ' . implode('<br />', array_unique($erros)) . '<br />';
       }

       return $sError;
   }

   function Recupera_Nome_Campo($campo) 
   {
       switch($campo)
       {
           case 'codigoprod':
               return "Código producto";
               break;
           case 'codigobar':
               return "Código barras";
               break;
           case 'nompro':
               return "Descripción";
               break;
           case 'idgrup':
               return "Familia o grupo";
               break;
           case 'idpro1':
               return "Proveedor principal";
               break;
           case 'tipo_producto':
               return "Tipo Producto";
               break;
           case 'idpro2':
               return "Proveedor secundario";
               break;
           case 'otro':
               return "Promoción";
               break;
           case 'otro2':
               return "Descuento %";
               break;
           case 'precio_editable':
               return "Precio editable en ventas";
               break;
           case 'maneja_tcs_lfs':
               return "Maneja TCS ó LFS";
               break;
           case 'stockmen':
               return "Existencia:";
               break;
           case 'unidmaymen':
               return "Maneja unidad Mayor y Menor";
               break;
           case 'unimay':
               return "Unidad Mayor";
               break;
           case 'unimen':
               return "Unidad";
               break;
           case 'unidad_ma':
               return "Unidad Mayor";
               break;
           case 'unidad_':
               return "Unidad P";
               break;
           case 'multiple_escala':
               return "Multiple Escala";
               break;
           case 'en_base_a':
               return "En base a Unidad:";
               break;
           case 'costomen':
               return "Costo compra";
               break;
           case 'costo_prom':
               return "Costo Promedio";
               break;
           case 'recmayamen':
               return "Factor multiplicador";
               break;
           case 'idiva':
               return "Impuesto";
               break;
           case 'existencia':
               return "Existencia en unidad menor:";
               break;
           case 'u_menor':
               return "U. Menor";
               break;
           case 'ubicacion':
               return "Ubicación";
               break;
           case 'activo':
               return "Activo";
               break;
           case 'colores':
               return "Maneja Colores?";
               break;
           case 'confcolor':
               return "Configurar Colores";
               break;
           case 'tallas':
               return "Maneja Tallas o tamaños?";
               break;
           case 'conftalla':
               return "Configurar Tallas o tamaños";
               break;
           case 'sabores':
               return "Maneja Sabores?";
               break;
           case 'sabor':
               return "Configurar Sabores";
               break;
           case 'fecha_vencimiento':
               return "Fecha Vencimiento";
               break;
           case 'lote':
               return "Lote";
               break;
           case 'serial_codbarras':
               return "Serial Codbarras";
               break;
           case 'relleno':
               return "";
               break;
           case 'control_costo':
               return "Precio mínimo de Venta";
               break;
           case 'por_preciominimo':
               return "% Ganancia";
               break;
           case 'sugerido_mayor':
               return "$ Sugerido U. Mayor";
               break;
           case 'sugerido_menor':
               return "$ Sugerido U. Menor";
               break;
           case 'preciofull':
               return "Preciofull";
               break;
           case 'precio2':
               return "Precio especial";
               break;
           case 'preciomay':
               return "Precio venta al mayor";
               break;
           case 'preciomen':
               return "Precio venta menudeo";
               break;
           case 'preciomen2':
               return "Precio especial menudeo";
               break;
           case 'preciomen3':
               return "Precio al mayor menudeo";
               break;
           case 'imagen':
               return "Imagen";
               break;
           case 'cod_cuenta':
               return "Grupo Contable";
               break;
           case 'idprod':
               return "Idprod";
               break;
           case 'id_marca':
               return "Marca";
               break;
           case 'id_linea':
               return "Linea";
               break;
           case 'codigobar2':
               return "Código barras 2";
               break;
           case 'codigobar3':
               return "Código barras 3";
               break;
           case 'para_registro_fe':
               return "Para Registro Fe";
               break;
           case 'costomay':
               return "Costo compra al mayor";
               break;
           case 'stockmay':
               return "Stock unidad mayor";
               break;
           case 'imagenprod':
               return "Imágen";
               break;
           case 'imconsumo':
               return "Imconsumo";
               break;
           case 'escombo':
               return "Escombo";
               break;
           case 'idcombo':
               return "Idcombo";
               break;
           case 'fecha_fab':
               return "Fecha Fab";
               break;
           case 'ultima_compra':
               return "Ultima Compra";
               break;
           case 'n_ultcompra':
               return "N Ultcompra";
               break;
           case 'ultima_venta':
               return "Ultima Venta";
               break;
           case 'n_ultventa':
               return "N Ultventa";
               break;
           case 'nube':
               return "Nube";
               break;
       }

       return $campo;
   }

   function dateDefaultFormat()
   {
       if (isset($this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_format']))
       {
           $sDate = str_replace('yyyy', 'Y', $this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_format']);
           $sDate = str_replace('mm',   'm', $sDate);
           $sDate = str_replace('dd',   'd', $sDate);
           return substr(chunk_split($sDate, 1, $this->Ini->Nm_conf_reg[$this->Ini->str_conf_reg]['data_sep']), 0, -1);
       }
       elseif ('en_us' == $this->Ini->str_lang)
       {
           return 'm/d/Y';
       }
       else
       {
           return 'd/m/Y';
       }
   } // dateDefaultFormat

//
//--------------------------------------------------------------------------------------
   function Valida_campos(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros, $filtro = '') 
   {
     global $nm_browser, $teste_validade;
     if (is_array($filtro) && empty($filtro)) {
         $filtro = '';
     }
//---------------------------------------------------------
     $this->sc_force_zero = array();

     if (!is_array($filtro) && '' == $filtro && isset($this->nm_form_submit) && '1' == $this->nm_form_submit && $this->scCsrfGetToken() != $this->csrf_token)
     {
          $this->Campos_Mens_erro .= (empty($this->Campos_Mens_erro)) ? "" : "<br />";
          $this->Campos_Mens_erro .= "CSRF: " . $this->Ini->Nm_lang['lang_errm_ajax_csrf'];
          if ($this->NM_ajax_flag)
          {
              if (!isset($this->NM_ajax_info['errList']['geral_form_productos_060522_mob']) || !is_array($this->NM_ajax_info['errList']['geral_form_productos_060522_mob']))
              {
                  $this->NM_ajax_info['errList']['geral_form_productos_060522_mob'] = array();
              }
              $this->NM_ajax_info['errList']['geral_form_productos_060522_mob'][] = "CSRF: " . $this->Ini->Nm_lang['lang_errm_ajax_csrf'];
          }
     }
      if ((!is_array($filtro) && ('' == $filtro || 'codigoprod' == $filtro)) || (is_array($filtro) && in_array('codigoprod', $filtro)))
        $this->ValidateField_codigoprod($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'codigobar' == $filtro)) || (is_array($filtro) && in_array('codigobar', $filtro)))
        $this->ValidateField_codigobar($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'nompro' == $filtro)) || (is_array($filtro) && in_array('nompro', $filtro)))
        $this->ValidateField_nompro($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'idgrup' == $filtro)) || (is_array($filtro) && in_array('idgrup', $filtro)))
        $this->ValidateField_idgrup($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'idpro1' == $filtro)) || (is_array($filtro) && in_array('idpro1', $filtro)))
        $this->ValidateField_idpro1($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'tipo_producto' == $filtro)) || (is_array($filtro) && in_array('tipo_producto', $filtro)))
        $this->ValidateField_tipo_producto($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'idpro2' == $filtro)) || (is_array($filtro) && in_array('idpro2', $filtro)))
        $this->ValidateField_idpro2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'otro' == $filtro)) || (is_array($filtro) && in_array('otro', $filtro)))
        $this->ValidateField_otro($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'otro2' == $filtro)) || (is_array($filtro) && in_array('otro2', $filtro)))
        $this->ValidateField_otro2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'precio_editable' == $filtro)) || (is_array($filtro) && in_array('precio_editable', $filtro)))
        $this->ValidateField_precio_editable($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'maneja_tcs_lfs' == $filtro)) || (is_array($filtro) && in_array('maneja_tcs_lfs', $filtro)))
        $this->ValidateField_maneja_tcs_lfs($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'stockmen' == $filtro)) || (is_array($filtro) && in_array('stockmen', $filtro)))
        $this->ValidateField_stockmen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'unidmaymen' == $filtro)) || (is_array($filtro) && in_array('unidmaymen', $filtro)))
        $this->ValidateField_unidmaymen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'unimay' == $filtro)) || (is_array($filtro) && in_array('unimay', $filtro)))
        $this->ValidateField_unimay($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'unimen' == $filtro)) || (is_array($filtro) && in_array('unimen', $filtro)))
        $this->ValidateField_unimen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'unidad_ma' == $filtro)) || (is_array($filtro) && in_array('unidad_ma', $filtro)))
        $this->ValidateField_unidad_ma($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'unidad_' == $filtro)) || (is_array($filtro) && in_array('unidad_', $filtro)))
        $this->ValidateField_unidad_($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'multiple_escala' == $filtro)) || (is_array($filtro) && in_array('multiple_escala', $filtro)))
        $this->ValidateField_multiple_escala($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'en_base_a' == $filtro)) || (is_array($filtro) && in_array('en_base_a', $filtro)))
        $this->ValidateField_en_base_a($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'costomen' == $filtro)) || (is_array($filtro) && in_array('costomen', $filtro)))
        $this->ValidateField_costomen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'costo_prom' == $filtro)) || (is_array($filtro) && in_array('costo_prom', $filtro)))
        $this->ValidateField_costo_prom($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'recmayamen' == $filtro)) || (is_array($filtro) && in_array('recmayamen', $filtro)))
        $this->ValidateField_recmayamen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'idiva' == $filtro)) || (is_array($filtro) && in_array('idiva', $filtro)))
        $this->ValidateField_idiva($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'existencia' == $filtro)) || (is_array($filtro) && in_array('existencia', $filtro)))
        $this->ValidateField_existencia($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'u_menor' == $filtro)) || (is_array($filtro) && in_array('u_menor', $filtro)))
        $this->ValidateField_u_menor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'ubicacion' == $filtro)) || (is_array($filtro) && in_array('ubicacion', $filtro)))
        $this->ValidateField_ubicacion($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'activo' == $filtro)) || (is_array($filtro) && in_array('activo', $filtro)))
        $this->ValidateField_activo($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'colores' == $filtro)) || (is_array($filtro) && in_array('colores', $filtro)))
        $this->ValidateField_colores($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'confcolor' == $filtro)) || (is_array($filtro) && in_array('confcolor', $filtro)))
        $this->ValidateField_confcolor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'tallas' == $filtro)) || (is_array($filtro) && in_array('tallas', $filtro)))
        $this->ValidateField_tallas($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'conftalla' == $filtro)) || (is_array($filtro) && in_array('conftalla', $filtro)))
        $this->ValidateField_conftalla($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'sabores' == $filtro)) || (is_array($filtro) && in_array('sabores', $filtro)))
        $this->ValidateField_sabores($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'sabor' == $filtro)) || (is_array($filtro) && in_array('sabor', $filtro)))
        $this->ValidateField_sabor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'fecha_vencimiento' == $filtro)) || (is_array($filtro) && in_array('fecha_vencimiento', $filtro)))
        $this->ValidateField_fecha_vencimiento($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'lote' == $filtro)) || (is_array($filtro) && in_array('lote', $filtro)))
        $this->ValidateField_lote($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'serial_codbarras' == $filtro)) || (is_array($filtro) && in_array('serial_codbarras', $filtro)))
        $this->ValidateField_serial_codbarras($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'relleno' == $filtro)) || (is_array($filtro) && in_array('relleno', $filtro)))
        $this->ValidateField_relleno($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'control_costo' == $filtro)) || (is_array($filtro) && in_array('control_costo', $filtro)))
        $this->ValidateField_control_costo($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'por_preciominimo' == $filtro)) || (is_array($filtro) && in_array('por_preciominimo', $filtro)))
        $this->ValidateField_por_preciominimo($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'sugerido_mayor' == $filtro)) || (is_array($filtro) && in_array('sugerido_mayor', $filtro)))
        $this->ValidateField_sugerido_mayor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'sugerido_menor' == $filtro)) || (is_array($filtro) && in_array('sugerido_menor', $filtro)))
        $this->ValidateField_sugerido_menor($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'preciofull' == $filtro)) || (is_array($filtro) && in_array('preciofull', $filtro)))
        $this->ValidateField_preciofull($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'precio2' == $filtro)) || (is_array($filtro) && in_array('precio2', $filtro)))
        $this->ValidateField_precio2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'preciomay' == $filtro)) || (is_array($filtro) && in_array('preciomay', $filtro)))
        $this->ValidateField_preciomay($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'preciomen' == $filtro)) || (is_array($filtro) && in_array('preciomen', $filtro)))
        $this->ValidateField_preciomen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'preciomen2' == $filtro)) || (is_array($filtro) && in_array('preciomen2', $filtro)))
        $this->ValidateField_preciomen2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'preciomen3' == $filtro)) || (is_array($filtro) && in_array('preciomen3', $filtro)))
        $this->ValidateField_preciomen3($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'imagen' == $filtro)) || (is_array($filtro) && in_array('imagen', $filtro)))
        $this->ValidateField_imagen($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'cod_cuenta' == $filtro)) || (is_array($filtro) && in_array('cod_cuenta', $filtro)))
        $this->ValidateField_cod_cuenta($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'idprod' == $filtro)) || (is_array($filtro) && in_array('idprod', $filtro)))
        $this->ValidateField_idprod($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'id_marca' == $filtro)) || (is_array($filtro) && in_array('id_marca', $filtro)))
        $this->ValidateField_id_marca($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'id_linea' == $filtro)) || (is_array($filtro) && in_array('id_linea', $filtro)))
        $this->ValidateField_id_linea($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'codigobar2' == $filtro)) || (is_array($filtro) && in_array('codigobar2', $filtro)))
        $this->ValidateField_codigobar2($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'codigobar3' == $filtro)) || (is_array($filtro) && in_array('codigobar3', $filtro)))
        $this->ValidateField_codigobar3($Campos_Crit, $Campos_Falta, $Campos_Erros);
      if ((!is_array($filtro) && ('' == $filtro || 'para_registro_fe' == $filtro)) || (is_array($filtro) && in_array('para_registro_fe', $filtro)))
        $this->ValidateField_para_registro_fe($Campos_Crit, $Campos_Falta, $Campos_Erros);
      $this->upload_img_doc($Campos_Crit, $Campos_Falta, $Campos_Erros);

      if (!isset($this->NM_ajax_flag) || 'validate_' != substr($this->NM_ajax_opcao, 0, 9))
      {
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_codigobar = $this->codigobar;
    $original_codigoprod = $this->codigoprod;
    $original_idprod = $this->idprod;
    $original_precio2 = $this->precio2;
    $original_preciofull = $this->preciofull;
    $original_preciomay = $this->preciomay;
    $original_preciomen = $this->preciomen;
    $original_preciomen2 = $this->preciomen2;
    $original_preciomen3 = $this->preciomen3;
    $original_recmayamen = $this->recmayamen;
    $original_unidmaymen = $this->unidmaymen;
    $original_unimay = $this->unimay;
}
  if ($this->sc_evento == "excluir" || $this->sc_evento == "delete")
	{goto salir;}

 
      $nm_select = "select control_costo from configuraciones where idconfiguraciones=1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->dat_conf = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->dat_conf[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->dat_conf = false;
          $this->dat_conf_erro = $this->Db->ErrorMsg();
      } 
;
if(isset($this->dat_conf[0][0]))
	{
	if($this->dat_conf[0][0]=='SI')
		{
		if($this->preciofull <$this->sugerido_mayor  or $this->precio2 <$this->sugerido_mayor  or $this->preciomay <$this->sugerido_mayor )
			{
			
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "¡Almenos uno de los precios de venta por unidad mayor, está por debajo del Valor mínimo de Venta!";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "¡Almenos uno de los precios de venta por unidad mayor, está por debajo del Valor mínimo de Venta!";
 }
;
			}
		
		if($this->preciomen <$this->sugerido_menor  or $this->preciomen2 <$this->sugerido_menor  or $this->preciomen3 <$this->sugerido_menor )
			{
			
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "¡Almenos uno de los precios de venta por detal, está por debajo del Valor mínimo de Venta!";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "¡Almenos uno de los precios de venta por detal, está por debajo del Valor mínimo de Venta!";
 }
;
			}
		}
	}

if($this->unidmaymen =='SI')
	{
	if(isset($this->unimay ) and ($this->unimay <>'') and $this->recmayamen >0)
		{
		goto salir;
		}
	else
		{
		
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "¡Almenos uno de los campos obligatorios para menejo de escala está sin diligenciar!";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "¡Almenos uno de los campos obligatorios para menejo de escala está sin diligenciar!";
 }
;
		}
	}

if($this->sc_evento == "alterar" || $this->sc_evento == "update")
	{
	 
      $nm_select = "select idprod, nompro from productos where codigobar='".$this->codigobar ."' or codigoprod='".$this->codigoprod ."' LIMIT 1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_pr1 = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_pr1[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_pr1 = false;
          $this->ds_pr1_erro = $this->Db->ErrorMsg();
      } 
;
	if(isset($this->ds_pr1[0][0]))
		{
		if($this->idprod <>$this->ds_pr1[0][0])
			{
			
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "Código de Barras o Código de Producto ya está asignado al producto: '".$this->ds_pr1[0][1]."'";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "Código de Barras o Código de Producto ya está asignado al producto: '".$this->ds_pr1[0][1]."'";
 }
;
			}
		}
	goto salir;
	}
else
	{
	 
      $nm_select = "select nompro  from productos where codigobar='".$this->codigobar ."' or codigoprod='".$this->codigoprod ."' LIMIT 1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_pr = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_pr[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_pr = false;
          $this->ds_pr_erro = $this->Db->ErrorMsg();
      } 
;
	if(isset($this->ds_pr[0][0]))
		{
		
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "Código de Barras o Código de Producto ya está asignado al producto: '".$this->ds_pr[0][0]."'";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "Código de Barras o Código de Producto ya está asignado al producto: '".$this->ds_pr[0][0]."'";
 }
;
		}
	else
		{
		goto salir;
	}
	}

salir:;
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_codigobar != $this->codigobar || (isset($bFlagRead_codigobar) && $bFlagRead_codigobar)))
    {
        $this->ajax_return_values_codigobar(true);
    }
    if (($original_codigoprod != $this->codigoprod || (isset($bFlagRead_codigoprod) && $bFlagRead_codigoprod)))
    {
        $this->ajax_return_values_codigoprod(true);
    }
    if (($original_idprod != $this->idprod || (isset($bFlagRead_idprod) && $bFlagRead_idprod)))
    {
        $this->ajax_return_values_idprod(true);
    }
    if (($original_precio2 != $this->precio2 || (isset($bFlagRead_precio2) && $bFlagRead_precio2)))
    {
        $this->ajax_return_values_precio2(true);
    }
    if (($original_preciofull != $this->preciofull || (isset($bFlagRead_preciofull) && $bFlagRead_preciofull)))
    {
        $this->ajax_return_values_preciofull(true);
    }
    if (($original_preciomay != $this->preciomay || (isset($bFlagRead_preciomay) && $bFlagRead_preciomay)))
    {
        $this->ajax_return_values_preciomay(true);
    }
    if (($original_preciomen != $this->preciomen || (isset($bFlagRead_preciomen) && $bFlagRead_preciomen)))
    {
        $this->ajax_return_values_preciomen(true);
    }
    if (($original_preciomen2 != $this->preciomen2 || (isset($bFlagRead_preciomen2) && $bFlagRead_preciomen2)))
    {
        $this->ajax_return_values_preciomen2(true);
    }
    if (($original_preciomen3 != $this->preciomen3 || (isset($bFlagRead_preciomen3) && $bFlagRead_preciomen3)))
    {
        $this->ajax_return_values_preciomen3(true);
    }
    if (($original_recmayamen != $this->recmayamen || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen)))
    {
        $this->ajax_return_values_recmayamen(true);
    }
    if (($original_unidmaymen != $this->unidmaymen || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen)))
    {
        $this->ajax_return_values_unidmaymen(true);
    }
    if (($original_unimay != $this->unimay || (isset($bFlagRead_unimay) && $bFlagRead_unimay)))
    {
        $this->ajax_return_values_unimay(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
      }
      if (!empty($Campos_Crit) || !empty($Campos_Falta) || !empty($this->Campos_Mens_erro))
      {
          if (!empty($this->sc_force_zero))
          {
              foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
              {
                  eval('$this->' . $sc_force_zero_field . ' = "";');
                  unset($this->sc_force_zero[$i_force_zero]);
              }
          }
      }
   }

    function ValidateField_codigoprod(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      $this->codigoprod = sc_strtoupper($this->codigoprod); 
      if ($this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['codigoprod']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['codigoprod'] == "on")) 
      { 
          if ($this->codigoprod == "")  
          { 
              $hasError = true;
              $Campos_Falta[] =  "Código producto" ; 
              if (!isset($Campos_Erros['codigoprod']))
              {
                  $Campos_Erros['codigoprod'] = array();
              }
              $Campos_Erros['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['codigoprod']) || !is_array($this->NM_ajax_info['errList']['codigoprod']))
                  {
                      $this->NM_ajax_info['errList']['codigoprod'] = array();
                  }
                  $this->NM_ajax_info['errList']['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          } 
      } 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->codigoprod) > 40) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código producto " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 40 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['codigoprod']))
              {
                  $Campos_Erros['codigoprod'] = array();
              }
              $Campos_Erros['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 40 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['codigoprod']) || !is_array($this->NM_ajax_info['errList']['codigoprod']))
              {
                  $this->NM_ajax_info['errList']['codigoprod'] = array();
              }
              $this->NM_ajax_info['errList']['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 40 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
      $Teste_trab = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .-_";
      if ($_SESSION['scriptcase']['charset'] != "UTF-8")
      {
          $Teste_trab = NM_conv_charset($Teste_trab, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
;
      $Teste_trab = $Teste_trab . chr(10) . chr(13) ; 
      $Teste_compara = $this->codigoprod ; 
      if ($this->nmgp_opcao != "excluir") 
      { 
          $Teste_critica = 0 ; 
          for ($x = 0; $x < mb_strlen($this->codigoprod, $_SESSION['scriptcase']['charset']); $x++) 
          { 
               for ($y = 0; $y < mb_strlen($Teste_trab, $_SESSION['scriptcase']['charset']); $y++) 
               { 
                    if (sc_substr($Teste_compara, $x, 1) == sc_substr($Teste_trab, $y, 1) ) 
                    { 
                        break ; 
                    } 
               } 
               if (sc_substr($Teste_compara, $x, 1) != sc_substr($Teste_trab, $y, 1) )  
               { 
                  $Teste_critica = 1 ; 
               } 
          } 
          if ($Teste_critica == 1) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código producto " . $this->Ini->Nm_lang['lang_errm_ivch']; 
              if (!isset($Campos_Erros['codigoprod']))
              {
                  $Campos_Erros['codigoprod'] = array();
              }
              $Campos_Erros['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_ivch'];
              if (!isset($this->NM_ajax_info['errList']['codigoprod']) || !is_array($this->NM_ajax_info['errList']['codigoprod']))
              {
                  $this->NM_ajax_info['errList']['codigoprod'] = array();
              }
              $this->NM_ajax_info['errList']['codigoprod'][] = $this->Ini->Nm_lang['lang_errm_ivch'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'codigoprod';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_codigoprod

    function ValidateField_codigobar(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      $this->codigobar = sc_strtoupper($this->codigobar); 
      if ($this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['codigobar']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['codigobar'] == "on")) 
      { 
          if ($this->codigobar == "")  
          { 
              $hasError = true;
              $Campos_Falta[] =  "Código barras" ; 
              if (!isset($Campos_Erros['codigobar']))
              {
                  $Campos_Erros['codigobar'] = array();
              }
              $Campos_Erros['codigobar'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['codigobar']) || !is_array($this->NM_ajax_info['errList']['codigobar']))
                  {
                      $this->NM_ajax_info['errList']['codigobar'] = array();
                  }
                  $this->NM_ajax_info['errList']['codigobar'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          } 
      } 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->codigobar) > 25) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código barras " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 25 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['codigobar']))
              {
                  $Campos_Erros['codigobar'] = array();
              }
              $Campos_Erros['codigobar'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 25 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['codigobar']) || !is_array($this->NM_ajax_info['errList']['codigobar']))
              {
                  $this->NM_ajax_info['errList']['codigobar'] = array();
              }
              $this->NM_ajax_info['errList']['codigobar'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 25 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
      $Teste_trab = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .-_";
      if ($_SESSION['scriptcase']['charset'] != "UTF-8")
      {
          $Teste_trab = NM_conv_charset($Teste_trab, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
;
      $Teste_trab = $Teste_trab . chr(10) . chr(13) ; 
      $Teste_compara = $this->codigobar ; 
      if ($this->nmgp_opcao != "excluir") 
      { 
          $Teste_critica = 0 ; 
          for ($x = 0; $x < mb_strlen($this->codigobar, $_SESSION['scriptcase']['charset']); $x++) 
          { 
               for ($y = 0; $y < mb_strlen($Teste_trab, $_SESSION['scriptcase']['charset']); $y++) 
               { 
                    if (sc_substr($Teste_compara, $x, 1) == sc_substr($Teste_trab, $y, 1) ) 
                    { 
                        break ; 
                    } 
               } 
               if (sc_substr($Teste_compara, $x, 1) != sc_substr($Teste_trab, $y, 1) )  
               { 
                  $Teste_critica = 1 ; 
               } 
          } 
          if ($Teste_critica == 1) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código barras " . $this->Ini->Nm_lang['lang_errm_ivch']; 
              if (!isset($Campos_Erros['codigobar']))
              {
                  $Campos_Erros['codigobar'] = array();
              }
              $Campos_Erros['codigobar'][] = $this->Ini->Nm_lang['lang_errm_ivch'];
              if (!isset($this->NM_ajax_info['errList']['codigobar']) || !is_array($this->NM_ajax_info['errList']['codigobar']))
              {
                  $this->NM_ajax_info['errList']['codigobar'] = array();
              }
              $this->NM_ajax_info['errList']['codigobar'][] = $this->Ini->Nm_lang['lang_errm_ivch'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'codigobar';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_codigobar

    function ValidateField_nompro(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      $this->nompro = sc_strtoupper($this->nompro); 
      if ($this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['nompro']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['nompro'] == "on")) 
      { 
          if ($this->nompro == "")  
          { 
              $hasError = true;
              $Campos_Falta[] =  "Descripción" ; 
              if (!isset($Campos_Erros['nompro']))
              {
                  $Campos_Erros['nompro'] = array();
              }
              $Campos_Erros['nompro'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['nompro']) || !is_array($this->NM_ajax_info['errList']['nompro']))
                  {
                      $this->NM_ajax_info['errList']['nompro'] = array();
                  }
                  $this->NM_ajax_info['errList']['nompro'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          } 
      } 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->nompro) > 200) 
          { 
              $hasError = true;
              $Campos_Crit .= "Descripción " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 200 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['nompro']))
              {
                  $Campos_Erros['nompro'] = array();
              }
              $Campos_Erros['nompro'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 200 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['nompro']) || !is_array($this->NM_ajax_info['errList']['nompro']))
              {
                  $this->NM_ajax_info['errList']['nompro'] = array();
              }
              $this->NM_ajax_info['errList']['nompro'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 200 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'nompro';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_nompro

    function ValidateField_idgrup(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->idgrup == "" && $this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['idgrup']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['idgrup'] == "on"))
      {
          $hasError = true;
          $Campos_Falta[] = "Familia o grupo" ; 
          if (!isset($Campos_Erros['idgrup']))
          {
              $Campos_Erros['idgrup'] = array();
          }
          $Campos_Erros['idgrup'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          if (!isset($this->NM_ajax_info['errList']['idgrup']) || !is_array($this->NM_ajax_info['errList']['idgrup']))
          {
              $this->NM_ajax_info['errList']['idgrup'] = array();
          }
          $this->NM_ajax_info['errList']['idgrup'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
      }
          if (!empty($this->idgrup) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']) && !in_array($this->idgrup, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['idgrup']))
              {
                  $Campos_Erros['idgrup'] = array();
              }
              $Campos_Erros['idgrup'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['idgrup']) || !is_array($this->NM_ajax_info['errList']['idgrup']))
              {
                  $this->NM_ajax_info['errList']['idgrup'] = array();
              }
              $this->NM_ajax_info['errList']['idgrup'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'idgrup';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_idgrup

    function ValidateField_idpro1(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['idpro1']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['idpro1'] == "on")) 
      { 
          if ($this->idpro1 == "")  
          { 
              $hasError = true;
              $Campos_Falta[] =  "Proveedor principal" ; 
              if (!isset($Campos_Erros['idpro1']))
              {
                  $Campos_Erros['idpro1'] = array();
              }
              $Campos_Erros['idpro1'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['idpro1']) || !is_array($this->NM_ajax_info['errList']['idpro1']))
                  {
                      $this->NM_ajax_info['errList']['idpro1'] = array();
                  }
                  $this->NM_ajax_info['errList']['idpro1'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          } 
      } 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->idpro1) > 19) 
          { 
              $hasError = true;
              $Campos_Crit .= "Proveedor principal " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['idpro1']))
              {
                  $Campos_Erros['idpro1'] = array();
              }
              $Campos_Erros['idpro1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['idpro1']) || !is_array($this->NM_ajax_info['errList']['idpro1']))
              {
                  $this->NM_ajax_info['errList']['idpro1'] = array();
              }
              $this->NM_ajax_info['errList']['idpro1'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'idpro1';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_idpro1

    function ValidateField_tipo_producto(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->tipo_producto) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']) && !in_array($this->tipo_producto, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['tipo_producto']))
                   {
                       $Campos_Erros['tipo_producto'] = array();
                   }
                   $Campos_Erros['tipo_producto'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['tipo_producto']) || !is_array($this->NM_ajax_info['errList']['tipo_producto']))
                   {
                       $this->NM_ajax_info['errList']['tipo_producto'] = array();
                   }
                   $this->NM_ajax_info['errList']['tipo_producto'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'tipo_producto';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_tipo_producto

    function ValidateField_idpro2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->idpro2) > 19) 
          { 
              $hasError = true;
              $Campos_Crit .= "Proveedor secundario " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['idpro2']))
              {
                  $Campos_Erros['idpro2'] = array();
              }
              $Campos_Erros['idpro2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['idpro2']) || !is_array($this->NM_ajax_info['errList']['idpro2']))
              {
                  $this->NM_ajax_info['errList']['idpro2'] = array();
              }
              $this->NM_ajax_info['errList']['idpro2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 19 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'idpro2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_idpro2

    function ValidateField_otro(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->otro == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->otro === "" || is_null($this->otro))  
      { 
          $this->otro = 0;
          $this->sc_force_zero[] = 'otro';
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'otro';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_otro

    function ValidateField_otro2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->otro2 === "" || is_null($this->otro2))  
      { 
          $this->otro2 = 0;
          $this->sc_force_zero[] = 'otro2';
      } 
      nm_limpa_numero($this->otro2, $this->field_config['otro2']['symbol_grp']) ; 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->otro2 != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->otro2) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Descuento %: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['otro2']))
                  {
                      $Campos_Erros['otro2'] = array();
                  }
                  $Campos_Erros['otro2'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['otro2']) || !is_array($this->NM_ajax_info['errList']['otro2']))
                  {
                      $this->NM_ajax_info['errList']['otro2'] = array();
                  }
                  $this->NM_ajax_info['errList']['otro2'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->otro2, 11, 0, -0, 99999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Descuento %; " ; 
                  if (!isset($Campos_Erros['otro2']))
                  {
                      $Campos_Erros['otro2'] = array();
                  }
                  $Campos_Erros['otro2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['otro2']) || !is_array($this->NM_ajax_info['errList']['otro2']))
                  {
                      $this->NM_ajax_info['errList']['otro2'] = array();
                  }
                  $this->NM_ajax_info['errList']['otro2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'otro2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_otro2

    function ValidateField_precio_editable(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->precio_editable == "" && $this->nmgp_opcao != "excluir")
      { 
        if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['precio_editable']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['precio_editable'] == "on")
        { 
          $hasError = true;
          $Campos_Falta[] = "Precio editable en ventas" ;
          if (!isset($Campos_Erros['precio_editable']))
          {
              $Campos_Erros['precio_editable'] = array();
          }
          $Campos_Erros['precio_editable'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['precio_editable']) || !is_array($this->NM_ajax_info['errList']['precio_editable']))
                  {
                      $this->NM_ajax_info['errList']['precio_editable'] = array();
                  }
                  $this->NM_ajax_info['errList']['precio_editable'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
        } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'precio_editable';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_precio_editable

    function ValidateField_maneja_tcs_lfs(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->maneja_tcs_lfs == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->maneja_tcs_lfs != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_maneja_tcs_lfs']) && !in_array($this->maneja_tcs_lfs, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_maneja_tcs_lfs']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['maneja_tcs_lfs']))
              {
                  $Campos_Erros['maneja_tcs_lfs'] = array();
              }
              $Campos_Erros['maneja_tcs_lfs'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['maneja_tcs_lfs']) || !is_array($this->NM_ajax_info['errList']['maneja_tcs_lfs']))
              {
                  $this->NM_ajax_info['errList']['maneja_tcs_lfs'] = array();
              }
              $this->NM_ajax_info['errList']['maneja_tcs_lfs'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'maneja_tcs_lfs';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_maneja_tcs_lfs

    function ValidateField_stockmen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao == "alterar")
      {
      if ($this->stockmen === "" || is_null($this->stockmen))  
      { 
          $this->stockmen = 0;
          $this->sc_force_zero[] = 'stockmen';
      } 
      }
      if (!empty($this->field_config['stockmen']['symbol_dec']))
      {
          nm_limpa_valor($this->stockmen, $this->field_config['stockmen']['symbol_dec'], $this->field_config['stockmen']['symbol_grp']) ; 
          if ('.' == substr($this->stockmen, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->stockmen, 1)))
              {
                  $this->stockmen = '';
              }
              else
              {
                  $this->stockmen = '0' . $this->stockmen;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->stockmen != '')  
          { 
              $iTestSize = 11;
              if ('-' == substr($this->stockmen, 0, 1))
              {
                  $iTestSize++;
              }
              elseif ('-' == substr($this->stockmen, -1))
              {
                  $iTestSize++;
                  $this->stockmen = '-' . substr($this->stockmen, 0, -1);
              }
              if (strlen($this->stockmen) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Existencia:: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['stockmen']))
                  {
                      $Campos_Erros['stockmen'] = array();
                  }
                  $Campos_Erros['stockmen'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['stockmen']) || !is_array($this->NM_ajax_info['errList']['stockmen']))
                  {
                      $this->NM_ajax_info['errList']['stockmen'] = array();
                  }
                  $this->NM_ajax_info['errList']['stockmen'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->stockmen, 8, 2, 0, 0, "S") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Existencia:; " ; 
                  if (!isset($Campos_Erros['stockmen']))
                  {
                      $Campos_Erros['stockmen'] = array();
                  }
                  $Campos_Erros['stockmen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['stockmen']) || !is_array($this->NM_ajax_info['errList']['stockmen']))
                  {
                      $this->NM_ajax_info['errList']['stockmen'] = array();
                  }
                  $this->NM_ajax_info['errList']['stockmen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'stockmen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_stockmen

    function ValidateField_unidmaymen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->unidmaymen == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->unidmaymen != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidmaymen']) && !in_array($this->unidmaymen, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidmaymen']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['unidmaymen']))
              {
                  $Campos_Erros['unidmaymen'] = array();
              }
              $Campos_Erros['unidmaymen'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['unidmaymen']) || !is_array($this->NM_ajax_info['errList']['unidmaymen']))
              {
                  $this->NM_ajax_info['errList']['unidmaymen'] = array();
              }
              $this->NM_ajax_info['errList']['unidmaymen'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'unidmaymen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_unidmaymen

    function ValidateField_unimay(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      $this->unimay = sc_strtoupper($this->unimay); 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->unimay) > 10) 
          { 
              $hasError = true;
              $Campos_Crit .= "Unidad Mayor " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['unimay']))
              {
                  $Campos_Erros['unimay'] = array();
              }
              $Campos_Erros['unimay'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['unimay']) || !is_array($this->NM_ajax_info['errList']['unimay']))
              {
                  $this->NM_ajax_info['errList']['unimay'] = array();
              }
              $this->NM_ajax_info['errList']['unimay'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'unimay';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_unimay

    function ValidateField_unimen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      $this->unimen = sc_strtoupper($this->unimen); 
      if ($this->nmgp_opcao != "excluir" && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['unimen']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['php_cmp_required']['unimen'] == "on")) 
      { 
          if ($this->unimen == "")  
          { 
              $hasError = true;
              $Campos_Falta[] =  "Unidad" ; 
              if (!isset($Campos_Erros['unimen']))
              {
                  $Campos_Erros['unimen'] = array();
              }
              $Campos_Erros['unimen'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
                  if (!isset($this->NM_ajax_info['errList']['unimen']) || !is_array($this->NM_ajax_info['errList']['unimen']))
                  {
                      $this->NM_ajax_info['errList']['unimen'] = array();
                  }
                  $this->NM_ajax_info['errList']['unimen'][] = $this->Ini->Nm_lang['lang_errm_ajax_rqrd'];
          } 
      } 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->unimen) > 10) 
          { 
              $hasError = true;
              $Campos_Crit .= "Unidad " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['unimen']))
              {
                  $Campos_Erros['unimen'] = array();
              }
              $Campos_Erros['unimen'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['unimen']) || !is_array($this->NM_ajax_info['errList']['unimen']))
              {
                  $this->NM_ajax_info['errList']['unimen'] = array();
              }
              $this->NM_ajax_info['errList']['unimen'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 10 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'unimen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_unimen

    function ValidateField_unidad_ma(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->unidad_ma) > 20) 
          { 
              $hasError = true;
              $Campos_Crit .= "Unidad Mayor " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['unidad_ma']))
              {
                  $Campos_Erros['unidad_ma'] = array();
              }
              $Campos_Erros['unidad_ma'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['unidad_ma']) || !is_array($this->NM_ajax_info['errList']['unidad_ma']))
              {
                  $this->NM_ajax_info['errList']['unidad_ma'] = array();
              }
              $this->NM_ajax_info['errList']['unidad_ma'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'unidad_ma';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_unidad_ma

    function ValidateField_unidad_(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->unidad_) > 20) 
          { 
              $hasError = true;
              $Campos_Crit .= "Unidad P " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['unidad_']))
              {
                  $Campos_Erros['unidad_'] = array();
              }
              $Campos_Erros['unidad_'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['unidad_']) || !is_array($this->NM_ajax_info['errList']['unidad_']))
              {
                  $this->NM_ajax_info['errList']['unidad_'] = array();
              }
              $this->NM_ajax_info['errList']['unidad_'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'unidad_';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_unidad_

    function ValidateField_multiple_escala(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->multiple_escala == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->multiple_escala != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_multiple_escala']) && !in_array($this->multiple_escala, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_multiple_escala']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['multiple_escala']))
              {
                  $Campos_Erros['multiple_escala'] = array();
              }
              $Campos_Erros['multiple_escala'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['multiple_escala']) || !is_array($this->NM_ajax_info['errList']['multiple_escala']))
              {
                  $this->NM_ajax_info['errList']['multiple_escala'] = array();
              }
              $this->NM_ajax_info['errList']['multiple_escala'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'multiple_escala';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_multiple_escala

    function ValidateField_en_base_a(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->en_base_a == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->en_base_a != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_en_base_a']) && !in_array($this->en_base_a, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_en_base_a']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['en_base_a']))
              {
                  $Campos_Erros['en_base_a'] = array();
              }
              $Campos_Erros['en_base_a'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['en_base_a']) || !is_array($this->NM_ajax_info['errList']['en_base_a']))
              {
                  $this->NM_ajax_info['errList']['en_base_a'] = array();
              }
              $this->NM_ajax_info['errList']['en_base_a'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'en_base_a';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_en_base_a

    function ValidateField_costomen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->costomen === "" || is_null($this->costomen))  
      { 
          $this->costomen = 0;
          $this->sc_force_zero[] = 'costomen';
      } 
      if (!empty($this->field_config['costomen']['symbol_dec']))
      {
          $this->sc_remove_currency($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp'], $this->field_config['costomen']['symbol_mon']); 
          nm_limpa_valor($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp']) ; 
          if ('.' == substr($this->costomen, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->costomen, 1)))
              {
                  $this->costomen = '';
              }
              else
              {
                  $this->costomen = '0' . $this->costomen;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->costomen != '')  
          { 
              $iTestSize = 13;
              if (strlen($this->costomen) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Costo compra: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['costomen']))
                  {
                      $Campos_Erros['costomen'] = array();
                  }
                  $Campos_Erros['costomen'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['costomen']) || !is_array($this->NM_ajax_info['errList']['costomen']))
                  {
                      $this->NM_ajax_info['errList']['costomen'] = array();
                  }
                  $this->NM_ajax_info['errList']['costomen'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->costomen, 10, 2, -0, 999999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Costo compra; " ; 
                  if (!isset($Campos_Erros['costomen']))
                  {
                      $Campos_Erros['costomen'] = array();
                  }
                  $Campos_Erros['costomen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['costomen']) || !is_array($this->NM_ajax_info['errList']['costomen']))
                  {
                      $this->NM_ajax_info['errList']['costomen'] = array();
                  }
                  $this->NM_ajax_info['errList']['costomen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'costomen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_costomen

    function ValidateField_costo_prom(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->costo_prom === "" || is_null($this->costo_prom))  
      { 
          $this->costo_prom = 0;
          $this->sc_force_zero[] = 'costo_prom';
      } 
      if (!empty($this->field_config['costo_prom']['symbol_dec']))
      {
          $this->sc_remove_currency($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp'], $this->field_config['costo_prom']['symbol_mon']); 
          nm_limpa_valor($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp']) ; 
          if ('.' == substr($this->costo_prom, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->costo_prom, 1)))
              {
                  $this->costo_prom = '';
              }
              else
              {
                  $this->costo_prom = '0' . $this->costo_prom;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->costo_prom != '')  
          { 
              $iTestSize = 13;
              if (strlen($this->costo_prom) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Costo Promedio: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['costo_prom']))
                  {
                      $Campos_Erros['costo_prom'] = array();
                  }
                  $Campos_Erros['costo_prom'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['costo_prom']) || !is_array($this->NM_ajax_info['errList']['costo_prom']))
                  {
                      $this->NM_ajax_info['errList']['costo_prom'] = array();
                  }
                  $this->NM_ajax_info['errList']['costo_prom'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->costo_prom, 10, 2, -0, 999999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Costo Promedio; " ; 
                  if (!isset($Campos_Erros['costo_prom']))
                  {
                      $Campos_Erros['costo_prom'] = array();
                  }
                  $Campos_Erros['costo_prom'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['costo_prom']) || !is_array($this->NM_ajax_info['errList']['costo_prom']))
                  {
                      $this->NM_ajax_info['errList']['costo_prom'] = array();
                  }
                  $this->NM_ajax_info['errList']['costo_prom'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'costo_prom';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_costo_prom

    function ValidateField_recmayamen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao == "alterar")
      {
      if ($this->recmayamen === "" || is_null($this->recmayamen))  
      { 
          $this->recmayamen = 0;
          $this->sc_force_zero[] = 'recmayamen';
      } 
      }
      nm_limpa_numero($this->recmayamen, $this->field_config['recmayamen']['symbol_grp']) ; 
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->recmayamen != '')  
          { 
              $iTestSize = 10;
              if (strlen($this->recmayamen) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Factor multiplicador: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['recmayamen']))
                  {
                      $Campos_Erros['recmayamen'] = array();
                  }
                  $Campos_Erros['recmayamen'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['recmayamen']) || !is_array($this->NM_ajax_info['errList']['recmayamen']))
                  {
                      $this->NM_ajax_info['errList']['recmayamen'] = array();
                  }
                  $this->NM_ajax_info['errList']['recmayamen'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->recmayamen, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Factor multiplicador; " ; 
                  if (!isset($Campos_Erros['recmayamen']))
                  {
                      $Campos_Erros['recmayamen'] = array();
                  }
                  $Campos_Erros['recmayamen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['recmayamen']) || !is_array($this->NM_ajax_info['errList']['recmayamen']))
                  {
                      $this->NM_ajax_info['errList']['recmayamen'] = array();
                  }
                  $this->NM_ajax_info['errList']['recmayamen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'recmayamen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_recmayamen

    function ValidateField_idiva(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->idiva) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']) && !in_array($this->idiva, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['idiva']))
                   {
                       $Campos_Erros['idiva'] = array();
                   }
                   $Campos_Erros['idiva'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['idiva']) || !is_array($this->NM_ajax_info['errList']['idiva']))
                   {
                       $this->NM_ajax_info['errList']['idiva'] = array();
                   }
                   $this->NM_ajax_info['errList']['idiva'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'idiva';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_idiva

    function ValidateField_existencia(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->existencia === "" || is_null($this->existencia))  
      { 
          $this->existencia = 0;
          $this->sc_force_zero[] = 'existencia';
      } 
      if (!empty($this->field_config['existencia']['symbol_dec']))
      {
          nm_limpa_valor($this->existencia, $this->field_config['existencia']['symbol_dec'], $this->field_config['existencia']['symbol_grp']) ; 
          if ('.' == substr($this->existencia, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->existencia, 1)))
              {
                  $this->existencia = '';
              }
              else
              {
                  $this->existencia = '0' . $this->existencia;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->existencia != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->existencia) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Existencia en unidad menor:: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['existencia']))
                  {
                      $Campos_Erros['existencia'] = array();
                  }
                  $Campos_Erros['existencia'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['existencia']) || !is_array($this->NM_ajax_info['errList']['existencia']))
                  {
                      $this->NM_ajax_info['errList']['existencia'] = array();
                  }
                  $this->NM_ajax_info['errList']['existencia'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->existencia, 8, 2, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Existencia en unidad menor:; " ; 
                  if (!isset($Campos_Erros['existencia']))
                  {
                      $Campos_Erros['existencia'] = array();
                  }
                  $Campos_Erros['existencia'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['existencia']) || !is_array($this->NM_ajax_info['errList']['existencia']))
                  {
                      $this->NM_ajax_info['errList']['existencia'] = array();
                  }
                  $this->NM_ajax_info['errList']['existencia'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'existencia';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_existencia

    function ValidateField_u_menor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->u_menor) != "")  
          { 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'u_menor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_u_menor

    function ValidateField_ubicacion(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->ubicacion) > 120) 
          { 
              $hasError = true;
              $Campos_Crit .= "Ubicación " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 120 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['ubicacion']))
              {
                  $Campos_Erros['ubicacion'] = array();
              }
              $Campos_Erros['ubicacion'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 120 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['ubicacion']) || !is_array($this->NM_ajax_info['errList']['ubicacion']))
              {
                  $this->NM_ajax_info['errList']['ubicacion'] = array();
              }
              $this->NM_ajax_info['errList']['ubicacion'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 120 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'ubicacion';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_ubicacion

    function ValidateField_activo(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->activo == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->activo != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_activo']) && !in_array($this->activo, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_activo']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['activo']))
              {
                  $Campos_Erros['activo'] = array();
              }
              $Campos_Erros['activo'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['activo']) || !is_array($this->NM_ajax_info['errList']['activo']))
              {
                  $this->NM_ajax_info['errList']['activo'] = array();
              }
              $this->NM_ajax_info['errList']['activo'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'activo';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_activo

    function ValidateField_colores(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->colores == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'colores';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_colores

    function ValidateField_confcolor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->confcolor) != "")  
          { 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'confcolor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_confcolor

    function ValidateField_tallas(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->tallas == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'tallas';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_tallas

    function ValidateField_conftalla(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->conftalla) != "")  
          { 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'conftalla';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_conftalla

    function ValidateField_sabores(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->sabores == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'sabores';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_sabores

    function ValidateField_sabor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->sabor) != "")  
          { 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'sabor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_sabor

    function ValidateField_fecha_vencimiento(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->fecha_vencimiento == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'fecha_vencimiento';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_fecha_vencimiento

    function ValidateField_lote(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->lote == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'lote';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_lote

    function ValidateField_serial_codbarras(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->serial_codbarras == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'serial_codbarras';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_serial_codbarras

    function ValidateField_relleno(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (trim($this->relleno) != "")  
          { 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'relleno';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_relleno

    function ValidateField_control_costo(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->control_costo == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
      if ($this->control_costo != "")
      { 
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo']) && !in_array($this->control_costo, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo']))
          {
              $hasError = true;
              $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($Campos_Erros['control_costo']))
              {
                  $Campos_Erros['control_costo'] = array();
              }
              $Campos_Erros['control_costo'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
              if (!isset($this->NM_ajax_info['errList']['control_costo']) || !is_array($this->NM_ajax_info['errList']['control_costo']))
              {
                  $this->NM_ajax_info['errList']['control_costo'] = array();
              }
              $this->NM_ajax_info['errList']['control_costo'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
          }
      }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'control_costo';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_control_costo

    function ValidateField_por_preciominimo(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->por_preciominimo === "" || is_null($this->por_preciominimo))  
      { 
          $this->por_preciominimo = 0;
          $this->sc_force_zero[] = 'por_preciominimo';
      } 
      if (!empty($this->field_config['por_preciominimo']['symbol_dec']))
      {
          nm_limpa_valor($this->por_preciominimo, $this->field_config['por_preciominimo']['symbol_dec'], $this->field_config['por_preciominimo']['symbol_grp']) ; 
          if ('.' == substr($this->por_preciominimo, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->por_preciominimo, 1)))
              {
                  $this->por_preciominimo = '';
              }
              else
              {
                  $this->por_preciominimo = '0' . $this->por_preciominimo;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->por_preciominimo != '')  
          { 
              $iTestSize = 5;
              if (strlen($this->por_preciominimo) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "% Ganancia: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['por_preciominimo']))
                  {
                      $Campos_Erros['por_preciominimo'] = array();
                  }
                  $Campos_Erros['por_preciominimo'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['por_preciominimo']) || !is_array($this->NM_ajax_info['errList']['por_preciominimo']))
                  {
                      $this->NM_ajax_info['errList']['por_preciominimo'] = array();
                  }
                  $this->NM_ajax_info['errList']['por_preciominimo'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->por_preciominimo, 2, 2, -0, 9999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "% Ganancia; " ; 
                  if (!isset($Campos_Erros['por_preciominimo']))
                  {
                      $Campos_Erros['por_preciominimo'] = array();
                  }
                  $Campos_Erros['por_preciominimo'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['por_preciominimo']) || !is_array($this->NM_ajax_info['errList']['por_preciominimo']))
                  {
                      $this->NM_ajax_info['errList']['por_preciominimo'] = array();
                  }
                  $this->NM_ajax_info['errList']['por_preciominimo'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'por_preciominimo';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_por_preciominimo

    function ValidateField_sugerido_mayor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->sugerido_mayor === "" || is_null($this->sugerido_mayor))  
      { 
          $this->sugerido_mayor = 0;
          $this->sc_force_zero[] = 'sugerido_mayor';
      } 
      if (!empty($this->field_config['sugerido_mayor']['symbol_dec']))
      {
          $this->sc_remove_currency($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp'], $this->field_config['sugerido_mayor']['symbol_mon']); 
          nm_limpa_valor($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp']) ; 
          if ('.' == substr($this->sugerido_mayor, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->sugerido_mayor, 1)))
              {
                  $this->sugerido_mayor = '';
              }
              else
              {
                  $this->sugerido_mayor = '0' . $this->sugerido_mayor;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->sugerido_mayor != '')  
          { 
              $iTestSize = 21;
              if (strlen($this->sugerido_mayor) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "$ Sugerido U. Mayor: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['sugerido_mayor']))
                  {
                      $Campos_Erros['sugerido_mayor'] = array();
                  }
                  $Campos_Erros['sugerido_mayor'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['sugerido_mayor']) || !is_array($this->NM_ajax_info['errList']['sugerido_mayor']))
                  {
                      $this->NM_ajax_info['errList']['sugerido_mayor'] = array();
                  }
                  $this->NM_ajax_info['errList']['sugerido_mayor'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->sugerido_mayor, 20, 0, -0, 1.0E+20, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "$ Sugerido U. Mayor; " ; 
                  if (!isset($Campos_Erros['sugerido_mayor']))
                  {
                      $Campos_Erros['sugerido_mayor'] = array();
                  }
                  $Campos_Erros['sugerido_mayor'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['sugerido_mayor']) || !is_array($this->NM_ajax_info['errList']['sugerido_mayor']))
                  {
                      $this->NM_ajax_info['errList']['sugerido_mayor'] = array();
                  }
                  $this->NM_ajax_info['errList']['sugerido_mayor'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'sugerido_mayor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_sugerido_mayor

    function ValidateField_sugerido_menor(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->sugerido_menor === "" || is_null($this->sugerido_menor))  
      { 
          $this->sugerido_menor = 0;
          $this->sc_force_zero[] = 'sugerido_menor';
      } 
      if (!empty($this->field_config['sugerido_menor']['symbol_dec']))
      {
          $this->sc_remove_currency($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp'], $this->field_config['sugerido_menor']['symbol_mon']); 
          nm_limpa_valor($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp']) ; 
          if ('.' == substr($this->sugerido_menor, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->sugerido_menor, 1)))
              {
                  $this->sugerido_menor = '';
              }
              else
              {
                  $this->sugerido_menor = '0' . $this->sugerido_menor;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->sugerido_menor != '')  
          { 
              $iTestSize = 21;
              if ('-' == substr($this->sugerido_menor, 0, 1))
              {
                  $iTestSize++;
              }
              elseif ('-' == substr($this->sugerido_menor, -1))
              {
                  $iTestSize++;
                  $this->sugerido_menor = '-' . substr($this->sugerido_menor, 0, -1);
              }
              if (strlen($this->sugerido_menor) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "$ Sugerido U. Menor: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['sugerido_menor']))
                  {
                      $Campos_Erros['sugerido_menor'] = array();
                  }
                  $Campos_Erros['sugerido_menor'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['sugerido_menor']) || !is_array($this->NM_ajax_info['errList']['sugerido_menor']))
                  {
                      $this->NM_ajax_info['errList']['sugerido_menor'] = array();
                  }
                  $this->NM_ajax_info['errList']['sugerido_menor'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->sugerido_menor, 20, 0, 0, 0, "S") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "$ Sugerido U. Menor; " ; 
                  if (!isset($Campos_Erros['sugerido_menor']))
                  {
                      $Campos_Erros['sugerido_menor'] = array();
                  }
                  $Campos_Erros['sugerido_menor'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['sugerido_menor']) || !is_array($this->NM_ajax_info['errList']['sugerido_menor']))
                  {
                      $this->NM_ajax_info['errList']['sugerido_menor'] = array();
                  }
                  $this->NM_ajax_info['errList']['sugerido_menor'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'sugerido_menor';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_sugerido_menor

    function ValidateField_preciofull(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->preciofull === "" || is_null($this->preciofull))  
      { 
          $this->preciofull = 0;
          $this->sc_force_zero[] = 'preciofull';
      } 
      if (!empty($this->field_config['preciofull']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp'], $this->field_config['preciofull']['symbol_mon']); 
          nm_limpa_valor($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp']) ; 
          if ('.' == substr($this->preciofull, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->preciofull, 1)))
              {
                  $this->preciofull = '';
              }
              else
              {
                  $this->preciofull = '0' . $this->preciofull;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->preciofull != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->preciofull) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Preciofull: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['preciofull']))
                  {
                      $Campos_Erros['preciofull'] = array();
                  }
                  $Campos_Erros['preciofull'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['preciofull']) || !is_array($this->NM_ajax_info['errList']['preciofull']))
                  {
                      $this->NM_ajax_info['errList']['preciofull'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciofull'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->preciofull, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Preciofull; " ; 
                  if (!isset($Campos_Erros['preciofull']))
                  {
                      $Campos_Erros['preciofull'] = array();
                  }
                  $Campos_Erros['preciofull'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['preciofull']) || !is_array($this->NM_ajax_info['errList']['preciofull']))
                  {
                      $this->NM_ajax_info['errList']['preciofull'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciofull'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'preciofull';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_preciofull

    function ValidateField_precio2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->precio2 === "" || is_null($this->precio2))  
      { 
          $this->precio2 = 0;
          $this->sc_force_zero[] = 'precio2';
      } 
      if (!empty($this->field_config['precio2']['symbol_dec']))
      {
          $this->sc_remove_currency($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp'], $this->field_config['precio2']['symbol_mon']); 
          nm_limpa_valor($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp']) ; 
          if ('.' == substr($this->precio2, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->precio2, 1)))
              {
                  $this->precio2 = '';
              }
              else
              {
                  $this->precio2 = '0' . $this->precio2;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->precio2 != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->precio2) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio especial: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['precio2']))
                  {
                      $Campos_Erros['precio2'] = array();
                  }
                  $Campos_Erros['precio2'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['precio2']) || !is_array($this->NM_ajax_info['errList']['precio2']))
                  {
                      $this->NM_ajax_info['errList']['precio2'] = array();
                  }
                  $this->NM_ajax_info['errList']['precio2'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->precio2, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio especial; " ; 
                  if (!isset($Campos_Erros['precio2']))
                  {
                      $Campos_Erros['precio2'] = array();
                  }
                  $Campos_Erros['precio2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['precio2']) || !is_array($this->NM_ajax_info['errList']['precio2']))
                  {
                      $this->NM_ajax_info['errList']['precio2'] = array();
                  }
                  $this->NM_ajax_info['errList']['precio2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'precio2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_precio2

    function ValidateField_preciomay(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->preciomay === "" || is_null($this->preciomay))  
      { 
          $this->preciomay = 0;
          $this->sc_force_zero[] = 'preciomay';
      } 
      if (!empty($this->field_config['preciomay']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp'], $this->field_config['preciomay']['symbol_mon']); 
          nm_limpa_valor($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp']) ; 
          if ('.' == substr($this->preciomay, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->preciomay, 1)))
              {
                  $this->preciomay = '';
              }
              else
              {
                  $this->preciomay = '0' . $this->preciomay;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->preciomay != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->preciomay) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio venta al mayor: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['preciomay']))
                  {
                      $Campos_Erros['preciomay'] = array();
                  }
                  $Campos_Erros['preciomay'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['preciomay']) || !is_array($this->NM_ajax_info['errList']['preciomay']))
                  {
                      $this->NM_ajax_info['errList']['preciomay'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomay'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->preciomay, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio venta al mayor; " ; 
                  if (!isset($Campos_Erros['preciomay']))
                  {
                      $Campos_Erros['preciomay'] = array();
                  }
                  $Campos_Erros['preciomay'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['preciomay']) || !is_array($this->NM_ajax_info['errList']['preciomay']))
                  {
                      $this->NM_ajax_info['errList']['preciomay'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomay'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'preciomay';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_preciomay

    function ValidateField_preciomen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->preciomen === "" || is_null($this->preciomen))  
      { 
          $this->preciomen = 0;
          $this->sc_force_zero[] = 'preciomen';
      } 
      if (!empty($this->field_config['preciomen']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp'], $this->field_config['preciomen']['symbol_mon']); 
          nm_limpa_valor($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp']) ; 
          if ('.' == substr($this->preciomen, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->preciomen, 1)))
              {
                  $this->preciomen = '';
              }
              else
              {
                  $this->preciomen = '0' . $this->preciomen;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->preciomen != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->preciomen) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio venta menudeo: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['preciomen']))
                  {
                      $Campos_Erros['preciomen'] = array();
                  }
                  $Campos_Erros['preciomen'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['preciomen']) || !is_array($this->NM_ajax_info['errList']['preciomen']))
                  {
                      $this->NM_ajax_info['errList']['preciomen'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->preciomen, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio venta menudeo; " ; 
                  if (!isset($Campos_Erros['preciomen']))
                  {
                      $Campos_Erros['preciomen'] = array();
                  }
                  $Campos_Erros['preciomen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['preciomen']) || !is_array($this->NM_ajax_info['errList']['preciomen']))
                  {
                      $this->NM_ajax_info['errList']['preciomen'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'preciomen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_preciomen

    function ValidateField_preciomen2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->preciomen2 === "" || is_null($this->preciomen2))  
      { 
          $this->preciomen2 = 0;
          $this->sc_force_zero[] = 'preciomen2';
      } 
      if (!empty($this->field_config['preciomen2']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp'], $this->field_config['preciomen2']['symbol_mon']); 
          nm_limpa_valor($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp']) ; 
          if ('.' == substr($this->preciomen2, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->preciomen2, 1)))
              {
                  $this->preciomen2 = '';
              }
              else
              {
                  $this->preciomen2 = '0' . $this->preciomen2;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->preciomen2 != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->preciomen2) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio especial menudeo: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['preciomen2']))
                  {
                      $Campos_Erros['preciomen2'] = array();
                  }
                  $Campos_Erros['preciomen2'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['preciomen2']) || !is_array($this->NM_ajax_info['errList']['preciomen2']))
                  {
                      $this->NM_ajax_info['errList']['preciomen2'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen2'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->preciomen2, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio especial menudeo; " ; 
                  if (!isset($Campos_Erros['preciomen2']))
                  {
                      $Campos_Erros['preciomen2'] = array();
                  }
                  $Campos_Erros['preciomen2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['preciomen2']) || !is_array($this->NM_ajax_info['errList']['preciomen2']))
                  {
                      $this->NM_ajax_info['errList']['preciomen2'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen2'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'preciomen2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_preciomen2

    function ValidateField_preciomen3(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->preciomen3 === "" || is_null($this->preciomen3))  
      { 
          $this->preciomen3 = 0;
          $this->sc_force_zero[] = 'preciomen3';
      } 
      if (!empty($this->field_config['preciomen3']['symbol_dec']))
      {
          $this->sc_remove_currency($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp'], $this->field_config['preciomen3']['symbol_mon']); 
          nm_limpa_valor($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp']) ; 
          if ('.' == substr($this->preciomen3, 0, 1))
          {
              if ('' == str_replace('0', '', substr($this->preciomen3, 1)))
              {
                  $this->preciomen3 = '';
              }
              else
              {
                  $this->preciomen3 = '0' . $this->preciomen3;
              }
          }
      }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->preciomen3 != '')  
          { 
              $iTestSize = 11;
              if (strlen($this->preciomen3) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio al mayor menudeo: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['preciomen3']))
                  {
                      $Campos_Erros['preciomen3'] = array();
                  }
                  $Campos_Erros['preciomen3'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['preciomen3']) || !is_array($this->NM_ajax_info['errList']['preciomen3']))
                  {
                      $this->NM_ajax_info['errList']['preciomen3'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen3'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->preciomen3, 10, 0, -0, 9999999999, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Precio al mayor menudeo; " ; 
                  if (!isset($Campos_Erros['preciomen3']))
                  {
                      $Campos_Erros['preciomen3'] = array();
                  }
                  $Campos_Erros['preciomen3'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['preciomen3']) || !is_array($this->NM_ajax_info['errList']['preciomen3']))
                  {
                      $this->NM_ajax_info['errList']['preciomen3'] = array();
                  }
                  $this->NM_ajax_info['errList']['preciomen3'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'preciomen3';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_preciomen3

    function ValidateField_imagen(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
        if ($this->nmgp_opcao != "excluir")
        {
            $sTestFile = $this->imagen;
            if ("" != $this->imagen && "S" != $this->imagen_limpa && !$teste_validade->ArqExtensao($this->imagen, array()))
            {
                $hasError = true;
                $Campos_Crit .= "Imagen: " . $this->Ini->Nm_lang['lang_errm_file_invl']; 
                if (!isset($Campos_Erros['imagen']))
                {
                    $Campos_Erros['imagen'] = array();
                }
                $Campos_Erros['imagen'][] = $this->Ini->Nm_lang['lang_errm_file_invl'];
                if (!isset($this->NM_ajax_info['errList']['imagen']) || !is_array($this->NM_ajax_info['errList']['imagen']))
                {
                    $this->NM_ajax_info['errList']['imagen'] = array();
                }
                $this->NM_ajax_info['errList']['imagen'][] = $this->Ini->Nm_lang['lang_errm_file_invl'];
            }
        }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'imagen';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_imagen

    function ValidateField_cod_cuenta(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->cod_cuenta) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']) && !in_array($this->cod_cuenta, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['cod_cuenta']))
                   {
                       $Campos_Erros['cod_cuenta'] = array();
                   }
                   $Campos_Erros['cod_cuenta'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['cod_cuenta']) || !is_array($this->NM_ajax_info['errList']['cod_cuenta']))
                   {
                       $this->NM_ajax_info['errList']['cod_cuenta'] = array();
                   }
                   $this->NM_ajax_info['errList']['cod_cuenta'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'cod_cuenta';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_cod_cuenta

    function ValidateField_idprod(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->idprod === "" || is_null($this->idprod))  
      { 
          $this->idprod = 0;
      } 
      nm_limpa_numero($this->idprod, $this->field_config['idprod']['symbol_grp']) ; 
      if ($this->nmgp_opcao == "incluir")
      { 
          if ($this->idprod != '')  
          { 
              $iTestSize = 10;
              if (strlen($this->idprod) > $iTestSize)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Idprod: " . $this->Ini->Nm_lang['lang_errm_size']; 
                  if (!isset($Campos_Erros['idprod']))
                  {
                      $Campos_Erros['idprod'] = array();
                  }
                  $Campos_Erros['idprod'][] = $this->Ini->Nm_lang['lang_errm_size'];
                  if (!isset($this->NM_ajax_info['errList']['idprod']) || !is_array($this->NM_ajax_info['errList']['idprod']))
                  {
                      $this->NM_ajax_info['errList']['idprod'] = array();
                  }
                  $this->NM_ajax_info['errList']['idprod'][] = $this->Ini->Nm_lang['lang_errm_size'];
              } 
              if ($teste_validade->Valor($this->idprod, 10, 0, 0, 0, "N") == false)  
              { 
                  $hasError = true;
                  $Campos_Crit .= "Idprod; " ; 
                  if (!isset($Campos_Erros['idprod']))
                  {
                      $Campos_Erros['idprod'] = array();
                  }
                  $Campos_Erros['idprod'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
                  if (!isset($this->NM_ajax_info['errList']['idprod']) || !is_array($this->NM_ajax_info['errList']['idprod']))
                  {
                      $this->NM_ajax_info['errList']['idprod'] = array();
                  }
                  $this->NM_ajax_info['errList']['idprod'][] = "" . $this->Ini->Nm_lang['lang_errm_ajax_data'] . "";
              } 
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'idprod';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_idprod

    function ValidateField_id_marca(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->id_marca) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']) && !in_array($this->id_marca, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['id_marca']))
                   {
                       $Campos_Erros['id_marca'] = array();
                   }
                   $Campos_Erros['id_marca'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['id_marca']) || !is_array($this->NM_ajax_info['errList']['id_marca']))
                   {
                       $this->NM_ajax_info['errList']['id_marca'] = array();
                   }
                   $this->NM_ajax_info['errList']['id_marca'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'id_marca';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_id_marca

    function ValidateField_id_linea(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
               if (!empty($this->id_linea) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']) && !in_array($this->id_linea, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']))
               {
                   $hasError = true;
                   $Campos_Crit .= $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($Campos_Erros['id_linea']))
                   {
                       $Campos_Erros['id_linea'] = array();
                   }
                   $Campos_Erros['id_linea'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
                   if (!isset($this->NM_ajax_info['errList']['id_linea']) || !is_array($this->NM_ajax_info['errList']['id_linea']))
                   {
                       $this->NM_ajax_info['errList']['id_linea'] = array();
                   }
                   $this->NM_ajax_info['errList']['id_linea'][] = $this->Ini->Nm_lang['lang_errm_ajax_data'];
               }
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'id_linea';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_id_linea

    function ValidateField_codigobar2(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->codigobar2) > 20) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código barras 2 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['codigobar2']))
              {
                  $Campos_Erros['codigobar2'] = array();
              }
              $Campos_Erros['codigobar2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['codigobar2']) || !is_array($this->NM_ajax_info['errList']['codigobar2']))
              {
                  $this->NM_ajax_info['errList']['codigobar2'] = array();
              }
              $this->NM_ajax_info['errList']['codigobar2'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'codigobar2';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_codigobar2

    function ValidateField_codigobar3(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->nmgp_opcao != "excluir") 
      { 
          if (NM_utf8_strlen($this->codigobar3) > 20) 
          { 
              $hasError = true;
              $Campos_Crit .= "Código barras 3 " . $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr']; 
              if (!isset($Campos_Erros['codigobar3']))
              {
                  $Campos_Erros['codigobar3'] = array();
              }
              $Campos_Erros['codigobar3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
              if (!isset($this->NM_ajax_info['errList']['codigobar3']) || !is_array($this->NM_ajax_info['errList']['codigobar3']))
              {
                  $this->NM_ajax_info['errList']['codigobar3'] = array();
              }
              $this->NM_ajax_info['errList']['codigobar3'][] = $this->Ini->Nm_lang['lang_errm_mxch'] . " 20 " . $this->Ini->Nm_lang['lang_errm_nchr'];
          } 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'codigobar3';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_codigobar3

    function ValidateField_para_registro_fe(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros)
    {
        global $teste_validade;
        $hasError = false;
      if ($this->para_registro_fe == "" && $this->nmgp_opcao != "excluir")
      { 
      } 
        if ($hasError) {
            global $sc_seq_vert;
            $fieldName = 'para_registro_fe';
            if (isset($sc_seq_vert) && '' != $sc_seq_vert) {
                $fieldName .= $sc_seq_vert;
            }
            $this->NM_ajax_info['fieldsWithErrors'][] = $fieldName;
        }
    } // ValidateField_para_registro_fe
//
//--------------------------------------------------------------------------------------
   function upload_img_doc(&$Campos_Crit, &$Campos_Falta, &$Campos_Erros, $filtro = '') 
   {
     global $nm_browser;
     if (!empty($Campos_Crit) || !empty($Campos_Falta))
     {
          return;
     }
      if ($this->nmgp_opcao != "excluir") 
      { 
          if ($this->imagen == "none") 
          { 
              $this->imagen = ""; 
          } 
          if ($this->imagen != "") 
          { 
              if (!function_exists('sc_upload_unprotect_chars'))
              {
                  include_once 'form_productos_060522_mob_doc_name.php';
              }
              $this->imagen = sc_upload_unprotect_chars($this->imagen);
              $this->imagen_scfile_name = sc_upload_unprotect_chars($this->imagen_scfile_name);
              if ($nm_browser == "Opera")  
              { 
                  $this->imagen_scfile_type = substr($this->imagen_scfile_type, 0, strpos($this->imagen_scfile_type, ";")) ; 
              } 
              if ($this->imagen_scfile_type == "image/pjpeg" || $this->imagen_scfile_type == "image/jpeg" || $this->imagen_scfile_type == "image/gif" ||  
                  $this->imagen_scfile_type == "image/png" || $this->imagen_scfile_type == "image/x-png" || $this->imagen_scfile_type == "image/bmp")  
              { 
                  if (is_file($this->imagen))  
                  { 
                      $this->NM_size_docs[$this->imagen_scfile_name] = $this->sc_file_size($this->imagen);
                      if ($this->nmgp_opcao == "incluir")
                      { 
                          $this->SC_IMG_imagen = $this->imagen;
                      } 
                      else 
                      { 
                          $arq_imagen = fopen($this->imagen, "r") ; 
                          $reg_imagen = fread($arq_imagen, filesize($this->imagen)) ; 
                          fclose($arq_imagen) ;  
                      } 
                      $this->imagen =  trim($this->imagen_scfile_name) ;  
                      $dir_img = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/"; 
                     if ($this->nmgp_opcao != "incluir")
                     { 
                      if (nm_mkdir($dir_img))  
                      { 
                          $arq_imagen = fopen($dir_img . trim($this->imagen_scfile_name), "w") ; 
                          fwrite($arq_imagen, $reg_imagen) ;  
                          fclose($arq_imagen) ;  
                      } 
                      else 
                      { 
                          $Campos_Crit .= "Imagen: " . $this->Ini->Nm_lang['lang_errm_ivdr']; 
                          $this->imagen = "";
                          if (!isset($Campos_Erros['imagen']))
                          {
                              $Campos_Erros['imagen'] = array();
                          }
                          $Campos_Erros['imagen'][] = $this->Ini->Nm_lang['lang_errm_ivdr'];
                          if (!isset($this->NM_ajax_info['errList']['imagen']) || !is_array($this->NM_ajax_info['errList']['imagen']))
                          {
                              $this->NM_ajax_info['errList']['imagen'] = array();
                          }
                          $this->NM_ajax_info['errList']['imagen'][] = $this->Ini->Nm_lang['lang_errm_ivdr'];
                      } 
                     } 
                  } 
                  else 
                  { 
                      $Campos_Crit .= "Imagen " . $this->Ini->Nm_lang['lang_errm_upld']; 
                      $this->imagen = "";
                      if (!isset($Campos_Erros['imagen']))
                      {
                          $Campos_Erros['imagen'] = array();
                      }
                      $Campos_Erros['imagen'][] = $this->Ini->Nm_lang['lang_errm_upld'];
                      if (!isset($this->NM_ajax_info['errList']['imagen']) || !is_array($this->NM_ajax_info['errList']['imagen']))
                      {
                          $this->NM_ajax_info['errList']['imagen'] = array();
                      }
                      $this->NM_ajax_info['errList']['imagen'][] = $this->Ini->Nm_lang['lang_errm_upld'];
                  } 
              } 
              else 
              { 
                  if ($nm_browser == "Konqueror")  
                  { 
                      $this->imagen = "" ; 
                  } 
                  else 
                  { 
                      $Campos_Crit .= "Imagen " . $this->Ini->Nm_lang['lang_errm_ivtp']; 
                      if (!isset($Campos_Erros['imagen']))
                      {
                          $Campos_Erros['imagen'] = array();
                      }
                      $Campos_Erros['imagen'][] = $this->Ini->Nm_lang['geracao_tp_inval'];
                      if (!isset($this->NM_ajax_info['errList']['imagen']) || !is_array($this->NM_ajax_info['errList']['imagen']))
                      {
                          $this->NM_ajax_info['errList']['imagen'] = array();
                      }
                      $this->NM_ajax_info['errList']['imagen'][] = $this->Ini->Nm_lang['geracao_tp_inval'];
                  } 
              } 
          } 
          elseif (!empty($this->imagen_salva) && $this->imagen_limpa != "S")
          {
              $this->imagen = $this->imagen_salva;
          }
      } 
      elseif (!empty($this->imagen_salva) && $this->imagen_limpa != "S")
      {
          $this->imagen = $this->imagen_salva;
      }
   }

    function removeDuplicateDttmError($aErrDate, &$aErrTime)
    {
        if (empty($aErrDate) || empty($aErrTime))
        {
            return;
        }

        foreach ($aErrDate as $sErrDate)
        {
            foreach ($aErrTime as $iErrTime => $sErrTime)
            {
                if ($sErrDate == $sErrTime)
                {
                    unset($aErrTime[$iErrTime]);
                }
            }
        }
    } // removeDuplicateDttmError

   function nm_guardar_campos()
   {
    global
           $sc_seq_vert;
    $this->nmgp_dados_form['codigoprod'] = $this->codigoprod;
    $this->nmgp_dados_form['codigobar'] = $this->codigobar;
    $this->nmgp_dados_form['nompro'] = $this->nompro;
    $this->nmgp_dados_form['idgrup'] = $this->idgrup;
    $this->nmgp_dados_form['idpro1'] = $this->idpro1;
    $this->nmgp_dados_form['tipo_producto'] = $this->tipo_producto;
    $this->nmgp_dados_form['idpro2'] = $this->idpro2;
    $this->nmgp_dados_form['otro'] = $this->otro;
    $this->nmgp_dados_form['otro2'] = $this->otro2;
    $this->nmgp_dados_form['precio_editable'] = $this->precio_editable;
    $this->nmgp_dados_form['maneja_tcs_lfs'] = $this->maneja_tcs_lfs;
    $this->nmgp_dados_form['stockmen'] = $this->stockmen;
    $this->nmgp_dados_form['unidmaymen'] = $this->unidmaymen;
    $this->nmgp_dados_form['unimay'] = $this->unimay;
    $this->nmgp_dados_form['unimen'] = $this->unimen;
    $this->nmgp_dados_form['unidad_ma'] = $this->unidad_ma;
    $this->nmgp_dados_form['unidad_'] = $this->unidad_;
    $this->nmgp_dados_form['multiple_escala'] = $this->multiple_escala;
    $this->nmgp_dados_form['en_base_a'] = $this->en_base_a;
    $this->nmgp_dados_form['costomen'] = $this->costomen;
    $this->nmgp_dados_form['costo_prom'] = $this->costo_prom;
    $this->nmgp_dados_form['recmayamen'] = $this->recmayamen;
    $this->nmgp_dados_form['idiva'] = $this->idiva;
    $this->nmgp_dados_form['existencia'] = $this->existencia;
    $this->nmgp_dados_form['u_menor'] = $this->u_menor;
    $this->nmgp_dados_form['ubicacion'] = $this->ubicacion;
    $this->nmgp_dados_form['activo'] = $this->activo;
    $this->nmgp_dados_form['colores'] = $this->colores;
    $this->nmgp_dados_form['confcolor'] = $this->confcolor;
    $this->nmgp_dados_form['tallas'] = $this->tallas;
    $this->nmgp_dados_form['conftalla'] = $this->conftalla;
    $this->nmgp_dados_form['sabores'] = $this->sabores;
    $this->nmgp_dados_form['sabor'] = $this->sabor;
    $this->nmgp_dados_form['fecha_vencimiento'] = $this->fecha_vencimiento;
    $this->nmgp_dados_form['lote'] = $this->lote;
    $this->nmgp_dados_form['serial_codbarras'] = $this->serial_codbarras;
    $this->nmgp_dados_form['relleno'] = $this->relleno;
    $this->nmgp_dados_form['control_costo'] = $this->control_costo;
    $this->nmgp_dados_form['por_preciominimo'] = $this->por_preciominimo;
    $this->nmgp_dados_form['sugerido_mayor'] = $this->sugerido_mayor;
    $this->nmgp_dados_form['sugerido_menor'] = $this->sugerido_menor;
    $this->nmgp_dados_form['preciofull'] = $this->preciofull;
    $this->nmgp_dados_form['precio2'] = $this->precio2;
    $this->nmgp_dados_form['preciomay'] = $this->preciomay;
    $this->nmgp_dados_form['preciomen'] = $this->preciomen;
    $this->nmgp_dados_form['preciomen2'] = $this->preciomen2;
    $this->nmgp_dados_form['preciomen3'] = $this->preciomen3;
    if (empty($this->imagen))
    {
        $this->imagen = $this->nmgp_dados_form['imagen'];
    }
    $this->nmgp_dados_form['imagen'] = $this->imagen;
    $this->nmgp_dados_form['imagen_limpa'] = $this->imagen_limpa;
    $this->nmgp_dados_form['cod_cuenta'] = $this->cod_cuenta;
    $this->nmgp_dados_form['idprod'] = $this->idprod;
    $this->nmgp_dados_form['id_marca'] = $this->id_marca;
    $this->nmgp_dados_form['id_linea'] = $this->id_linea;
    $this->nmgp_dados_form['codigobar2'] = $this->codigobar2;
    $this->nmgp_dados_form['codigobar3'] = $this->codigobar3;
    $this->nmgp_dados_form['para_registro_fe'] = $this->para_registro_fe;
    $this->nmgp_dados_form['costomay'] = $this->costomay;
    $this->nmgp_dados_form['stockmay'] = $this->stockmay;
    if (empty($this->imagenprod))
    {
        $this->imagenprod = $this->nmgp_dados_form['imagenprod'];
    }
    $this->nmgp_dados_form['imagenprod'] = $this->imagenprod;
    $this->nmgp_dados_form['imagenprod_limpa'] = $this->imagenprod_limpa;
    $this->nmgp_dados_form['imconsumo'] = $this->imconsumo;
    $this->nmgp_dados_form['escombo'] = $this->escombo;
    $this->nmgp_dados_form['idcombo'] = $this->idcombo;
    $this->nmgp_dados_form['fecha_fab'] = $this->fecha_fab;
    $this->nmgp_dados_form['ultima_compra'] = $this->ultima_compra;
    $this->nmgp_dados_form['n_ultcompra'] = $this->n_ultcompra;
    $this->nmgp_dados_form['ultima_venta'] = $this->ultima_venta;
    $this->nmgp_dados_form['n_ultventa'] = $this->n_ultventa;
    $this->nmgp_dados_form['nube'] = $this->nube;
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form'] = $this->nmgp_dados_form;
   }
   function nm_tira_formatacao()
   {
      global $nm_form_submit;
         $this->Before_unformat = array();
         $this->formatado = false;
      $this->Before_unformat['otro2'] = $this->otro2;
      nm_limpa_numero($this->otro2, $this->field_config['otro2']['symbol_grp']) ; 
      $this->Before_unformat['stockmen'] = $this->stockmen;
      if (!empty($this->field_config['stockmen']['symbol_dec']))
      {
         nm_limpa_valor($this->stockmen, $this->field_config['stockmen']['symbol_dec'], $this->field_config['stockmen']['symbol_grp']);
      }
      $this->Before_unformat['costomen'] = $this->costomen;
      if (!empty($this->field_config['costomen']['symbol_dec']))
      {
         $this->sc_remove_currency($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp'], $this->field_config['costomen']['symbol_mon']);
         nm_limpa_valor($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp']);
      }
      $this->Before_unformat['costo_prom'] = $this->costo_prom;
      if (!empty($this->field_config['costo_prom']['symbol_dec']))
      {
         $this->sc_remove_currency($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp'], $this->field_config['costo_prom']['symbol_mon']);
         nm_limpa_valor($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp']);
      }
      $this->Before_unformat['recmayamen'] = $this->recmayamen;
      nm_limpa_numero($this->recmayamen, $this->field_config['recmayamen']['symbol_grp']) ; 
      $this->Before_unformat['existencia'] = $this->existencia;
      if (!empty($this->field_config['existencia']['symbol_dec']))
      {
         nm_limpa_valor($this->existencia, $this->field_config['existencia']['symbol_dec'], $this->field_config['existencia']['symbol_grp']);
      }
      $this->Before_unformat['por_preciominimo'] = $this->por_preciominimo;
      if (!empty($this->field_config['por_preciominimo']['symbol_dec']))
      {
         nm_limpa_valor($this->por_preciominimo, $this->field_config['por_preciominimo']['symbol_dec'], $this->field_config['por_preciominimo']['symbol_grp']);
      }
      $this->Before_unformat['sugerido_mayor'] = $this->sugerido_mayor;
      if (!empty($this->field_config['sugerido_mayor']['symbol_dec']))
      {
         $this->sc_remove_currency($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp'], $this->field_config['sugerido_mayor']['symbol_mon']);
         nm_limpa_valor($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp']);
      }
      $this->Before_unformat['sugerido_menor'] = $this->sugerido_menor;
      if (!empty($this->field_config['sugerido_menor']['symbol_dec']))
      {
         $this->sc_remove_currency($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp'], $this->field_config['sugerido_menor']['symbol_mon']);
         nm_limpa_valor($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp']);
      }
      $this->Before_unformat['preciofull'] = $this->preciofull;
      if (!empty($this->field_config['preciofull']['symbol_dec']))
      {
         $this->sc_remove_currency($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp'], $this->field_config['preciofull']['symbol_mon']);
         nm_limpa_valor($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp']);
      }
      $this->Before_unformat['precio2'] = $this->precio2;
      if (!empty($this->field_config['precio2']['symbol_dec']))
      {
         $this->sc_remove_currency($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp'], $this->field_config['precio2']['symbol_mon']);
         nm_limpa_valor($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp']);
      }
      $this->Before_unformat['preciomay'] = $this->preciomay;
      if (!empty($this->field_config['preciomay']['symbol_dec']))
      {
         $this->sc_remove_currency($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp'], $this->field_config['preciomay']['symbol_mon']);
         nm_limpa_valor($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp']);
      }
      $this->Before_unformat['preciomen'] = $this->preciomen;
      if (!empty($this->field_config['preciomen']['symbol_dec']))
      {
         $this->sc_remove_currency($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp'], $this->field_config['preciomen']['symbol_mon']);
         nm_limpa_valor($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp']);
      }
      $this->Before_unformat['preciomen2'] = $this->preciomen2;
      if (!empty($this->field_config['preciomen2']['symbol_dec']))
      {
         $this->sc_remove_currency($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp'], $this->field_config['preciomen2']['symbol_mon']);
         nm_limpa_valor($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp']);
      }
      $this->Before_unformat['preciomen3'] = $this->preciomen3;
      if (!empty($this->field_config['preciomen3']['symbol_dec']))
      {
         $this->sc_remove_currency($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp'], $this->field_config['preciomen3']['symbol_mon']);
         nm_limpa_valor($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp']);
      }
      $this->Before_unformat['idprod'] = $this->idprod;
      nm_limpa_numero($this->idprod, $this->field_config['idprod']['symbol_grp']) ; 
      $this->Before_unformat['costomay'] = $this->costomay;
      if (!empty($this->field_config['costomay']['symbol_dec']))
      {
         $this->sc_remove_currency($this->costomay, $this->field_config['costomay']['symbol_dec'], $this->field_config['costomay']['symbol_grp'], $this->field_config['costomay']['symbol_mon']);
         nm_limpa_valor($this->costomay, $this->field_config['costomay']['symbol_dec'], $this->field_config['costomay']['symbol_grp']);
      }
      $this->Before_unformat['stockmay'] = $this->stockmay;
      if (!empty($this->field_config['stockmay']['symbol_dec']))
      {
         nm_limpa_valor($this->stockmay, $this->field_config['stockmay']['symbol_dec'], $this->field_config['stockmay']['symbol_grp']);
      }
      $this->Before_unformat['imconsumo'] = $this->imconsumo;
      nm_limpa_numero($this->imconsumo, $this->field_config['imconsumo']['symbol_grp']) ; 
      $this->Before_unformat['idcombo'] = $this->idcombo;
      nm_limpa_numero($this->idcombo, $this->field_config['idcombo']['symbol_grp']) ; 
      $this->Before_unformat['ultima_compra'] = $this->ultima_compra;
      nm_limpa_data($this->ultima_compra, $this->field_config['ultima_compra']['date_sep']) ; 
      $this->Before_unformat['ultima_venta'] = $this->ultima_venta;
      nm_limpa_data($this->ultima_venta, $this->field_config['ultima_venta']['date_sep']) ; 
   }
   function sc_add_currency(&$value, $symbol, $pos)
   {
       if ('' == $value)
       {
           return;
       }
       $value = (1 == $pos || 3 == $pos) ? $symbol . ' ' . $value : $value . ' ' . $symbol;
   }
   function sc_remove_currency(&$value, $symbol_dec, $symbol_tho, $symbol_mon)
   {
       $value = preg_replace('~&#x0*([0-9a-f]+);~i', '', $value);
       $sNew  = str_replace($symbol_mon, '', $value);
       if ($sNew != $value)
       {
           $value = str_replace(' ', '', $sNew);
           return;
       }
       $aTest = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', $symbol_dec, $symbol_tho);
       $sNew  = '';
       for ($i = 0; $i < strlen($value); $i++)
       {
           if ($this->sc_test_currency_char($value[$i], $aTest))
           {
               $sNew .= $value[$i];
           }
       }
       $value = $sNew;
   }
   function sc_test_currency_char($char, $test)
   {
       $found = false;
       foreach ($test as $test_char)
       {
           if ($char === $test_char)
           {
               $found = true;
           }
       }
       return $found;
   }
   function nm_clear_val($Nome_Campo)
   {
      if ($Nome_Campo == "otro2")
      {
          nm_limpa_numero($this->otro2, $this->field_config['otro2']['symbol_grp']) ; 
      }
      if ($Nome_Campo == "stockmen")
      {
          if (!empty($this->field_config['stockmen']['symbol_dec']))
          {
             nm_limpa_valor($this->stockmen, $this->field_config['stockmen']['symbol_dec'], $this->field_config['stockmen']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "costomen")
      {
          if (!empty($this->field_config['costomen']['symbol_dec']))
          {
             $this->sc_remove_currency($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp'], $this->field_config['costomen']['symbol_mon']);
             nm_limpa_valor($this->costomen, $this->field_config['costomen']['symbol_dec'], $this->field_config['costomen']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "costo_prom")
      {
          if (!empty($this->field_config['costo_prom']['symbol_dec']))
          {
             $this->sc_remove_currency($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp'], $this->field_config['costo_prom']['symbol_mon']);
             nm_limpa_valor($this->costo_prom, $this->field_config['costo_prom']['symbol_dec'], $this->field_config['costo_prom']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "recmayamen")
      {
          nm_limpa_numero($this->recmayamen, $this->field_config['recmayamen']['symbol_grp']) ; 
      }
      if ($Nome_Campo == "existencia")
      {
          if (!empty($this->field_config['existencia']['symbol_dec']))
          {
             nm_limpa_valor($this->existencia, $this->field_config['existencia']['symbol_dec'], $this->field_config['existencia']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "por_preciominimo")
      {
          if (!empty($this->field_config['por_preciominimo']['symbol_dec']))
          {
             nm_limpa_valor($this->por_preciominimo, $this->field_config['por_preciominimo']['symbol_dec'], $this->field_config['por_preciominimo']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "sugerido_mayor")
      {
          if (!empty($this->field_config['sugerido_mayor']['symbol_dec']))
          {
             $this->sc_remove_currency($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp'], $this->field_config['sugerido_mayor']['symbol_mon']);
             nm_limpa_valor($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_dec'], $this->field_config['sugerido_mayor']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "sugerido_menor")
      {
          if (!empty($this->field_config['sugerido_menor']['symbol_dec']))
          {
             $this->sc_remove_currency($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp'], $this->field_config['sugerido_menor']['symbol_mon']);
             nm_limpa_valor($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_dec'], $this->field_config['sugerido_menor']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "preciofull")
      {
          if (!empty($this->field_config['preciofull']['symbol_dec']))
          {
             $this->sc_remove_currency($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp'], $this->field_config['preciofull']['symbol_mon']);
             nm_limpa_valor($this->preciofull, $this->field_config['preciofull']['symbol_dec'], $this->field_config['preciofull']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "precio2")
      {
          if (!empty($this->field_config['precio2']['symbol_dec']))
          {
             $this->sc_remove_currency($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp'], $this->field_config['precio2']['symbol_mon']);
             nm_limpa_valor($this->precio2, $this->field_config['precio2']['symbol_dec'], $this->field_config['precio2']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "preciomay")
      {
          if (!empty($this->field_config['preciomay']['symbol_dec']))
          {
             $this->sc_remove_currency($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp'], $this->field_config['preciomay']['symbol_mon']);
             nm_limpa_valor($this->preciomay, $this->field_config['preciomay']['symbol_dec'], $this->field_config['preciomay']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "preciomen")
      {
          if (!empty($this->field_config['preciomen']['symbol_dec']))
          {
             $this->sc_remove_currency($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp'], $this->field_config['preciomen']['symbol_mon']);
             nm_limpa_valor($this->preciomen, $this->field_config['preciomen']['symbol_dec'], $this->field_config['preciomen']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "preciomen2")
      {
          if (!empty($this->field_config['preciomen2']['symbol_dec']))
          {
             $this->sc_remove_currency($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp'], $this->field_config['preciomen2']['symbol_mon']);
             nm_limpa_valor($this->preciomen2, $this->field_config['preciomen2']['symbol_dec'], $this->field_config['preciomen2']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "preciomen3")
      {
          if (!empty($this->field_config['preciomen3']['symbol_dec']))
          {
             $this->sc_remove_currency($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp'], $this->field_config['preciomen3']['symbol_mon']);
             nm_limpa_valor($this->preciomen3, $this->field_config['preciomen3']['symbol_dec'], $this->field_config['preciomen3']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "idprod")
      {
          nm_limpa_numero($this->idprod, $this->field_config['idprod']['symbol_grp']) ; 
      }
      if ($Nome_Campo == "costomay")
      {
          if (!empty($this->field_config['costomay']['symbol_dec']))
          {
             $this->sc_remove_currency($this->costomay, $this->field_config['costomay']['symbol_dec'], $this->field_config['costomay']['symbol_grp'], $this->field_config['costomay']['symbol_mon']);
             nm_limpa_valor($this->costomay, $this->field_config['costomay']['symbol_dec'], $this->field_config['costomay']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "stockmay")
      {
          if (!empty($this->field_config['stockmay']['symbol_dec']))
          {
             nm_limpa_valor($this->stockmay, $this->field_config['stockmay']['symbol_dec'], $this->field_config['stockmay']['symbol_grp']);
          }
      }
      if ($Nome_Campo == "imconsumo")
      {
          nm_limpa_numero($this->imconsumo, $this->field_config['imconsumo']['symbol_grp']) ; 
      }
      if ($Nome_Campo == "idcombo")
      {
          nm_limpa_numero($this->idcombo, $this->field_config['idcombo']['symbol_grp']) ; 
      }
   }
   function nm_formatar_campos($format_fields = array())
   {
      global $nm_form_submit;
     if (isset($this->formatado) && $this->formatado)
     {
         return;
     }
     $this->formatado = true;
      if ('' !== $this->otro2 || (!empty($format_fields) && isset($format_fields['otro2'])))
      {
          nmgp_Form_Num_Val($this->otro2, $this->field_config['otro2']['symbol_grp'], $this->field_config['otro2']['symbol_dec'], "0", "S", $this->field_config['otro2']['format_neg'], "", "", "-", $this->field_config['otro2']['symbol_fmt']) ; 
      }
      if ('' !== $this->stockmen || (!empty($format_fields) && isset($format_fields['stockmen'])))
      {
          nmgp_Form_Num_Val($this->stockmen, $this->field_config['stockmen']['symbol_grp'], $this->field_config['stockmen']['symbol_dec'], "2", "S", $this->field_config['stockmen']['format_neg'], "", "", "-", $this->field_config['stockmen']['symbol_fmt']) ; 
      }
      if ('' !== $this->costomen || (!empty($format_fields) && isset($format_fields['costomen'])))
      {
          nmgp_Form_Num_Val($this->costomen, $this->field_config['costomen']['symbol_grp'], $this->field_config['costomen']['symbol_dec'], "2", "S", $this->field_config['costomen']['format_neg'], "", "", "-", $this->field_config['costomen']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['costomen']['symbol_mon'];
          $this->sc_add_currency($this->costomen, $sMonSymb, $this->field_config['costomen']['format_pos']); 
      }
      if ('' !== $this->costo_prom || (!empty($format_fields) && isset($format_fields['costo_prom'])))
      {
          nmgp_Form_Num_Val($this->costo_prom, $this->field_config['costo_prom']['symbol_grp'], $this->field_config['costo_prom']['symbol_dec'], "2", "S", $this->field_config['costo_prom']['format_neg'], "", "", "-", $this->field_config['costo_prom']['symbol_fmt']) ; 
      }
      if ('' !== $this->recmayamen || (!empty($format_fields) && isset($format_fields['recmayamen'])))
      {
          nmgp_Form_Num_Val($this->recmayamen, $this->field_config['recmayamen']['symbol_grp'], $this->field_config['recmayamen']['symbol_dec'], "0", "S", $this->field_config['recmayamen']['format_neg'], "", "", "-", $this->field_config['recmayamen']['symbol_fmt']) ; 
      }
      if ('' !== $this->existencia || (!empty($format_fields) && isset($format_fields['existencia'])))
      {
          nmgp_Form_Num_Val($this->existencia, $this->field_config['existencia']['symbol_grp'], $this->field_config['existencia']['symbol_dec'], "2", "S", $this->field_config['existencia']['format_neg'], "", "", "-", $this->field_config['existencia']['symbol_fmt']) ; 
      }
      if ('' !== $this->por_preciominimo || (!empty($format_fields) && isset($format_fields['por_preciominimo'])))
      {
          nmgp_Form_Num_Val($this->por_preciominimo, $this->field_config['por_preciominimo']['symbol_grp'], $this->field_config['por_preciominimo']['symbol_dec'], "2", "S", $this->field_config['por_preciominimo']['format_neg'], "", "", "-", $this->field_config['por_preciominimo']['symbol_fmt']) ; 
      }
      if ('' !== $this->sugerido_mayor || (!empty($format_fields) && isset($format_fields['sugerido_mayor'])))
      {
          nmgp_Form_Num_Val($this->sugerido_mayor, $this->field_config['sugerido_mayor']['symbol_grp'], $this->field_config['sugerido_mayor']['symbol_dec'], "0", "S", $this->field_config['sugerido_mayor']['format_neg'], "", "", "-", $this->field_config['sugerido_mayor']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['sugerido_mayor']['symbol_mon'];
          $this->sc_add_currency($this->sugerido_mayor, $sMonSymb, $this->field_config['sugerido_mayor']['format_pos']); 
      }
      if ('' !== $this->sugerido_menor || (!empty($format_fields) && isset($format_fields['sugerido_menor'])))
      {
          nmgp_Form_Num_Val($this->sugerido_menor, $this->field_config['sugerido_menor']['symbol_grp'], $this->field_config['sugerido_menor']['symbol_dec'], "0", "S", $this->field_config['sugerido_menor']['format_neg'], "", "", "-", $this->field_config['sugerido_menor']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['sugerido_menor']['symbol_mon'];
          $this->sc_add_currency($this->sugerido_menor, $sMonSymb, $this->field_config['sugerido_menor']['format_pos']); 
      }
      if ('' !== $this->preciofull || (!empty($format_fields) && isset($format_fields['preciofull'])))
      {
          nmgp_Form_Num_Val($this->preciofull, $this->field_config['preciofull']['symbol_grp'], $this->field_config['preciofull']['symbol_dec'], "0", "S", $this->field_config['preciofull']['format_neg'], "", "", "-", $this->field_config['preciofull']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['preciofull']['symbol_mon'];
          $this->sc_add_currency($this->preciofull, $sMonSymb, $this->field_config['preciofull']['format_pos']); 
      }
      if ('' !== $this->precio2 || (!empty($format_fields) && isset($format_fields['precio2'])))
      {
          nmgp_Form_Num_Val($this->precio2, $this->field_config['precio2']['symbol_grp'], $this->field_config['precio2']['symbol_dec'], "0", "S", $this->field_config['precio2']['format_neg'], "", "", "-", $this->field_config['precio2']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['precio2']['symbol_mon'];
          $this->sc_add_currency($this->precio2, $sMonSymb, $this->field_config['precio2']['format_pos']); 
      }
      if ('' !== $this->preciomay || (!empty($format_fields) && isset($format_fields['preciomay'])))
      {
          nmgp_Form_Num_Val($this->preciomay, $this->field_config['preciomay']['symbol_grp'], $this->field_config['preciomay']['symbol_dec'], "0", "S", $this->field_config['preciomay']['format_neg'], "", "", "-", $this->field_config['preciomay']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['preciomay']['symbol_mon'];
          $this->sc_add_currency($this->preciomay, $sMonSymb, $this->field_config['preciomay']['format_pos']); 
      }
      if ('' !== $this->preciomen || (!empty($format_fields) && isset($format_fields['preciomen'])))
      {
          nmgp_Form_Num_Val($this->preciomen, $this->field_config['preciomen']['symbol_grp'], $this->field_config['preciomen']['symbol_dec'], "0", "S", $this->field_config['preciomen']['format_neg'], "", "", "-", $this->field_config['preciomen']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['preciomen']['symbol_mon'];
          $this->sc_add_currency($this->preciomen, $sMonSymb, $this->field_config['preciomen']['format_pos']); 
      }
      if ('' !== $this->preciomen2 || (!empty($format_fields) && isset($format_fields['preciomen2'])))
      {
          nmgp_Form_Num_Val($this->preciomen2, $this->field_config['preciomen2']['symbol_grp'], $this->field_config['preciomen2']['symbol_dec'], "0", "S", $this->field_config['preciomen2']['format_neg'], "", "", "-", $this->field_config['preciomen2']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['preciomen2']['symbol_mon'];
          $this->sc_add_currency($this->preciomen2, $sMonSymb, $this->field_config['preciomen2']['format_pos']); 
      }
      if ('' !== $this->preciomen3 || (!empty($format_fields) && isset($format_fields['preciomen3'])))
      {
          nmgp_Form_Num_Val($this->preciomen3, $this->field_config['preciomen3']['symbol_grp'], $this->field_config['preciomen3']['symbol_dec'], "0", "S", $this->field_config['preciomen3']['format_neg'], "", "", "-", $this->field_config['preciomen3']['symbol_fmt']) ; 
          $sMonSymb = $this->field_config['preciomen3']['symbol_mon'];
          $this->sc_add_currency($this->preciomen3, $sMonSymb, $this->field_config['preciomen3']['format_pos']); 
      }
      if ('' !== $this->idprod || (!empty($format_fields) && isset($format_fields['idprod'])))
      {
          nmgp_Form_Num_Val($this->idprod, $this->field_config['idprod']['symbol_grp'], $this->field_config['idprod']['symbol_dec'], "0", "S", $this->field_config['idprod']['format_neg'], "", "", "-", $this->field_config['idprod']['symbol_fmt']) ; 
      }
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";

      if (false !== strpos($nm_mask, '9') || false !== strpos($nm_mask, 'a') || false !== strpos($nm_mask, '*'))
      {
          $new_campo = '';
          $a_mask_ord  = array();
          $i_mask_size = -1;

          foreach (explode(';', $nm_mask) as $str_mask)
          {
              $a_mask_ord[ $this->nm_conta_mask_chars($str_mask) ] = $str_mask;
          }
          ksort($a_mask_ord);

          foreach ($a_mask_ord as $i_size => $s_mask)
          {
              if (-1 == $i_mask_size)
              {
                  $i_mask_size = $i_size;
              }
              elseif (strlen($nm_campo) >= $i_size && strlen($nm_campo) > $i_mask_size)
              {
                  $i_mask_size = $i_size;
              }
          }
          $nm_mask = $a_mask_ord[$i_mask_size];

          for ($i = 0; $i < strlen($nm_mask); $i++)
          {
              $test_mask = substr($nm_mask, $i, 1);
              
              if ('9' == $test_mask || 'a' == $test_mask || '*' == $test_mask)
              {
                  $new_campo .= substr($nm_campo, 0, 1);
                  $nm_campo   = substr($nm_campo, 1);
              }
              else
              {
                  $new_campo .= $test_mask;
              }
          }

                  $nm_campo = $new_campo;

          return;
      }

      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont1 < $cont2 && $tam_campo <= $cont2 && $tam_campo > $cont1)
              {
                  $trab_mask = $ver_duas[1];
              }
              elseif ($cont1 > $cont2 && $tam_campo <= $cont2)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conta_mask_chars($sMask)
   {
       $iLength = 0;

       for ($i = 0; $i < strlen($sMask); $i++)
       {
           if (in_array($sMask[$i], array('9', 'a', '*')))
           {
               $iLength++;
           }
       }

       return $iLength;
   }
   function nm_tira_mask(&$nm_campo, $nm_mask, $nm_chars = '')
   { 
      $mask_dados = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $tam_mask   = strlen($nm_mask);
      $trab_saida = "";

      if (false !== strpos($nm_mask, '9') || false !== strpos($nm_mask, 'a') || false !== strpos($nm_mask, '*'))
      {
          $raw_campo = $this->sc_clear_mask($nm_campo, $nm_chars);
          $raw_mask  = $this->sc_clear_mask($nm_mask, $nm_chars);
          $new_campo = '';

          $test_mask = substr($raw_mask, 0, 1);
          $raw_mask  = substr($raw_mask, 1);

          while ('' != $raw_campo)
          {
              $test_val  = substr($raw_campo, 0, 1);
              $raw_campo = substr($raw_campo, 1);
              $ord       = ord($test_val);
              $found     = false;

              switch ($test_mask)
              {
                  case '9':
                      if (48 <= $ord && 57 >= $ord)
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;

                  case 'a':
                      if ((65 <= $ord && 90 >= $ord) || (97 <= $ord && 122 >= $ord))
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;

                  case '*':
                      if ((48 <= $ord && 57 >= $ord) || (65 <= $ord && 90 >= $ord) || (97 <= $ord && 122 >= $ord))
                      {
                          $new_campo .= $test_val;
                          $found      = true;
                      }
                      break;
              }

              if ($found)
              {
                  $test_mask = substr($raw_mask, 0, 1);
                  $raw_mask  = substr($raw_mask, 1);
              }
          }

          $nm_campo = $new_campo;

          return;
      }

      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          for ($x=0; $x < strlen($mask_dados); $x++)
          {
              if (is_numeric(substr($mask_dados, $x, 1)))
              {
                  $trab_saida .= substr($mask_dados, $x, 1);
              }
          }
          $nm_campo = $trab_saida;
          return;
      }
      if ($tam_mask > $tam_campo)
      {
         $mask_desfaz = "";
         for ($mask_ind = 0; $tam_mask > $tam_campo; $mask_ind++)
         {
              $mask_char = substr($trab_mask, $mask_ind, 1);
              if ($mask_char == "z")
              {
                  $tam_mask--;
              }
              else
              {
                  $mask_desfaz .= $mask_char;
              }
              if ($mask_ind == $tam_campo)
              {
                  $tam_mask = $tam_campo;
              }
         }
         $trab_mask = $mask_desfaz . substr($trab_mask, $mask_ind);
      }
      $mask_saida = "";
      for ($mask_ind = strlen($trab_mask); $mask_ind > 0; $mask_ind--)
      {
          $mask_char = substr($trab_mask, $mask_ind - 1, 1);
          if ($mask_char == "x" || $mask_char == "z")
          {
              if ($tam_campo > 0)
              {
                  $mask_saida = substr($mask_dados, $tam_campo - 1, 1) . $mask_saida;
              }
          }
          else
          {
              if ($mask_char != substr($mask_dados, $tam_campo - 1, 1) && $tam_campo > 0)
              {
                  $mask_saida = substr($mask_dados, $tam_campo - 1, 1) . $mask_saida;
                  $mask_ind--;
              }
          }
          $tam_campo--;
      }
      if ($tam_campo > 0)
      {
         $mask_saida = substr($mask_dados, 0, $tam_campo) . $mask_saida;
      }
      $nm_campo = $mask_saida;
   }

   function sc_clear_mask($value, $chars)
   {
       $new = '';

       for ($i = 0; $i < strlen($value); $i++)
       {
           if (false === strpos($chars, $value[$i]))
           {
               $new .= $value[$i];
           }
       }

       return $new;
   }
//
   function nm_limpa_alfa(&$str)
   {
       if (get_magic_quotes_gpc())
       {
           if (is_array($str))
           {
               $x = 0;
               foreach ($str as $cada_str)
               {
                   $str[$x] = stripslashes($str[$x]);
                   $x++;
               }
           }
           else
           {
               $str = stripslashes($str);
           }
       }
   }
   function nm_conv_data_db($dt_in, $form_in, $form_out, $replaces = array())
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT") {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT") {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "SC_FORMAT_REGION") {
           $this->nm_data->SetaData($dt_in, strtoupper($form_in));
           $prep_out  = (strpos(strtolower($form_in), "dd") !== false) ? "dd" : "";
           $prep_out .= (strpos(strtolower($form_in), "mm") !== false) ? "mm" : "";
           $prep_out .= (strpos(strtolower($form_in), "aa") !== false) ? "aaaa" : "";
           $prep_out .= (strpos(strtolower($form_in), "yy") !== false) ? "aaaa" : "";
           return $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", $prep_out));
       }
       else {
           nm_conv_form_data($dt_out, $form_in, $form_out, $replaces);
           return $dt_out;
       }
   }

   function returnWhere($aCond, $sOp = 'AND')
   {
       $aWhere = array();
       foreach ($aCond as $sCond)
       {
           $this->handleWhereCond($sCond);
           if ('' != $sCond)
           {
               $aWhere[] = $sCond;
           }
       }
       if (empty($aWhere))
       {
           return '';
       }
       else
       {
           return ' WHERE (' . implode(') ' . $sOp . ' (', $aWhere) . ')';
       }
   } // returnWhere

   function handleWhereCond(&$sCond)
   {
       $sCond = trim($sCond);
       if ('where' == strtolower(substr($sCond, 0, 5)))
       {
           $sCond = trim(substr($sCond, 5));
       }
   } // handleWhereCond

   function ajax_return_values()
   {
          $this->ajax_return_values_codigoprod();
          $this->ajax_return_values_codigobar();
          $this->ajax_return_values_nompro();
          $this->ajax_return_values_idgrup();
          $this->ajax_return_values_idpro1();
          $this->ajax_return_values_tipo_producto();
          $this->ajax_return_values_idpro2();
          $this->ajax_return_values_otro();
          $this->ajax_return_values_otro2();
          $this->ajax_return_values_precio_editable();
          $this->ajax_return_values_maneja_tcs_lfs();
          $this->ajax_return_values_stockmen();
          $this->ajax_return_values_unidmaymen();
          $this->ajax_return_values_unimay();
          $this->ajax_return_values_unimen();
          $this->ajax_return_values_unidad_ma();
          $this->ajax_return_values_unidad_();
          $this->ajax_return_values_multiple_escala();
          $this->ajax_return_values_en_base_a();
          $this->ajax_return_values_costomen();
          $this->ajax_return_values_costo_prom();
          $this->ajax_return_values_recmayamen();
          $this->ajax_return_values_idiva();
          $this->ajax_return_values_existencia();
          $this->ajax_return_values_u_menor();
          $this->ajax_return_values_ubicacion();
          $this->ajax_return_values_activo();
          $this->ajax_return_values_colores();
          $this->ajax_return_values_confcolor();
          $this->ajax_return_values_tallas();
          $this->ajax_return_values_conftalla();
          $this->ajax_return_values_sabores();
          $this->ajax_return_values_sabor();
          $this->ajax_return_values_fecha_vencimiento();
          $this->ajax_return_values_lote();
          $this->ajax_return_values_serial_codbarras();
          $this->ajax_return_values_relleno();
          $this->ajax_return_values_control_costo();
          $this->ajax_return_values_por_preciominimo();
          $this->ajax_return_values_sugerido_mayor();
          $this->ajax_return_values_sugerido_menor();
          $this->ajax_return_values_preciofull();
          $this->ajax_return_values_precio2();
          $this->ajax_return_values_preciomay();
          $this->ajax_return_values_preciomen();
          $this->ajax_return_values_preciomen2();
          $this->ajax_return_values_preciomen3();
          $this->ajax_return_values_imagen();
          $this->ajax_return_values_cod_cuenta();
          $this->ajax_return_values_idprod();
          $this->ajax_return_values_id_marca();
          $this->ajax_return_values_id_linea();
          $this->ajax_return_values_codigobar2();
          $this->ajax_return_values_codigobar3();
          $this->ajax_return_values_para_registro_fe();
          if ('navigate_form' == $this->NM_ajax_opcao)
          {
              $this->NM_ajax_info['clearUpload']      = 'S';
              $this->NM_ajax_info['navStatus']['ret'] = $this->Nav_permite_ret ? 'S' : 'N';
              $this->NM_ajax_info['navStatus']['ava'] = $this->Nav_permite_ava ? 'S' : 'N';
              $this->NM_ajax_info['fldList']['idprod']['keyVal'] = form_productos_060522_mob_pack_protect_string($this->nmgp_dados_form['idprod']);
          }
   } // ajax_return_values

          //----- codigoprod
   function ajax_return_values_codigoprod($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("codigoprod", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->codigoprod);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['codigoprod'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- codigobar
   function ajax_return_values_codigobar($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("codigobar", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->codigobar);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['codigobar'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- nompro
   function ajax_return_values_nompro($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("nompro", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->nompro);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['nompro'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- idgrup
   function ajax_return_values_idgrup($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("idgrup", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->idgrup);
              $aLookup = array();
              $this->_tmp_lookup_idgrup = $this->idgrup;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array(); 
}
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string('Seleccione familia o grupo')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT idgrupo, nomgrupo  FROM grupo  ORDER BY nomgrupo";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"idgrup\"";
          if (isset($this->NM_ajax_info['select_html']['idgrup']) && !empty($this->NM_ajax_info['select_html']['idgrup']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['idgrup']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->idgrup == $sValue)
                  {
                      $this->_tmp_lookup_idgrup = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['idgrup'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['idgrup']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['idgrup']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['idgrup']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['idgrup']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['idgrup']['labList'] = $aLabel;
          }
   }

          //----- idpro1
   function ajax_return_values_idpro1($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("idpro1", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->idpro1);
              $aLookup = array();
              $this->_tmp_lookup_idpro1 = $this->idpro1;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idtercero, concat(documento, \"- \",nombres) FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idtercero, documento&\"- \"&nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }
   else
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro1), 1, -1) . " ORDER BY documento, nombres";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   if ('' != $this->idpro1 && '' != $this->idpro1 && '' != $this->idpro1 && '' != $this->idpro1 && '' != $this->idpro1 && '' != $this->idpro1 && '' != $this->idpro1)
   {
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro1'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   }
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['idpro1'] = array(
                       'row'    => '',
               'type'    => 'select2_ac',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['idpro1']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['idpro1']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['idpro1']['labList'] = $aLabel;
          $val_output = isset($aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->idpro1))]) ? $aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->idpro1))] : "";
          $this->NM_ajax_info['fldList']['idpro1_autocomp'] = array(
               'type'    => 'text',
               'valList' => array($val_output),
              );
          }
   }

          //----- tipo_producto
   function ajax_return_values_tipo_producto($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("tipo_producto", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->tipo_producto);
              $aLookup = array();
              $this->_tmp_lookup_tipo_producto = $this->tipo_producto;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array(); 
}
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string(' ')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp + ' - ' + descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT codigo_tp, concat(codigo_tp,' - ', descripcion_tp)  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp&' - '&descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp + ' - ' + descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   else
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"tipo_producto\"";
          if (isset($this->NM_ajax_info['select_html']['tipo_producto']) && !empty($this->NM_ajax_info['select_html']['tipo_producto']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['tipo_producto']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->tipo_producto == $sValue)
                  {
                      $this->_tmp_lookup_tipo_producto = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['tipo_producto'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['tipo_producto']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['tipo_producto']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['tipo_producto']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['tipo_producto']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['tipo_producto']['labList'] = $aLabel;
          }
   }

          //----- idpro2
   function ajax_return_values_idpro2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("idpro2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->idpro2);
              $aLookup = array();
              $this->_tmp_lookup_idpro2 = $this->idpro2;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idtercero, concat(documento, \"- \",nombres) FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idtercero, documento&\"- \"&nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idtercero, documento + \"- \" + nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }
   else
   {
       $nm_comando = "SELECT idtercero, documento||\"- \"||nombres FROM terceros WHERE (proveedor='SI') AND idtercero = " . substr($this->Db->qstr($this->idpro2), 1, -1) . " ORDER BY documento, nombres";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   if ('' != $this->idpro2 && '' != $this->idpro2 && '' != $this->idpro2 && '' != $this->idpro2 && '' != $this->idpro2 && '' != $this->idpro2 && '' != $this->idpro2)
   {
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idpro2'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   }
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['idpro2'] = array(
                       'row'    => '',
               'type'    => 'select2_ac',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['idpro2']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['idpro2']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['idpro2']['labList'] = $aLabel;
          $val_output = isset($aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->idpro2))]) ? $aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->idpro2))] : "";
          $this->NM_ajax_info['fldList']['idpro2_autocomp'] = array(
               'type'    => 'text',
               'valList' => array($val_output),
              );
          }
   }

          //----- otro
   function ajax_return_values_otro($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("otro", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->otro);
              $aLookup = array();
              $this->_tmp_lookup_otro = $this->otro;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('0') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("No")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('1') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Sí")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_otro'][] = '0';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_otro'][] = '1';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"otro\"";
          if (isset($this->NM_ajax_info['select_html']['otro']) && !empty($this->NM_ajax_info['select_html']['otro']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['otro']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->otro == $sValue)
                  {
                      $this->_tmp_lookup_otro = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['otro'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['otro']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['otro']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['otro']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['otro']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['otro']['labList'] = $aLabel;
          }
   }

          //----- otro2
   function ajax_return_values_otro2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("otro2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->otro2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['otro2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- precio_editable
   function ajax_return_values_precio_editable($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("precio_editable", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->precio_editable);
              $aLookup = array();
              $this->_tmp_lookup_precio_editable = $this->precio_editable;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_precio_editable'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_precio_editable'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"precio_editable\"";
          if (isset($this->NM_ajax_info['select_html']['precio_editable']) && !empty($this->NM_ajax_info['select_html']['precio_editable']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['precio_editable']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->precio_editable == $sValue)
                  {
                      $this->_tmp_lookup_precio_editable = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['precio_editable'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['precio_editable']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['precio_editable']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['precio_editable']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['precio_editable']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['precio_editable']['labList'] = $aLabel;
          }
   }

          //----- maneja_tcs_lfs
   function ajax_return_values_maneja_tcs_lfs($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("maneja_tcs_lfs", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->maneja_tcs_lfs);
              $aLookup = array();
              $this->_tmp_lookup_maneja_tcs_lfs = $this->maneja_tcs_lfs;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NA') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("No Aplica")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('CTS') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Color, Talla y Sabor")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('LFS') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Lote, F. Vencimiento, Serial")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_maneja_tcs_lfs'][] = 'NA';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_maneja_tcs_lfs'][] = 'CTS';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_maneja_tcs_lfs'][] = 'LFS';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['maneja_tcs_lfs']) && !empty($this->NM_ajax_info['select_html']['maneja_tcs_lfs']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['maneja_tcs_lfs']);
          }
          $this->NM_ajax_info['fldList']['maneja_tcs_lfs'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['maneja_tcs_lfs']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['maneja_tcs_lfs']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['maneja_tcs_lfs']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['maneja_tcs_lfs']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['maneja_tcs_lfs']['labList'] = $aLabel;
          }
   }

          //----- stockmen
   function ajax_return_values_stockmen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("stockmen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->stockmen);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['stockmen'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- unidmaymen
   function ajax_return_values_unidmaymen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("unidmaymen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->unidmaymen);
              $aLookup = array();
              $this->_tmp_lookup_unidmaymen = $this->unidmaymen;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidmaymen'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidmaymen'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['unidmaymen']) && !empty($this->NM_ajax_info['select_html']['unidmaymen']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['unidmaymen']);
          }
          $this->NM_ajax_info['fldList']['unidmaymen'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 2,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['unidmaymen']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['unidmaymen']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['unidmaymen']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['unidmaymen']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['unidmaymen']['labList'] = $aLabel;
          }
   }

          //----- unimay
   function ajax_return_values_unimay($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("unimay", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->unimay);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['unimay'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- unimen
   function ajax_return_values_unimen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("unimen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->unimen);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['unimen'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- unidad_ma
   function ajax_return_values_unidad_ma($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("unidad_ma", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->unidad_ma);
              $aLookup = array();
              $this->_tmp_lookup_unidad_ma = $this->unidad_ma;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT codigo_um, descripcion_um FROM unidades_medida WHERE codigo_um = '" . substr($this->Db->qstr($this->unidad_ma), 1, -1) . "' ORDER BY descripcion_um";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_ma'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['unidad_ma'] = array(
                       'row'    => '',
               'type'    => 'select2_ac',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['unidad_ma']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['unidad_ma']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['unidad_ma']['labList'] = $aLabel;
          $val_output = isset($aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->unidad_ma))]) ? $aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->unidad_ma))] : "";
          $this->NM_ajax_info['fldList']['unidad_ma_autocomp'] = array(
               'type'    => 'text',
               'valList' => array($val_output),
              );
          }
   }

          //----- unidad_
   function ajax_return_values_unidad_($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("unidad_", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->unidad_);
              $aLookup = array();
              $this->_tmp_lookup_unidad_ = $this->unidad_;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT codigo_um, descripcion_um FROM unidades_medida WHERE codigo_um = '" . substr($this->Db->qstr($this->unidad_), 1, -1) . "' ORDER BY descripcion_um";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->SelectLimit($nm_comando, 10, 0))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_unidad_'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['unidad_'] = array(
                       'row'    => '',
               'type'    => 'select2_ac',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['unidad_']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['unidad_']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['unidad_']['labList'] = $aLabel;
          $val_output = isset($aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->unidad_))]) ? $aLookup[0][form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($this->unidad_))] : "";
          $this->NM_ajax_info['fldList']['unidad__autocomp'] = array(
               'type'    => 'text',
               'valList' => array($val_output),
              );
          }
   }

          //----- multiple_escala
   function ajax_return_values_multiple_escala($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("multiple_escala", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->multiple_escala);
              $aLookup = array();
              $this->_tmp_lookup_multiple_escala = $this->multiple_escala;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_multiple_escala'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_multiple_escala'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['multiple_escala']) && !empty($this->NM_ajax_info['select_html']['multiple_escala']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['multiple_escala']);
          }
          $this->NM_ajax_info['fldList']['multiple_escala'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['multiple_escala']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['multiple_escala']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['multiple_escala']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['multiple_escala']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['multiple_escala']['labList'] = $aLabel;
          }
   }

          //----- en_base_a
   function ajax_return_values_en_base_a($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("en_base_a", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->en_base_a);
              $aLookup = array();
              $this->_tmp_lookup_en_base_a = $this->en_base_a;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('UMAY') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("UNIDAD MAYOR")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('UMEN') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("UNIDAD")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_en_base_a'][] = 'UMAY';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_en_base_a'][] = 'UMEN';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['en_base_a']) && !empty($this->NM_ajax_info['select_html']['en_base_a']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['en_base_a']);
          }
          $this->NM_ajax_info['fldList']['en_base_a'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['en_base_a']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['en_base_a']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['en_base_a']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['en_base_a']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['en_base_a']['labList'] = $aLabel;
          }
   }

          //----- costomen
   function ajax_return_values_costomen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("costomen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->costomen);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['costomen'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- costo_prom
   function ajax_return_values_costo_prom($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("costo_prom", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->costo_prom);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['costo_prom'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- recmayamen
   function ajax_return_values_recmayamen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("recmayamen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->recmayamen);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['recmayamen'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- idiva
   function ajax_return_values_idiva($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("idiva", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->idiva);
              $aLookup = array();
              $this->_tmp_lookup_idiva = $this->idiva;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idiva, trifa + ' - ' + tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idiva, concat(trifa,' - ',tipo_impuesto)  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idiva, trifa&' - '&tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idiva, trifa + ' - ' + tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   else
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"idiva\"";
          if (isset($this->NM_ajax_info['select_html']['idiva']) && !empty($this->NM_ajax_info['select_html']['idiva']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['idiva']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->idiva == $sValue)
                  {
                      $this->_tmp_lookup_idiva = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['idiva'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['idiva']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['idiva']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['idiva']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['idiva']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['idiva']['labList'] = $aLabel;
          }
   }

          //----- existencia
   function ajax_return_values_existencia($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("existencia", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->existencia);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['existencia'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- u_menor
   function ajax_return_values_u_menor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("u_menor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->u_menor);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['u_menor'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- ubicacion
   function ajax_return_values_ubicacion($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("ubicacion", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->ubicacion);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['ubicacion'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- activo
   function ajax_return_values_activo($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("activo", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->activo);
              $aLookup = array();
              $this->_tmp_lookup_activo = $this->activo;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_activo'][] = 'SI';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_activo'][] = 'NO';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['activo']) && !empty($this->NM_ajax_info['select_html']['activo']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['activo']);
          }
          $this->NM_ajax_info['fldList']['activo'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 2,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['activo']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['activo']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['activo']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['activo']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['activo']['labList'] = $aLabel;
          }
   }

          //----- colores
   function ajax_return_values_colores($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("colores", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->colores);
              $aLookup = array();
              $this->_tmp_lookup_colores = $this->colores;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_colores'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_colores'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"colores\"";
          if (isset($this->NM_ajax_info['select_html']['colores']) && !empty($this->NM_ajax_info['select_html']['colores']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['colores']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->colores == $sValue)
                  {
                      $this->_tmp_lookup_colores = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['colores'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['colores']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['colores']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['colores']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['colores']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['colores']['labList'] = $aLabel;
          }
   }

          //----- confcolor
   function ajax_return_values_confcolor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("confcolor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->confcolor);
              $aLookup = array();
          if (!is_file($this->Ini->root  . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__colors_11216.png"))
          { 
              $confcolor = "&nbsp;" ;  
          } 
          else 
          { 
              $confcolor = "<img border=\"0\" src=\"" . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__colors_11216.png\"/>" ; 
          } 
    $sTmpImgHtml = "<a href=\"javascript:nm_gp_submit('" . $this->Ini->link_form_colorxproducto_edit . "', '$this->nm_location', 'par_idproducto*scin" . $this->nmgp_dados_form['idprod'] . "*scoutNM_btn_insert*scinS*scoutNM_btn_update*scinS*scoutNM_btn_delete*scinS*scoutNM_btn_navega*scinN*scout', '', '_self', '0', '0', 'form_colorxproducto')\"><font color=\"" . $this->Ini->cor_link_dados . "\">" . $confcolor . "</font></a>";
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['confcolor'] = array(
                       'row'    => '',
               'type'    => 'imagehtml',
               'valList' => array($sTmpValue),
               'imgHtml' => $sTmpImgHtml,
              );
          }
   }

          //----- tallas
   function ajax_return_values_tallas($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("tallas", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->tallas);
              $aLookup = array();
              $this->_tmp_lookup_tallas = $this->tallas;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tallas'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tallas'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"tallas\"";
          if (isset($this->NM_ajax_info['select_html']['tallas']) && !empty($this->NM_ajax_info['select_html']['tallas']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['tallas']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->tallas == $sValue)
                  {
                      $this->_tmp_lookup_tallas = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['tallas'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['tallas']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['tallas']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['tallas']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['tallas']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['tallas']['labList'] = $aLabel;
          }
   }

          //----- conftalla
   function ajax_return_values_conftalla($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("conftalla", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->conftalla);
              $aLookup = array();
          if (!is_file($this->Ini->root  . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__tapemeasure_5131.png"))
          { 
              $conftalla = "&nbsp;" ;  
          } 
          else 
          { 
              $conftalla = "<img border=\"0\" src=\"" . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__tapemeasure_5131.png\"/>" ; 
          } 
    $sTmpImgHtml = "<a href=\"javascript:nm_gp_submit('" . $this->Ini->link_form_tallaxproducto_edit . "', '$this->nm_location', 'par_idproducto*scin" . $this->nmgp_dados_form['idprod'] . "*scoutNM_btn_insert*scinS*scoutNM_btn_update*scinS*scoutNM_btn_delete*scinS*scoutNM_btn_navega*scinN*scout', '', '_self', '0', '0', 'form_tallaxproducto')\"><font color=\"" . $this->Ini->cor_link_dados . "\">" . $conftalla . "</font></a>";
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['conftalla'] = array(
                       'row'    => '',
               'type'    => 'imagehtml',
               'valList' => array($sTmpValue),
               'imgHtml' => $sTmpImgHtml,
              );
          }
   }

          //----- sabores
   function ajax_return_values_sabores($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("sabores", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->sabores);
              $aLookup = array();
              $this->_tmp_lookup_sabores = $this->sabores;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_sabores'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_sabores'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"sabores\"";
          if (isset($this->NM_ajax_info['select_html']['sabores']) && !empty($this->NM_ajax_info['select_html']['sabores']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['sabores']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->sabores == $sValue)
                  {
                      $this->_tmp_lookup_sabores = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['sabores'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['sabores']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['sabores']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['sabores']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['sabores']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['sabores']['labList'] = $aLabel;
          }
   }

          //----- sabor
   function ajax_return_values_sabor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("sabor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->sabor);
              $aLookup = array();
          if (!is_file($this->Ini->root  . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__icons9-cucurucho-de-helado-32.png"))
          { 
              $sabor = "&nbsp;" ;  
          } 
          else 
          { 
              $sabor = "<img border=\"0\" src=\"" . $this->Ini->path_imag_cab . "/usr__NM__ico__NM__icons9-cucurucho-de-helado-32.png\"/>" ; 
          } 
    $sTmpImgHtml = "<a href=\"javascript:nm_gp_submit('" . $this->Ini->link_form_saborxproducto_edit . "', '$this->nm_location', 'par_idproducto*scin" . $this->nmgp_dados_form['idprod'] . "*scoutNM_btn_insert*scinS*scoutNM_btn_update*scinS*scoutNM_btn_delete*scinS*scoutNM_btn_navega*scinN*scout', '', '_self', '0', '0', 'form_saborxproducto')\"><font color=\"" . $this->Ini->cor_link_dados . "\">" . $sabor . "</font></a>";
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['sabor'] = array(
                       'row'    => '',
               'type'    => 'imagehtml',
               'valList' => array($sTmpValue),
               'imgHtml' => $sTmpImgHtml,
              );
          }
   }

          //----- fecha_vencimiento
   function ajax_return_values_fecha_vencimiento($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("fecha_vencimiento", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->fecha_vencimiento);
              $aLookup = array();
              $this->_tmp_lookup_fecha_vencimiento = $this->fecha_vencimiento;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_fecha_vencimiento'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_fecha_vencimiento'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"fecha_vencimiento\"";
          if (isset($this->NM_ajax_info['select_html']['fecha_vencimiento']) && !empty($this->NM_ajax_info['select_html']['fecha_vencimiento']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['fecha_vencimiento']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->fecha_vencimiento == $sValue)
                  {
                      $this->_tmp_lookup_fecha_vencimiento = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['fecha_vencimiento'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['fecha_vencimiento']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['fecha_vencimiento']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['fecha_vencimiento']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['fecha_vencimiento']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['fecha_vencimiento']['labList'] = $aLabel;
          }
   }

          //----- lote
   function ajax_return_values_lote($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("lote", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->lote);
              $aLookup = array();
              $this->_tmp_lookup_lote = $this->lote;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_lote'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_lote'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"lote\"";
          if (isset($this->NM_ajax_info['select_html']['lote']) && !empty($this->NM_ajax_info['select_html']['lote']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['lote']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->lote == $sValue)
                  {
                      $this->_tmp_lookup_lote = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['lote'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['lote']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['lote']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['lote']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['lote']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['lote']['labList'] = $aLabel;
          }
   }

          //----- serial_codbarras
   function ajax_return_values_serial_codbarras($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("serial_codbarras", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->serial_codbarras);
              $aLookup = array();
              $this->_tmp_lookup_serial_codbarras = $this->serial_codbarras;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_serial_codbarras'][] = 'NO';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_serial_codbarras'][] = 'SI';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"serial_codbarras\"";
          if (isset($this->NM_ajax_info['select_html']['serial_codbarras']) && !empty($this->NM_ajax_info['select_html']['serial_codbarras']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['serial_codbarras']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->serial_codbarras == $sValue)
                  {
                      $this->_tmp_lookup_serial_codbarras = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['serial_codbarras'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['serial_codbarras']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['serial_codbarras']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['serial_codbarras']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['serial_codbarras']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['serial_codbarras']['labList'] = $aLabel;
          }
   }

          //----- relleno
   function ajax_return_values_relleno($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("relleno", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->relleno);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['relleno'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- control_costo
   function ajax_return_values_control_costo($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("control_costo", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->control_costo);
              $aLookup = array();
              $this->_tmp_lookup_control_costo = $this->control_costo;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NA') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("No Aplica")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('UC') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Último Costo")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('CP') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Costo Promedio")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('PC') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("Porcentaje utilidad %")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo'][] = 'NA';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo'][] = 'UC';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo'][] = 'CP';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_control_costo'][] = 'PC';
          $aLookupOrig = $aLookup;
          $sOptComp = "";
          if (isset($this->NM_ajax_info['select_html']['control_costo']) && !empty($this->NM_ajax_info['select_html']['control_costo']))
          {
              $sOptComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['control_costo']);
          }
          $this->NM_ajax_info['fldList']['control_costo'] = array(
                       'row'    => '',
               'type'    => 'radio',
               'switch'  => false,
               'valList' => array($sTmpValue),
               'colNum'  => 1,
               'optComp'  => $sOptComp,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['control_costo']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['control_costo']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['control_costo']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['control_costo']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['control_costo']['labList'] = $aLabel;
          }
   }

          //----- por_preciominimo
   function ajax_return_values_por_preciominimo($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("por_preciominimo", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->por_preciominimo);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['por_preciominimo'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- sugerido_mayor
   function ajax_return_values_sugerido_mayor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("sugerido_mayor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->sugerido_mayor);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['sugerido_mayor'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- sugerido_menor
   function ajax_return_values_sugerido_menor($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("sugerido_menor", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->sugerido_menor);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['sugerido_menor'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- preciofull
   function ajax_return_values_preciofull($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("preciofull", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->preciofull);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['preciofull'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- precio2
   function ajax_return_values_precio2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("precio2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->precio2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['precio2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- preciomay
   function ajax_return_values_preciomay($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("preciomay", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->preciomay);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['preciomay'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- preciomen
   function ajax_return_values_preciomen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("preciomen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->preciomen);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['preciomen'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- preciomen2
   function ajax_return_values_preciomen2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("preciomen2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->preciomen2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['preciomen2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- preciomen3
   function ajax_return_values_preciomen3($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("preciomen3", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->preciomen3);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['preciomen3'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($sTmpValue),
              );
          }
   }

          //----- imagen
   function ajax_return_values_imagen($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("imagen", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->imagen);
              $aLookup = array();
   $out_imagen = '';
   $out1_imagen = '';
   if ($this->imagen != "" && $this->imagen != "none")   
   { 
       $path_imagen = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/" . $this->imagen;
       $md5_imagen  = md5("/" . $_SESSION['gnit'] . "/" . $this->imagen);
       nm_fix_SubDirUpload($this->imagen,$this->Ini->root . $this->Ini->path_imagens,"/" . $_SESSION['gnit'] . "/");
        if (is_file($path_imagen))  
       { 
           $NM_ler_img = true;
           $out_imagen = $this->Ini->path_imag_temp . "/sc_imagen_" . $md5_imagen . ".gif" ;  
           $out1_imagen = $out_imagen; 
           if (is_file($this->Ini->root . $out_imagen))  
           { 
               $NM_img_time = filemtime($this->Ini->root . $out_imagen) + 0; 
               if ($this->Ini->nm_timestamp < $NM_img_time)  
               { 
                   $NM_ler_img = false;
               } 
           } 
           if ($NM_ler_img) 
           { 
               $tmp_imagen = fopen($path_imagen, "r") ; 
               $reg_imagen = fread($tmp_imagen, filesize($path_imagen)) ; 
               fclose($tmp_imagen) ;  
               $arq_imagen = fopen($this->Ini->root . $out_imagen, "w") ;  
               fwrite($arq_imagen, $reg_imagen) ;  
               fclose($arq_imagen) ;  
           } 
           $sc_obj_img = new nm_trata_img($this->Ini->root . $out_imagen, true);
           $this->nmgp_return_img['imagen'][0] = $sc_obj_img->getHeight();
           $this->nmgp_return_img['imagen'][1] = $sc_obj_img->getWidth();
           $NM_redim_img = true;
           $md5_imagen  = md5($this->imagen);
           $out_imagen = $this->Ini->path_imag_temp . "/sc_imagen_150150" . $md5_imagen . ".gif" ;  
           if (is_file($this->Ini->root . $out_imagen))  
           { 
               $NM_img_time = filemtime($this->Ini->root . $out_imagen) + 0; 
               if ($this->Ini->nm_timestamp < $NM_img_time)  
               { 
                   $NM_redim_img = false;
               } 
           } 
           if ($NM_redim_img) 
           { 
               if (!$this->Ini->Gd_missing)
               { 
                   $sc_obj_img = new nm_trata_img($this->Ini->root . $out1_imagen, true);
                   $sc_obj_img->setWidth(150);
                   $sc_obj_img->setHeight(150);
                   $sc_obj_img->setManterAspecto(true);
                   $sc_obj_img->createImg($this->Ini->root . $out_imagen);
               } 
               else 
               { 
                   $out_imagen = $out1_imagen;
               } 
           } 
       } 
   } 
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['imagen'] = array(
                       'row'    => '',
               'type'    => 'imagem',
               'valList' => array($sTmpValue),
               'imgFile' => $out_imagen,
               'imgOrig' => $out1_imagen,
               'keepImg' => $sKeepImage,
               'hideName' => 'S',
              );
          }
   }

          //----- cod_cuenta
   function ajax_return_values_cod_cuenta($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("cod_cuenta", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->cod_cuenta);
              $aLookup = array();
              $this->_tmp_lookup_cod_cuenta = $this->cod_cuenta;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array(); 
}
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string('SELECCIONE')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT codigo, codigo + ' - ' + descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT codigo, concat(codigo,' - ',descripcion)  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT codigo, codigo&' - '&descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT codigo, codigo + ' - ' + descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   else
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"cod_cuenta\"";
          if (isset($this->NM_ajax_info['select_html']['cod_cuenta']) && !empty($this->NM_ajax_info['select_html']['cod_cuenta']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['cod_cuenta']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->cod_cuenta == $sValue)
                  {
                      $this->_tmp_lookup_cod_cuenta = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['cod_cuenta'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['cod_cuenta']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['cod_cuenta']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['cod_cuenta']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['cod_cuenta']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['cod_cuenta']['labList'] = $aLabel;
          }
   }

          //----- idprod
   function ajax_return_values_idprod($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("idprod", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->idprod);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['idprod'] = array(
                       'row'    => '',
               'type'    => 'label',
               'valList' => array($sTmpValue),
               'labList' => array($this->form_format_readonly("idprod", $this->form_encode_input($sTmpValue))),
              );
          }
   }

          //----- id_marca
   function ajax_return_values_id_marca($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("id_marca", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->id_marca);
              $aLookup = array();
              $this->_tmp_lookup_id_marca = $this->id_marca;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array(); 
}
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string('SELECCIONE')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT id_marca, cod_marca + ' - ' + nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT id_marca, concat(cod_marca,' - ',nombre_marca)  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT id_marca, cod_marca&' - '&nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT id_marca, cod_marca + ' - ' + nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   else
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"id_marca\"";
          if (isset($this->NM_ajax_info['select_html']['id_marca']) && !empty($this->NM_ajax_info['select_html']['id_marca']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['id_marca']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->id_marca == $sValue)
                  {
                      $this->_tmp_lookup_id_marca = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['id_marca'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['id_marca']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['id_marca']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['id_marca']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['id_marca']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['id_marca']['labList'] = $aLabel;
          }
   }

          //----- id_linea
   function ajax_return_values_id_linea($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("id_linea", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->id_linea);
              $aLookup = array();
              $this->_tmp_lookup_id_linea = $this->id_linea;

 
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array(); 
}
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string('SELECCIONE')));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'][] = '';
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT id_linea, cod_linea + ' - ' + nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT id_linea, concat(cod_linea,' - ',nombre_linea)  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT id_linea, cod_linea&' - '&nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT id_linea, cod_linea + ' - ' + nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   else
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $aLookup[] = array(form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[0])) => str_replace('<', '&lt;', form_productos_060522_mob_pack_protect_string(NM_charset_to_utf8($rs->fields[1]))));
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"id_linea\"";
          if (isset($this->NM_ajax_info['select_html']['id_linea']) && !empty($this->NM_ajax_info['select_html']['id_linea']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['id_linea']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->id_linea == $sValue)
                  {
                      $this->_tmp_lookup_id_linea = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['id_linea'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
               'optList' => $aLookup,
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['id_linea']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['id_linea']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['id_linea']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['id_linea']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['id_linea']['labList'] = $aLabel;
          }
   }

          //----- codigobar2
   function ajax_return_values_codigobar2($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("codigobar2", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->codigobar2);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['codigobar2'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- codigobar3
   function ajax_return_values_codigobar3($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("codigobar3", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->codigobar3);
              $aLookup = array();
          $aLookupOrig = $aLookup;
          $this->NM_ajax_info['fldList']['codigobar3'] = array(
                       'row'    => '',
               'type'    => 'text',
               'valList' => array($this->form_encode_input($sTmpValue)),
              );
          }
   }

          //----- para_registro_fe
   function ajax_return_values_para_registro_fe($bForce = false)
   {
          if ('navigate_form' == $this->NM_ajax_opcao || 'backup_line' == $this->NM_ajax_opcao || (isset($this->nmgp_refresh_fields) && in_array("para_registro_fe", $this->nmgp_refresh_fields)) || $bForce)
          {
              $sTmpValue = NM_charset_to_utf8($this->para_registro_fe);
              $aLookup = array();
              $this->_tmp_lookup_para_registro_fe = $this->para_registro_fe;

$aLookup[] = array(form_productos_060522_mob_pack_protect_string('SI') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("SI")));
$aLookup[] = array(form_productos_060522_mob_pack_protect_string('NO') => str_replace('<', '&lt;',form_productos_060522_mob_pack_protect_string("NO")));
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_para_registro_fe'][] = 'SI';
$_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_para_registro_fe'][] = 'NO';
          $aLookupOrig = $aLookup;
          $sSelComp = "name=\"para_registro_fe\"";
          if (isset($this->NM_ajax_info['select_html']['para_registro_fe']) && !empty($this->NM_ajax_info['select_html']['para_registro_fe']))
          {
              $sSelComp = str_replace('{SC_100PERC_CLASS_INPUT}', $this->classes_100perc_fields['input'], $this->NM_ajax_info['select_html']['para_registro_fe']);
          }
          $sLookup = '';
          if (empty($aLookup))
          {
              $aLookup[] = array('' => '');
          }
          foreach ($aLookup as $aOption)
          {
              foreach ($aOption as $sValue => $sLabel)
              {

                  if ($this->para_registro_fe == $sValue)
                  {
                      $this->_tmp_lookup_para_registro_fe = $sLabel;
                  }

                  $sOpt     = ($sValue !== $sLabel) ? $sValue : $sLabel;
                  $sLookup .= "<option value=\"" . $sOpt . "\">" . $sLabel . "</option>";
              }
          }
          $aLookup  = $sLookup;
          $this->NM_ajax_info['fldList']['para_registro_fe'] = array(
                       'row'    => '',
               'type'    => 'select',
               'valList' => array($sTmpValue),
              );
          $aLabel     = array();
          $aLabelTemp = array();
          foreach ($this->NM_ajax_info['fldList']['para_registro_fe']['valList'] as $i => $v)
          {
              $this->NM_ajax_info['fldList']['para_registro_fe']['valList'][$i] = form_productos_060522_mob_pack_protect_string($v);
          }
          foreach ($aLookupOrig as $aValData)
          {
              if (in_array(key($aValData), $this->NM_ajax_info['fldList']['para_registro_fe']['valList']))
              {
                  $aLabelTemp[key($aValData)] = current($aValData);
              }
          }
          foreach ($this->NM_ajax_info['fldList']['para_registro_fe']['valList'] as $iIndex => $sValue)
          {
              $aLabel[$iIndex] = (isset($aLabelTemp[$sValue])) ? $aLabelTemp[$sValue] : $sValue;
          }
          $this->NM_ajax_info['fldList']['para_registro_fe']['labList'] = $aLabel;
          }
   }

    function fetchUniqueUploadName($originalName, $uploadDir, $fieldName)
    {
        $originalName = trim($originalName);
        if ('' == $originalName)
        {
            return $originalName;
        }
        if (!@is_dir($uploadDir))
        {
            return $originalName;
        }
        if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName]))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName] = array();
            $resDir = @opendir($uploadDir);
            if (!$resDir)
            {
                return $originalName;
            }
            while (false !== ($fileName = @readdir($resDir)))
            {
                if (@is_file($uploadDir . $fileName))
                {
                    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName][] = $fileName;
                }
            }
            @closedir($resDir);
        }
        if (!in_array($originalName, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName]))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName][] = $originalName;
            return $originalName;
        }
        else
        {
            $newName = $this->fetchFileNextName($originalName, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName]);
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['upload_dir'][$fieldName][] = $newName;
            return $newName;
        }
    } // fetchUniqueUploadName

    function fetchFileNextName($uniqueName, $uniqueList)
    {
        $aPathinfo     = pathinfo($uniqueName);
        $fileExtension = $aPathinfo['extension'];
        $fileName      = $aPathinfo['filename'];
        $foundName     = false;
        $nameIt        = 1;
        if ('' != $fileExtension)
        {
            $fileExtension = '.' . $fileExtension;
        }
        while (!$foundName)
        {
            $testName = $fileName . '(' . $nameIt . ')' . $fileExtension;
            if (in_array($testName, $uniqueList))
            {
                $nameIt++;
            }
            else
            {
                $foundName = true;
                return $testName;
            }
        }
    } // fetchFileNextName

   function ajax_add_parameters()
   {
       $this->NM_ajax_info['btnVars']['var_btn_escalas_gidpro'] = $this->form_encode_input($this->nmgp_dados_form['idprod']);
   } // ajax_add_parameters
  function nm_proc_onload($bFormat = true)
  {
      if (!$this->NM_ajax_flag || !isset($this->nmgp_refresh_fields)) {
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_colores = $this->colores;
    $original_confcolor = $this->confcolor;
    $original_conftalla = $this->conftalla;
    $original_control_costo = $this->control_costo;
    $original_costomen = $this->costomen;
    $original_en_base_a = $this->en_base_a;
    $original_existencia = $this->existencia;
    $original_fecha_vencimiento = $this->fecha_vencimiento;
    $original_idiva = $this->idiva;
    $original_idprod = $this->idprod;
    $original_lote = $this->lote;
    $original_maneja_tcs_lfs = $this->maneja_tcs_lfs;
    $original_multiple_escala = $this->multiple_escala;
    $original_otro = $this->otro;
    $original_otro2 = $this->otro2;
    $original_por_preciominimo = $this->por_preciominimo;
    $original_precio2 = $this->precio2;
    $original_preciofull = $this->preciofull;
    $original_preciomay = $this->preciomay;
    $original_recmayamen = $this->recmayamen;
    $original_sabor = $this->sabor;
    $original_sabores = $this->sabores;
    $original_serial_codbarras = $this->serial_codbarras;
    $original_stockmen = $this->stockmen;
    $original_tallas = $this->tallas;
    $original_u_menor = $this->u_menor;
    $original_unidad_ = $this->unidad_;
    $original_unidad_ma = $this->unidad_ma;
    $original_unidmaymen = $this->unidmaymen;
    $original_unimay = $this->unimay;
    $original_unimen = $this->unimen;
}
if (!isset($this->sc_temp_regimen_emp)) {$this->sc_temp_regimen_emp = (isset($_SESSION['regimen_emp'])) ? $_SESSION['regimen_emp'] : "";}
  
	
	
if ($this->sc_temp_regimen_emp==0)
	{
	$this->idiva =1;
	$this->nmgp_cmp_hidden["idiva"] = "off"; $this->NM_ajax_info['fieldDisplay']['idiva'] = 'off';
	}
if($this->stockmen >0)
	{
		$this->NM_ajax_info['buttonDisplay']['delete'] = $this->nmgp_botoes["delete"] = "off";;
	}
else
	{$this->NM_ajax_info['buttonDisplay']['delete'] = $this->nmgp_botoes["delete"] = "on";;}

if($this->otro ==1)
	{
	$this->nmgp_cmp_hidden["otro2"] = "on"; $this->NM_ajax_info['fieldDisplay']['otro2'] = 'on';
	}
else
	{
	$this->nmgp_cmp_hidden["otro2"] = "off"; $this->NM_ajax_info['fieldDisplay']['otro2'] = 'off';
	}
if ($this->unidmaymen =='NO')
	{
	$this->sc_ajax_javascript('nm_field_disabled', array("unimay=disabled;costomay=disabled;recmayamen=disabled", ""));
;
	$this->sc_ajax_javascript('nm_field_disabled', array("preciofull=disabled;precio2=disabled;preciomay=disabled", ""));
;
	}
else
	{
	$this->sc_ajax_javascript('nm_field_disabled', array("unimay=;costomay=;recmayamen=", ""));
;
	$this->sc_ajax_javascript('nm_field_disabled', array("preciofull=;precio2=;preciomay=", ""));
;
	}

if($this->idprod >0)
	{
	if($this->maneja_tcs_lfs =='CTS')
		{
		$this->nmgp_cmp_hidden["colores"] = "on"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'on';
		$this->nmgp_cmp_hidden["tallas"] = "on"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'on';
		$this->nmgp_cmp_hidden["sabores"] = "on"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'on';
		$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
		$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
		$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
		if($this->colores =='SI')
			{
			$this->nmgp_cmp_hidden["confcolor"] = "on"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'on';
			}
		else
			{
			$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
			}
		if($this->tallas =='SI')
			{
			$this->nmgp_cmp_hidden["conftalla"] = "on"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'on';
			}
		else
			{
			$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
			}
		if($this->sabores =='SI')
			{
			$this->nmgp_cmp_hidden["sabor"] = "on"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'on';
			}
		else
			{
			$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
			}
		}
	else if($this->maneja_tcs_lfs =='LFS')
		{
		$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
		$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
		$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
		$this->nmgp_cmp_hidden["fecha_vencimiento"] = "on"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'on';
		$this->nmgp_cmp_hidden["lote"] = "on"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'on';
		$this->nmgp_cmp_hidden["serial_codbarras"] = "on"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'on';
		$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
		$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
		$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
		}
	else
		{
		$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
		$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
		$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
		$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
		$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
		$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
		$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
		$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
		$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
		}
	}
else
	{
	$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
	$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
	$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
	$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
	$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
	$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
	$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
	$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
	$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
	}
 
      $nm_select = "select control_costo from configuraciones where idconfiguraciones=1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->dt_conf = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->dt_conf[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->dt_conf = false;
          $this->dt_conf_erro = $this->Db->ErrorMsg();
      } 
;
if(isset($this->dt_conf[0][0]))
	{
	if($this->dt_conf[0][0]=='SI')
		{
		$this->nmgp_cmp_hidden["control_costo"] = "on"; $this->NM_ajax_info['fieldDisplay']['control_costo'] = 'on';
		if($this->control_costo =='PC')
			{
			$this->nmgp_cmp_hidden["por_preciominimo"] = "on"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'on';
			}
		else
			{
			$this->nmgp_cmp_hidden["por_preciominimo"] = "off"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'off';
			}
		}
	else
		{
		$this->nmgp_cmp_hidden["control_costo"] = "off"; $this->NM_ajax_info['fieldDisplay']['control_costo'] = 'off';
		$this->nmgp_cmp_hidden["por_preciominimo"] = "off"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'off';
		}
	}
else
	{
	$this->nmgp_cmp_hidden["control_costo"] = "off"; $this->NM_ajax_info['fieldDisplay']['control_costo'] = 'off';
	$this->nmgp_cmp_hidden["por_preciominimo"] = "off"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'off';
	}
$vTIva=0;
if($this->idprod >0)
	{
	if($this->recmayamen ==0)
		{
		$vFmult=0;
		}
	else
		{
		$vFmult=$this->recmayamen ;
		}
	 
      $nm_select = "select trifa from iva where idiva=$this->idiva "; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->d_iva = array();
     if ($this->idiva != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->d_iva[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->d_iva = false;
          $this->d_iva_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->d_iva[0][0]))
	{
	if($this->d_iva[0][0]>0)
		{
		$vTIva=round(($this->d_iva[0][0]/100), 2);
		}
	}
	if($this->control_costo =='NA' or $this->control_costo =='UC')
		{
		if($this->unidmaymen =='SI')
			{
			$this->sugerido_mayor =round(($this->costomen *(1+$vTIva)), 0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$this->sugerido_menor =round(($vCstUMenor)*(1+$vTIva), 0);
			}
		else
			{
			$this->sugerido_menor =round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =0;
			}
		}
	elseif($this->control_costo =='CP')
		{
		if($this->unidmaymen =='SI')
			{
			$this->sugerido_mayor =round(($this->costomen *(1+$vTIva)), 0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$this->sugerido_menor =round(($vCstUMenor)*(1+$vTIva), 0);
			}
		else
			{
			$this->sugerido_menor =round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =0;
			}
		}
	else
		{
		$this->nmgp_cmp_hidden["por_preciominimo"] = "on"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'on';
		if($this->unidmaymen =='SI')
			{
			$vCosUMay=round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =round($vCosUMay*(1+($this->por_preciominimo /100)),0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			}
		else
			{
			$vCstUMenor=$this->costomen ;
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			$this->sugerido_mayor =0;
			}
		}
		
	
	}
$this->sc_field_readonly("sugerido_menor", 'on');
$this->sc_field_readonly("sugerido_mayor", 'on');

if($this->idprod >0 and $this->unidmaymen =='SI')
	{
	
	if($this->multiple_escala =='SI')
		{
		 
      $nm_select = "select factor, factor_base, representacion_factor, unidad_base from escalas_productos where id_producto=$this->idprod  order by factor, factor_base DESC Limit 1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vEscala = array();
      $this->vescala = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vEscala[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vescala[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vEscala = false;
          $this->vEscala_erro = $this->Db->ErrorMsg();
          $this->vescala = false;
          $this->vescala_erro = $this->Db->ErrorMsg();
      } 
     } 
;
		if(isset($this->vescala[0][0]))
			{
			$this->existencia =$this->stockmen *$this->vescala[0][0]*$this->vescala[0][1];
			$this->u_menor =$this->vescala[0][2].' de '.$this->vescala[0][3];
			}
		else
			{
			$this->existencia =$this->stockmen *$this->recmayamen ;
			}
		}
	else
		{
		$this->existencia =$this->stockmen *$this->recmayamen ;
		
		}
	if(isset($this->unimay ) and !empty($this->unimay ))
		{
		$u_mayor =$this->unimay ;
		}
	else
		{
		$u_mayor =$this->unimen ;
		}
	}

if($this->unidmaymen =='SI' or $this->multiple_escala =='SI')
	{
	$this->NM_ajax_info['buttonDisplay']['recalcular'] = $this->nmgp_botoes["recalcular"] = "on";;
	$this->NM_ajax_info['buttonDisplay']['escalas'] = $this->nmgp_botoes["escalas"] = "on";;
	}
else
	{
	$this->NM_ajax_info['buttonDisplay']['recalcular'] = $this->nmgp_botoes["recalcular"] = "off";;
	$this->NM_ajax_info['buttonDisplay']['escalas'] = $this->nmgp_botoes["escalas"] = "off";;
	}

if($this->idprod >0)
	{
	 
      $nm_select = "select i.idinv, d.idcombo from inventario i LEFT join detallecombos d on i.idpro=d.idproducto where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_bus = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[1] = str_replace(',', '.', $SCrx->fields[1]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 $SCrx->fields[1] = (strpos(strtolower($SCrx->fields[1]), "e")) ? (float)$SCrx->fields[1] : $SCrx->fields[1];
                 $SCrx->fields[1] = (string)$SCrx->fields[1];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_bus[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_bus = false;
          $this->ds_bus_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->ds_bus[0][0]) or (isset($this->ds_bus[0][1])))
		{
		$this->NM_ajax_info['buttonDisplay']['delete'] = $this->nmgp_botoes["delete"] = "off";;
		}
	else
		{
		$this->NM_ajax_info['buttonDisplay']['delete'] = $this->nmgp_botoes["delete"] = "on";;
		}
	}
if($this->unidmaymen =='NO')
	{
	$this->sc_field_readonly("unidad_ma", 'on');
	$this->sc_field_readonly("unidad_ma", 'on');
	}
else
	{
	$this->sc_field_readonly("unimay", 'off');
	$this->sc_field_readonly("recmayamen", 'off');
	}


if ($this->unidmaymen =='NO')
	{
	$this->en_base_a ='UMAY';
	$this->multiple_escala ='NO';
	$this->sc_field_readonly("unidad_ma", 'on');
	$this->sc_field_readonly("costomay", 'on');
	$this->sc_field_readonly("recmayamen", 'on');
	$this->sc_field_readonly("multiple_escala", 'on');
	$this->sc_field_readonly("en_base_a", 'on');
	$this->sc_field_readonly("preciofull", 'on');
	$this->sc_field_readonly("precio2", 'on');
	$this->sc_field_readonly("preciomay", 'on');
	}
else
	{
	$this->sc_field_readonly("unidad_ma", 'off');
	$this->sc_field_readonly("costomay", 'off');
	$this->sc_field_readonly("recmayamen", 'off');
	$this->sc_field_readonly("multiple_escala", 'off');
	$this->sc_field_readonly("en_base_a", 'off');
	$this->sc_field_readonly("preciofull", 'off');
	$this->sc_field_readonly("precio2", 'off');
	$this->sc_field_readonly("preciomay", 'off');
	
	$this->sc_field_readonly("multiple_escala", 'off');
	$this->sc_field_readonly("en_base_a", 'off');

	}

if($this->idprod >0)
	{
	$this->unidad_ =$this->unimen ;
	$this->unidad_ma =$this->unimay ;
	}
else
	{
	$this->en_base_a ='UMAY';
	$this->multiple_escala ='NO';
	$this->sc_field_readonly("unidad_ma", 'on');
	$this->sc_field_readonly("costomay", 'on');
	$this->sc_field_readonly("recmayamen", 'on');
	$this->sc_field_readonly("multiple_escala", 'on');
	$this->sc_field_readonly("en_base_a", 'on');
	$this->sc_field_readonly("preciofull", 'on');
	$this->sc_field_readonly("precio2", 'on');
	$this->sc_field_readonly("preciomay", 'on');
	}
if (isset($this->sc_temp_regimen_emp)) { $_SESSION['regimen_emp'] = $this->sc_temp_regimen_emp;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_colores != $this->colores || (isset($bFlagRead_colores) && $bFlagRead_colores)))
    {
        $this->ajax_return_values_colores(true);
    }
    if (($original_confcolor != $this->confcolor || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor)))
    {
        $this->ajax_return_values_confcolor(true);
    }
    if (($original_conftalla != $this->conftalla || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla)))
    {
        $this->ajax_return_values_conftalla(true);
    }
    if (($original_control_costo != $this->control_costo || (isset($bFlagRead_control_costo) && $bFlagRead_control_costo)))
    {
        $this->ajax_return_values_control_costo(true);
    }
    if (($original_costomen != $this->costomen || (isset($bFlagRead_costomen) && $bFlagRead_costomen)))
    {
        $this->ajax_return_values_costomen(true);
    }
    if (($original_en_base_a != $this->en_base_a || (isset($bFlagRead_en_base_a) && $bFlagRead_en_base_a)))
    {
        $this->ajax_return_values_en_base_a(true);
    }
    if (($original_existencia != $this->existencia || (isset($bFlagRead_existencia) && $bFlagRead_existencia)))
    {
        $this->ajax_return_values_existencia(true);
    }
    if (($original_fecha_vencimiento != $this->fecha_vencimiento || (isset($bFlagRead_fecha_vencimiento) && $bFlagRead_fecha_vencimiento)))
    {
        $this->ajax_return_values_fecha_vencimiento(true);
    }
    if (($original_idiva != $this->idiva || (isset($bFlagRead_idiva) && $bFlagRead_idiva)))
    {
        $this->ajax_return_values_idiva(true);
    }
    if (($original_idprod != $this->idprod || (isset($bFlagRead_idprod) && $bFlagRead_idprod)))
    {
        $this->ajax_return_values_idprod(true);
    }
    if (($original_lote != $this->lote || (isset($bFlagRead_lote) && $bFlagRead_lote)))
    {
        $this->ajax_return_values_lote(true);
    }
    if (($original_maneja_tcs_lfs != $this->maneja_tcs_lfs || (isset($bFlagRead_maneja_tcs_lfs) && $bFlagRead_maneja_tcs_lfs)))
    {
        $this->ajax_return_values_maneja_tcs_lfs(true);
    }
    if (($original_multiple_escala != $this->multiple_escala || (isset($bFlagRead_multiple_escala) && $bFlagRead_multiple_escala)))
    {
        $this->ajax_return_values_multiple_escala(true);
    }
    if (($original_otro != $this->otro || (isset($bFlagRead_otro) && $bFlagRead_otro)))
    {
        $this->ajax_return_values_otro(true);
    }
    if (($original_otro2 != $this->otro2 || (isset($bFlagRead_otro2) && $bFlagRead_otro2)))
    {
        $this->ajax_return_values_otro2(true);
    }
    if (($original_por_preciominimo != $this->por_preciominimo || (isset($bFlagRead_por_preciominimo) && $bFlagRead_por_preciominimo)))
    {
        $this->ajax_return_values_por_preciominimo(true);
    }
    if (($original_precio2 != $this->precio2 || (isset($bFlagRead_precio2) && $bFlagRead_precio2)))
    {
        $this->ajax_return_values_precio2(true);
    }
    if (($original_preciofull != $this->preciofull || (isset($bFlagRead_preciofull) && $bFlagRead_preciofull)))
    {
        $this->ajax_return_values_preciofull(true);
    }
    if (($original_preciomay != $this->preciomay || (isset($bFlagRead_preciomay) && $bFlagRead_preciomay)))
    {
        $this->ajax_return_values_preciomay(true);
    }
    if (($original_recmayamen != $this->recmayamen || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen)))
    {
        $this->ajax_return_values_recmayamen(true);
    }
    if (($original_sabor != $this->sabor || (isset($bFlagRead_sabor) && $bFlagRead_sabor)))
    {
        $this->ajax_return_values_sabor(true);
    }
    if (($original_sabores != $this->sabores || (isset($bFlagRead_sabores) && $bFlagRead_sabores)))
    {
        $this->ajax_return_values_sabores(true);
    }
    if (($original_serial_codbarras != $this->serial_codbarras || (isset($bFlagRead_serial_codbarras) && $bFlagRead_serial_codbarras)))
    {
        $this->ajax_return_values_serial_codbarras(true);
    }
    if (($original_stockmen != $this->stockmen || (isset($bFlagRead_stockmen) && $bFlagRead_stockmen)))
    {
        $this->ajax_return_values_stockmen(true);
    }
    if (($original_tallas != $this->tallas || (isset($bFlagRead_tallas) && $bFlagRead_tallas)))
    {
        $this->ajax_return_values_tallas(true);
    }
    if (($original_u_menor != $this->u_menor || (isset($bFlagRead_u_menor) && $bFlagRead_u_menor)))
    {
        $this->ajax_return_values_u_menor(true);
    }
    if (($original_unidad_ != $this->unidad_ || (isset($bFlagRead_unidad_) && $bFlagRead_unidad_)))
    {
        $this->ajax_return_values_unidad_(true);
    }
    if (($original_unidad_ma != $this->unidad_ma || (isset($bFlagRead_unidad_ma) && $bFlagRead_unidad_ma)))
    {
        $this->ajax_return_values_unidad_ma(true);
    }
    if (($original_unidmaymen != $this->unidmaymen || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen)))
    {
        $this->ajax_return_values_unidmaymen(true);
    }
    if (($original_unimay != $this->unimay || (isset($bFlagRead_unimay) && $bFlagRead_unimay)))
    {
        $this->ajax_return_values_unimay(true);
    }
    if (($original_unimen != $this->unimen || (isset($bFlagRead_unimen) && $bFlagRead_unimen)))
    {
        $this->ajax_return_values_unimen(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
      }
      $this->nm_guardar_campos();
      if ($bFormat) $this->nm_formatar_campos();
  }
//
//----------------------------------------------------
//-----> 
//----------------------------------------------------
//
   function nm_troca_decimal($sc_parm1, $sc_parm2) 
   { 
      $this->stockmen = str_replace($sc_parm1, $sc_parm2, $this->stockmen); 
      $this->costomen = str_replace($sc_parm1, $sc_parm2, $this->costomen); 
      $this->costo_prom = str_replace($sc_parm1, $sc_parm2, $this->costo_prom); 
      $this->existencia = str_replace($sc_parm1, $sc_parm2, $this->existencia); 
      $this->por_preciominimo = str_replace($sc_parm1, $sc_parm2, $this->por_preciominimo); 
      $this->sugerido_mayor = str_replace($sc_parm1, $sc_parm2, $this->sugerido_mayor); 
      $this->sugerido_menor = str_replace($sc_parm1, $sc_parm2, $this->sugerido_menor); 
      $this->preciofull = str_replace($sc_parm1, $sc_parm2, $this->preciofull); 
      $this->precio2 = str_replace($sc_parm1, $sc_parm2, $this->precio2); 
      $this->preciomay = str_replace($sc_parm1, $sc_parm2, $this->preciomay); 
      $this->preciomen = str_replace($sc_parm1, $sc_parm2, $this->preciomen); 
      $this->preciomen2 = str_replace($sc_parm1, $sc_parm2, $this->preciomen2); 
      $this->preciomen3 = str_replace($sc_parm1, $sc_parm2, $this->preciomen3); 
      $this->costomay = str_replace($sc_parm1, $sc_parm2, $this->costomay); 
      $this->stockmay = str_replace($sc_parm1, $sc_parm2, $this->stockmay); 
   } 
   function nm_poe_aspas_decimal() 
   { 
      $this->stockmen = "'" . $this->stockmen . "'";
      $this->costomen = "'" . $this->costomen . "'";
      $this->costo_prom = "'" . $this->costo_prom . "'";
      $this->existencia = "'" . $this->existencia . "'";
      $this->por_preciominimo = "'" . $this->por_preciominimo . "'";
      $this->sugerido_mayor = "'" . $this->sugerido_mayor . "'";
      $this->sugerido_menor = "'" . $this->sugerido_menor . "'";
      $this->preciofull = "'" . $this->preciofull . "'";
      $this->precio2 = "'" . $this->precio2 . "'";
      $this->preciomay = "'" . $this->preciomay . "'";
      $this->preciomen = "'" . $this->preciomen . "'";
      $this->preciomen2 = "'" . $this->preciomen2 . "'";
      $this->preciomen3 = "'" . $this->preciomen3 . "'";
      $this->costomay = "'" . $this->costomay . "'";
      $this->stockmay = "'" . $this->stockmay . "'";
   } 
   function nm_tira_aspas_decimal() 
   { 
      $this->stockmen = str_replace("'", "", $this->stockmen); 
      $this->costomen = str_replace("'", "", $this->costomen); 
      $this->costo_prom = str_replace("'", "", $this->costo_prom); 
      $this->existencia = str_replace("'", "", $this->existencia); 
      $this->por_preciominimo = str_replace("'", "", $this->por_preciominimo); 
      $this->sugerido_mayor = str_replace("'", "", $this->sugerido_mayor); 
      $this->sugerido_menor = str_replace("'", "", $this->sugerido_menor); 
      $this->preciofull = str_replace("'", "", $this->preciofull); 
      $this->precio2 = str_replace("'", "", $this->precio2); 
      $this->preciomay = str_replace("'", "", $this->preciomay); 
      $this->preciomen = str_replace("'", "", $this->preciomen); 
      $this->preciomen2 = str_replace("'", "", $this->preciomen2); 
      $this->preciomen3 = str_replace("'", "", $this->preciomen3); 
      $this->costomay = str_replace("'", "", $this->costomay); 
      $this->stockmay = str_replace("'", "", $this->stockmay); 
   } 
//----------- 

   function return_after_insert()
   {
      global $sc_where;
      $sc_where_pos = " WHERE ((idprod > $this->idprod))";
      if ('' != $sc_where)
      {
          if ('where ' == strtolower(substr(trim($sc_where), 0, 6)))
          {
              $sc_where = substr(trim($sc_where), 6);
          }
          if ('and ' == strtolower(substr(trim($sc_where), 0, 4)))
          {
              $sc_where = substr(trim($sc_where), 4);
          }
          $sc_where_pos .= ' AND (' . $sc_where . ')';
          $sc_where = ' WHERE ' . $sc_where;
      }
      if ('' != $this->idprod)
      {
          $nmgp_sel_count = 'SELECT COUNT(*) AS countTest FROM ' . $this->Ini->nm_tabela . $sc_where_pos;
          $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_sel_count;
          $rsc = $this->Db->Execute($nmgp_sel_count);
          if ($rsc === false && !$rsc->EOF)
          {
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg());
              exit;
          }
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = $rsc->fields[0];
          $rsc->Close();
      }
   }

   function temRegistros($sWhere)
   {
       if ('' == $sWhere)
       {
           return false;
       }
       $nmgp_sel_count = 'SELECT COUNT(*) AS countTest FROM ' . $this->Ini->nm_tabela . ' WHERE ' . $sWhere;
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_sel_count; 
       $rsc = $this->Db->Execute($nmgp_sel_count); 
       if ($rsc === false && !$rsc->EOF)
       {
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg());
           exit; 
       }
       $iTotal = $rsc->fields[0];
       $rsc->Close();
       return 0 < $iTotal;
   } // temRegistros

   function deletaRegistros($sWhere)
   {
       if ('' == $sWhere)
       {
           return false;
       }
       $nmgp_sel_count = 'DELETE FROM ' . $this->Ini->nm_tabela . ' WHERE ' . $sWhere;
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_sel_count; 
       $rsc = $this->Db->Execute($nmgp_sel_count); 
       $bResult = $rsc;
       $rsc->Close();
       return $bResult == true;
   } // deletaRegistros
    function handleDbErrorMessage(&$dbErrorMessage, $dbErrorCode)
    {
        if (1267 == $dbErrorCode) {
            $dbErrorMessage = $this->Ini->Nm_lang['lang_errm_db_invalid_collation'];
        }
    }


   function nm_acessa_banco() 
   { 
      global  $nm_form_submit, $teste_validade, $sc_where;
 
      $NM_val_null = array();
      $NM_val_form = array();
      $this->sc_erro_insert = "";
      $this->sc_erro_update = "";
      $this->sc_erro_delete = "";
      if (!empty($this->sc_force_zero))
      {
          foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
          {
              eval('if ($this->' . $sc_force_zero_field . ' == 0) {$this->' . $sc_force_zero_field . ' = "";}');
          }
      }
      $this->sc_force_zero = array();
    if ("alterar" == $this->nmgp_opcao) {
      $this->sc_evento = $this->nmgp_opcao;
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  

$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
    }
    if ("excluir" == $this->nmgp_opcao) {
      $this->sc_evento = $this->nmgp_opcao;
      $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_idprod = $this->idprod;
}
              /* inventario */
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM inventario WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM inventario WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM inventario WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM inventario WHERE idpro = " . $this->idprod ;
      }
      else
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM inventario WHERE idpro = " . $this->idprod ;
      }
       
      $nm_select = $sc_cmd_dependency; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->dataset_inventario = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->dataset_inventario[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->dataset_inventario = false;
          $this->dataset_inventario_erro = $this->Db->ErrorMsg();
      } 
;

      if($this->dataset_inventario[0][0] > 0)
      {
          
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "" . $this->Ini->Nm_lang['lang_errm_dele_rhcr'] . "";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "" . $this->Ini->Nm_lang['lang_errm_dele_rhcr'] . "";
 }
;
      }

            /* transferencia */
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM transferencia WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM transferencia WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM transferencia WHERE idpro = " . $this->idprod ;
      }
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM transferencia WHERE idpro = " . $this->idprod ;
      }
      else
      {
          $sc_cmd_dependency = "SELECT COUNT(*) AS countTest FROM transferencia WHERE idpro = " . $this->idprod ;
      }
       
      $nm_select = $sc_cmd_dependency; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->dataset_transferencia = array();
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->dataset_transferencia[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->dataset_transferencia = false;
          $this->dataset_transferencia_erro = $this->Db->ErrorMsg();
      } 
;

      if($this->dataset_transferencia[0][0] > 0)
      {
          
 if (!isset($this->Campos_Mens_erro)){$this->Campos_Mens_erro = "";}
 if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= "" . $this->Ini->Nm_lang['lang_errm_dele_rhcr'] . "";
 if ('submit_form' == $this->NM_ajax_opcao || 'event_' == substr($this->NM_ajax_opcao, 0, 6) || (isset($this->wizard_action) && 'change_step' == $this->wizard_action))
 {
  if (isset($this->wizard_action) && 'change_step' == $this->wizard_action) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } elseif ('submit_form' == $this->NM_ajax_opcao) {
   $sErrorIndex = 'geral_form_productos_060522_mob';
  } else {
   $sErrorIndex = substr(substr($this->NM_ajax_opcao, 0, strrpos($this->NM_ajax_opcao, '_')), 6);
  }
  $this->NM_ajax_info['errList'][$sErrorIndex][] = "" . $this->Ini->Nm_lang['lang_errm_dele_rhcr'] . "";
 }
;
      }
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_idprod != $this->idprod || (isset($bFlagRead_idprod) && $bFlagRead_idprod)))
    {
        $this->ajax_return_values_idprod(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
    }
      if (!empty($this->Campos_Mens_erro)) 
      {
          $this->Erro->mensagem(__FILE__, __LINE__, "critica", $this->Campos_Mens_erro); 
          $this->Campos_Mens_erro = ""; 
          $this->nmgp_opc_ant = $this->nmgp_opcao ; 
          if ($this->nmgp_opcao == "incluir") 
          { 
              $GLOBALS["erro_incl"] = 1; 
          }
          else
          { 
              $this->sc_evento = ""; 
          }
          if ($this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "incluir" || $this->nmgp_opcao == "excluir") 
          {
              $this->nmgp_opcao = "nada"; 
          } 
          $this->NM_rollback_db(); 
          $this->Campos_Mens_erro = ""; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $salva_opcao = $this->nmgp_opcao; 
      if ($this->sc_evento != "novo" && $this->sc_evento != "incluir") 
      { 
          $this->sc_evento = ""; 
      } 
      if (!in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) && !$this->Ini->sc_tem_trans_banco && in_array($this->nmgp_opcao, array('excluir', 'incluir', 'alterar')))
      { 
          $this->Ini->sc_tem_trans_banco = $this->Db->BeginTrans(); 
      } 
      $NM_val_form['codigoprod'] = $this->codigoprod;
      $NM_val_form['codigobar'] = $this->codigobar;
      $NM_val_form['nompro'] = $this->nompro;
      $NM_val_form['idgrup'] = $this->idgrup;
      $NM_val_form['idpro1'] = $this->idpro1;
      $NM_val_form['tipo_producto'] = $this->tipo_producto;
      $NM_val_form['idpro2'] = $this->idpro2;
      $NM_val_form['otro'] = $this->otro;
      $NM_val_form['otro2'] = $this->otro2;
      $NM_val_form['precio_editable'] = $this->precio_editable;
      $NM_val_form['maneja_tcs_lfs'] = $this->maneja_tcs_lfs;
      $NM_val_form['stockmen'] = $this->stockmen;
      $NM_val_form['unidmaymen'] = $this->unidmaymen;
      $NM_val_form['unimay'] = $this->unimay;
      $NM_val_form['unimen'] = $this->unimen;
      $NM_val_form['unidad_ma'] = $this->unidad_ma;
      $NM_val_form['unidad_'] = $this->unidad_;
      $NM_val_form['multiple_escala'] = $this->multiple_escala;
      $NM_val_form['en_base_a'] = $this->en_base_a;
      $NM_val_form['costomen'] = $this->costomen;
      $NM_val_form['costo_prom'] = $this->costo_prom;
      $NM_val_form['recmayamen'] = $this->recmayamen;
      $NM_val_form['idiva'] = $this->idiva;
      $NM_val_form['existencia'] = $this->existencia;
      $NM_val_form['u_menor'] = $this->u_menor;
      $NM_val_form['ubicacion'] = $this->ubicacion;
      $NM_val_form['activo'] = $this->activo;
      $NM_val_form['colores'] = $this->colores;
      $NM_val_form['confcolor'] = $this->confcolor;
      $NM_val_form['tallas'] = $this->tallas;
      $NM_val_form['conftalla'] = $this->conftalla;
      $NM_val_form['sabores'] = $this->sabores;
      $NM_val_form['sabor'] = $this->sabor;
      $NM_val_form['fecha_vencimiento'] = $this->fecha_vencimiento;
      $NM_val_form['lote'] = $this->lote;
      $NM_val_form['serial_codbarras'] = $this->serial_codbarras;
      $NM_val_form['relleno'] = $this->relleno;
      $NM_val_form['control_costo'] = $this->control_costo;
      $NM_val_form['por_preciominimo'] = $this->por_preciominimo;
      $NM_val_form['sugerido_mayor'] = $this->sugerido_mayor;
      $NM_val_form['sugerido_menor'] = $this->sugerido_menor;
      $NM_val_form['preciofull'] = $this->preciofull;
      $NM_val_form['precio2'] = $this->precio2;
      $NM_val_form['preciomay'] = $this->preciomay;
      $NM_val_form['preciomen'] = $this->preciomen;
      $NM_val_form['preciomen2'] = $this->preciomen2;
      $NM_val_form['preciomen3'] = $this->preciomen3;
      $NM_val_form['imagen'] = $this->imagen;
      $NM_val_form['cod_cuenta'] = $this->cod_cuenta;
      $NM_val_form['idprod'] = $this->idprod;
      $NM_val_form['id_marca'] = $this->id_marca;
      $NM_val_form['id_linea'] = $this->id_linea;
      $NM_val_form['codigobar2'] = $this->codigobar2;
      $NM_val_form['codigobar3'] = $this->codigobar3;
      $NM_val_form['para_registro_fe'] = $this->para_registro_fe;
      $NM_val_form['costomay'] = $this->costomay;
      $NM_val_form['stockmay'] = $this->stockmay;
      $NM_val_form['imagenprod'] = $this->imagenprod;
      $NM_val_form['imconsumo'] = $this->imconsumo;
      $NM_val_form['escombo'] = $this->escombo;
      $NM_val_form['idcombo'] = $this->idcombo;
      $NM_val_form['fecha_fab'] = $this->fecha_fab;
      $NM_val_form['ultima_compra'] = $this->ultima_compra;
      $NM_val_form['n_ultcompra'] = $this->n_ultcompra;
      $NM_val_form['ultima_venta'] = $this->ultima_venta;
      $NM_val_form['n_ultventa'] = $this->n_ultventa;
      $NM_val_form['nube'] = $this->nube;
      if ($this->idprod === "" || is_null($this->idprod))  
      { 
          $this->idprod = 0;
      } 
      if ($this->costomay === "" || is_null($this->costomay))  
      { 
          $this->costomay = 0;
          $this->sc_force_zero[] = 'costomay';
      } 
      if ($this->costomen === "" || is_null($this->costomen))  
      { 
          $this->costomen = 0;
          $this->sc_force_zero[] = 'costomen';
      } 
      if ($this->nmgp_opcao == "alterar")
      {
      if ($this->recmayamen === "" || is_null($this->recmayamen))  
      { 
          $this->recmayamen = 0;
          $this->sc_force_zero[] = 'recmayamen';
      } 
      }
      if ($this->preciomen === "" || is_null($this->preciomen))  
      { 
          $this->preciomen = 0;
          $this->sc_force_zero[] = 'preciomen';
      } 
      if ($this->preciomen2 === "" || is_null($this->preciomen2))  
      { 
          $this->preciomen2 = 0;
          $this->sc_force_zero[] = 'preciomen2';
      } 
      if ($this->preciomen3 === "" || is_null($this->preciomen3))  
      { 
          $this->preciomen3 = 0;
          $this->sc_force_zero[] = 'preciomen3';
      } 
      if ($this->precio2 === "" || is_null($this->precio2))  
      { 
          $this->precio2 = 0;
          $this->sc_force_zero[] = 'precio2';
      } 
      if ($this->preciomay === "" || is_null($this->preciomay))  
      { 
          $this->preciomay = 0;
          $this->sc_force_zero[] = 'preciomay';
      } 
      if ($this->preciofull === "" || is_null($this->preciofull))  
      { 
          $this->preciofull = 0;
          $this->sc_force_zero[] = 'preciofull';
      } 
      if ($this->nmgp_opcao == "alterar")
      {
      if ($this->stockmay === "" || is_null($this->stockmay))  
      { 
          $this->stockmay = 0;
          $this->sc_force_zero[] = 'stockmay';
      } 
      }
      if ($this->nmgp_opcao == "alterar")
      {
      if ($this->stockmen === "" || is_null($this->stockmen))  
      { 
          $this->stockmen = 0;
          $this->sc_force_zero[] = 'stockmen';
      } 
      }
      if ($this->idgrup === "" || is_null($this->idgrup))  
      { 
          $this->idgrup = 0;
          $this->sc_force_zero[] = 'idgrup';
      } 
      if ($this->idpro1 === "" || is_null($this->idpro1))  
      { 
          $this->idpro1 = 0;
          $this->sc_force_zero[] = 'idpro1';
      } 
      if ($this->idpro2 === "" || is_null($this->idpro2))  
      { 
          $this->idpro2 = 0;
          $this->sc_force_zero[] = 'idpro2';
      } 
      if ($this->idiva === "" || is_null($this->idiva))  
      { 
          $this->idiva = 0;
          $this->sc_force_zero[] = 'idiva';
      } 
      if ($this->otro === "" || is_null($this->otro))  
      { 
          $this->otro = 0;
          $this->sc_force_zero[] = 'otro';
      } 
      if ($this->otro2 === "" || is_null($this->otro2))  
      { 
          $this->otro2 = 0;
          $this->sc_force_zero[] = 'otro2';
      } 
      if ($this->imconsumo === "" || is_null($this->imconsumo))  
      { 
          $this->imconsumo = 0;
          $this->sc_force_zero[] = 'imconsumo';
      } 
      if ($this->idcombo === "" || is_null($this->idcombo))  
      { 
          $this->idcombo = 0;
          $this->sc_force_zero[] = 'idcombo';
      } 
      if ($this->por_preciominimo === "" || is_null($this->por_preciominimo))  
      { 
          $this->por_preciominimo = 0;
          $this->sc_force_zero[] = 'por_preciominimo';
      } 
      if ($this->id_marca === "" || is_null($this->id_marca))  
      { 
          $this->id_marca = 0;
          $this->sc_force_zero[] = 'id_marca';
      } 
      if ($this->id_linea === "" || is_null($this->id_linea))  
      { 
          $this->id_linea = 0;
          $this->sc_force_zero[] = 'id_linea';
      } 
      if ($this->existencia === "" || is_null($this->existencia))  
      { 
          $this->existencia = 0;
          $this->sc_force_zero[] = 'existencia';
      } 
      if ($this->costo_prom === "" || is_null($this->costo_prom))  
      { 
          $this->costo_prom = 0;
          $this->sc_force_zero[] = 'costo_prom';
      } 
      $nm_bases_lob_geral = array_merge($this->Ini->nm_bases_oracle, $this->Ini->nm_bases_ibase, $this->Ini->nm_bases_informix, $this->Ini->nm_bases_mysql, $this->Ini->nm_bases_access, $this->Ini->nm_bases_sqlite, array('pdo_ibm'), array('pdo_sqlsrv'));
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",") 
      {
          $this->nm_troca_decimal(".", ",");
      }
      if ($this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "incluir") 
      {
          $this->codigobar_before_qstr = $this->codigobar;
          $this->codigobar = substr($this->Db->qstr($this->codigobar), 1, -1); 
          if ($this->codigobar == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->codigobar = "null"; 
              $NM_val_null[] = "codigobar";
          } 
          $this->codigoprod_before_qstr = $this->codigoprod;
          $this->codigoprod = substr($this->Db->qstr($this->codigoprod), 1, -1); 
          if ($this->codigoprod == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->codigoprod = "null"; 
              $NM_val_null[] = "codigoprod";
          } 
          $this->nompro_before_qstr = $this->nompro;
          $this->nompro = substr($this->Db->qstr($this->nompro), 1, -1); 
          if ($this->nompro == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->nompro = "null"; 
              $NM_val_null[] = "nompro";
          } 
          if ($this->unidmaymen == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->unidmaymen = "null"; 
              $NM_val_null[] = "unidmaymen";
          } 
          $this->unimay_before_qstr = $this->unimay;
          $this->unimay = substr($this->Db->qstr($this->unimay), 1, -1); 
          if ($this->unimay == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->unimay = "null"; 
              $NM_val_null[] = "unimay";
          } 
          $this->unimen_before_qstr = $this->unimen;
          $this->unimen = substr($this->Db->qstr($this->unimen), 1, -1); 
          if ($this->unimen == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->unimen = "null"; 
              $NM_val_null[] = "unimen";
          } 
          if ($this->nmgp_opcao == "alterar") 
          {
          }
          if ($this->nmgp_opcao == "alterar") 
          {
          }
          if ($this->nmgp_opcao == "alterar") 
          {
          }
          if ($this->colores == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->colores = "null"; 
              $NM_val_null[] = "colores";
          } 
          if ($this->tallas == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->tallas = "null"; 
              $NM_val_null[] = "tallas";
          } 
          if ($this->sabores == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->sabores = "null"; 
              $NM_val_null[] = "sabores";
          } 
          if ($this->imagenprod == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->imagenprod = "null"; 
              $NM_val_null[] = "imagenprod";
          } 
          if ($this->escombo == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->escombo = "null"; 
              $NM_val_null[] = "escombo";
          } 
          if ($this->precio_editable == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->precio_editable = "null"; 
              $NM_val_null[] = "precio_editable";
          } 
          $this->cod_cuenta_before_qstr = $this->cod_cuenta;
          $this->cod_cuenta = substr($this->Db->qstr($this->cod_cuenta), 1, -1); 
          if ($this->cod_cuenta == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->cod_cuenta = "null"; 
              $NM_val_null[] = "cod_cuenta";
          } 
          if ($this->fecha_vencimiento == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->fecha_vencimiento = "null"; 
              $NM_val_null[] = "fecha_vencimiento";
          } 
          if ($this->fecha_fab == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->fecha_fab = "null"; 
              $NM_val_null[] = "fecha_fab";
          } 
          if ($this->lote == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->lote = "null"; 
              $NM_val_null[] = "lote";
          } 
          if ($this->serial_codbarras == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->serial_codbarras = "null"; 
              $NM_val_null[] = "serial_codbarras";
          } 
          if ($this->maneja_tcs_lfs == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->maneja_tcs_lfs = "null"; 
              $NM_val_null[] = "maneja_tcs_lfs";
          } 
          if ($this->control_costo == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->control_costo = "null"; 
              $NM_val_null[] = "control_costo";
          } 
          if ($this->ultima_compra == "")  
          { 
              $this->ultima_compra = "null"; 
              $NM_val_null[] = "ultima_compra";
          } 
          $this->n_ultcompra_before_qstr = $this->n_ultcompra;
          $this->n_ultcompra = substr($this->Db->qstr($this->n_ultcompra), 1, -1); 
          if ($this->n_ultcompra == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->n_ultcompra = "null"; 
              $NM_val_null[] = "n_ultcompra";
          } 
          if ($this->ultima_venta == "")  
          { 
              $this->ultima_venta = "null"; 
              $NM_val_null[] = "ultima_venta";
          } 
          $this->n_ultventa_before_qstr = $this->n_ultventa;
          $this->n_ultventa = substr($this->Db->qstr($this->n_ultventa), 1, -1); 
          if ($this->n_ultventa == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->n_ultventa = "null"; 
              $NM_val_null[] = "n_ultventa";
          } 
          $this->codigobar2_before_qstr = $this->codigobar2;
          $this->codigobar2 = substr($this->Db->qstr($this->codigobar2), 1, -1); 
          if ($this->codigobar2 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->codigobar2 = "null"; 
              $NM_val_null[] = "codigobar2";
          } 
          $this->codigobar3_before_qstr = $this->codigobar3;
          $this->codigobar3 = substr($this->Db->qstr($this->codigobar3), 1, -1); 
          if ($this->codigobar3 == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->codigobar3 = "null"; 
              $NM_val_null[] = "codigobar3";
          } 
          if ($this->nube == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->nube = "null"; 
              $NM_val_null[] = "nube";
          } 
          if ($this->multiple_escala == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->multiple_escala = "null"; 
              $NM_val_null[] = "multiple_escala";
          } 
          if ($this->en_base_a == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->en_base_a = "null"; 
              $NM_val_null[] = "en_base_a";
          } 
          if ($this->activo == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->activo = "null"; 
              $NM_val_null[] = "activo";
          } 
          $this->tipo_producto_before_qstr = $this->tipo_producto;
          $this->tipo_producto = substr($this->Db->qstr($this->tipo_producto), 1, -1); 
          if ($this->tipo_producto == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->tipo_producto = "null"; 
              $NM_val_null[] = "tipo_producto";
          } 
          $this->imagen_before_qstr = $this->imagen;
          $this->imagen = substr($this->Db->qstr($this->imagen), 1, -1); 
          if ($this->imagen == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->imagen = "null"; 
              $NM_val_null[] = "imagen";
          } 
          $this->ubicacion_before_qstr = $this->ubicacion;
          $this->ubicacion = substr($this->Db->qstr($this->ubicacion), 1, -1); 
          if ($this->ubicacion == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->ubicacion = "null"; 
              $NM_val_null[] = "ubicacion";
          } 
          if ($this->para_registro_fe == "" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))  
          { 
              $this->para_registro_fe = "null"; 
              $NM_val_null[] = "para_registro_fe";
          } 
      }
      if ($this->nmgp_opcao == "alterar") 
      {
          $SC_fields_update = array(); 
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",") 
          {
              $this->nm_poe_aspas_decimal();
          }
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          else  
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod ";
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              if ($this->NM_ajax_flag)
              {
                 form_productos_060522_mob_pack_ajax_response();
              }
              exit; 
          }  
          $bUpdateOk = true;
          $tmp_result = (int) $rs1->fields[0]; 
          if ($tmp_result != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "critica", $this->Ini->Nm_lang['lang_errm_nfnd']); 
              $this->nmgp_opcao = "nada"; 
              $bUpdateOk = false;
              $this->sc_evento = 'update';
          } 
          $aUpdateOk = array();
          $bUpdateOk = $bUpdateOk && empty($aUpdateOk);
          if ($bUpdateOk)
          { 
              $rs1->Close(); 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              else 
              { 
                  $comando = "UPDATE " . $this->Ini->nm_tabela . " SET ";  
                  $SC_fields_update[] = "codigobar = '$this->codigobar', codigoprod = '$this->codigoprod', nompro = '$this->nompro', unidmaymen = '$this->unidmaymen', unimay = '$this->unimay', unimen = '$this->unimen', costomen = $this->costomen, recmayamen = $this->recmayamen, preciomen = $this->preciomen, preciomen2 = $this->preciomen2, preciomen3 = $this->preciomen3, precio2 = $this->precio2, preciomay = $this->preciomay, preciofull = $this->preciofull, stockmen = $this->stockmen, idgrup = $this->idgrup, idpro1 = $this->idpro1, idpro2 = $this->idpro2, idiva = $this->idiva, otro = $this->otro, otro2 = $this->otro2, colores = '$this->colores', tallas = '$this->tallas', sabores = '$this->sabores', precio_editable = '$this->precio_editable', cod_cuenta = '$this->cod_cuenta', fecha_vencimiento = '$this->fecha_vencimiento', lote = '$this->lote', serial_codbarras = '$this->serial_codbarras', maneja_tcs_lfs = '$this->maneja_tcs_lfs', control_costo = '$this->control_costo', por_preciominimo = $this->por_preciominimo, id_marca = $this->id_marca, id_linea = $this->id_linea, codigobar2 = '$this->codigobar2', codigobar3 = '$this->codigobar3', existencia = $this->existencia, multiple_escala = '$this->multiple_escala', en_base_a = '$this->en_base_a', activo = '$this->activo', tipo_producto = '$this->tipo_producto', costo_prom = $this->costo_prom, ubicacion = '$this->ubicacion', para_registro_fe = '$this->para_registro_fe'"; 
              } 
              if (isset($NM_val_form['costomay']) && $NM_val_form['costomay'] != $this->nmgp_dados_select['costomay']) 
              { 
                  $SC_fields_update[] = "costomay = $this->costomay"; 
              } 
              if (isset($NM_val_form['stockmay']) && $NM_val_form['stockmay'] != $this->nmgp_dados_select['stockmay']) 
              { 
                  $SC_fields_update[] = "stockmay = $this->stockmay"; 
              } 
              if (isset($NM_val_form['imconsumo']) && $NM_val_form['imconsumo'] != $this->nmgp_dados_select['imconsumo']) 
              { 
                  $SC_fields_update[] = "imconsumo = $this->imconsumo"; 
              } 
              if (isset($NM_val_form['idcombo']) && $NM_val_form['idcombo'] != $this->nmgp_dados_select['idcombo']) 
              { 
                  $SC_fields_update[] = "idcombo = $this->idcombo"; 
              } 
              if (isset($NM_val_form['fecha_fab']) && $NM_val_form['fecha_fab'] != $this->nmgp_dados_select['fecha_fab']) 
              { 
                  $SC_fields_update[] = "fecha_fab = '$this->fecha_fab'"; 
              } 
              if (isset($NM_val_form['ultima_compra']) && $NM_val_form['ultima_compra'] != $this->nmgp_dados_select['ultima_compra']) 
              { 
                  if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
                  { 
                      $SC_fields_update[] = "ultima_compra = #$this->ultima_compra#"; 
                  } 
                  elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
                  { 
                      $SC_fields_update[] = "ultima_compra = EXTEND('" . $this->ultima_compra . "', YEAR TO DAY)"; 
                  } 
                  else
                  { 
                      $SC_fields_update[] = "ultima_compra = " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ""; 
                  } 
              } 
              if (isset($NM_val_form['n_ultcompra']) && $NM_val_form['n_ultcompra'] != $this->nmgp_dados_select['n_ultcompra']) 
              { 
                  $SC_fields_update[] = "n_ultcompra = '$this->n_ultcompra'"; 
              } 
              if (isset($NM_val_form['ultima_venta']) && $NM_val_form['ultima_venta'] != $this->nmgp_dados_select['ultima_venta']) 
              { 
                  if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
                  { 
                      $SC_fields_update[] = "ultima_venta = #$this->ultima_venta#"; 
                  } 
                  elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
                  { 
                      $SC_fields_update[] = "ultima_venta = EXTEND('" . $this->ultima_venta . "', YEAR TO DAY)"; 
                  } 
                  else
                  { 
                      $SC_fields_update[] = "ultima_venta = " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ""; 
                  } 
              } 
              if (isset($NM_val_form['n_ultventa']) && $NM_val_form['n_ultventa'] != $this->nmgp_dados_select['n_ultventa']) 
              { 
                  $SC_fields_update[] = "n_ultventa = '$this->n_ultventa'"; 
              } 
              if (isset($NM_val_form['nube']) && $NM_val_form['nube'] != $this->nmgp_dados_select['nube']) 
              { 
                  $SC_fields_update[] = "nube = '$this->nube'"; 
              } 
              $aDoNotUpdate = array();
              $aEraseFiles  = array();
              $temp_cmd_sql = "";
              if ($this->imagen_limpa == "S")
              {
                  $sDirErase     = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/";
                  $aEraseFiles[] = array('dir' => $sDirErase, 'file' => $this->nmgp_dados_form['imagen']);
                  if ($this->imagen != "null")
                  {
                      $this->imagen = '';
                  }
                  if (in_array(strtolower($this->Ini->nm_tpbanco), $nm_bases_lob_geral))
                  {
                      $temp_cmd_sql = "imagen = '" . $this->imagen . "'";
                  }
                  else
                  {
                      $temp_cmd_sql = "imagen = '" . $this->imagen . "'";
                  }
                  $this->imagen = "";
              }
              else
              {
                  if ($this->imagen != "none" && $this->imagen != "")
                  {
                      $NM_conteudo =  $this->imagen;
                      if (in_array(strtolower($this->Ini->nm_tpbanco), $nm_bases_lob_geral))
                      {
                      $temp_cmd_sql .= " imagen = '$NM_conteudo'";
                      }
                      else
                      {
                          $temp_cmd_sql .= " imagen = '$NM_conteudo'";
                      }
                  }
                  else
                  {
                      $aDoNotUpdate[] = "imagen";
                  }
              }
              if (!empty($temp_cmd_sql))
              {
                  $SC_fields_update[] = $temp_cmd_sql;
              }
              $comando .= implode(",", $SC_fields_update);  
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              {
                  $comando .= " WHERE idprod = $this->idprod ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $comando .= " WHERE idprod = $this->idprod ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $comando .= " WHERE idprod = $this->idprod ";  
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $comando .= " WHERE idprod = $this->idprod ";  
              }  
              else  
              {
                  $comando .= " WHERE idprod = $this->idprod ";  
              }  
              $comando = str_replace("N'null'", "null", $comando) ; 
              $comando = str_replace("'null'", "null", $comando) ; 
              $comando = str_replace("#null#", "null", $comando) ; 
              $comando = str_replace($this->Ini->date_delim . "null" . $this->Ini->date_delim1, "null", $comando) ; 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                $comando = str_replace("EXTEND('', YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND('', YEAR TO DAY)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO DAY)", "null", $comando) ; 
              }  
              $useUpdateProcedure = false;
              if (!empty($SC_fields_update) || $useUpdateProcedure)
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = $comando; 
                  $rs = $this->Db->Execute($comando);  
                  if ($rs === false) 
                  { 
                      if (FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "MAIL SENT") && FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "WARNING"))
                      {
                          $dbErrorMessage = $this->Db->ErrorMsg();
                          $dbErrorCode = $this->Db->ErrorNo();
                          $this->handleDbErrorMessage($dbErrorMessage, $dbErrorCode);
                          $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_updt'], $dbErrorMessage, true);
                          if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler']) 
                          { 
                              $this->sc_erro_update = $dbErrorMessage;
                              $this->NM_rollback_db(); 
                              if ($this->NM_ajax_flag)
                              {
                                  form_productos_060522_mob_pack_ajax_response();
                              }
                              exit;  
                          }   
                      }   
                  }   
              }   
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
              { 
              }   
              $this->codigobar = $this->codigobar_before_qstr;
              $this->codigoprod = $this->codigoprod_before_qstr;
              $this->nompro = $this->nompro_before_qstr;
              $this->unimay = $this->unimay_before_qstr;
              $this->unimen = $this->unimen_before_qstr;
              $this->cod_cuenta = $this->cod_cuenta_before_qstr;
              $this->n_ultcompra = $this->n_ultcompra_before_qstr;
              $this->n_ultventa = $this->n_ultventa_before_qstr;
              $this->codigobar2 = $this->codigobar2_before_qstr;
              $this->codigobar3 = $this->codigobar3_before_qstr;
              $this->tipo_producto = $this->tipo_producto_before_qstr;
              $this->imagen = $this->imagen_before_qstr;
              $this->ubicacion = $this->ubicacion_before_qstr;
              if (in_array(strtolower($this->Ini->nm_tpbanco), $nm_bases_lob_geral))
              { 
              }   
              if ($this->imagen_limpa == "S")
              {
                  $this->NM_ajax_info['fldList']['imagen_salva'] = array(
                      'row'     => '',
                      'type'    => 'text',
                      'valList' => array(''),
                  );
              }
              if (!empty($aEraseFiles))
              {
                  foreach ($aEraseFiles as $aEraseData)
                  {
                      $sEraseFile = $aEraseData['dir'] . $aEraseData['file'];
                      if (@is_file($sEraseFile))
                      {
                          @unlink($sEraseFile);
                      }
                  }
              }
              $this->sc_evento = "update"; 
              $this->nmgp_opcao = "igual"; 
              $this->nm_flag_iframe = true;
              if ($this->lig_edit_lookup)
              {
                  $this->lig_edit_lookup_call = true;
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['db_changed'] = true;
              if ($this->NM_ajax_flag) {
                  $this->NM_ajax_info['clearUpload'] = 'S';
                  $this->NM_ajax_info['fldList']['imagen_salva'] = array(
                      'row'     => '',
                      'type'    => 'text',
                      'valList' => array(''),
                  );
              }


              if     (isset($NM_val_form) && isset($NM_val_form['idprod'])) { $this->idprod = $NM_val_form['idprod']; }
              elseif (isset($this->idprod)) { $this->nm_limpa_alfa($this->idprod); }
              if     (isset($NM_val_form) && isset($NM_val_form['codigobar'])) { $this->codigobar = $NM_val_form['codigobar']; }
              elseif (isset($this->codigobar)) { $this->nm_limpa_alfa($this->codigobar); }
              if     (isset($NM_val_form) && isset($NM_val_form['codigoprod'])) { $this->codigoprod = $NM_val_form['codigoprod']; }
              elseif (isset($this->codigoprod)) { $this->nm_limpa_alfa($this->codigoprod); }
              if     (isset($NM_val_form) && isset($NM_val_form['nompro'])) { $this->nompro = $NM_val_form['nompro']; }
              elseif (isset($this->nompro)) { $this->nm_limpa_alfa($this->nompro); }
              if     (isset($NM_val_form) && isset($NM_val_form['unimay'])) { $this->unimay = $NM_val_form['unimay']; }
              elseif (isset($this->unimay)) { $this->nm_limpa_alfa($this->unimay); }
              if     (isset($NM_val_form) && isset($NM_val_form['unimen'])) { $this->unimen = $NM_val_form['unimen']; }
              elseif (isset($this->unimen)) { $this->nm_limpa_alfa($this->unimen); }
              if     (isset($NM_val_form) && isset($NM_val_form['costomen'])) { $this->costomen = $NM_val_form['costomen']; }
              elseif (isset($this->costomen)) { $this->nm_limpa_alfa($this->costomen); }
              if     (isset($NM_val_form) && isset($NM_val_form['recmayamen'])) { $this->recmayamen = $NM_val_form['recmayamen']; }
              elseif (isset($this->recmayamen)) { $this->nm_limpa_alfa($this->recmayamen); }
              if     (isset($NM_val_form) && isset($NM_val_form['preciomen'])) { $this->preciomen = $NM_val_form['preciomen']; }
              elseif (isset($this->preciomen)) { $this->nm_limpa_alfa($this->preciomen); }
              if     (isset($NM_val_form) && isset($NM_val_form['preciomen2'])) { $this->preciomen2 = $NM_val_form['preciomen2']; }
              elseif (isset($this->preciomen2)) { $this->nm_limpa_alfa($this->preciomen2); }
              if     (isset($NM_val_form) && isset($NM_val_form['preciomen3'])) { $this->preciomen3 = $NM_val_form['preciomen3']; }
              elseif (isset($this->preciomen3)) { $this->nm_limpa_alfa($this->preciomen3); }
              if     (isset($NM_val_form) && isset($NM_val_form['precio2'])) { $this->precio2 = $NM_val_form['precio2']; }
              elseif (isset($this->precio2)) { $this->nm_limpa_alfa($this->precio2); }
              if     (isset($NM_val_form) && isset($NM_val_form['preciomay'])) { $this->preciomay = $NM_val_form['preciomay']; }
              elseif (isset($this->preciomay)) { $this->nm_limpa_alfa($this->preciomay); }
              if     (isset($NM_val_form) && isset($NM_val_form['preciofull'])) { $this->preciofull = $NM_val_form['preciofull']; }
              elseif (isset($this->preciofull)) { $this->nm_limpa_alfa($this->preciofull); }
              if     (isset($NM_val_form) && isset($NM_val_form['stockmen'])) { $this->stockmen = $NM_val_form['stockmen']; }
              elseif (isset($this->stockmen)) { $this->nm_limpa_alfa($this->stockmen); }
              if     (isset($NM_val_form) && isset($NM_val_form['idgrup'])) { $this->idgrup = $NM_val_form['idgrup']; }
              elseif (isset($this->idgrup)) { $this->nm_limpa_alfa($this->idgrup); }
              if     (isset($NM_val_form) && isset($NM_val_form['idpro1'])) { $this->idpro1 = $NM_val_form['idpro1']; }
              elseif (isset($this->idpro1)) { $this->nm_limpa_alfa($this->idpro1); }
              if     (isset($NM_val_form) && isset($NM_val_form['idpro2'])) { $this->idpro2 = $NM_val_form['idpro2']; }
              elseif (isset($this->idpro2)) { $this->nm_limpa_alfa($this->idpro2); }
              if     (isset($NM_val_form) && isset($NM_val_form['idiva'])) { $this->idiva = $NM_val_form['idiva']; }
              elseif (isset($this->idiva)) { $this->nm_limpa_alfa($this->idiva); }
              if     (isset($NM_val_form) && isset($NM_val_form['otro'])) { $this->otro = $NM_val_form['otro']; }
              elseif (isset($this->otro)) { $this->nm_limpa_alfa($this->otro); }
              if     (isset($NM_val_form) && isset($NM_val_form['otro2'])) { $this->otro2 = $NM_val_form['otro2']; }
              elseif (isset($this->otro2)) { $this->nm_limpa_alfa($this->otro2); }
              if     (isset($NM_val_form) && isset($NM_val_form['cod_cuenta'])) { $this->cod_cuenta = $NM_val_form['cod_cuenta']; }
              elseif (isset($this->cod_cuenta)) { $this->nm_limpa_alfa($this->cod_cuenta); }
              if     (isset($NM_val_form) && isset($NM_val_form['por_preciominimo'])) { $this->por_preciominimo = $NM_val_form['por_preciominimo']; }
              elseif (isset($this->por_preciominimo)) { $this->nm_limpa_alfa($this->por_preciominimo); }
              if     (isset($NM_val_form) && isset($NM_val_form['id_marca'])) { $this->id_marca = $NM_val_form['id_marca']; }
              elseif (isset($this->id_marca)) { $this->nm_limpa_alfa($this->id_marca); }
              if     (isset($NM_val_form) && isset($NM_val_form['id_linea'])) { $this->id_linea = $NM_val_form['id_linea']; }
              elseif (isset($this->id_linea)) { $this->nm_limpa_alfa($this->id_linea); }
              if     (isset($NM_val_form) && isset($NM_val_form['codigobar2'])) { $this->codigobar2 = $NM_val_form['codigobar2']; }
              elseif (isset($this->codigobar2)) { $this->nm_limpa_alfa($this->codigobar2); }
              if     (isset($NM_val_form) && isset($NM_val_form['codigobar3'])) { $this->codigobar3 = $NM_val_form['codigobar3']; }
              elseif (isset($this->codigobar3)) { $this->nm_limpa_alfa($this->codigobar3); }
              if     (isset($NM_val_form) && isset($NM_val_form['existencia'])) { $this->existencia = $NM_val_form['existencia']; }
              elseif (isset($this->existencia)) { $this->nm_limpa_alfa($this->existencia); }
              if     (isset($NM_val_form) && isset($NM_val_form['tipo_producto'])) { $this->tipo_producto = $NM_val_form['tipo_producto']; }
              elseif (isset($this->tipo_producto)) { $this->nm_limpa_alfa($this->tipo_producto); }
              if     (isset($NM_val_form) && isset($NM_val_form['costo_prom'])) { $this->costo_prom = $NM_val_form['costo_prom']; }
              elseif (isset($this->costo_prom)) { $this->nm_limpa_alfa($this->costo_prom); }
              if     (isset($NM_val_form) && isset($NM_val_form['ubicacion'])) { $this->ubicacion = $NM_val_form['ubicacion']; }
              elseif (isset($this->ubicacion)) { $this->nm_limpa_alfa($this->ubicacion); }

              $this->nm_formatar_campos();
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
              }

              $aOldRefresh               = $this->nmgp_refresh_fields;
              $this->nmgp_refresh_fields = array_diff(array('codigoprod', 'codigobar', 'nompro', 'idgrup', 'idpro1', 'tipo_producto', 'idpro2', 'otro', 'otro2', 'precio_editable', 'maneja_tcs_lfs', 'stockmen', 'unidmaymen', 'unimay', 'unimen', 'unidad_ma', 'unidad_', 'multiple_escala', 'en_base_a', 'costomen', 'costo_prom', 'recmayamen', 'idiva', 'existencia', 'u_menor', 'ubicacion', 'activo', 'colores', 'confcolor', 'tallas', 'conftalla', 'sabores', 'sabor', 'fecha_vencimiento', 'lote', 'serial_codbarras', 'relleno', 'control_costo', 'por_preciominimo', 'sugerido_mayor', 'sugerido_menor', 'preciofull', 'precio2', 'preciomay', 'preciomen', 'preciomen2', 'preciomen3', 'imagen', 'cod_cuenta', 'idprod', 'id_marca', 'id_linea', 'codigobar2', 'codigobar3', 'para_registro_fe'), $aDoNotUpdate);
              $this->ajax_return_values();
              $this->nmgp_refresh_fields = $aOldRefresh;

              $this->nm_tira_formatacao();
          }  
      }  
      if ($this->nmgp_opcao == "incluir") 
      { 
          $NM_cmp_auto = "";
          $NM_seq_auto = "";
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",") 
          {
              $this->nm_poe_aspas_decimal();
          }
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
          { 
              $NM_seq_auto = "NULL, ";
              $NM_cmp_auto = "idprod, ";
          } 
          $bInsertOk = true;
          $aInsertOk = array(); 
          $bInsertOk = $bInsertOk && empty($aInsertOk);
          if (!isset($_POST['nmgp_ins_valid']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['insert_validation'] != $_POST['nmgp_ins_valid'])
          {
              $bInsertOk = false;
              $this->Erro->mensagem(__FILE__, __LINE__, 'security', $this->Ini->Nm_lang['lang_errm_inst_vald']);
              if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler'])
              {
                  $this->nmgp_opcao = 'refresh_insert';
                  if ($this->NM_ajax_flag)
                  {
                      form_productos_060522_mob_pack_ajax_response();
                      exit;
                  }
              }
          }
          if ($bInsertOk)
          { 
              $_test_file = $this->fetchUniqueUploadName($this->imagenprod_scfile_name, $dir_file, "imagenprod");
              if (trim($this->imagenprod_scfile_name) != $_test_file)
              {
                  $this->imagenprod_scfile_name = $_test_file;
                  $this->imagenprod = $_test_file;
              }
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              { 
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES ('$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '$this->imagenprod', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, #$this->ultima_compra#, '$this->n_ultcompra', #$this->ultima_venta#, '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif ($this->Ini->nm_tpbanco == "pdo_sqlsrv")
              { 
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES ('$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              { 
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES ('$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '$this->imagenprod', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
              { 
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES ('$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '$this->imagenprod', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', EMPTY_BLOB(), $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', null, $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, EXTEND('$this->ultima_compra', YEAR TO DAY), '$this->n_ultcompra', EXTEND('$this->ultima_venta', YEAR TO DAY), '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              elseif ($this->Ini->nm_tpbanco =='pdo_ibm')
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', EMPTY_BLOB(), $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              else
              {
                  $compl_insert     = ""; 
                  $compl_insert_val = ""; 
                  if ($this->recmayamen != "")
                  { 
                       $compl_insert     .= ", recmayamen";
                       $compl_insert_val .= ", $this->recmayamen";
                  } 
                  if ($this->stockmay != "")
                  { 
                       $compl_insert     .= ", stockmay";
                       $compl_insert_val .= ", $this->stockmay";
                  } 
                  if ($this->stockmen != "")
                  { 
                       $compl_insert     .= ", stockmen";
                       $compl_insert_val .= ", $this->stockmen";
                  } 
                  $comando = "INSERT INTO " . $this->Ini->nm_tabela . " (" . $NM_cmp_auto . "codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe $compl_insert) VALUES (" . $NM_seq_auto . "'$this->codigobar', '$this->codigoprod', '$this->nompro', '$this->unidmaymen', '$this->unimay', '$this->unimen', $this->costomay, $this->costomen, $this->preciomen, $this->preciomen2, $this->preciomen3, $this->precio2, $this->preciomay, $this->preciofull, $this->idgrup, $this->idpro1, $this->idpro2, $this->idiva, $this->otro, $this->otro2, '$this->colores', '$this->tallas', '$this->sabores', '$this->imagenprod', $this->imconsumo, $this->idcombo, '$this->precio_editable', '$this->cod_cuenta', '$this->fecha_vencimiento', '$this->fecha_fab', '$this->lote', '$this->serial_codbarras', '$this->maneja_tcs_lfs', '$this->control_costo', $this->por_preciominimo, $this->id_marca, $this->id_linea, " . $this->Ini->date_delim . $this->ultima_compra . $this->Ini->date_delim1 . ", '$this->n_ultcompra', " . $this->Ini->date_delim . $this->ultima_venta . $this->Ini->date_delim1 . ", '$this->n_ultventa', '$this->codigobar2', '$this->codigobar3', '$this->nube', $this->existencia, '$this->multiple_escala', '$this->en_base_a', '$this->activo', '$this->tipo_producto', $this->costo_prom, '$this->imagen', '$this->ubicacion', '$this->para_registro_fe' $compl_insert_val)"; 
              }
              $comando = str_replace("N'null'", "null", $comando) ; 
              $comando = str_replace("'null'", "null", $comando) ; 
              $comando = str_replace("#null#", "null", $comando) ; 
              $comando = str_replace($this->Ini->date_delim . "null" . $this->Ini->date_delim1, "null", $comando) ; 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                $comando = str_replace("EXTEND('', YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO FRACTION)", "null", $comando) ; 
                $comando = str_replace("EXTEND('', YEAR TO DAY)", "null", $comando) ; 
                $comando = str_replace("EXTEND(null, YEAR TO DAY)", "null", $comando) ; 
              }  
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $comando; 
              $rs = $this->Db->Execute($comando); 
              if ($rs === false)  
              { 
                  if (FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "MAIL SENT") && FALSE === strpos(strtoupper($this->Db->ErrorMsg()), "WARNING"))
                  {
                      $dbErrorMessage = $this->Db->ErrorMsg();
                      $dbErrorCode = $this->Db->ErrorNo();
                      $this->handleDbErrorMessage($dbErrorMessage, $dbErrorCode);
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_inst'], $dbErrorMessage, true);
                      if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler'])
                      { 
                          $this->sc_erro_insert = $dbErrorMessage;
                          $this->nmgp_opcao     = 'refresh_insert';
                          $this->NM_rollback_db(); 
                          if ($this->NM_ajax_flag)
                          {
                              form_productos_060522_mob_pack_ajax_response();
                              exit; 
                          }
                      }  
                  }  
              }  
              if ('refresh_insert' != $this->nmgp_opcao)
              {
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select @@identity"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      $this->NM_rollback_db(); 
                      if ($this->NM_ajax_flag)
                      {
                          form_productos_060522_mob_pack_ajax_response();
                      }
                      exit; 
                  } 
                  $this->idprod =  $rsy->fields[0];
                 $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select last_insert_id()"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SELECT dbinfo('sqlca.sqlerrd1') FROM " . $this->Ini->nm_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select .currval from dual"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
              { 
                  $str_tabela = "SYSIBM.SYSDUMMY1"; 
                  if($this->Ini->nm_con_use_schema == "N") 
                  { 
                          $str_tabela = "SYSDUMMY1"; 
                  } 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SELECT IDENTITY_VAL_LOCAL() FROM " . $str_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select CURRVAL('')"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select gen_id(, 0) from " . $this->Ini->nm_tabela; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
              { 
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select last_insert_rowid()"; 
                  $rsy = $this->Db->Execute($_SESSION['scriptcase']['sc_sql_ult_comando']); 
                  if ($rsy === false && !$rsy->EOF)  
                  { 
                      $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
                      exit; 
                  } 
                  $this->idprod = $rsy->fields[0];
                  $rsy->Close(); 
              } 
              $this->codigobar = $this->codigobar_before_qstr;
              $this->codigoprod = $this->codigoprod_before_qstr;
              $this->nompro = $this->nompro_before_qstr;
              $this->unimay = $this->unimay_before_qstr;
              $this->unimen = $this->unimen_before_qstr;
              $this->cod_cuenta = $this->cod_cuenta_before_qstr;
              $this->n_ultcompra = $this->n_ultcompra_before_qstr;
              $this->n_ultventa = $this->n_ultventa_before_qstr;
              $this->codigobar2 = $this->codigobar2_before_qstr;
              $this->codigobar3 = $this->codigobar3_before_qstr;
              $this->tipo_producto = $this->tipo_producto_before_qstr;
              $this->imagen = $this->imagen_before_qstr;
              $this->ubicacion = $this->ubicacion_before_qstr;
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
              { 
              }   
              if (in_array(strtolower($this->Ini->nm_tpbanco), $nm_bases_lob_geral))
              { 
                  if (trim($this->imagenprod ) != "") 
                  { 
                      $_SESSION['scriptcase']['sc_sql_ult_comando'] = "UpdateBlob(" . $this->Ini->nm_tabela . ",  imagenprod , $this->imagenprod,  \"idprod = $this->idprod\")"; 
                      $rs = $this->Db->UpdateBlob($this->Ini->nm_tabela, "imagenprod", $this->imagenprod,  "idprod = $this->idprod"); 
                      if ($rs === false)  
                      { 
                          $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_inst'], $this->Db->ErrorMsg()); 
                          $this->NM_rollback_db(); 
                          if ($this->NM_ajax_flag)
                          {
                              form_productos_060522_mob_pack_ajax_response();
                          }
                          exit; 
                      }  
                  }  
              }  
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['db_changed'] = true;

              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']))
              {
                  unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']);
              }

              $dir_img = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/"; 
              if (nm_mkdir($dir_img))  
              { 
                  $arq_imagen = fopen($this->SC_IMG_imagen, "r") ; 
                  $reg_imagen = fread($arq_imagen, filesize($this->SC_IMG_imagen)) ; 
                  fclose($arq_imagen) ;  
                  $arq_imagen = fopen($dir_img . trim($this->imagen_scfile_name), "w") ; 
                  fwrite($arq_imagen, $reg_imagen) ;  
                  fclose($arq_imagen) ;  
              } 
              $this->sc_evento = "insert"; 
              $this->codigobar = $this->codigobar_before_qstr;
              $this->codigoprod = $this->codigoprod_before_qstr;
              $this->nompro = $this->nompro_before_qstr;
              $this->unimay = $this->unimay_before_qstr;
              $this->unimen = $this->unimen_before_qstr;
              $this->cod_cuenta = $this->cod_cuenta_before_qstr;
              $this->n_ultcompra = $this->n_ultcompra_before_qstr;
              $this->n_ultventa = $this->n_ultventa_before_qstr;
              $this->codigobar2 = $this->codigobar2_before_qstr;
              $this->codigobar3 = $this->codigobar3_before_qstr;
              $this->tipo_producto = $this->tipo_producto_before_qstr;
              $this->imagen = $this->imagen_before_qstr;
              $this->ubicacion = $this->ubicacion_before_qstr;
              $this->sc_insert_on = true; 
              if (empty($this->sc_erro_insert)) {
                  $this->record_insert_ok = true;
              } 
              if ('refresh_insert' != $this->nmgp_opcao && (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_redir_insert']) || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_redir_insert'] != "S"))
              {
              $this->nmgp_opcao   = "igual"; 
              $this->nmgp_opc_ant = "igual"; 
              $this->nmgp_botoes['sc_btn_0'] = "on";
              $this->nmgp_botoes['sc_btn_1'] = "on";
              $this->nmgp_botoes['sc_btn_2'] = "on";
              $this->nmgp_botoes['escalas'] = "on";
              $this->nmgp_botoes['recalcular'] = "on";
              $this->return_after_insert();
              }
              $this->nm_flag_iframe = true;
          } 
          if ($this->lig_edit_lookup)
          {
              $this->lig_edit_lookup_call = true;
          }
      } 
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",") 
      {
          $this->nm_tira_aspas_decimal();
      }
      if ($this->nmgp_opcao == "excluir") 
      { 
          $this->idprod = substr($this->Db->qstr($this->idprod), 1, -1); 

          $bDelecaoOk = true;
          $sMsgErro   = '';

          if ($bDelecaoOk)
          {

          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          else  
          {
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod"; 
              $rs1 = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              exit; 
          }  
          if ($rs1 === false)  
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dbas'], $this->Db->ErrorMsg()); 
              exit; 
          }  
          $aEraseFiles = array();
          $tmp_result = (int) $rs1->fields[0]; 
          if ($tmp_result != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "critica", $this->Ini->Nm_lang['lang_errm_dele_nfnd']); 
              $this->nmgp_opcao = "nada"; 
              $this->sc_evento = 'delete';
          } 
          else 
          { 
              $rs1->Close(); 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
              }  
              else  
              {
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = "DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "; 
                  $rs = $this->Db->Execute("DELETE FROM " . $this->Ini->nm_tabela . " where idprod = $this->idprod "); 
              }  
              if ($rs === false) 
              { 
                  $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dele'], $this->Db->ErrorMsg(), true); 
                  if (isset($_SESSION['scriptcase']['erro_handler']) && $_SESSION['scriptcase']['erro_handler']) 
                  { 
                      $this->sc_erro_delete = $this->Db->ErrorMsg();  
                      $this->NM_rollback_db(); 
                      if ($this->NM_ajax_flag)
                      {
                          form_productos_060522_mob_pack_ajax_response();
                          exit; 
                      }
                  } 
              } 
                  $sDirErase     = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/"; 
                  $aEraseFiles[] = array('dir' => $sDirErase, 'file' => $this->nmgp_dados_form['imagen']);
              if (!empty($aEraseFiles))
              {
                  foreach ($aEraseFiles as $aEraseData)
                  {
                      $sEraseFile = $aEraseData['dir'] . $aEraseData['file'];
                      if (@is_file($sEraseFile))
                      {
                          @unlink($sEraseFile);
                      }
                  }
              }
              $this->sc_evento = "delete"; 
              if (empty($this->sc_erro_delete)) {
                  $this->record_delete_ok = true;
              }
              $this->nmgp_opcao = "avanca"; 
              $this->nm_flag_iframe = true;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']--; 
              if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] < 0)
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = 0; 
              }

              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['db_changed'] = true;

              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']))
              {
                  unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']);
              }

              if ($this->lig_edit_lookup)
              {
                  $this->lig_edit_lookup_call = true;
              }
          }

          }
          else
          {
              $this->sc_evento = "delete"; 
              $this->nmgp_opcao = "igual"; 
              $this->Erro->mensagem(__FILE__, __LINE__, "critica", $sMsgErro); 
          }

      }  
      if (!empty($this->sc_force_zero))
      {
          foreach ($this->sc_force_zero as $i_force_zero => $sc_force_zero_field)
          {
              eval('if ($this->' . $sc_force_zero_field . ' == 0) {$this->' . $sc_force_zero_field . ' = "";}');
          }
      }
      $this->sc_force_zero = array();
      if (!empty($NM_val_null))
      {
          foreach ($NM_val_null as $i_val_null => $sc_val_null_field)
          {
              eval('$this->' . $sc_val_null_field . ' = "";');
          }
      }
    if ("insert" == $this->sc_evento && $this->nmgp_opcao != "nada") {
        if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",")
        {
            $this->nm_troca_decimal(",", ".");
        }
        $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_codigobar = $this->codigobar;
    $original_idprod = $this->idprod;
    $original_maneja_tcs_lfs = $this->maneja_tcs_lfs;
    $original_nompro = $this->nompro;
    $original_precio2 = $this->precio2;
    $original_preciofull = $this->preciofull;
    $original_preciomay = $this->preciomay;
    $original_preciomen = $this->preciomen;
    $original_preciomen2 = $this->preciomen2;
    $original_preciomen3 = $this->preciomen3;
    $original_recmayamen = $this->recmayamen;
    $original_unidmaymen = $this->unidmaymen;
    $original_unimay = $this->unimay;
    $original_unimen = $this->unimen;
}
if (!isset($this->sc_temp_par_idproducto)) {$this->sc_temp_par_idproducto = (isset($_SESSION['par_idproducto'])) ? $_SESSION['par_idproducto'] : "";}
if (!isset($this->sc_temp_gidtercero)) {$this->sc_temp_gidtercero = (isset($_SESSION['gidtercero'])) ? $_SESSION['gidtercero'] : "";}
  $this->sc_temp_par_idproducto=$this->idprod ;


     $nm_select ="insert into log set usuario='".$this->sc_temp_gidtercero."',accion='AGREGAR', observaciones='EL USUARIO AGREGÓ EL PRODUCTO: $this->codigobar  - $this->nompro , TIPO PRODUCTO: $this->maneja_tcs_lfs' "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;

if($this->unidmaymen =='SI' and $this->recmayamen >=1)
	{
	 
      $nm_select = "select id_escala from escalas_productos where id_producto='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vCep = array();
      $this->vcep = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vCep[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vcep[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vCep = false;
          $this->vCep_erro = $this->Db->ErrorMsg();
          $this->vcep = false;
          $this->vcep_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->vcep[0][0]))
		{
		
		}
	else
		{
		
     $nm_select ="insert escalas_productos set id_producto='".$this->idprod ."', factor=1, unidad_base='".$this->unimay ."', factor_base=1, precio_full='".$this->preciofull ."', precio_medio='".$this->precio2 ."', precio_minimo='".$this->preciomay ."', descripcion_prod='".$this->nompro ."', representacion_factor='".$this->unimay ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		
		
     $nm_select ="insert escalas_productos set id_producto='".$this->idprod ."', factor=1, unidad_base='".$this->unimay ."', factor_base='".$this->recmayamen ."', precio_full='".$this->preciomen ."', precio_medio='".$this->preciomen2 ."', precio_minimo='".$this->preciomen3 ."', descripcion_prod='".$this->nompro ."', representacion_factor='".$this->unimen ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}
if (isset($this->sc_temp_gidtercero)) { $_SESSION['gidtercero'] = $this->sc_temp_gidtercero;}
if (isset($this->sc_temp_par_idproducto)) { $_SESSION['par_idproducto'] = $this->sc_temp_par_idproducto;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_codigobar != $this->codigobar || (isset($bFlagRead_codigobar) && $bFlagRead_codigobar)))
    {
        $this->ajax_return_values_codigobar(true);
    }
    if (($original_idprod != $this->idprod || (isset($bFlagRead_idprod) && $bFlagRead_idprod)))
    {
        $this->ajax_return_values_idprod(true);
    }
    if (($original_maneja_tcs_lfs != $this->maneja_tcs_lfs || (isset($bFlagRead_maneja_tcs_lfs) && $bFlagRead_maneja_tcs_lfs)))
    {
        $this->ajax_return_values_maneja_tcs_lfs(true);
    }
    if (($original_nompro != $this->nompro || (isset($bFlagRead_nompro) && $bFlagRead_nompro)))
    {
        $this->ajax_return_values_nompro(true);
    }
    if (($original_precio2 != $this->precio2 || (isset($bFlagRead_precio2) && $bFlagRead_precio2)))
    {
        $this->ajax_return_values_precio2(true);
    }
    if (($original_preciofull != $this->preciofull || (isset($bFlagRead_preciofull) && $bFlagRead_preciofull)))
    {
        $this->ajax_return_values_preciofull(true);
    }
    if (($original_preciomay != $this->preciomay || (isset($bFlagRead_preciomay) && $bFlagRead_preciomay)))
    {
        $this->ajax_return_values_preciomay(true);
    }
    if (($original_preciomen != $this->preciomen || (isset($bFlagRead_preciomen) && $bFlagRead_preciomen)))
    {
        $this->ajax_return_values_preciomen(true);
    }
    if (($original_preciomen2 != $this->preciomen2 || (isset($bFlagRead_preciomen2) && $bFlagRead_preciomen2)))
    {
        $this->ajax_return_values_preciomen2(true);
    }
    if (($original_preciomen3 != $this->preciomen3 || (isset($bFlagRead_preciomen3) && $bFlagRead_preciomen3)))
    {
        $this->ajax_return_values_preciomen3(true);
    }
    if (($original_recmayamen != $this->recmayamen || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen)))
    {
        $this->ajax_return_values_recmayamen(true);
    }
    if (($original_unidmaymen != $this->unidmaymen || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen)))
    {
        $this->ajax_return_values_unidmaymen(true);
    }
    if (($original_unimay != $this->unimay || (isset($bFlagRead_unimay) && $bFlagRead_unimay)))
    {
        $this->ajax_return_values_unimay(true);
    }
    if (($original_unimen != $this->unimen || (isset($bFlagRead_unimen) && $bFlagRead_unimen)))
    {
        $this->ajax_return_values_unimen(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
    }
    if ("update" == $this->sc_evento && $this->nmgp_opcao != "nada") {
        $_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    $original_codigobar = $this->codigobar;
    $original_existencia = $this->existencia;
    $original_idprod = $this->idprod;
    $original_maneja_tcs_lfs = $this->maneja_tcs_lfs;
    $original_nompro = $this->nompro;
    $original_precio2 = $this->precio2;
    $original_preciofull = $this->preciofull;
    $original_preciomay = $this->preciomay;
    $original_preciomen = $this->preciomen;
    $original_preciomen2 = $this->preciomen2;
    $original_preciomen3 = $this->preciomen3;
    $original_recmayamen = $this->recmayamen;
    $original_unidmaymen = $this->unidmaymen;
    $original_unimay = $this->unimay;
    $original_unimen = $this->unimen;
}
if (!isset($this->sc_temp_par_idproducto)) {$this->sc_temp_par_idproducto = (isset($_SESSION['par_idproducto'])) ? $_SESSION['par_idproducto'] : "";}
if (!isset($this->sc_temp_gidtercero)) {$this->sc_temp_gidtercero = (isset($_SESSION['gidtercero'])) ? $_SESSION['gidtercero'] : "";}
  $this->sc_temp_par_idproducto=$this->idprod ;

     $nm_select ="insert into log set usuario='".$this->sc_temp_gidtercero."',accion='EDITAR', observaciones='EL USUARIO EDITÓ EL PRODUCTO: $this->codigobar  - $this->nompro , TIPO PRODUCTO: $this->maneja_tcs_lfs' "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;


$vStmen=0;
if($this->unidmaymen =='SI' and $this->recmayamen >=1)
	{
	 
      $nm_select = "select id_escala from escalas_productos where id_producto='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vCep = array();
      $this->vcep = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vCep[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vcep[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vCep = false;
          $this->vCep_erro = $this->Db->ErrorMsg();
          $this->vcep = false;
          $this->vcep_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->vcep[0][0]))
		{
		goto salir;
		}
	else
		{
		$vStmen=round($this->existencia /$this->recmayamen , 3);
		
     $nm_select ="insert escalas_productos set id_producto='".$this->idprod ."', factor=1, unidad_base='".$this->unimay ."', factor_base=1, precio_full='".$this->preciofull ."', precio_medio='".$this->precio2 ."', precio_minimo='".$this->preciomay ."', descripcion_prod='".$this->nompro ."', representacion_factor='".$this->unimay ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		
		
     $nm_select ="insert escalas_productos set id_producto='".$this->idprod ."', factor=1, unidad_base='".$this->unimay ."', factor_base='".$this->recmayamen ."', precio_full='".$this->preciomen ."', precio_medio='".$this->preciomen2 ."', precio_minimo='".$this->preciomen3 ."', descripcion_prod='".$this->nompro ."', representacion_factor='".$this->unimen ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		
		
     $nm_select ="update productos set stockmen=$vStmen idprod='".$this->idprod ."'"; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}
salir:;
if (isset($this->sc_temp_gidtercero)) { $_SESSION['gidtercero'] = $this->sc_temp_gidtercero;}
if (isset($this->sc_temp_par_idproducto)) { $_SESSION['par_idproducto'] = $this->sc_temp_par_idproducto;}
if (isset($this->NM_ajax_flag) && $this->NM_ajax_flag)
{
    if (($original_codigobar != $this->codigobar || (isset($bFlagRead_codigobar) && $bFlagRead_codigobar)))
    {
        $this->ajax_return_values_codigobar(true);
    }
    if (($original_existencia != $this->existencia || (isset($bFlagRead_existencia) && $bFlagRead_existencia)))
    {
        $this->ajax_return_values_existencia(true);
    }
    if (($original_idprod != $this->idprod || (isset($bFlagRead_idprod) && $bFlagRead_idprod)))
    {
        $this->ajax_return_values_idprod(true);
    }
    if (($original_maneja_tcs_lfs != $this->maneja_tcs_lfs || (isset($bFlagRead_maneja_tcs_lfs) && $bFlagRead_maneja_tcs_lfs)))
    {
        $this->ajax_return_values_maneja_tcs_lfs(true);
    }
    if (($original_nompro != $this->nompro || (isset($bFlagRead_nompro) && $bFlagRead_nompro)))
    {
        $this->ajax_return_values_nompro(true);
    }
    if (($original_precio2 != $this->precio2 || (isset($bFlagRead_precio2) && $bFlagRead_precio2)))
    {
        $this->ajax_return_values_precio2(true);
    }
    if (($original_preciofull != $this->preciofull || (isset($bFlagRead_preciofull) && $bFlagRead_preciofull)))
    {
        $this->ajax_return_values_preciofull(true);
    }
    if (($original_preciomay != $this->preciomay || (isset($bFlagRead_preciomay) && $bFlagRead_preciomay)))
    {
        $this->ajax_return_values_preciomay(true);
    }
    if (($original_preciomen != $this->preciomen || (isset($bFlagRead_preciomen) && $bFlagRead_preciomen)))
    {
        $this->ajax_return_values_preciomen(true);
    }
    if (($original_preciomen2 != $this->preciomen2 || (isset($bFlagRead_preciomen2) && $bFlagRead_preciomen2)))
    {
        $this->ajax_return_values_preciomen2(true);
    }
    if (($original_preciomen3 != $this->preciomen3 || (isset($bFlagRead_preciomen3) && $bFlagRead_preciomen3)))
    {
        $this->ajax_return_values_preciomen3(true);
    }
    if (($original_recmayamen != $this->recmayamen || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen)))
    {
        $this->ajax_return_values_recmayamen(true);
    }
    if (($original_unidmaymen != $this->unidmaymen || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen)))
    {
        $this->ajax_return_values_unidmaymen(true);
    }
    if (($original_unimay != $this->unimay || (isset($bFlagRead_unimay) && $bFlagRead_unimay)))
    {
        $this->ajax_return_values_unimay(true);
    }
    if (($original_unimen != $this->unimen || (isset($bFlagRead_unimen) && $bFlagRead_unimen)))
    {
        $this->ajax_return_values_unimen(true);
    }
}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off'; 
    }
      if (!empty($this->Campos_Mens_erro)) 
      {
          $this->Erro->mensagem(__FILE__, __LINE__, "critica", $this->Campos_Mens_erro); 
          $this->Campos_Mens_erro = ""; 
          $this->nmgp_opc_ant = $salva_opcao ; 
          if ($salva_opcao == "incluir") 
          { 
              $GLOBALS["erro_incl"] = 1; 
          }
          if ($this->nmgp_opcao == "alterar" || $this->nmgp_opcao == "incluir" || $this->nmgp_opcao == "excluir") 
          {
              $this->nmgp_opcao = "nada"; 
          } 
          $this->sc_evento = ""; 
          $this->NM_rollback_db(); 
          return; 
      }
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ",")
   {
       $this->nm_troca_decimal(".", ",");
   }
      if ($salva_opcao == "incluir" && $GLOBALS["erro_incl"] != 1) 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['parms'] = "idprod?#?$this->idprod?@?"; 
      }
      $this->nmgp_dados_form['imagen'] = ""; 
      $this->imagen_limpa = ""; 
      $this->imagen_salva = ""; 
      $this->NM_commit_db(); 
      if ($this->sc_evento != "insert" && $this->sc_evento != "update" && $this->sc_evento != "delete")
      { 
          $this->idprod = null === $this->idprod ? null : substr($this->Db->qstr($this->idprod), 1, -1); 
      } 
      if (isset($this->NM_where_filter))
      {
          $this->NM_where_filter = str_replace("@percent@", "%", $this->NM_where_filter);
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'] = trim($this->NM_where_filter);
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']))
          {
              unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']);
          }
      }
      $sc_where_filter = '';
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form'])
      {
          $sc_where_filter = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'] && $sc_where_filter != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'])
      {
          if (empty($sc_where_filter))
          {
              $sc_where_filter = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'];
          }
          else
          {
              $sc_where_filter .= " and (" . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'] . ")";
          }
      }
//------------ 
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "R")
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['iframe_evento'] = $this->sc_evento; 
      } 
      if (!isset($this->nmgp_opcao) || empty($this->nmgp_opcao)) 
      { 
          if (empty($this->idprod)) 
          { 
              $this->nmgp_opcao = "inicio"; 
          } 
          else 
          { 
              $this->nmgp_opcao = "igual"; 
          } 
      } 
      if (isset($_POST['master_nav']) && 'on' == $_POST['master_nav']) 
      { 
          $this->nmgp_opcao = "inicio";
      } 
      if ($this->nmgp_opcao != "nada" && (trim($this->idprod) == "")) 
      { 
          if ($this->nmgp_opcao == "avanca")  
          { 
              $this->nmgp_opcao = "final"; 
          } 
          elseif ($this->nmgp_opcao != "novo")
          { 
              $this->nmgp_opcao = "inicio"; 
          } 
      } 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
      { 
          $GLOBALS["NM_ERRO_IBASE"] = 1;  
      } 
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" && $this->sc_evento == "insert")
      {
          $this->nmgp_opcao = "final";
      }
      $sc_where = trim("");
      if (substr(strtolower($sc_where), 0, 5) == "where")
      {
          $sc_where  = substr($sc_where , 5);
      }
      if (!empty($sc_where))
      {
          $sc_where = " where " . $sc_where . " ";
      }
      if ('' != $sc_where_filter)
      {
          $sc_where = ('' != $sc_where) ? $sc_where . ' and (' . $sc_where_filter . ')' : ' where ' . $sc_where_filter;
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']))
      { 
          $nmgp_select = "SELECT count(*) AS countTest from " . $this->Ini->nm_tabela . $sc_where; 
          $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
          $rt = $this->Db->Execute($nmgp_select) ; 
          if ($rt === false && !$rt->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          $qt_geral_reg_form_productos_060522_mob = isset($rt->fields[0]) ? $rt->fields[0] - 1 : 0; 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'] = $qt_geral_reg_form_productos_060522_mob;
          $rt->Close(); 
          if ($this->nmgp_opcao == "igual" && isset($this->NM_btn_navega) && 'S' == $this->NM_btn_navega && !empty($this->idprod))
          {
              $Reg_OK      = false;
              $Count_start = -1;
              $sc_order_by = "";
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $Sel_Chave = "idprod"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $Sel_Chave = "idprod"; 
              }
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $Sel_Chave = "idprod"; 
              }
              else  
              {
                  $Sel_Chave = "idprod"; 
              }
              $nmgp_select = "SELECT " . $Sel_Chave . " from " . $this->Ini->nm_tabela . $sc_where; 
              $sc_order_by = "idprod desc";
              $sc_order_by = str_replace("order by ", "", $sc_order_by);
              $sc_order_by = str_replace("ORDER BY ", "", trim($sc_order_by));
              if (!empty($sc_order_by))
              {
                  $nmgp_select .= " order by $sc_order_by "; 
              }
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
              $rt = $this->Db->Execute($nmgp_select) ; 
              if ($rt === false && !$rt->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
              { 
                  $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
                  exit ; 
              }  
              while (!$rt->EOF && !$Reg_OK)
              { 
                  if ($rt->fields[0] == $this->idprod)
                  { 
                      $Reg_OK = true;
                  }  
                  $Count_start++;
                  $rt->MoveNext();
              }  
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = $Count_start;
              $rt->Close(); 
          }
      } 
      else 
      { 
          $qt_geral_reg_form_productos_060522_mob = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'];
      } 
      if ($this->nmgp_opcao == "inicio") 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = 0; 
      } 
      if ($this->nmgp_opcao == "avanca")  
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']++; 
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] > $qt_geral_reg_form_productos_060522_mob)
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = $qt_geral_reg_form_productos_060522_mob; 
          }
      } 
      if ($this->nmgp_opcao == "retorna") 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']--; 
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] < 0)
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = 0; 
          }
      } 
      if ($this->nmgp_opcao == "final") 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = $qt_geral_reg_form_productos_060522_mob; 
      } 
      if ($this->nmgp_opcao == "navpage" && ($this->nmgp_ordem - 1) <= $qt_geral_reg_form_productos_060522_mob) 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = $this->nmgp_ordem - 1; 
      } 
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']) || empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] = 0;
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_qtd'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] + 1;
      $this->NM_ajax_info['navSummary']['reg_ini'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] + 1; 
      $this->NM_ajax_info['navSummary']['reg_qtd'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_qtd']; 
      $this->NM_ajax_info['navSummary']['reg_tot'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'] + 1; 
      $this->NM_gera_nav_page(); 
      $this->NM_ajax_info['navPage'] = $this->SC_nav_page; 
      $GLOBALS["NM_ERRO_IBASE"] = 0;  
//---------- 
      if ($this->nmgp_opcao != "novo" && $this->nmgp_opcao != "nada" && $this->nmgp_opcao != "refresh_insert") 
      { 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['parms'] = ""; 
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 1;  
          } 
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
          { 
              $nmgp_select = "SELECT idprod, codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, recmayamen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, stockmay, stockmen, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, escombo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, str_replace (convert(char(10),ultima_compra,102), '.', '-') + ' ' + convert(char(8),ultima_compra,20), n_ultcompra, str_replace (convert(char(10),ultima_venta,102), '.', '-') + ' ' + convert(char(8),ultima_venta,20), n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          { 
              $nmgp_select = "SELECT idprod, codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, recmayamen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, stockmay, stockmen, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, escombo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, convert(char(23),ultima_compra,121), n_ultcompra, convert(char(23),ultima_venta,121), n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          { 
              $nmgp_select = "SELECT idprod, codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, recmayamen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, stockmay, stockmen, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, escombo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe from " . $this->Ini->nm_tabela ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $nmgp_select = "SELECT idprod, codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, recmayamen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, stockmay, stockmen, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, LOTOFILE(imagenprod, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_imagenprod', 'client'), imconsumo, escombo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, EXTEND(ultima_compra, YEAR TO DAY), n_ultcompra, EXTEND(ultima_venta, YEAR TO DAY), n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe from " . $this->Ini->nm_tabela ; 
          } 
          else 
          { 
              $nmgp_select = "SELECT idprod, codigobar, codigoprod, nompro, unidmaymen, unimay, unimen, costomay, costomen, recmayamen, preciomen, preciomen2, preciomen3, precio2, preciomay, preciofull, stockmay, stockmen, idgrup, idpro1, idpro2, idiva, otro, otro2, colores, tallas, sabores, imagenprod, imconsumo, escombo, idcombo, precio_editable, cod_cuenta, fecha_vencimiento, fecha_fab, lote, serial_codbarras, maneja_tcs_lfs, control_costo, por_preciominimo, id_marca, id_linea, ultima_compra, n_ultcompra, ultima_venta, n_ultventa, codigobar2, codigobar3, nube, existencia, multiple_escala, en_base_a, activo, tipo_producto, costo_prom, imagen, ubicacion, para_registro_fe from " . $this->Ini->nm_tabela ; 
          } 
          $aWhere = array();
          $aWhere[] = $sc_where_filter;
          if ($this->nmgp_opcao == "igual" || (($_SESSION['sc_session'][$this->Ini->sc_page]['form_adm_clientes']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_adm_clientes']['run_iframe'] == "R") && ($this->sc_evento == "insert" || $this->sc_evento == "update")) )
          { 
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
              {
                  $aWhere[] = "idprod = $this->idprod"; 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
              {
                  $aWhere[] = "idprod = $this->idprod"; 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
              {
                  $aWhere[] = "idprod = $this->idprod"; 
              }  
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              {
                  $aWhere[] = "idprod = $this->idprod"; 
              }  
              else  
              {
                  $aWhere[] = "idprod = $this->idprod"; 
              }  
              if (!empty($sc_where_filter))  
              {
                  $teste_select = $nmgp_select . $this->returnWhere($aWhere);
                  $_SESSION['scriptcase']['sc_sql_ult_comando'] = $teste_select; 
                  $rs = $this->Db->Execute($teste_select); 
                  if ($rs->EOF)
                  {
                     $aWhere = array($sc_where_filter);
                  }  
                  $rs->Close(); 
              }  
          } 
          $nmgp_select .= $this->returnWhere($aWhere) . ' ';
          $sc_order_by = "";
          $sc_order_by = "idprod desc";
          $sc_order_by = str_replace("order by ", "", $sc_order_by);
          $sc_order_by = str_replace("ORDER BY ", "", trim($sc_order_by));
          if (!empty($sc_order_by))
          {
              $nmgp_select .= " order by $sc_order_by "; 
          }
          if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "R")
          {
              if ($this->sc_evento == "insert" || $this->sc_evento == "update")
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['select'] = $nmgp_select;
                  $this->nm_gera_html();
              } 
              elseif (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['select']))
              { 
                  $nmgp_select = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['select'];
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['select'] = ""; 
              } 
          } 
          if ($this->nmgp_opcao == "igual") 
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
              $rs = $this->Db->Execute($nmgp_select) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase) || in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']) ; 
          } 
          elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = "SelectLimit($nmgp_select, 1, " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] . ")" ; 
              $rs = $this->Db->SelectLimit($nmgp_select, 1, $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']) ; 
          } 
          else  
          { 
              $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
              $rs = $this->Db->Execute($nmgp_select) ; 
              if (!$rs === false && !$rs->EOF) 
              { 
                  $rs->Move($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start']) ;  
              } 
          } 
          if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
          { 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          if ($rs === false && $GLOBALS["NM_ERRO_IBASE"] == 1) 
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_nfnd_extr'], $this->Db->ErrorMsg()); 
              exit ; 
          }  
          if ($rs->EOF) 
          { 
              if (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter']))
              {
                  $this->nmgp_form_empty        = true;
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['first']   = "off";
                  $this->NM_ajax_info['buttonDisplay']['back']    = $this->nmgp_botoes['back']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['forward'] = $this->nmgp_botoes['forward'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['last']    = $this->nmgp_botoes['last']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['update']  = $this->nmgp_botoes['update']  = "off";
                  $this->NM_ajax_info['buttonDisplay']['delete']  = $this->nmgp_botoes['delete']  = "off";
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['insert']  = "off";
                  $this->NM_ajax_info['buttonDisplay']['sc_btn_0'] = $this->nmgp_botoes['sc_btn_0'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['sc_btn_1'] = $this->nmgp_botoes['sc_btn_1'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['sc_btn_2'] = $this->nmgp_botoes['sc_btn_2'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['escalas'] = $this->nmgp_botoes['escalas'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['recalcular'] = $this->nmgp_botoes['recalcular'] = "off";
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter'] = true;
                  return; 
              }
              if ($this->nmgp_botoes['insert'] != "on")
              {
                  $this->nmgp_form_empty        = true;
                  $this->NM_ajax_info['buttonDisplay']['first']   = $this->nmgp_botoes['first']   = "off";
                  $this->NM_ajax_info['buttonDisplay']['back']    = $this->nmgp_botoes['back']    = "off";
                  $this->NM_ajax_info['buttonDisplay']['forward'] = $this->nmgp_botoes['forward'] = "off";
                  $this->NM_ajax_info['buttonDisplay']['last']    = $this->nmgp_botoes['last']    = "off";
              }
              $this->nmgp_opcao = "novo"; 
              $this->nm_flag_saida_novo = "S"; 
              $rs->Close(); 
              $this->NM_ajax_info['buttonDisplay']['sc_btn_0'] = $this->nmgp_botoes['sc_btn_0'] = "on";
              $this->NM_ajax_info['buttonDisplay']['sc_btn_1'] = $this->nmgp_botoes['sc_btn_1'] = "on";
              $this->NM_ajax_info['buttonDisplay']['sc_btn_2'] = $this->nmgp_botoes['sc_btn_2'] = "on";
              $this->NM_ajax_info['buttonDisplay']['escalas'] = $this->nmgp_botoes['escalas'] = "off";
              $this->NM_ajax_info['buttonDisplay']['recalcular'] = $this->nmgp_botoes['recalcular'] = "off";
              if ($this->aba_iframe)
              {
                  $this->NM_ajax_info['buttonDisplay']['exit'] = $this->nmgp_botoes['exit'] = 'off';
              }
          } 
          if ($rs === false && $GLOBALS["NM_ERRO_IBASE"] == 1) 
          { 
              $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->Erro->mensagem (__FILE__, __LINE__, "critica", $this->Ini->Nm_lang['lang_errm_nfnd_extr']); 
              $this->nmgp_opcao = "novo"; 
          }  
          if ($this->nmgp_opcao != "novo") 
          { 
              $this->idprod = $rs->fields[0] ; 
              $this->nmgp_dados_select['idprod'] = $this->idprod;
              $this->codigobar = $rs->fields[1] ; 
              $this->nmgp_dados_select['codigobar'] = $this->codigobar;
              $this->codigoprod = $rs->fields[2] ; 
              $this->nmgp_dados_select['codigoprod'] = $this->codigoprod;
              $this->nompro = $rs->fields[3] ; 
              $this->nmgp_dados_select['nompro'] = $this->nompro;
              $this->unidmaymen = $rs->fields[4] ; 
              $this->nmgp_dados_select['unidmaymen'] = $this->unidmaymen;
              $this->unimay = $rs->fields[5] ; 
              $this->nmgp_dados_select['unimay'] = $this->unimay;
              $this->unimen = $rs->fields[6] ; 
              $this->nmgp_dados_select['unimen'] = $this->unimen;
              $this->costomay = $rs->fields[7] ; 
              $this->nmgp_dados_select['costomay'] = $this->costomay;
              $this->costomen = $rs->fields[8] ; 
              $this->nmgp_dados_select['costomen'] = $this->costomen;
              $this->recmayamen = $rs->fields[9] ; 
              $this->nmgp_dados_select['recmayamen'] = $this->recmayamen;
              $this->preciomen = $rs->fields[10] ; 
              $this->nmgp_dados_select['preciomen'] = $this->preciomen;
              $this->preciomen2 = $rs->fields[11] ; 
              $this->nmgp_dados_select['preciomen2'] = $this->preciomen2;
              $this->preciomen3 = $rs->fields[12] ; 
              $this->nmgp_dados_select['preciomen3'] = $this->preciomen3;
              $this->precio2 = $rs->fields[13] ; 
              $this->nmgp_dados_select['precio2'] = $this->precio2;
              $this->preciomay = $rs->fields[14] ; 
              $this->nmgp_dados_select['preciomay'] = $this->preciomay;
              $this->preciofull = $rs->fields[15] ; 
              $this->nmgp_dados_select['preciofull'] = $this->preciofull;
              $this->stockmay = $rs->fields[16] ; 
              $this->nmgp_dados_select['stockmay'] = $this->stockmay;
              $this->stockmen = $rs->fields[17] ; 
              $this->nmgp_dados_select['stockmen'] = $this->stockmen;
              $this->idgrup = $rs->fields[18] ; 
              $this->nmgp_dados_select['idgrup'] = $this->idgrup;
              $this->idpro1 = $rs->fields[19] ; 
              $this->nmgp_dados_select['idpro1'] = $this->idpro1;
              $this->idpro2 = $rs->fields[20] ; 
              $this->nmgp_dados_select['idpro2'] = $this->idpro2;
              $this->idiva = $rs->fields[21] ; 
              $this->nmgp_dados_select['idiva'] = $this->idiva;
              $this->otro = $rs->fields[22] ; 
              $this->nmgp_dados_select['otro'] = $this->otro;
              $this->otro2 = $rs->fields[23] ; 
              $this->nmgp_dados_select['otro2'] = $this->otro2;
              $this->colores = $rs->fields[24] ; 
              $this->nmgp_dados_select['colores'] = $this->colores;
              $this->tallas = $rs->fields[25] ; 
              $this->nmgp_dados_select['tallas'] = $this->tallas;
              $this->sabores = $rs->fields[26] ; 
              $this->nmgp_dados_select['sabores'] = $this->sabores;
              if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
              { 
                  $this->imagenprod = $this->Db->BlobDecode($rs->fields[27]) ; 
              } 
              elseif ($this->Ini->nm_tpbanco == 'pdo_oracle')
              { 
                  $this->imagenprod = $this->Db->BlobDecode($rs->fields[27]) ; 
              } 
              elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
              { 
                  if(isset($rs->fields[27]) && !empty($rs->fields[27]) && is_file($rs->fields[27])) 
                  { 
                     $this->imagenprod = file_get_contents($rs->fields[27]);
                  }else 
                  { 
                     $this->imagenprod = ''; 
                  } 
              } 
              else
              { 
                  $this->imagenprod = $rs->fields[27] ; 
              } 
              $this->nmgp_dados_select['imagenprod'] = $this->imagenprod;
              $this->imconsumo = $rs->fields[28] ; 
              $this->nmgp_dados_select['imconsumo'] = $this->imconsumo;
              $this->escombo = $rs->fields[29] ; 
              $this->nmgp_dados_select['escombo'] = $this->escombo;
              $this->idcombo = $rs->fields[30] ; 
              $this->nmgp_dados_select['idcombo'] = $this->idcombo;
              $this->precio_editable = $rs->fields[31] ; 
              $this->nmgp_dados_select['precio_editable'] = $this->precio_editable;
              $this->cod_cuenta = $rs->fields[32] ; 
              $this->nmgp_dados_select['cod_cuenta'] = $this->cod_cuenta;
              $this->fecha_vencimiento = $rs->fields[33] ; 
              $this->nmgp_dados_select['fecha_vencimiento'] = $this->fecha_vencimiento;
              $this->fecha_fab = $rs->fields[34] ; 
              $this->nmgp_dados_select['fecha_fab'] = $this->fecha_fab;
              $this->lote = $rs->fields[35] ; 
              $this->nmgp_dados_select['lote'] = $this->lote;
              $this->serial_codbarras = $rs->fields[36] ; 
              $this->nmgp_dados_select['serial_codbarras'] = $this->serial_codbarras;
              $this->maneja_tcs_lfs = $rs->fields[37] ; 
              $this->nmgp_dados_select['maneja_tcs_lfs'] = $this->maneja_tcs_lfs;
              $this->control_costo = $rs->fields[38] ; 
              $this->nmgp_dados_select['control_costo'] = $this->control_costo;
              $this->por_preciominimo = $rs->fields[39] ; 
              $this->nmgp_dados_select['por_preciominimo'] = $this->por_preciominimo;
              $this->id_marca = $rs->fields[40] ; 
              $this->nmgp_dados_select['id_marca'] = $this->id_marca;
              $this->id_linea = $rs->fields[41] ; 
              $this->nmgp_dados_select['id_linea'] = $this->id_linea;
              $this->ultima_compra = $rs->fields[42] ; 
              $this->nmgp_dados_select['ultima_compra'] = $this->ultima_compra;
              $this->n_ultcompra = $rs->fields[43] ; 
              $this->nmgp_dados_select['n_ultcompra'] = $this->n_ultcompra;
              $this->ultima_venta = $rs->fields[44] ; 
              $this->nmgp_dados_select['ultima_venta'] = $this->ultima_venta;
              $this->n_ultventa = $rs->fields[45] ; 
              $this->nmgp_dados_select['n_ultventa'] = $this->n_ultventa;
              $this->codigobar2 = $rs->fields[46] ; 
              $this->nmgp_dados_select['codigobar2'] = $this->codigobar2;
              $this->codigobar3 = $rs->fields[47] ; 
              $this->nmgp_dados_select['codigobar3'] = $this->codigobar3;
              $this->nube = $rs->fields[48] ; 
              $this->nmgp_dados_select['nube'] = $this->nube;
              $this->existencia = $rs->fields[49] ; 
              $this->nmgp_dados_select['existencia'] = $this->existencia;
              $this->multiple_escala = $rs->fields[50] ; 
              $this->nmgp_dados_select['multiple_escala'] = $this->multiple_escala;
              $this->en_base_a = $rs->fields[51] ; 
              $this->nmgp_dados_select['en_base_a'] = $this->en_base_a;
              $this->activo = $rs->fields[52] ; 
              $this->nmgp_dados_select['activo'] = $this->activo;
              $this->tipo_producto = $rs->fields[53] ; 
              $this->nmgp_dados_select['tipo_producto'] = $this->tipo_producto;
              $this->costo_prom = $rs->fields[54] ; 
              $this->nmgp_dados_select['costo_prom'] = $this->costo_prom;
              $this->imagen = $rs->fields[55] ; 
              $this->nmgp_dados_select['imagen'] = $this->imagen;
              $this->ubicacion = $rs->fields[56] ; 
              $this->nmgp_dados_select['ubicacion'] = $this->ubicacion;
              $this->para_registro_fe = $rs->fields[57] ; 
              $this->nmgp_dados_select['para_registro_fe'] = $this->para_registro_fe;
          $GLOBALS["NM_ERRO_IBASE"] = 0; 
              $this->nm_troca_decimal(",", ".");
              $this->idprod = (string)$this->idprod; 
              $this->costomay = (string)$this->costomay; 
              $this->costomen = (string)$this->costomen; 
              $this->recmayamen = (string)$this->recmayamen; 
              $this->preciomen = (string)$this->preciomen; 
              $this->preciomen2 = (string)$this->preciomen2; 
              $this->preciomen3 = (string)$this->preciomen3; 
              $this->precio2 = (string)$this->precio2; 
              $this->preciomay = (string)$this->preciomay; 
              $this->preciofull = (string)$this->preciofull; 
              $this->stockmay = (string)$this->stockmay; 
              $this->stockmen = (string)$this->stockmen; 
              $this->idgrup = (string)$this->idgrup; 
              $this->idpro1 = (string)$this->idpro1; 
              $this->idpro2 = (string)$this->idpro2; 
              $this->idiva = (string)$this->idiva; 
              $this->otro = (string)$this->otro; 
              $this->otro2 = (string)$this->otro2; 
              $this->imconsumo = (string)$this->imconsumo; 
              $this->idcombo = (string)$this->idcombo; 
              $this->por_preciominimo = (string)$this->por_preciominimo; 
              $this->id_marca = (string)$this->id_marca; 
              $this->id_linea = (string)$this->id_linea; 
              $this->existencia = (string)$this->existencia; 
              $this->costo_prom = (string)$this->costo_prom; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['parms'] = "idprod?#?$this->idprod?@?";
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sub_dir'][0]  = "";
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sub_dir'][1]  = "/" . $_SESSION['gnit'] . "/";
          } 
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select'] = $this->nmgp_dados_select;
          if (!$this->NM_ajax_flag || 'backup_line' != $this->NM_ajax_opcao)
          {
              $this->Nav_permite_ret = 0 != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'];
              $this->Nav_permite_ava = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] < $qt_geral_reg_form_productos_060522_mob;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opcao']   = '';
          }
      } 
      if ($this->nmgp_opcao == "novo" || $this->nmgp_opcao == "refresh_insert") 
      { 
          $this->sc_evento_old = $this->sc_evento;
          $this->sc_evento = "novo";
          if ('refresh_insert' == $this->nmgp_opcao)
          {
              $this->nmgp_opcao = 'novo';
          }
          else
          {
              $this->nm_formatar_campos();
              $this->idprod = "";  
              $this->nmgp_dados_form["idprod"] = $this->idprod;
              $this->codigobar = "";  
              $this->nmgp_dados_form["codigobar"] = $this->codigobar;
              $this->codigoprod = "";  
              $this->nmgp_dados_form["codigoprod"] = $this->codigoprod;
              $this->nompro = "";  
              $this->nmgp_dados_form["nompro"] = $this->nompro;
              $this->unidmaymen = "";  
              $this->nmgp_dados_form["unidmaymen"] = $this->unidmaymen;
              $this->unimay = "";  
              $this->nmgp_dados_form["unimay"] = $this->unimay;
              $this->unimen = "94";  
              $this->nmgp_dados_form["unimen"] = $this->unimen;
              $this->costomay = "";  
              $this->nmgp_dados_form["costomay"] = $this->costomay;
              $this->costomen = "";  
              $this->nmgp_dados_form["costomen"] = $this->costomen;
              $this->recmayamen = "0";  
              $this->nmgp_dados_form["recmayamen"] = $this->recmayamen;
              $this->preciomen = "0";  
              $this->nmgp_dados_form["preciomen"] = $this->preciomen;
              $this->preciomen2 = "0";  
              $this->nmgp_dados_form["preciomen2"] = $this->preciomen2;
              $this->preciomen3 = "0";  
              $this->nmgp_dados_form["preciomen3"] = $this->preciomen3;
              $this->precio2 = "0";  
              $this->nmgp_dados_form["precio2"] = $this->precio2;
              $this->preciomay = "0";  
              $this->nmgp_dados_form["preciomay"] = $this->preciomay;
              $this->preciofull = "0";  
              $this->nmgp_dados_form["preciofull"] = $this->preciofull;
              $this->stockmay = "0";  
              $this->nmgp_dados_form["stockmay"] = $this->stockmay;
              $this->stockmen = "0";  
              $this->nmgp_dados_form["stockmen"] = $this->stockmen;
              $this->idgrup = "";  
              $this->nmgp_dados_form["idgrup"] = $this->idgrup;
              $this->idpro1 = "1";  
              $this->nmgp_dados_form["idpro1"] = $this->idpro1;
              $this->idpro2 = "";  
              $this->nmgp_dados_form["idpro2"] = $this->idpro2;
              $this->idiva = "";  
              $this->nmgp_dados_form["idiva"] = $this->idiva;
              $this->otro = "";  
              $this->nmgp_dados_form["otro"] = $this->otro;
              $this->otro2 = "0";  
              $this->nmgp_dados_form["otro2"] = $this->otro2;
              $this->colores = "";  
              $this->nmgp_dados_form["colores"] = $this->colores;
              $this->tallas = "";  
              $this->nmgp_dados_form["tallas"] = $this->tallas;
              $this->sabores = "";  
              $this->nmgp_dados_form["sabores"] = $this->sabores;
              $this->imagenprod = "";  
              $this->imagenprod_ul_name = "" ;  
              $this->imagenprod_ul_type = "" ;  
              $this->nmgp_dados_form["imagenprod"] = $this->imagenprod;
              $this->imconsumo = "";  
              $this->nmgp_dados_form["imconsumo"] = $this->imconsumo;
              $this->escombo = "";  
              $this->nmgp_dados_form["escombo"] = $this->escombo;
              $this->idcombo = "";  
              $this->nmgp_dados_form["idcombo"] = $this->idcombo;
              $this->precio_editable = "SI";  
              $this->nmgp_dados_form["precio_editable"] = $this->precio_editable;
              $this->cod_cuenta = "";  
              $this->nmgp_dados_form["cod_cuenta"] = $this->cod_cuenta;
              $this->fecha_vencimiento = "";  
              $this->nmgp_dados_form["fecha_vencimiento"] = $this->fecha_vencimiento;
              $this->fecha_fab = "";  
              $this->nmgp_dados_form["fecha_fab"] = $this->fecha_fab;
              $this->lote = "";  
              $this->nmgp_dados_form["lote"] = $this->lote;
              $this->serial_codbarras = "";  
              $this->nmgp_dados_form["serial_codbarras"] = $this->serial_codbarras;
              $this->maneja_tcs_lfs = "";  
              $this->nmgp_dados_form["maneja_tcs_lfs"] = $this->maneja_tcs_lfs;
              $this->control_costo = "";  
              $this->nmgp_dados_form["control_costo"] = $this->control_costo;
              $this->por_preciominimo = "";  
              $this->nmgp_dados_form["por_preciominimo"] = $this->por_preciominimo;
              $this->id_marca = "";  
              $this->nmgp_dados_form["id_marca"] = $this->id_marca;
              $this->id_linea = "";  
              $this->nmgp_dados_form["id_linea"] = $this->id_linea;
              $this->ultima_compra = "";  
              $this->ultima_compra_hora = "" ;  
              $this->nmgp_dados_form["ultima_compra"] = $this->ultima_compra;
              $this->n_ultcompra = "";  
              $this->nmgp_dados_form["n_ultcompra"] = $this->n_ultcompra;
              $this->ultima_venta = "";  
              $this->ultima_venta_hora = "" ;  
              $this->nmgp_dados_form["ultima_venta"] = $this->ultima_venta;
              $this->n_ultventa = "";  
              $this->nmgp_dados_form["n_ultventa"] = $this->n_ultventa;
              $this->codigobar2 = "";  
              $this->nmgp_dados_form["codigobar2"] = $this->codigobar2;
              $this->codigobar3 = "";  
              $this->nmgp_dados_form["codigobar3"] = $this->codigobar3;
              $this->nube = "";  
              $this->nmgp_dados_form["nube"] = $this->nube;
              $this->existencia = "";  
              $this->nmgp_dados_form["existencia"] = $this->existencia;
              $this->multiple_escala = "";  
              $this->nmgp_dados_form["multiple_escala"] = $this->multiple_escala;
              $this->en_base_a = "";  
              $this->nmgp_dados_form["en_base_a"] = $this->en_base_a;
              $this->activo = "";  
              $this->nmgp_dados_form["activo"] = $this->activo;
              $this->tipo_producto = "PT";  
              $this->nmgp_dados_form["tipo_producto"] = $this->tipo_producto;
              $this->costo_prom = "";  
              $this->nmgp_dados_form["costo_prom"] = $this->costo_prom;
              $this->imagen = "";  
              $this->imagen_ul_name = "" ;  
              $this->imagen_ul_type = "" ;  
              $this->nmgp_dados_form["imagen"] = $this->imagen;
              $this->ubicacion = "";  
              $this->nmgp_dados_form["ubicacion"] = $this->ubicacion;
              $this->para_registro_fe = "NO";  
              $this->nmgp_dados_form["para_registro_fe"] = $this->para_registro_fe;
              $this->sugerido_mayor = "";  
              $this->nmgp_dados_form["sugerido_mayor"] = $this->sugerido_mayor;
              $this->sugerido_menor = "";  
              $this->nmgp_dados_form["sugerido_menor"] = $this->sugerido_menor;
              $this->confcolor = "";  
              $this->nmgp_dados_form["confcolor"] = $this->confcolor;
              $this->conftalla = "";  
              $this->nmgp_dados_form["conftalla"] = $this->conftalla;
              $this->relleno = "";  
              $this->nmgp_dados_form["relleno"] = $this->relleno;
              $this->sabor = "";  
              $this->nmgp_dados_form["sabor"] = $this->sabor;
              $this->u_menor = "";  
              $this->nmgp_dados_form["u_menor"] = $this->u_menor;
              $this->unidad_ = "94";  
              $this->nmgp_dados_form["unidad_"] = $this->unidad_;
              $this->unidad_ma = "";  
              $this->nmgp_dados_form["unidad_ma"] = $this->unidad_ma;
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_form'] = $this->nmgp_dados_form;
              $this->formatado = false;
              if ($this->nmgp_clone != "S")
              {
                  unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_clone']['imagenprod']);  
              }
              if ($this->nmgp_clone == "S" && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select']))
              {
                  $this->nmgp_dados_select = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_select'];
                  $this->codigobar = $this->nmgp_dados_select['codigobar'];  
                  $this->codigoprod = $this->nmgp_dados_select['codigoprod'];  
                  $this->nompro = $this->nmgp_dados_select['nompro'];  
                  $this->unidmaymen = $this->nmgp_dados_select['unidmaymen'];  
                  $this->unimay = $this->nmgp_dados_select['unimay'];  
                  $this->unimen = $this->nmgp_dados_select['unimen'];  
                  $this->costomay = $this->nmgp_dados_select['costomay'];  
                  $this->costomen = $this->nmgp_dados_select['costomen'];  
                  $this->recmayamen = $this->nmgp_dados_select['recmayamen'];  
                  $this->preciomen = $this->nmgp_dados_select['preciomen'];  
                  $this->preciomen2 = $this->nmgp_dados_select['preciomen2'];  
                  $this->preciomen3 = $this->nmgp_dados_select['preciomen3'];  
                  $this->precio2 = $this->nmgp_dados_select['precio2'];  
                  $this->preciomay = $this->nmgp_dados_select['preciomay'];  
                  $this->preciofull = $this->nmgp_dados_select['preciofull'];  
                  $this->stockmay = $this->nmgp_dados_select['stockmay'];  
                  $this->stockmen = $this->nmgp_dados_select['stockmen'];  
                  $this->idgrup = $this->nmgp_dados_select['idgrup'];  
                  $this->idpro1 = $this->nmgp_dados_select['idpro1'];  
                  $this->idpro2 = $this->nmgp_dados_select['idpro2'];  
                  $this->idiva = $this->nmgp_dados_select['idiva'];  
                  $this->otro = $this->nmgp_dados_select['otro'];  
                  $this->otro2 = $this->nmgp_dados_select['otro2'];  
                  $this->colores = $this->nmgp_dados_select['colores'];  
                  $this->tallas = $this->nmgp_dados_select['tallas'];  
                  $this->sabores = $this->nmgp_dados_select['sabores'];  
                  $this->imagenprod = $this->nmgp_dados_select['imagenprod'];  
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dados_clone']['imagenprod'] = $this->nmgp_dados_select['imagenprod'];  
                  $this->imconsumo = $this->nmgp_dados_select['imconsumo'];  
                  $this->escombo = $this->nmgp_dados_select['escombo'];  
                  $this->idcombo = $this->nmgp_dados_select['idcombo'];  
                  $this->precio_editable = $this->nmgp_dados_select['precio_editable'];  
                  $this->cod_cuenta = $this->nmgp_dados_select['cod_cuenta'];  
                  $this->fecha_vencimiento = $this->nmgp_dados_select['fecha_vencimiento'];  
                  $this->fecha_fab = $this->nmgp_dados_select['fecha_fab'];  
                  $this->lote = $this->nmgp_dados_select['lote'];  
                  $this->serial_codbarras = $this->nmgp_dados_select['serial_codbarras'];  
                  $this->maneja_tcs_lfs = $this->nmgp_dados_select['maneja_tcs_lfs'];  
                  $this->control_costo = $this->nmgp_dados_select['control_costo'];  
                  $this->por_preciominimo = $this->nmgp_dados_select['por_preciominimo'];  
                  $this->id_marca = $this->nmgp_dados_select['id_marca'];  
                  $this->id_linea = $this->nmgp_dados_select['id_linea'];  
                  $this->ultima_compra = $this->nmgp_dados_select['ultima_compra'];  
                  $this->n_ultcompra = $this->nmgp_dados_select['n_ultcompra'];  
                  $this->ultima_venta = $this->nmgp_dados_select['ultima_venta'];  
                  $this->n_ultventa = $this->nmgp_dados_select['n_ultventa'];  
                  $this->codigobar2 = $this->nmgp_dados_select['codigobar2'];  
                  $this->codigobar3 = $this->nmgp_dados_select['codigobar3'];  
                  $this->nube = $this->nmgp_dados_select['nube'];  
                  $this->existencia = $this->nmgp_dados_select['existencia'];  
                  $this->multiple_escala = $this->nmgp_dados_select['multiple_escala'];  
                  $this->en_base_a = $this->nmgp_dados_select['en_base_a'];  
                  $this->activo = $this->nmgp_dados_select['activo'];  
                  $this->tipo_producto = $this->nmgp_dados_select['tipo_producto'];  
                  $this->costo_prom = $this->nmgp_dados_select['costo_prom'];  
                  $this->imagen = $this->nmgp_dados_select['imagen'];  
                  $this->ubicacion = $this->nmgp_dados_select['ubicacion'];  
                  $this->para_registro_fe = $this->nmgp_dados_select['para_registro_fe'];  
              }
          }
          if (($this->Embutida_form || $this->Embutida_multi) && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key']))
          {
              foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['foreign_key'] as $sFKName => $sFKValue)
              {
                   if (isset($this->sc_conv_var[$sFKName]))
                   {
                       $sFKName = $this->sc_conv_var[$sFKName];
                   }
                  eval("\$this->" . $sFKName . " = \"" . $sFKValue . "\";");
              }
          }
      }  
//
//
//-- 
      if ($this->nmgp_opcao != "novo") 
      {
      }
      if (!isset($this->nmgp_refresh_fields)) 
      { 
          $this->nm_proc_onload();
      }
  }
// 
//-- 
   function nm_db_retorna($str_where_param = '') 
   {  
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $str_where_filter = ('' != $str_where_param) ? ' and ' . $str_where_param : '';
     if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter); 
     }  
     else  
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " where idprod < $this->idprod" . $str_where_filter); 
     }  
     if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
     { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
     }  
     if (isset($rs->fields[0]) && $rs->fields[0] != "") 
     { 
         $this->idprod = substr($this->Db->qstr($rs->fields[0]), 1, -1); 
         $rs->Close();  
         $this->nmgp_opcao = "igual";  
         return ;  
     } 
     else 
     { 
        $this->nmgp_opcao = "inicio";  
        $rs->Close();  
        return ; 
     } 
   } 
// 
//-- 
   function nm_db_avanca($str_where_param = '') 
   {  
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $str_where_filter = ('' != $str_where_param) ? ' and ' . $str_where_param : '';
     if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter); 
     }  
     else  
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " where idprod > $this->idprod" . $str_where_filter); 
     }  
     if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
     { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
     }  
     if (isset($rs->fields[0]) && $rs->fields[0] != "") 
     { 
         $this->idprod = substr($this->Db->qstr($rs->fields[0]), 1, -1); 
         $rs->Close();  
         $this->nmgp_opcao = "igual";  
         return ;  
     } 
     else 
     { 
        $this->nmgp_opcao = "final";  
        $rs->Close();  
        return ; 
     } 
   } 
// 
//-- 
   function nm_db_inicio($str_where_param = '') 
   {   
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select count(*) AS countTest from " . $this->Ini->nm_tabela; 
     $rs = $this->Db->Execute("select count(*) AS countTest from " . $this->Ini->nm_tabela);
     if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
     { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
     }  
     if ($rs->fields[0] == 0) 
     { 
         $this->nmgp_opcao = "novo"; 
         $this->nm_flag_saida_novo = "S"; 
         $rs->Close(); 
         if ($this->aba_iframe)
         {
             $this->nmgp_botoes['exit'] = 'off';
         }
         return;
     }
     $str_where_filter = ('' != $str_where_param) ? ' where ' . $str_where_param : '';
     if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     else  
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select min(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
     { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
     }  
     if (!isset($rs->fields[0]) || $rs->EOF) 
     { 
         if (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter']))
         { 
             $rs->Close();  
             return ; 
         } 
         $this->nm_flag_saida_novo = "S"; 
         $this->nmgp_opcao = "novo";  
         $rs->Close();  
         if ($this->aba_iframe)
         {
             $this->nmgp_botoes['exit'] = 'off';
         }
         return ; 
     } 
     $this->idprod = substr($this->Db->qstr($rs->fields[0]), 1, -1); 
     $rs->Close();  
     $this->nmgp_opcao = "igual";  
     return ;  
   } 
// 
//-- 
   function nm_db_final($str_where_param = '') 
   { 
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $str_where_filter = ('' != $str_where_param) ? ' where ' . $str_where_param : '';
     if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     else  
     {
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = "select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter; 
         $rs = $this->Db->Execute("select max(idprod) from " . $this->Ini->nm_tabela . " " . $str_where_filter); 
     }  
     if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
     { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
     }  
     if (!isset($rs->fields[0]) || $rs->EOF) 
     { 
         $this->nm_flag_saida_novo = "S"; 
         $this->nmgp_opcao = "novo";  
         $rs->Close();  
         if ($this->aba_iframe)
         {
             $this->nmgp_botoes['exit'] = 'off';
         }
         return ; 
     } 
     $this->idprod = substr($this->Db->qstr($rs->fields[0]), 1, -1); 
     $rs->Close();  
     $this->nmgp_opcao = "igual";  
     return ;  
   } 
   function NM_gera_nav_page() 
   {
       $this->SC_nav_page = "";
       $Arr_result        = array();
       $Ind_result        = 0;
       $Reg_Page   = 1;
       $Max_link   = 5;
       $Mid_link   = ceil($Max_link / 2);
       $Corr_link  = (($Max_link % 2) == 0) ? 0 : 1;
       $rec_tot    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'] + 1;
       $rec_fim    = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['reg_start'] + 1;
       $rec_fim    = ($rec_fim > $rec_tot) ? $rec_tot : $rec_fim;
       if ($rec_tot == 0)
       {
           return;
       }
       $Qtd_Pages  = ceil($rec_tot / $Reg_Page);
       $Page_Atu   = ceil($rec_fim / $Reg_Page);
       $Link_ini   = 1;
       if ($Page_Atu > $Max_link)
       {
           $Link_ini = $Page_Atu - $Mid_link + $Corr_link;
       }
       elseif ($Page_Atu > $Mid_link)
       {
           $Link_ini = $Page_Atu - $Mid_link + $Corr_link;
       }
       if (($Qtd_Pages - $Link_ini) < $Max_link)
       {
           $Link_ini = ($Qtd_Pages - $Max_link) + 1;
       }
       if ($Link_ini < 1)
       {
           $Link_ini = 1;
       }
       for ($x = 0; $x < $Max_link && $Link_ini <= $Qtd_Pages; $x++)
       {
           $rec = (($Link_ini - 1) * $Reg_Page) + 1;
           if ($Link_ini == $Page_Atu)
           {
               $Arr_result[$Ind_result] = '<span class="scFormToolbarNavOpen" style="vertical-align: middle;">' . $Link_ini . '</span>';
           }
           else
           {
               $Arr_result[$Ind_result] = '<a class="scFormToolbarNav" style="vertical-align: middle;" href="javascript: nm_navpage(' . $rec . ')">' . $Link_ini . '</a>';
           }
           $Link_ini++;
           $Ind_result++;
           if (($x + 1) < $Max_link && $Link_ini <= $Qtd_Pages && '' != $this->Ini->Str_toolbarnav_separator && @is_file($this->Ini->root . $this->Ini->path_img_global . $this->Ini->Str_toolbarnav_separator))
           {
               $Arr_result[$Ind_result] = '<img src="' . $this->Ini->path_img_global . $this->Ini->Str_toolbarnav_separator . '" align="absmiddle" style="vertical-align: middle;">';
               $Ind_result++;
           }
       }
       if ($_SESSION['scriptcase']['reg_conf']['css_dir'] == "RTL")
       {
           krsort($Arr_result);
       }
       foreach ($Arr_result as $Ind_result => $Lin_result)
       {
           $this->SC_nav_page .= $Lin_result;
       }
   }
        function initializeRecordState() {
                $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'] = array();
        }

        function storeRecordState($sc_seq_vert = 0) {
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'])) {
                        $this->initializeRecordState();
                }
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert])) {
                        $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert] = array();
                }

                $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert]['buttons'] = array(
                        'delete' => $this->nmgp_botoes['delete'],
                        'update' => $this->nmgp_botoes['update']
                );
        }

        function loadRecordState($sc_seq_vert = 0) {
                if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state']) || !isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert])) {
                        return;
                }

                if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert]['buttons']['delete'])) {
                        $this->nmgp_botoes['delete'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert]['buttons']['delete'];
                }
                if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert]['buttons']['update'])) {
                        $this->nmgp_botoes['update'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522']['record_state'][$sc_seq_vert]['buttons']['update'];
                }
        }

//
function codigoprod_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_codigobar = $this->codigobar;
$original_codigoprod = $this->codigoprod;

$this->codigobar   = $this->codigoprod ;

$modificado_codigobar = $this->codigobar;
$modificado_codigoprod = $this->codigoprod;
$this->nm_formatar_campos('codigobar', 'codigoprod');
if ($original_codigobar !== $modificado_codigobar || isset($this->nmgp_cmp_readonly['codigobar']) || (isset($bFlagRead_codigobar) && $bFlagRead_codigobar))
{
    $this->ajax_return_values_codigobar(true);
}
if ($original_codigoprod !== $modificado_codigoprod || isset($this->nmgp_cmp_readonly['codigoprod']) || (isset($bFlagRead_codigoprod) && $bFlagRead_codigoprod))
{
    $this->ajax_return_values_codigoprod(true);
}
$this->NM_ajax_info['event_field'] = 'codigoprod';
form_productos_060522_mob_pack_ajax_response();
exit;


$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function colores_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_colores = $this->colores;
$original_confcolor = $this->confcolor;

if($this->idprod >0)
	{	
	if ($this->colores =='SI')
		{
		$this->nmgp_cmp_hidden["confcolor"] = "on"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'on';
		
     $nm_select ="update productos set colores='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
		
     $nm_select ="update productos set colores='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_colores = $this->colores;
$modificado_confcolor = $this->confcolor;
$this->nm_formatar_campos('idprod', 'colores', 'confcolor');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_colores !== $modificado_colores || isset($this->nmgp_cmp_readonly['colores']) || (isset($bFlagRead_colores) && $bFlagRead_colores))
{
    $this->ajax_return_values_colores(true);
}
if ($original_confcolor !== $modificado_confcolor || isset($this->nmgp_cmp_readonly['confcolor']) || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor))
{
    $this->ajax_return_values_confcolor(true);
}
$this->NM_ajax_info['event_field'] = 'colores';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function control_costo_onClick()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_recmayamen = $this->recmayamen;
$original_idiva = $this->idiva;
$original_control_costo = $this->control_costo;
$original_por_preciominimo = $this->por_preciominimo;
$original_unidmaymen = $this->unidmaymen;
$original_sugerido_mayor = $this->sugerido_mayor;
$original_costomen = $this->costomen;
$original_sugerido_menor = $this->sugerido_menor;
$original_idprod = $this->idprod;

if($this->recmayamen ==0)
		{
		$vFmult=0;
		}
	else
		{
		$vFmult=$this->recmayamen ;
		}
	 
      $nm_select = "select trifa from iva where idiva=$this->idiva "; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->d_iva = array();
     if ($this->idiva != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->d_iva[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->d_iva = false;
          $this->d_iva_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if($this->d_iva[0][0]>0)
		{
		$vTIva=round(($this->d_iva[0][0]/100), 2);
		}
	else
		{
		$vTIva=0;
		}
	if($this->control_costo =='NA' or $this->control_costo =='UC')
		{
		$this->por_preciominimo =0;
		$this->nmgp_cmp_hidden["por_preciominimo"] = "off"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'off';
		if($this->unidmaymen =='SI')
			{
			$this->sugerido_mayor =round(($this->costomen *(1+$vTIva)), 0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$this->sugerido_menor =round(($vCstUMenor)*(1+$vTIva), 0);
			}
		else
			{
			$this->sugerido_menor =round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =0;
			}
		}
	elseif($this->control_costo =='CP')
		{
		$this->por_preciominimo =0;
		$this->nmgp_cmp_hidden["por_preciominimo"] = "off"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'off';
		if($this->unidmaymen =='SI')
			{
			$this->sugerido_mayor =round(($this->costomen *(1+$vTIva)), 0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$this->sugerido_menor =round(($vCstUMenor)*(1+$vTIva), 0);
			}
		else
			{
			$this->sugerido_menor =round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =0;
			}
		}
	else
		{
		$this->nmgp_cmp_hidden["por_preciominimo"] = "on"; $this->NM_ajax_info['fieldDisplay']['por_preciominimo'] = 'on';
		$this->sc_set_focus('por_preciominimo');
		if($this->unidmaymen =='SI')
			{
			$vCosUMay=round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =round($vCosUMay*(1+($this->por_preciominimo /100)),0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			}
		else
			{
			$vCstUMenor=$this->costomen ;
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			$this->sugerido_mayor =0;
			}
		}
		


$modificado_recmayamen = $this->recmayamen;
$modificado_idiva = $this->idiva;
$modificado_control_costo = $this->control_costo;
$modificado_por_preciominimo = $this->por_preciominimo;
$modificado_unidmaymen = $this->unidmaymen;
$modificado_sugerido_mayor = $this->sugerido_mayor;
$modificado_costomen = $this->costomen;
$modificado_sugerido_menor = $this->sugerido_menor;
$modificado_idprod = $this->idprod;
$this->nm_formatar_campos('recmayamen', 'idiva', 'control_costo', 'por_preciominimo', 'unidmaymen', 'sugerido_mayor', 'costomen', 'sugerido_menor', 'idprod');
if ($original_recmayamen !== $modificado_recmayamen || isset($this->nmgp_cmp_readonly['recmayamen']) || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen))
{
    $this->ajax_return_values_recmayamen(true);
}
if ($original_idiva !== $modificado_idiva || isset($this->nmgp_cmp_readonly['idiva']) || (isset($bFlagRead_idiva) && $bFlagRead_idiva))
{
    $this->ajax_return_values_idiva(true);
}
if ($original_control_costo !== $modificado_control_costo || isset($this->nmgp_cmp_readonly['control_costo']) || (isset($bFlagRead_control_costo) && $bFlagRead_control_costo))
{
    $this->ajax_return_values_control_costo(true);
}
if ($original_por_preciominimo !== $modificado_por_preciominimo || isset($this->nmgp_cmp_readonly['por_preciominimo']) || (isset($bFlagRead_por_preciominimo) && $bFlagRead_por_preciominimo))
{
    $this->ajax_return_values_por_preciominimo(true);
}
if ($original_unidmaymen !== $modificado_unidmaymen || isset($this->nmgp_cmp_readonly['unidmaymen']) || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen))
{
    $this->ajax_return_values_unidmaymen(true);
}
if ($original_sugerido_mayor !== $modificado_sugerido_mayor || isset($this->nmgp_cmp_readonly['sugerido_mayor']) || (isset($bFlagRead_sugerido_mayor) && $bFlagRead_sugerido_mayor))
{
    $this->ajax_return_values_sugerido_mayor(true);
}
if ($original_costomen !== $modificado_costomen || isset($this->nmgp_cmp_readonly['costomen']) || (isset($bFlagRead_costomen) && $bFlagRead_costomen))
{
    $this->ajax_return_values_costomen(true);
}
if ($original_sugerido_menor !== $modificado_sugerido_menor || isset($this->nmgp_cmp_readonly['sugerido_menor']) || (isset($bFlagRead_sugerido_menor) && $bFlagRead_sugerido_menor))
{
    $this->ajax_return_values_sugerido_menor(true);
}
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
$this->NM_ajax_info['event_field'] = 'control';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function en_base_a_onClick()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_en_base_a = $this->en_base_a;
$original_unidmaymen = $this->unidmaymen;
$original_idprod = $this->idprod;

if($this->en_base_a =='UMEN')
	{
	if($this->unidmaymen =='NO')
		{
		$this->sc_set_focus('costomen');
		$this->en_base_a ='UMAY';
		$this->sc_field_readonly("en_base_a", 'on');
		goto err;
		}
	else
		{
		goto Noerr;
		}
	}
else
	{
	 
      $nm_select = "select idinv from inventario where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_inv = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_inv[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_inv = false;
          $this->ds_inv_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->ds_inv[0][0]))
		{
		$this->en_base_a ='UMAY';
		$this->sc_field_readonly("en_base_a", 'on');
		goto err2;
		}
	}
err:;
$this->sc_ajax_message("Prodcuto no tiene configurado Manejo de Unidad Mayor!", "ADVERTENCIA", "", "");
goto Noerr;
err2:;
$this->sc_ajax_message("Producto tiene movimiento, no se puede cambiar Multiple escala Y/O Factores!!", "ADVERTENCIA", "", "");
Noerr:;

$modificado_en_base_a = $this->en_base_a;
$modificado_unidmaymen = $this->unidmaymen;
$modificado_idprod = $this->idprod;
$this->nm_formatar_campos('en_base_a', 'unidmaymen', 'idprod');
if ($original_en_base_a !== $modificado_en_base_a || isset($this->nmgp_cmp_readonly['en_base_a']) || (isset($bFlagRead_en_base_a) && $bFlagRead_en_base_a))
{
    $this->ajax_return_values_en_base_a(true);
}
if ($original_unidmaymen !== $modificado_unidmaymen || isset($this->nmgp_cmp_readonly['unidmaymen']) || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen))
{
    $this->ajax_return_values_unidmaymen(true);
}
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
$this->NM_ajax_info['event_field'] = 'en';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function fecha_vencimiento_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_fecha_vencimiento = $this->fecha_vencimiento;

if($this->idprod >0)
	{	
	if ($this->fecha_vencimiento =='SI')
		{
		
     $nm_select ="update productos set fecha_vencimiento='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		
     $nm_select ="update productos set fecha_vencimiento='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_fecha_vencimiento = $this->fecha_vencimiento;
$this->nm_formatar_campos('idprod', 'fecha_vencimiento');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_fecha_vencimiento !== $modificado_fecha_vencimiento || isset($this->nmgp_cmp_readonly['fecha_vencimiento']) || (isset($bFlagRead_fecha_vencimiento) && $bFlagRead_fecha_vencimiento))
{
    $this->ajax_return_values_fecha_vencimiento(true);
}
$this->NM_ajax_info['event_field'] = 'fecha';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function idpro1_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idpro2 = $this->idpro2;
$original_idpro1 = $this->idpro1;

if($this->idpro2 ==0)
	{
	$this->idpro2 =$this->idpro1 ;
	}


$modificado_idpro2 = $this->idpro2;
$modificado_idpro1 = $this->idpro1;
$this->nm_formatar_campos('idpro2', 'idpro1');
if ($original_idpro2 !== $modificado_idpro2 || isset($this->nmgp_cmp_readonly['idpro2']) || (isset($bFlagRead_idpro2) && $bFlagRead_idpro2))
{
    $this->ajax_return_values_idpro2(true);
}
if ($original_idpro1 !== $modificado_idpro1 || isset($this->nmgp_cmp_readonly['idpro1']) || (isset($bFlagRead_idpro1) && $bFlagRead_idpro1))
{
    $this->ajax_return_values_idpro1(true);
}
$this->NM_ajax_info['event_field'] = 'idpro1';
form_productos_060522_mob_pack_ajax_response();
exit;


$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function lote_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_lote = $this->lote;

if($this->idprod >0)
	{	
	if ($this->lote =='SI')
		{
		
     $nm_select ="update productos set lote='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		
     $nm_select ="update productos set lote='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_lote = $this->lote;
$this->nm_formatar_campos('idprod', 'lote');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_lote !== $modificado_lote || isset($this->nmgp_cmp_readonly['lote']) || (isset($bFlagRead_lote) && $bFlagRead_lote))
{
    $this->ajax_return_values_lote(true);
}
$this->NM_ajax_info['event_field'] = 'lote';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function maneja_tcs_lfs_onClick()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
if (!isset($this->sc_temp_gidtercero)) {$this->sc_temp_gidtercero = (isset($_SESSION['gidtercero'])) ? $_SESSION['gidtercero'] : "";}
  
$original_idprod = $this->idprod;
$original_maneja_tcs_lfs = $this->maneja_tcs_lfs;
$original_fecha_vencimiento = $this->fecha_vencimiento;
$original_lote = $this->lote;
$original_serial_codbarras = $this->serial_codbarras;
$original_codigobar = $this->codigobar;
$original_nompro = $this->nompro;
$original_colores = $this->colores;
$original_tallas = $this->tallas;
$original_sabores = $this->sabores;
$original_confcolor = $this->confcolor;
$original_conftalla = $this->conftalla;
$original_sabor = $this->sabor;

if($this->idprod >0)
{
	
		 
      $nm_select = "select maneja_tcs_lfs from productos where idprod='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vcts_actual = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vcts_actual[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vcts_actual = false;
          $this->vcts_actual_erro = $this->Db->ErrorMsg();
      } 
     } 
;

		if(isset($this->vcts_actual[0][0]) and ($this->maneja_tcs_lfs =='CTS' and $this->fecha_vencimiento =='NO' and $this->lote =='NO' and $this->serial_codbarras =='NO'))
		{
			$tcs_lfs_pordefecto = $this->vcts_actual[0][0];
			
			
			 
      $nm_select = "select idpro from inventario where idpro='".$this->idprod ."' order by idinv desc limit 1"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->vSiMovimientos = array();
      $this->vsimovimientos = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->vSiMovimientos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                      $this->vsimovimientos[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->vSiMovimientos = false;
          $this->vSiMovimientos_erro = $this->Db->ErrorMsg();
          $this->vsimovimientos = false;
          $this->vsimovimientos_erro = $this->Db->ErrorMsg();
      } 
     } 
;
			
			
				
     $nm_select ="update productos set maneja_tcs_lfs='$this->maneja_tcs_lfs' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
				
				
     $nm_select ="insert into log set usuario='".$this->sc_temp_gidtercero."',accion='EDITAR', observaciones='EL USUARIO EDITÓ EL PRODUCTO: $this->codigobar  - $this->nompro , TIPO PRODUCTO: $this->maneja_tcs_lfs' "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;

					CTS:;
					$this->nmgp_cmp_hidden["colores"] = "on"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'on';
					$this->nmgp_cmp_hidden["tallas"] = "on"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'on';
					$this->nmgp_cmp_hidden["sabores"] = "on"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'on';
					$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
					$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
					$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
					if($this->colores =='SI')
						{
						$this->nmgp_cmp_hidden["confcolor"] = "on"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'on';
						}
					else
						{
						$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
						}
					if($this->tallas =='SI')
						{
						$this->nmgp_cmp_hidden["conftalla"] = "on"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'on';
						}
					else
						{
						$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
						}
					if($this->sabores =='SI')
						{
						$this->nmgp_cmp_hidden["sabor"] = "on"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'on';
						}
					else
						{
						$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
						}
					}
				else if($this->maneja_tcs_lfs =='LFS' and $this->colores =='NO' and $this->tallas =='NO' and $this->sabores =='NO')
					{
					$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
					$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
					$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
					$this->nmgp_cmp_hidden["fecha_vencimiento"] = "on"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'on';
					$this->nmgp_cmp_hidden["lote"] = "on"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'on';
					$this->nmgp_cmp_hidden["serial_codbarras"] = "on"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'on';
					$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
					$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
					$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
					}
				else
					{
					$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
					$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
					$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
					$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
					$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
					$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
					$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
					$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
					$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';

					if($this->fecha_vencimiento !='NO' or $this->lote !='NO' or $this->serial_codbarras !='NO') 
						{
						echo "¡PRODUCTO MANEJA LFS !";
						$this->maneja_tcs_lfs ='LFS';
						$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
						$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
						$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
						$this->nmgp_cmp_hidden["fecha_vencimiento"] = "on"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'on';
						$this->nmgp_cmp_hidden["lote"] = "on"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'on';
						$this->nmgp_cmp_hidden["serial_codbarras"] = "on"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'on';
						$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
						$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
						$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
						}
					else if ($this->colores !='NO' or $this->tallas !='NO' or $this->sabores !='NO')
						{
						echo "¡PRODUCTO MANEJA CTS !";
						$this->maneja_tcs_lfs ='CTS';
						goto CTS;

						}

			
				
		}
	}
else
	{
	$this->nmgp_cmp_hidden["confcolor"] = "off"; $this->NM_ajax_info['fieldDisplay']['confcolor'] = 'off';
	$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
	$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
	$this->nmgp_cmp_hidden["colores"] = "off"; $this->NM_ajax_info['fieldDisplay']['colores'] = 'off';
	$this->nmgp_cmp_hidden["tallas"] = "off"; $this->NM_ajax_info['fieldDisplay']['tallas'] = 'off';
	$this->nmgp_cmp_hidden["sabores"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabores'] = 'off';
	$this->nmgp_cmp_hidden["fecha_vencimiento"] = "off"; $this->NM_ajax_info['fieldDisplay']['fecha_vencimiento'] = 'off';
	$this->nmgp_cmp_hidden["lote"] = "off"; $this->NM_ajax_info['fieldDisplay']['lote'] = 'off';
	$this->nmgp_cmp_hidden["serial_codbarras"] = "off"; $this->NM_ajax_info['fieldDisplay']['serial_codbarras'] = 'off';
	}




if (isset($this->sc_temp_gidtercero)) { $_SESSION['gidtercero'] = $this->sc_temp_gidtercero;}
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
$modificado_idprod = $this->idprod;
$modificado_maneja_tcs_lfs = $this->maneja_tcs_lfs;
$modificado_fecha_vencimiento = $this->fecha_vencimiento;
$modificado_lote = $this->lote;
$modificado_serial_codbarras = $this->serial_codbarras;
$modificado_codigobar = $this->codigobar;
$modificado_nompro = $this->nompro;
$modificado_colores = $this->colores;
$modificado_tallas = $this->tallas;
$modificado_sabores = $this->sabores;
$modificado_confcolor = $this->confcolor;
$modificado_conftalla = $this->conftalla;
$modificado_sabor = $this->sabor;
$this->nm_formatar_campos('idprod', 'maneja_tcs_lfs', 'fecha_vencimiento', 'lote', 'serial_codbarras', 'codigobar', 'nompro', 'colores', 'tallas', 'sabores', 'confcolor', 'conftalla', 'sabor');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_maneja_tcs_lfs !== $modificado_maneja_tcs_lfs || isset($this->nmgp_cmp_readonly['maneja_tcs_lfs']) || (isset($bFlagRead_maneja_tcs_lfs) && $bFlagRead_maneja_tcs_lfs))
{
    $this->ajax_return_values_maneja_tcs_lfs(true);
}
if ($original_fecha_vencimiento !== $modificado_fecha_vencimiento || isset($this->nmgp_cmp_readonly['fecha_vencimiento']) || (isset($bFlagRead_fecha_vencimiento) && $bFlagRead_fecha_vencimiento))
{
    $this->ajax_return_values_fecha_vencimiento(true);
}
if ($original_lote !== $modificado_lote || isset($this->nmgp_cmp_readonly['lote']) || (isset($bFlagRead_lote) && $bFlagRead_lote))
{
    $this->ajax_return_values_lote(true);
}
if ($original_serial_codbarras !== $modificado_serial_codbarras || isset($this->nmgp_cmp_readonly['serial_codbarras']) || (isset($bFlagRead_serial_codbarras) && $bFlagRead_serial_codbarras))
{
    $this->ajax_return_values_serial_codbarras(true);
}
if ($original_codigobar !== $modificado_codigobar || isset($this->nmgp_cmp_readonly['codigobar']) || (isset($bFlagRead_codigobar) && $bFlagRead_codigobar))
{
    $this->ajax_return_values_codigobar(true);
}
if ($original_nompro !== $modificado_nompro || isset($this->nmgp_cmp_readonly['nompro']) || (isset($bFlagRead_nompro) && $bFlagRead_nompro))
{
    $this->ajax_return_values_nompro(true);
}
if ($original_colores !== $modificado_colores || isset($this->nmgp_cmp_readonly['colores']) || (isset($bFlagRead_colores) && $bFlagRead_colores))
{
    $this->ajax_return_values_colores(true);
}
if ($original_tallas !== $modificado_tallas || isset($this->nmgp_cmp_readonly['tallas']) || (isset($bFlagRead_tallas) && $bFlagRead_tallas))
{
    $this->ajax_return_values_tallas(true);
}
if ($original_sabores !== $modificado_sabores || isset($this->nmgp_cmp_readonly['sabores']) || (isset($bFlagRead_sabores) && $bFlagRead_sabores))
{
    $this->ajax_return_values_sabores(true);
}
if ($original_confcolor !== $modificado_confcolor || isset($this->nmgp_cmp_readonly['confcolor']) || (isset($bFlagRead_confcolor) && $bFlagRead_confcolor))
{
    $this->ajax_return_values_confcolor(true);
}
if ($original_conftalla !== $modificado_conftalla || isset($this->nmgp_cmp_readonly['conftalla']) || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla))
{
    $this->ajax_return_values_conftalla(true);
}
if ($original_sabor !== $modificado_sabor || isset($this->nmgp_cmp_readonly['sabor']) || (isset($bFlagRead_sabor) && $bFlagRead_sabor))
{
    $this->ajax_return_values_sabor(true);
}
$this->NM_ajax_info['event_field'] = 'maneja';
form_productos_060522_mob_pack_ajax_response();
exit;
}
function multiple_escala_onClick()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_multiple_escala = $this->multiple_escala;
$original_unidmaymen = $this->unidmaymen;
$original_idprod = $this->idprod;

if($this->multiple_escala =='SI')
	{
	if($this->unidmaymen =='NO')
		{
		$this->multiple_escala ='NO';
		$this->sc_field_readonly("multiple_escala", 'on');
		goto err;
		
		}
	else
		{
		goto Noerr;
		}
	}
else
	{
	 
      $nm_select = "select idinv from inventario where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_inv = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_inv[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_inv = false;
          $this->ds_inv_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->ds_inv[0][0]))
		{
		$this->multiple_escala ='SI';
		goto err2;
		}
	goto Noerr;
	}

err:;
$this->sc_ajax_message("Prodcuto no tiene configurado Manejo de Unidad Mayor!", "ADVERTENCIA", "", "");
goto Noerr;
err2:;
$this->sc_ajax_message("Producto tiene movimiento, no se puede quitar Multiple escala Y/O Factores!!", "ADVERTENCIA", "", "");
Noerr:;

$modificado_multiple_escala = $this->multiple_escala;
$modificado_unidmaymen = $this->unidmaymen;
$modificado_idprod = $this->idprod;
$this->nm_formatar_campos('multiple_escala', 'unidmaymen', 'idprod');
if ($original_multiple_escala !== $modificado_multiple_escala || isset($this->nmgp_cmp_readonly['multiple_escala']) || (isset($bFlagRead_multiple_escala) && $bFlagRead_multiple_escala))
{
    $this->ajax_return_values_multiple_escala(true);
}
if ($original_unidmaymen !== $modificado_unidmaymen || isset($this->nmgp_cmp_readonly['unidmaymen']) || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen))
{
    $this->ajax_return_values_unidmaymen(true);
}
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
$this->NM_ajax_info['event_field'] = 'multiple';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function otro_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_otro = $this->otro;
$original_otro2 = $this->otro2;

if($this->otro ==1)
	{
	$this->nmgp_cmp_hidden["otro2"] = "on"; $this->NM_ajax_info['fieldDisplay']['otro2'] = 'on';
	$this->sc_set_focus('otro2');
	}
else
	{
	$this->otro2 =0;
	$this->nmgp_cmp_hidden["otro2"] = "off"; $this->NM_ajax_info['fieldDisplay']['otro2'] = 'off';
	}


$modificado_otro = $this->otro;
$modificado_otro2 = $this->otro2;
$this->nm_formatar_campos('otro', 'otro2');
if ($original_otro !== $modificado_otro || isset($this->nmgp_cmp_readonly['otro']) || (isset($bFlagRead_otro) && $bFlagRead_otro))
{
    $this->ajax_return_values_otro(true);
}
if ($original_otro2 !== $modificado_otro2 || isset($this->nmgp_cmp_readonly['otro2']) || (isset($bFlagRead_otro2) && $bFlagRead_otro2))
{
    $this->ajax_return_values_otro2(true);
}
$this->NM_ajax_info['event_field'] = 'otro';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function por_preciominimo_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_recmayamen = $this->recmayamen;
$original_idiva = $this->idiva;
$original_unidmaymen = $this->unidmaymen;
$original_costomen = $this->costomen;
$original_sugerido_mayor = $this->sugerido_mayor;
$original_por_preciominimo = $this->por_preciominimo;
$original_sugerido_menor = $this->sugerido_menor;

if($this->recmayamen ==0)
		{
		$vFmult=0;
		}
	else
		{
		$vFmult=$this->recmayamen ;
		}
	 
      $nm_select = "select trifa from iva where idiva=$this->idiva "; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->d_iva = array();
     if ($this->idiva != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->d_iva[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->d_iva = false;
          $this->d_iva_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if($this->d_iva[0][0]>0)
		{
		$vTIva=round(($this->d_iva[0][0]/100), 2);
		}
	else
		{
		$vTIva=0;
		}
		
		if($this->unidmaymen =='SI')
			{
			$vCosUMay=round(($this->costomen *(1+$vTIva)), 0);
			$this->sugerido_mayor =round($vCosUMay*(1+($this->por_preciominimo /100)),0);
			$vCosUMen=$this->costomen /$vFmult;
			$vCstUMenor=round($vCosUMen, 0);
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			}
		else
			{
			$vCstUMenor=$this->costomen ;
			$vCostmasIva=round(($vCstUMenor)*(1+$vTIva), 0);
			$this->sugerido_menor =round($vCostmasIva*(1+($this->por_preciominimo /100)), 0);
			$this->sugerido_mayor =0;
			}

$modificado_recmayamen = $this->recmayamen;
$modificado_idiva = $this->idiva;
$modificado_unidmaymen = $this->unidmaymen;
$modificado_costomen = $this->costomen;
$modificado_sugerido_mayor = $this->sugerido_mayor;
$modificado_por_preciominimo = $this->por_preciominimo;
$modificado_sugerido_menor = $this->sugerido_menor;
$this->nm_formatar_campos('recmayamen', 'idiva', 'unidmaymen', 'costomen', 'sugerido_mayor', 'por_preciominimo', 'sugerido_menor');
if ($original_recmayamen !== $modificado_recmayamen || isset($this->nmgp_cmp_readonly['recmayamen']) || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen))
{
    $this->ajax_return_values_recmayamen(true);
}
if ($original_idiva !== $modificado_idiva || isset($this->nmgp_cmp_readonly['idiva']) || (isset($bFlagRead_idiva) && $bFlagRead_idiva))
{
    $this->ajax_return_values_idiva(true);
}
if ($original_unidmaymen !== $modificado_unidmaymen || isset($this->nmgp_cmp_readonly['unidmaymen']) || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen))
{
    $this->ajax_return_values_unidmaymen(true);
}
if ($original_costomen !== $modificado_costomen || isset($this->nmgp_cmp_readonly['costomen']) || (isset($bFlagRead_costomen) && $bFlagRead_costomen))
{
    $this->ajax_return_values_costomen(true);
}
if ($original_sugerido_mayor !== $modificado_sugerido_mayor || isset($this->nmgp_cmp_readonly['sugerido_mayor']) || (isset($bFlagRead_sugerido_mayor) && $bFlagRead_sugerido_mayor))
{
    $this->ajax_return_values_sugerido_mayor(true);
}
if ($original_por_preciominimo !== $modificado_por_preciominimo || isset($this->nmgp_cmp_readonly['por_preciominimo']) || (isset($bFlagRead_por_preciominimo) && $bFlagRead_por_preciominimo))
{
    $this->ajax_return_values_por_preciominimo(true);
}
if ($original_sugerido_menor !== $modificado_sugerido_menor || isset($this->nmgp_cmp_readonly['sugerido_menor']) || (isset($bFlagRead_sugerido_menor) && $bFlagRead_sugerido_menor))
{
    $this->ajax_return_values_sugerido_menor(true);
}
$this->NM_ajax_info['event_field'] = 'por';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function recmayamen_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_recmayamen = $this->recmayamen;

 
      $nm_select = "select idinv from inventario where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_inv = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_inv[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_inv = false;
          $this->ds_inv_erro = $this->Db->ErrorMsg();
      } 
     } 
;
if(isset($this->ds_inv[0][0]))
	{
	 
      $nm_select = "select recmayamen from productos where idprod='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_p = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_p[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_p = false;
          $this->ds_p_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->ds_p[0][0]) and $this->ds_p[0][0]==0)
		{
		goto Noerr;
		}
	else if(isset($this->ds_p[0][0]) and $this->ds_p[0][0]<>0)
		{
		$this->recmayamen =$this->ds_p[0][0];
		}
	goto err;
	}
else
	{
	goto Noerr;
	}
err:;
$this->sc_ajax_message("Producto tiene movimiento, no se puede Modificar el Factor!", "ADVERTENCIA", "", "");
Noerr:;

$modificado_idprod = $this->idprod;
$modificado_recmayamen = $this->recmayamen;
$this->nm_formatar_campos('idprod', 'recmayamen');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_recmayamen !== $modificado_recmayamen || isset($this->nmgp_cmp_readonly['recmayamen']) || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen))
{
    $this->ajax_return_values_recmayamen(true);
}
$this->NM_ajax_info['event_field'] = 'recmayamen';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function sabores_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_sabores = $this->sabores;
$original_sabor = $this->sabor;

if($this->idprod >0)
	{
	if($this->sabores =='SI')
		{
		$this->nmgp_cmp_hidden["sabor"] = "on"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'on';
		
     $nm_select ="update productos set sabores='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		$this->nmgp_cmp_hidden["sabor"] = "off"; $this->NM_ajax_info['fieldDisplay']['sabor'] = 'off';
		
     $nm_select ="update productos set sabores='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_sabores = $this->sabores;
$modificado_sabor = $this->sabor;
$this->nm_formatar_campos('idprod', 'sabores', 'sabor');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_sabores !== $modificado_sabores || isset($this->nmgp_cmp_readonly['sabores']) || (isset($bFlagRead_sabores) && $bFlagRead_sabores))
{
    $this->ajax_return_values_sabores(true);
}
if ($original_sabor !== $modificado_sabor || isset($this->nmgp_cmp_readonly['sabor']) || (isset($bFlagRead_sabor) && $bFlagRead_sabor))
{
    $this->ajax_return_values_sabor(true);
}
$this->NM_ajax_info['event_field'] = 'sabores';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function serial_codbarras_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_serial_codbarras = $this->serial_codbarras;

if($this->idprod >0)
	{	
	if ($this->serial_codbarras =='SI')
		{
		
     $nm_select ="update productos set serial_codbarras='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		
     $nm_select ="update productos set serial_codbarras='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_serial_codbarras = $this->serial_codbarras;
$this->nm_formatar_campos('idprod', 'serial_codbarras');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_serial_codbarras !== $modificado_serial_codbarras || isset($this->nmgp_cmp_readonly['serial_codbarras']) || (isset($bFlagRead_serial_codbarras) && $bFlagRead_serial_codbarras))
{
    $this->ajax_return_values_serial_codbarras(true);
}
$this->NM_ajax_info['event_field'] = 'serial';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function tallas_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_idprod = $this->idprod;
$original_tallas = $this->tallas;
$original_conftalla = $this->conftalla;

if($this->idprod >0)
	{
	if($this->tallas =='SI')
		{
		$this->nmgp_cmp_hidden["conftalla"] = "on"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'on';
		
     $nm_select ="update productos set tallas='SI' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	else
		{
		$this->nmgp_cmp_hidden["conftalla"] = "off"; $this->NM_ajax_info['fieldDisplay']['conftalla'] = 'off';
		
     $nm_select ="update productos set tallas='NO' where idprod=$this->idprod "; 
         $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
         $rf = $this->Db->Execute($nm_select);
         if ($rf === false)
         {
             $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
             $this->NM_rollback_db(); 
             if ($this->NM_ajax_flag)
             {
                form_productos_060522_mob_pack_ajax_response();
             }
             exit;
         }
         $rf->Close();
      ;
		}
	}


$modificado_idprod = $this->idprod;
$modificado_tallas = $this->tallas;
$modificado_conftalla = $this->conftalla;
$this->nm_formatar_campos('idprod', 'tallas', 'conftalla');
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_tallas !== $modificado_tallas || isset($this->nmgp_cmp_readonly['tallas']) || (isset($bFlagRead_tallas) && $bFlagRead_tallas))
{
    $this->ajax_return_values_tallas(true);
}
if ($original_conftalla !== $modificado_conftalla || isset($this->nmgp_cmp_readonly['conftalla']) || (isset($bFlagRead_conftalla) && $bFlagRead_conftalla))
{
    $this->ajax_return_values_conftalla(true);
}
$this->NM_ajax_info['event_field'] = 'tallas';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function unidad__onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_unimen = $this->unimen;
$original_unidad_ = $this->unidad_;

$this->unimen =$this->unidad_ ;

$modificado_unimen = $this->unimen;
$modificado_unidad_ = $this->unidad_;
$this->nm_formatar_campos('unimen', 'unidad_');
if ($original_unimen !== $modificado_unimen || isset($this->nmgp_cmp_readonly['unimen']) || (isset($bFlagRead_unimen) && $bFlagRead_unimen))
{
    $this->ajax_return_values_unimen(true);
}
if ($original_unidad_ !== $modificado_unidad_ || isset($this->nmgp_cmp_readonly['unidad_']) || (isset($bFlagRead_unidad_) && $bFlagRead_unidad_))
{
    $this->ajax_return_values_unidad_(true);
}
$this->NM_ajax_info['event_field'] = 'unidad';
form_productos_060522_mob_pack_ajax_response();
exit;


$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function unidad_ma_onChange()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_unimay = $this->unimay;
$original_unidad_ma = $this->unidad_ma;

$this->unimay =$this->unidad_ma ;

$modificado_unimay = $this->unimay;
$modificado_unidad_ma = $this->unidad_ma;
$this->nm_formatar_campos('unimay', 'unidad_ma');
if ($original_unimay !== $modificado_unimay || isset($this->nmgp_cmp_readonly['unimay']) || (isset($bFlagRead_unimay) && $bFlagRead_unimay))
{
    $this->ajax_return_values_unimay(true);
}
if ($original_unidad_ma !== $modificado_unidad_ma || isset($this->nmgp_cmp_readonly['unidad_ma']) || (isset($bFlagRead_unidad_ma) && $bFlagRead_unidad_ma))
{
    $this->ajax_return_values_unidad_ma(true);
}
$this->NM_ajax_info['event_field'] = 'unidad';
form_productos_060522_mob_pack_ajax_response();
exit;


$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
function unidmaymen_onClick()
{
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'on';
  
$original_unidmaymen = $this->unidmaymen;
$original_idprod = $this->idprod;
$original_recmayamen = $this->recmayamen;
$original_multiple_escala = $this->multiple_escala;
$original_en_base_a = $this->en_base_a;
$original_unidad_ma = $this->unidad_ma;
$original_preciofull = $this->preciofull;
$original_precio2 = $this->precio2;
$original_preciomay = $this->preciomay;

if ($this->unidmaymen =='NO')
	{
	 
      $nm_select = "select idinv from inventario where idpro='".$this->idprod ."'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_select; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->ds_inv = array();
     if ($this->idprod != "")
     { 
      if ($SCrx = $this->Db->Execute($nm_select)) 
      { 
          $SCy = 0; 
          $nm_count = $SCrx->FieldCount();
          while (!$SCrx->EOF)
          { 
                 $SCrx->fields[0] = str_replace(',', '.', $SCrx->fields[0]);
                 $SCrx->fields[0] = (strpos(strtolower($SCrx->fields[0]), "e")) ? (float)$SCrx->fields[0] : $SCrx->fields[0];
                 $SCrx->fields[0] = (string)$SCrx->fields[0];
                 for ($SCx = 0; $SCx < $nm_count; $SCx++)
                 { 
                      $this->ds_inv[$SCy] [$SCx] = $SCrx->fields[$SCx];
                 }
                 $SCy++; 
                 $SCrx->MoveNext();
          } 
          $SCrx->Close();
      } 
      elseif (isset($GLOBALS["NM_ERRO_IBASE"]) && $GLOBALS["NM_ERRO_IBASE"] != 1)  
      { 
          $this->ds_inv = false;
          $this->ds_inv_erro = $this->Db->ErrorMsg();
      } 
     } 
;
	if(isset($this->ds_inv[0][0]))
		{
		$this->unidmaymen ='SI';
		goto err;
		}
	
	if($this->recmayamen >=1 or $this->multiple_escala =='SI')
		{
		$this->sc_set_focus('recmayamen');
		$this->unidmaymen ='SI';
		goto err2;
		}
	$this->en_base_a ='UMAY';
	$this->multiple_escala ='NO';
	$this->sc_field_readonly("unidad_ma", 'on');
	$this->sc_field_readonly("costomay", 'on');
	$this->sc_field_readonly("recmayamen", 'on');
	$this->sc_field_readonly("multiple_escala", 'on');
	$this->sc_field_readonly("en_base_a", 'on');
	$this->sc_field_readonly("preciofull", 'on');
	$this->sc_field_readonly("precio2", 'on');
	$this->sc_field_readonly("preciomay", 'on');
	$this->sc_set_focus('unimen');
	goto Noerr;
	}
else
	{
	$this->sc_field_readonly("unidad_ma", 'off');
	$this->sc_field_readonly("costomay", 'off');
	$this->sc_field_readonly("recmayamen", 'off');
	$this->sc_field_readonly("multiple_escala", 'off');
	$this->sc_field_readonly("en_base_a", 'off');
	$this->sc_field_readonly("preciofull", 'off');
	$this->sc_field_readonly("precio2", 'off');
	$this->sc_field_readonly("preciomay", 'off');
	$this->sc_set_focus('unidad_ma');
	$this->sc_field_readonly("multiple_escala", 'off');
	$this->sc_field_readonly("en_base_a", 'off');
	goto Noerr;
	}
err:;
$this->sc_ajax_message("Producto tiene movimiento, no se puede quitar escala Y/O Factor!", "ADVERTENCIA", "", "");
goto Noerr;
err2:;
$this->sc_ajax_message("El producto tiene configurado Factor Multiplicador y/o Multiples escalas!", "ADVERTENCIA", "", "");
Noerr:;


$modificado_unidmaymen = $this->unidmaymen;
$modificado_idprod = $this->idprod;
$modificado_recmayamen = $this->recmayamen;
$modificado_multiple_escala = $this->multiple_escala;
$modificado_en_base_a = $this->en_base_a;
$modificado_unidad_ma = $this->unidad_ma;
$modificado_preciofull = $this->preciofull;
$modificado_precio2 = $this->precio2;
$modificado_preciomay = $this->preciomay;
$this->nm_formatar_campos('unidmaymen', 'idprod', 'recmayamen', 'multiple_escala', 'en_base_a', 'unidad_ma', 'preciofull', 'precio2', 'preciomay');
if ($original_unidmaymen !== $modificado_unidmaymen || isset($this->nmgp_cmp_readonly['unidmaymen']) || (isset($bFlagRead_unidmaymen) && $bFlagRead_unidmaymen))
{
    $this->ajax_return_values_unidmaymen(true);
}
if ($original_idprod !== $modificado_idprod || isset($this->nmgp_cmp_readonly['idprod']) || (isset($bFlagRead_idprod) && $bFlagRead_idprod))
{
    $this->ajax_return_values_idprod(true);
}
if ($original_recmayamen !== $modificado_recmayamen || isset($this->nmgp_cmp_readonly['recmayamen']) || (isset($bFlagRead_recmayamen) && $bFlagRead_recmayamen))
{
    $this->ajax_return_values_recmayamen(true);
}
if ($original_multiple_escala !== $modificado_multiple_escala || isset($this->nmgp_cmp_readonly['multiple_escala']) || (isset($bFlagRead_multiple_escala) && $bFlagRead_multiple_escala))
{
    $this->ajax_return_values_multiple_escala(true);
}
if ($original_en_base_a !== $modificado_en_base_a || isset($this->nmgp_cmp_readonly['en_base_a']) || (isset($bFlagRead_en_base_a) && $bFlagRead_en_base_a))
{
    $this->ajax_return_values_en_base_a(true);
}
if ($original_unidad_ma !== $modificado_unidad_ma || isset($this->nmgp_cmp_readonly['unidad_ma']) || (isset($bFlagRead_unidad_ma) && $bFlagRead_unidad_ma))
{
    $this->ajax_return_values_unidad_ma(true);
}
if ($original_preciofull !== $modificado_preciofull || isset($this->nmgp_cmp_readonly['preciofull']) || (isset($bFlagRead_preciofull) && $bFlagRead_preciofull))
{
    $this->ajax_return_values_preciofull(true);
}
if ($original_precio2 !== $modificado_precio2 || isset($this->nmgp_cmp_readonly['precio2']) || (isset($bFlagRead_precio2) && $bFlagRead_precio2))
{
    $this->ajax_return_values_precio2(true);
}
if ($original_preciomay !== $modificado_preciomay || isset($this->nmgp_cmp_readonly['preciomay']) || (isset($bFlagRead_preciomay) && $bFlagRead_preciomay))
{
    $this->ajax_return_values_preciomay(true);
}
$this->NM_ajax_info['event_field'] = 'unidmaymen';
form_productos_060522_mob_pack_ajax_response();
exit;
$_SESSION['scriptcase']['form_productos_060522_mob']['contr_erro'] = 'off';
}
//
 function nm_gera_html()
 {
    global
           $nm_url_saida, $nmgp_url_saida, $nm_saida_global, $nm_apl_dependente, $glo_subst, $sc_check_excl, $sc_check_incl, $nmgp_num_form, $NM_run_iframe;
     if ($this->Embutida_proc)
     {
         return;
     }
     if ($this->nmgp_form_show == 'off')
     {
         exit;
     }
      if (isset($NM_run_iframe) && $NM_run_iframe == 1)
      {
          $this->nmgp_botoes['exit'] = "off";
      }
     $HTTP_REFERER = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : ""; 
     $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
     $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['botoes'] = $this->nmgp_botoes;
     if ($this->nmgp_opcao != "recarga" && $this->nmgp_opcao != "muda_form")
     {
         $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_ant'] = $this->nmgp_opcao;
     }
     else
     {
         $this->nmgp_opcao = $this->nmgp_opc_ant;
     }
     if (!empty($this->Campos_Mens_erro)) 
     {
         $this->Erro->mensagem(__FILE__, __LINE__, "critica", $this->Campos_Mens_erro); 
         $this->Campos_Mens_erro = "";
     }
     if (($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" || $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "R") && $this->nm_flag_iframe && empty($this->nm_todas_criticas))
     {
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe_ajax']))
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'] = array("edit", "");
          }
          else
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'] .= "&nmgp_opcao=edit";
          }
          if ($this->sc_evento == "insert" && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F")
          {
              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe_ajax']))
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'] = array("edit", "fim");
              }
              else
              {
                  $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'] .= "&rec=fim";
              }
          }
          $this->NM_close_db(); 
          $sJsParent = '';
          if ($this->NM_ajax_flag && isset($this->NM_ajax_info['param']['buffer_output']) && $this->NM_ajax_info['param']['buffer_output'])
          {
              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe_ajax']))
              {
                  $this->NM_ajax_info['ajaxJavascript'][] = array("parent.ajax_navigate", $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit']);
              }
              else
              {
                  $sJsParent .= 'parent';
                  $this->NM_ajax_info['redir']['metodo'] = 'location';
                  $this->NM_ajax_info['redir']['action'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'];
                  $this->NM_ajax_info['redir']['target'] = $sJsParent;
              }
              form_productos_060522_mob_pack_ajax_response();
              exit;
          }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">

         <html><body>
         <script type="text/javascript">
<?php
    
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe_ajax']))
    {
        $opc = ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['run_iframe'] == "F" && $this->sc_evento == "insert") ? "fim" : "";
        echo "parent.ajax_navigate('edit', '" .$opc . "');";
    }
    else
    {
        echo $sJsParent . "parent.location = '" . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['retorno_edit'] . "';";
    }
?>
         </script>
         </body></html>
<?php
         exit;
     }
//-- 
   if ($this->nmgp_opcao == "novo")
   { 
       $out_imagen = "";  
   } 
   else 
   { 
       $out_imagen = $this->imagen;  
   } 
   if ($this->imagen != "" && $this->imagen != "none")   
   { 
       $path_imagen = $this->Ini->root . $this->Ini->path_imagens . "/" . $_SESSION['gnit'] . "/" . "/" . $this->imagen;
       $md5_imagen  = md5("/" . $_SESSION['gnit'] . "/" . $this->imagen);
       nm_fix_SubDirUpload($this->imagen,$this->Ini->root . $this->Ini->path_imagens,"/" . $_SESSION['gnit'] . "/");
        if (is_file($path_imagen))  
       { 
           $NM_ler_img = true;
           $out_imagen = $this->Ini->path_imag_temp . "/sc_imagen_" . $md5_imagen . ".gif" ;  
           if (is_file($this->Ini->root . $out_imagen))  
           { 
               $NM_img_time = filemtime($this->Ini->root . $out_imagen) + 0; 
               if ($this->Ini->nm_timestamp < $NM_img_time)  
               { 
                   $NM_ler_img = false;
               } 
           } 
           if ($NM_ler_img) 
           { 
               $tmp_imagen = fopen($path_imagen, "r") ; 
               $reg_imagen = fread($tmp_imagen, filesize($path_imagen)) ; 
               fclose($tmp_imagen) ;  
               $arq_imagen = fopen($this->Ini->root . $out_imagen, "w") ;  
               fwrite($arq_imagen, $reg_imagen) ;  
               fclose($arq_imagen) ;  
           } 
           $sc_obj_img = new nm_trata_img($this->Ini->root . $out_imagen, true);
           $this->nmgp_return_img['imagen'][0] = $sc_obj_img->getHeight();
           $this->nmgp_return_img['imagen'][1] = $sc_obj_img->getWidth();
           $NM_redim_img = true;
           $out1_imagen = $out_imagen; 
           $md5_imagen  = md5("/" . $_SESSION['gnit'] . "/" . $this->imagen);
           $out_imagen = $this->Ini->path_imag_temp . "/sc_imagen_150150" . $md5_imagen . ".gif" ;  
           if (is_file($this->Ini->root . $out_imagen))  
           { 
               $NM_img_time = filemtime($this->Ini->root . $out_imagen) + 0; 
               if ($this->Ini->nm_timestamp < $NM_img_time)  
               { 
                   $NM_redim_img = false;
               } 
           } 
           if ($NM_redim_img) 
           { 
               if (!$this->Ini->Gd_missing)
               { 
                   $sc_obj_img = new nm_trata_img($this->Ini->root . $out1_imagen, true);
                   $sc_obj_img->setWidth(150);
                   $sc_obj_img->setHeight(150);
                   $sc_obj_img->setManterAspecto(true);
                   $sc_obj_img->createImg($this->Ini->root . $out_imagen);
               } 
               else 
               { 
                   $out_imagen = $out1_imagen;
               } 
           } 
       if ($this->Ini->Export_img_zip) {
           $this->Ini->Img_export_zip[] = $this->Ini->root . $out_imagen;
           $out_imagen = str_replace($this->Ini->path_imag_temp . "/", "", $out_imagen);
       } 
       } 
   } 
   if (isset($_POST['nmgp_opcao']) && 'excluir' == $_POST['nmgp_opcao'] && $this->sc_evento != "delete" && (!isset($this->sc_evento_old) || 'delete' != $this->sc_evento_old))
   {
       global $temp_out_imagen;
       if (isset($temp_out_imagen))
       {
           $out_imagen = $temp_out_imagen;
       }
       global $temp_out1_imagen;
       if (isset($temp_out1_imagen))
       {
           $out1_imagen = $temp_out1_imagen;
       }
   }
        $this->initFormPages();
        include_once("form_productos_060522_mob_form0.php");
        include_once("form_productos_060522_mob_form1.php");
        $this->hideFormPages();
 }

        function initFormPages() {
                $this->Ini->nm_page_names = array(
                        'Pag1' => '0',
                        'Pag2' => '1',
                );

                $this->Ini->nm_page_blocks = array(
                        'Pag1' => array(
                                0 => 'on',
                                1 => 'on',
                                2 => 'on',
                                3 => 'on',
                                4 => 'on',
                                5 => 'on',
                                6 => 'on',
                        ),
                        'Pag2' => array(
                                7 => 'on',
                                8 => 'on',
                        ),
                );

                $this->Ini->nm_block_page = array(
                        0 => 'Pag1',
                        1 => 'Pag1',
                        2 => 'Pag1',
                        3 => 'Pag1',
                        4 => 'Pag1',
                        5 => 'Pag1',
                        6 => 'Pag1',
                        7 => 'Pag2',
                        8 => 'Pag2',
                );

                if (!empty($this->Ini->nm_hidden_blocos)) {
                        foreach ($this->Ini->nm_hidden_blocos as $blockNo => $blockStatus) {
                                if ('off' == $blockStatus) {
                                        $this->Ini->nm_page_blocks[ $this->Ini->nm_block_page[$blockNo] ][$blockNo] = 'off';
                                }
                        }
                }

                foreach ($this->Ini->nm_page_blocks as $pageName => $pageBlocks) {
                        $hasDisplayedBlock = false;

                        foreach ($pageBlocks as $blockNo => $blockStatus) {
                                if ('on' == $blockStatus) {
                                        $hasDisplayedBlock = true;
                                }
                        }

                        if (!$hasDisplayedBlock) {
                                $this->Ini->nm_hidden_pages[$pageName] = 'off';
                        }
                }
        } // initFormPages

        function hideFormPages() {
                if (!empty($this->Ini->nm_hidden_pages)) {
?>
<script type="text/javascript">
$(function() {
        scResetPagesDisplay();
<?php
                        foreach ($this->Ini->nm_hidden_pages as $pageName => $pageStatus) {
                                if ('off' == $pageStatus) {
?>
        scHidePage("<?php echo $this->Ini->nm_page_names[$pageName]; ?>");
<?php
                                }
                        }
?>
        scCheckNoPageSelected();
});
</script>
<?php
                }
        } // hideFormPages

    function form_format_readonly($field, $value)
    {
        $result = $value;

        $this->form_highlight_search($result, $field, $value);

        return $result;
    }

    function form_highlight_search(&$result, $field, $value)
    {
        if ($this->proc_fast_search) {
            $this->form_highlight_search_quicksearch($result, $field, $value);
        }
    }

    function form_highlight_search_quicksearch(&$result, $field, $value)
    {
        $searchOk = false;
        if ('SC_all_Cmp' == $this->nmgp_fast_search && in_array($field, array("idprod", "codigobar", "nompro", "unimay", "unimen", "costomay", "costomen", "recmayamen", "preciomay", "preciomen", "stockmay", "stockmen", "idgrup", "idpro1", "idpro2", "idiva", "otro", "otro2", "otro3"))) {
            $searchOk = true;
        }
        elseif ($field == $this->nmgp_fast_search && in_array($field, array(""))) {
            $searchOk = true;
        }

        if (!$searchOk || '' == $this->nmgp_arg_fast_search) {
            return;
        }

        $htmlIni = '<div class="highlight" style="background-color: #fafaca; display: inline-block">';
        $htmlFim = '</div>';

        if ('qp' == $this->nmgp_cond_fast_search) {
            $keywords = preg_quote($this->nmgp_arg_fast_search, '/');
            $result = preg_replace('/'. $keywords .'/i', $htmlIni . '$0' . $htmlFim, $result);
        } elseif ('eq' == $this->nmgp_cond_fast_search) {
            if (strcasecmp($this->nmgp_arg_fast_search, $value) == 0) {
                $result = $htmlIni. $result .$htmlFim;
            }
        }
    }


    function form_encode_input($string)
    {
        if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['table_refresh']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['table_refresh'])
        {
            return NM_encode_input(NM_encode_input($string));
        }
        else
        {
            return NM_encode_input($string);
        }
    } // form_encode_input

   function jqueryCalendarDtFormat($sFormat, $sSep)
   {
       $sFormat = chunk_split(str_replace('yyyy', 'yy', $sFormat), 2, $sSep);

       if ($sSep == substr($sFormat, -1))
       {
           $sFormat = substr($sFormat, 0, -1);
       }

       return $sFormat;
   } // jqueryCalendarDtFormat

   function jqueryCalendarTimeStart($sFormat)
   {
       $aDateParts = explode(';', $sFormat);

       if (2 == sizeof($aDateParts))
       {
           $sTime = $aDateParts[1];
       }
       else
       {
           $sTime = 'hh:mm:ss';
       }

       return str_replace(array('h', 'm', 'i', 's'), array('0', '0', '0', '0'), $sTime);
   } // jqueryCalendarTimeStart

   function jqueryCalendarWeekInit($sDay)
   {
       switch ($sDay) {
           case 'MO': return 1; break;
           case 'TU': return 2; break;
           case 'WE': return 3; break;
           case 'TH': return 4; break;
           case 'FR': return 5; break;
           case 'SA': return 6; break;
           default  : return 7; break;
       }
   } // jqueryCalendarWeekInit

   function jqueryIconFile($sModule)
   {
       $sImage = '';
       if ('calendar' == $sModule)
       {
           if (isset($this->arr_buttons['bcalendario']) && isset($this->arr_buttons['bcalendario']['type']) && 'image' == $this->arr_buttons['bcalendario']['type'] && 'only_fontawesomeicon' != $this->arr_buttons['bcalendario']['display'])
           {
               $sImage = $this->arr_buttons['bcalendario']['image'];
           }
       }
       elseif ('calculator' == $sModule)
       {
           if (isset($this->arr_buttons['bcalculadora']) && isset($this->arr_buttons['bcalculadora']['type']) && 'image' == $this->arr_buttons['bcalculadora']['type'] && 'only_fontawesomeicon' != $this->arr_buttons['bcalculadora']['display'])
           {
               $sImage = $this->arr_buttons['bcalculadora']['image'];
           }
       }

       return '' == $sImage ? '' : $this->Ini->path_icones . '/' . $sImage;
   } // jqueryIconFile

   function jqueryFAFile($sModule)
   {
       $sFA = '';
       if ('calendar' == $sModule)
       {
           if (isset($this->arr_buttons['bcalendario']) && isset($this->arr_buttons['bcalendario']['type']) && ('image' == $this->arr_buttons['bcalendario']['type'] || 'button' == $this->arr_buttons['bcalendario']['type']) && 'only_fontawesomeicon' == $this->arr_buttons['bcalendario']['display'])
           {
               $sFA = $this->arr_buttons['bcalendario']['fontawesomeicon'];
           }
       }
       elseif ('calculator' == $sModule)
       {
           if (isset($this->arr_buttons['bcalculadora']) && isset($this->arr_buttons['bcalculadora']['type']) && ('image' == $this->arr_buttons['bcalculadora']['type'] || 'button' == $this->arr_buttons['bcalculadora']['type']) && 'only_fontawesomeicon' == $this->arr_buttons['bcalculadora']['display'])
           {
               $sFA = $this->arr_buttons['bcalculadora']['fontawesomeicon'];
           }
       }

       return '' == $sFA ? '' : "<span class='scButton_fontawesome " . $sFA . "'></span>";
   } // jqueryFAFile

   function jqueryButtonText($sModule)
   {
       $sClass = '';
       $sText  = '';
       if ('calendar' == $sModule)
       {
           if (isset($this->arr_buttons['bcalendario']) && isset($this->arr_buttons['bcalendario']['type']) && ('image' == $this->arr_buttons['bcalendario']['type'] || 'button' == $this->arr_buttons['bcalendario']['type']))
           {
               if ('only_text' == $this->arr_buttons['bcalendario']['display'])
               {
                   $sClass = 'scButton_' . $this->arr_buttons['bcalendario']['style'];
                   $sText  = $this->arr_buttons['bcalendario']['value'];
               }
               elseif ('text_fontawesomeicon' == $this->arr_buttons['bcalendario']['display'])
               {
                   $sClass = 'scButton_' . $this->arr_buttons['bcalendario']['style'];
                   if ('text_right' == $this->arr_buttons['bcalendario']['display_position'])
                   {
                       $sText = "<i class='icon_fa " . $this->arr_buttons['bcalendario']['fontawesomeicon'] . "'></i> " . $this->arr_buttons['bcalendario']['value'];
                   }
                   else
                   {
                       $sText = $this->arr_buttons['bcalendario']['value'] . " <i class='icon_fa " . $this->arr_buttons['bcalendario']['fontawesomeicon'] . "'></i>";
                   }
               }
           }
       }
       elseif ('calculator' == $sModule)
       {
           if (isset($this->arr_buttons['bcalculadora']) && isset($this->arr_buttons['bcalculadora']['type']) && ('image' == $this->arr_buttons['bcalculadora']['type'] || 'button' == $this->arr_buttons['bcalculadora']['type']))
           {
               if ('only_text' == $this->arr_buttons['bcalculadora']['display'])
               {
                   $sClass = 'scButton_' . $this->arr_buttons['bcalendario']['style'];
                   $sText  = $this->arr_buttons['bcalculadora']['value'];
               }
               elseif ('text_fontawesomeicon' == $this->arr_buttons['bcalculadora']['display'])
               {
                   $sClass = 'scButton_' . $this->arr_buttons['bcalendario']['style'];
                   if ('text_right' == $this->arr_buttons['bcalendario']['display_position'])
                   {
                       $sText = "<i class='icon_fa " . $this->arr_buttons['bcalculadora']['fontawesomeicon'] . "'></i> " . $this->arr_buttons['bcalculadora']['value'];
                   }
                   else
                   {
                       $sText = $this->arr_buttons['bcalculadora']['value'] . " <i class='icon_fa " . $this->arr_buttons['bcalculadora']['fontawesomeicon'] . "'></i> ";
                   }
               }
           }
       }

       return '' == $sText ? array('', '') : array($sText, $sClass);
   } // jqueryButtonText


    function scCsrfGetToken()
    {
        if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['csrf_token']))
        {
            $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['csrf_token'] = $this->scCsrfGenerateToken();
        }

        return $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['csrf_token'];
    }

    function scCsrfGenerateToken()
    {
        $aSources = array(
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            '1234567890',
            '!@$*()-_[]{},.;:'
        );

        $sRandom = '';

        $aSourcesSizes = array();
        $iSourceSize   = sizeof($aSources) - 1;
        for ($i = 0; $i <= $iSourceSize; $i++)
        {
            $aSourcesSizes[$i] = strlen($aSources[$i]) - 1;
        }

        for ($i = 0; $i < 64; $i++)
        {
            $iSource = $this->scCsrfRandom(0, $iSourceSize);
            $sRandom .= substr($aSources[$iSource], $this->scCsrfRandom(0, $aSourcesSizes[$iSource]), 1);
        }

        return $sRandom;
    }

    function scCsrfRandom($iMin, $iMax)
    {
        return mt_rand($iMin, $iMax);
    }

        function addUrlParam($url, $param, $value) {
                $urlParts  = explode('?', $url);
                $urlParams = isset($urlParts[1]) ? explode('&', $urlParts[1]) : array();
                $objParams = array();
                foreach ($urlParams as $paramInfo) {
                        $paramParts = explode('=', $paramInfo);
                        $objParams[ $paramParts[0] ] = isset($paramParts[1]) ? $paramParts[1] : '';
                }
                $objParams[$param] = $value;
                $urlParams = array();
                foreach ($objParams as $paramName => $paramValue) {
                        $urlParams[] = $paramName . '=' . $paramValue;
                }
                return $urlParts[0] . '?' . implode('&', $urlParams);
        }
 function allowedCharsCharset($charlist)
 {
     if ($_SESSION['scriptcase']['charset'] != 'UTF-8')
     {
         $charlist = NM_conv_charset($charlist, $_SESSION['scriptcase']['charset'], 'UTF-8');
     }
     return str_replace("'", "\'", $charlist);
 }

function sc_file_size($file, $format = false)
{
    if ('' == $file) {
        return '';
    }
    if (!@is_file($file)) {
        return '';
    }
    $fileSize = @filesize($file);
    if ($format) {
        $suffix = '';
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' KB';
        }
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' MB';
        }
        if (1024 >= $fileSize) {
            $fileSize /= 1024;
            $suffix    = ' GB';
        }
        $fileSize = $fileSize . $suffix;
    }
    return $fileSize;
}


 function new_date_format($type, $field)
 {
     $new_date_format_out = '';

     if ('DT' == $type)
     {
         $date_format  = $this->field_config[$field]['date_format'];
         $date_sep     = $this->field_config[$field]['date_sep'];
         $date_display = $this->field_config[$field]['date_display'];
         $time_format  = '';
         $time_sep     = '';
         $time_display = '';
     }
     elseif ('DH' == $type)
     {
         $date_format  = false !== strpos($this->field_config[$field]['date_format'] , ';') ? substr($this->field_config[$field]['date_format'] , 0, strpos($this->field_config[$field]['date_format'] , ';')) : $this->field_config[$field]['date_format'];
         $date_sep     = $this->field_config[$field]['date_sep'];
         $date_display = false !== strpos($this->field_config[$field]['date_display'], ';') ? substr($this->field_config[$field]['date_display'], 0, strpos($this->field_config[$field]['date_display'], ';')) : $this->field_config[$field]['date_display'];
         $time_format  = false !== strpos($this->field_config[$field]['date_format'] , ';') ? substr($this->field_config[$field]['date_format'] , strpos($this->field_config[$field]['date_format'] , ';') + 1) : '';
         $time_sep     = $this->field_config[$field]['time_sep'];
         $time_display = false !== strpos($this->field_config[$field]['date_display'], ';') ? substr($this->field_config[$field]['date_display'], strpos($this->field_config[$field]['date_display'], ';') + 1) : '';
     }
     elseif ('HH' == $type)
     {
         $date_format  = '';
         $date_sep     = '';
         $date_display = '';
         $time_format  = $this->field_config[$field]['date_format'];
         $time_sep     = $this->field_config[$field]['time_sep'];
         $time_display = $this->field_config[$field]['date_display'];
     }

     if ('DT' == $type || 'DH' == $type)
     {
         $date_array = array();
         $date_index = 0;
         $date_ult   = '';
         for ($i = 0; $i < strlen($date_format); $i++)
         {
             $char = strtolower(substr($date_format, $i, 1));
             if (in_array($char, array('d', 'm', 'y', 'a')))
             {
                 if ('a' == $char)
                 {
                     $char = 'y';
                 }
                 if ($char == $date_ult)
                 {
                     $date_array[$date_index] .= $char;
                 }
                 else
                 {
                     if ('' != $date_ult)
                     {
                         $date_index++;
                     }
                     $date_array[$date_index] = $char;
                 }
             }
             $date_ult = $char;
         }

         $disp_array = array();
         $date_index = 0;
         $date_ult   = '';
         for ($i = 0; $i < strlen($date_display); $i++)
         {
             $char = strtolower(substr($date_display, $i, 1));
             if (in_array($char, array('d', 'm', 'y', 'a')))
             {
                 if ('a' == $char)
                 {
                     $char = 'y';
                 }
                 if ($char == $date_ult)
                 {
                     $disp_array[$date_index] .= $char;
                 }
                 else
                 {
                     if ('' != $date_ult)
                     {
                         $date_index++;
                     }
                     $disp_array[$date_index] = $char;
                 }
             }
             $date_ult = $char;
         }

         $date_final = array();
         foreach ($date_array as $date_part)
         {
             if (in_array($date_part, $disp_array))
             {
                 $date_final[] = $date_part;
             }
         }

         $date_format = implode($date_sep, $date_final);
     }
     if ('HH' == $type || 'DH' == $type)
     {
         $time_array = array();
         $time_index = 0;
         $time_ult   = '';
         for ($i = 0; $i < strlen($time_format); $i++)
         {
             $char = strtolower(substr($time_format, $i, 1));
             if (in_array($char, array('h', 'i', 's')))
             {
                 if ($char == $time_ult)
                 {
                     $time_array[$time_index] .= $char;
                 }
                 else
                 {
                     if ('' != $time_ult)
                     {
                         $time_index++;
                     }
                     $time_array[$time_index] = $char;
                 }
             }
             $time_ult = $char;
         }

         $disp_array = array();
         $time_index = 0;
         $time_ult   = '';
         for ($i = 0; $i < strlen($time_display); $i++)
         {
             $char = strtolower(substr($time_display, $i, 1));
             if (in_array($char, array('h', 'i', 's')))
             {
                 if ($char == $time_ult)
                 {
                     $disp_array[$time_index] .= $char;
                 }
                 else
                 {
                     if ('' != $time_ult)
                     {
                         $time_index++;
                     }
                     $disp_array[$time_index] = $char;
                 }
             }
             $time_ult = $char;
         }

         $time_final = array();
         foreach ($time_array as $time_part)
         {
             if (in_array($time_part, $disp_array))
             {
                 $time_final[] = $time_part;
             }
         }

         $time_format = implode($time_sep, $time_final);
     }

     if ('DT' == $type)
     {
         $old_date_format = $date_format;
     }
     elseif ('DH' == $type)
     {
         $old_date_format = $date_format . ';' . $time_format;
     }
     elseif ('HH' == $type)
     {
         $old_date_format = $time_format;
     }

     for ($i = 0; $i < strlen($old_date_format); $i++)
     {
         $char = substr($old_date_format, $i, 1);
         if ('/' == $char)
         {
             $new_date_format_out .= $date_sep;
         }
         elseif (':' == $char)
         {
             $new_date_format_out .= $time_sep;
         }
         else
         {
             $new_date_format_out .= $char;
         }
     }

     $this->field_config[$field]['date_format'] = $new_date_format_out;
     if ('DH' == $type)
     {
         $new_date_format_out                                  = explode(';', $new_date_format_out);
         $this->field_config[$field]['date_format_js']        = $new_date_format_out[0];
         $this->field_config[$field . '_hora']['date_format'] = $new_date_format_out[1];
         $this->field_config[$field . '_hora']['time_sep']    = $this->field_config[$field]['time_sep'];
     }
 } // new_date_format

   function Form_lookup_idgrup()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   $nm_comando = "SELECT idgrupo, nomgrupo  FROM grupo  ORDER BY nomgrupo";

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idgrup'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_tipo_producto()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp + ' - ' + descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT codigo_tp, concat(codigo_tp,' - ', descripcion_tp)  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp&' - '&descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp + ' - ' + descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }
   else
   {
       $nm_comando = "SELECT codigo_tp, codigo_tp||' - '||descripcion_tp  FROM tipo_producto  ORDER BY codigo_tp, descripcion_tp";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_tipo_producto'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_otro()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "No?#?0?#?S?@?";
       $nmgp_def_dados .= "Sí?#?1?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_precio_editable()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_maneja_tcs_lfs()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "No Aplica?#?NA?#?S?@?";
       $nmgp_def_dados .= "Color, Talla y Sabor?#?CTS?#?N?@?";
       $nmgp_def_dados .= "Lote, F. Vencimiento, Serial?#?LFS?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_unidmaymen()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_multiple_escala()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_en_base_a()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "UNIDAD MAYOR?#?UMAY?#?S?@?";
       $nmgp_def_dados .= "UNIDAD?#?UMEN?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_idiva()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT idiva, trifa + ' - ' + tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT idiva, concat(trifa,' - ',tipo_impuesto)  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT idiva, trifa&' - '&tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT idiva, trifa + ' - ' + tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }
   else
   {
       $nm_comando = "SELECT idiva, trifa||' - '||tipo_impuesto  FROM iva  ORDER BY trifa, tipo_impuesto";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_idiva'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_activo()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?SI?#?S?@?";
       $nmgp_def_dados .= "NO?#?NO?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_colores()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_tallas()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_sabores()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_fecha_vencimiento()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_lote()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_serial_codbarras()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "NO?#?NO?#?S?@?";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_control_costo()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "No Aplica?#?NA?#?S?@?";
       $nmgp_def_dados .= "Último Costo?#?UC?#?N?@?";
       $nmgp_def_dados .= "Costo Promedio?#?CP?#?N?@?";
       $nmgp_def_dados .= "Porcentaje utilidad %?#?PC?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function Form_lookup_cod_cuenta()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT codigo, codigo + ' - ' + descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT codigo, concat(codigo,' - ',descripcion)  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT codigo, codigo&' - '&descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT codigo, codigo + ' - ' + descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }
   else
   {
       $nm_comando = "SELECT codigo, codigo||' - '||descripcion  FROM grupos_contables  ORDER BY codigo, descripcion";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_cod_cuenta'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_id_marca()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT id_marca, cod_marca + ' - ' + nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT id_marca, concat(cod_marca,' - ',nombre_marca)  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT id_marca, cod_marca&' - '&nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT id_marca, cod_marca + ' - ' + nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }
   else
   {
       $nm_comando = "SELECT id_marca, cod_marca||' - '||nombre_marca  FROM marca  ORDER BY cod_marca, nombre_marca";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_marca'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_id_linea()
   {
$nmgp_def_dados = "" ; 
if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']))
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']); 
}
else
{
    $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array(); 
}
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $GLOBALS["NM_ERRO_IBASE"] = 1;  
   } 
   $nm_nao_carga = false;
   $nmgp_def_dados = "" ; 
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']))
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array_unique($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea']); 
   }
   else
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'] = array(); 
    }

   $old_value_otro2 = $this->otro2;
   $old_value_stockmen = $this->stockmen;
   $old_value_costomen = $this->costomen;
   $old_value_costo_prom = $this->costo_prom;
   $old_value_recmayamen = $this->recmayamen;
   $old_value_existencia = $this->existencia;
   $old_value_por_preciominimo = $this->por_preciominimo;
   $old_value_sugerido_mayor = $this->sugerido_mayor;
   $old_value_sugerido_menor = $this->sugerido_menor;
   $old_value_preciofull = $this->preciofull;
   $old_value_precio2 = $this->precio2;
   $old_value_preciomay = $this->preciomay;
   $old_value_preciomen = $this->preciomen;
   $old_value_preciomen2 = $this->preciomen2;
   $old_value_preciomen3 = $this->preciomen3;
   $old_value_idprod = $this->idprod;
   $this->nm_tira_formatacao();


   $unformatted_value_otro2 = $this->otro2;
   $unformatted_value_stockmen = $this->stockmen;
   $unformatted_value_costomen = $this->costomen;
   $unformatted_value_costo_prom = $this->costo_prom;
   $unformatted_value_recmayamen = $this->recmayamen;
   $unformatted_value_existencia = $this->existencia;
   $unformatted_value_por_preciominimo = $this->por_preciominimo;
   $unformatted_value_sugerido_mayor = $this->sugerido_mayor;
   $unformatted_value_sugerido_menor = $this->sugerido_menor;
   $unformatted_value_preciofull = $this->preciofull;
   $unformatted_value_precio2 = $this->precio2;
   $unformatted_value_preciomay = $this->preciomay;
   $unformatted_value_preciomen = $this->preciomen;
   $unformatted_value_preciomen2 = $this->preciomen2;
   $unformatted_value_preciomen3 = $this->preciomen3;
   $unformatted_value_idprod = $this->idprod;

   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
   {
       $nm_comando = "SELECT id_linea, cod_linea + ' - ' + nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
   {
       $nm_comando = "SELECT id_linea, concat(cod_linea,' - ',nombre_linea)  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nm_comando = "SELECT id_linea, cod_linea&' - '&nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nm_comando = "SELECT id_linea, cod_linea + ' - ' + nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }
   else
   {
       $nm_comando = "SELECT id_linea, cod_linea||' - '||nombre_linea  FROM linea  ORDER BY cod_linea, nombre_linea";
   }

   $this->otro2 = $old_value_otro2;
   $this->stockmen = $old_value_stockmen;
   $this->costomen = $old_value_costomen;
   $this->costo_prom = $old_value_costo_prom;
   $this->recmayamen = $old_value_recmayamen;
   $this->existencia = $old_value_existencia;
   $this->por_preciominimo = $old_value_por_preciominimo;
   $this->sugerido_mayor = $old_value_sugerido_mayor;
   $this->sugerido_menor = $old_value_sugerido_menor;
   $this->preciofull = $old_value_preciofull;
   $this->precio2 = $old_value_precio2;
   $this->preciomay = $old_value_preciomay;
   $this->preciomen = $old_value_preciomen;
   $this->preciomen2 = $old_value_preciomen2;
   $this->preciomen3 = $old_value_preciomen3;
   $this->idprod = $old_value_idprod;

   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
   if ($nm_comando != "" && $rs = $this->Db->Execute($nm_comando))
   {
       while (!$rs->EOF) 
       { 
              $rs->fields[0] = str_replace(',', '.', $rs->fields[0]);
              $rs->fields[0] = (strpos(strtolower($rs->fields[0]), "e")) ? (float)$rs->fields[0] : $rs->fields[0];
              $rs->fields[0] = (string)$rs->fields[0];
              $nmgp_def_dados .= $rs->fields[1] . "?#?" ; 
              $nmgp_def_dados .= $rs->fields[0] . "?#?N?@?" ; 
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['Lookup_id_linea'][] = $rs->fields[0];
              $rs->MoveNext() ; 
       } 
       $rs->Close() ; 
   } 
   elseif ($GLOBALS["NM_ERRO_IBASE"] != 1 && $nm_comando != "")  
   {  
       $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit; 
   } 
   $GLOBALS["NM_ERRO_IBASE"] = 0; 
   $todox = str_replace("?#?@?#?", "?#?@ ?#?", trim($nmgp_def_dados)) ; 
   $todo  = explode("?@?", $todox) ; 
   return $todo;

   }
   function Form_lookup_para_registro_fe()
   {
       $nmgp_def_dados  = "";
       $nmgp_def_dados .= "SI?#?SI?#?N?@?";
       $nmgp_def_dados .= "NO?#?NO?#?N?@?";
       $todo = explode("?@?", $nmgp_def_dados);
       return $todo;

   }
   function SC_fast_search($in_fields, $arg_search, $data_search)
   {
      $fields = (strpos($in_fields, "SC_all_Cmp") !== false) ? array("SC_all_Cmp") : explode(";", $in_fields);
      $this->NM_case_insensitive = false;
      if (empty($data_search)) 
      {
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter']);
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total']);
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['fast_search']);
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal'])) 
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'] = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal'];
          }
          if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter'])
          {
              unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter']);
              $this->NM_ajax_info['empty_filter'] = 'ok';
              form_productos_060522_mob_pack_ajax_response();
              exit;
          }
          return;
      }
      $comando = "";
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($data_search))
      {
          $data_search = NM_conv_charset($data_search, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
      $sv_data = $data_search;
      foreach ($fields as $field) {
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "idprod", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "codigobar", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "nompro", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "unimay", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "unimen", $arg_search, $data_search);
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "costomay", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "costomen", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "recmayamen", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "preciomay", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "preciomen", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "stockmay", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "stockmen", $arg_search, str_replace(",", ".", $data_search));
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_idgrup($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "idgrup", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_idpro1($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "idpro1", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_idpro2($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "idpro2", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_idiva($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "idiva", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $data_lookup = $this->SC_lookup_otro($arg_search, $data_search);
              if (is_array($data_lookup) && !empty($data_lookup)) 
              {
                  $this->SC_monta_condicao($comando, "otro", $arg_search, $data_lookup);
              }
          }
          if ($field == "SC_all_Cmp") 
          {
              $this->SC_monta_condicao($comando, "otro2", $arg_search, str_replace(",", ".", $data_search));
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal']) && !empty($comando)) 
      {
          $comando = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_detal'] . " and (" .  $comando . ")";
      }
      if (empty($comando)) 
      {
          $comando = " 1 <> 1 "; 
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form']) && '' != $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form'])
      {
          $sc_where = " where " . $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter_form'] . " and (" . $comando . ")";
      }
      else
      {
         $sc_where = " where " . $comando;
      }
      $nmgp_select = "SELECT count(*) AS countTest from " . $this->Ini->nm_tabela . $sc_where; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
      $rt = $this->Db->Execute($nmgp_select) ; 
      if ($rt === false && !$rt->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
      { 
          $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
          exit ; 
      }  
      $qt_geral_reg_form_productos_060522_mob = isset($rt->fields[0]) ? $rt->fields[0] - 1 : 0; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['total'] = $qt_geral_reg_form_productos_060522_mob;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['where_filter'] = $comando;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['fast_search'][0] = $field;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['fast_search'][1] = $arg_search;
      $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['fast_search'][2] = $sv_data;
      $rt->Close(); 
      if (isset($rt->fields[0]) && $rt->fields[0] > 0 &&  isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter'])
      {
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter']);
          $this->NM_ajax_info['empty_filter'] = 'ok';
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
      elseif (!isset($rt->fields[0]) || $rt->fields[0] == 0)
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['empty_filter'] = true;
          $this->NM_ajax_info['empty_filter'] = 'ok';
          form_productos_060522_mob_pack_ajax_response();
          exit;
      }
   }
   function SC_monta_condicao(&$comando, $nome, $condicao, $campo, $tp_campo="")
   {
      $nm_aspas   = "'";
      $nm_aspas1  = "'";
      $nm_numeric = array();
      $Nm_datas   = array();
      $nm_esp_postgres = array();
      $campo_join = strtolower(str_replace(".", "_", $nome));
      $nm_ini_lower = "";
      $nm_fim_lower = "";
      $nm_numeric[] = "idprod";$nm_numeric[] = "costomay";$nm_numeric[] = "costomen";$nm_numeric[] = "recmayamen";$nm_numeric[] = "preciomen";$nm_numeric[] = "preciomen2";$nm_numeric[] = "preciomen3";$nm_numeric[] = "precio2";$nm_numeric[] = "preciomay";$nm_numeric[] = "preciofull";$nm_numeric[] = "stockmay";$nm_numeric[] = "stockmen";$nm_numeric[] = "idgrup";$nm_numeric[] = "idpro1";$nm_numeric[] = "idpro2";$nm_numeric[] = "idiva";$nm_numeric[] = "otro";$nm_numeric[] = "otro2";$nm_numeric[] = "imconsumo";$nm_numeric[] = "idcombo";$nm_numeric[] = "por_preciominimo";$nm_numeric[] = "id_marca";$nm_numeric[] = "id_linea";$nm_numeric[] = "existencia";$nm_numeric[] = "costo_prom";$nm_numeric[] = "";
      if (in_array($campo_join, $nm_numeric))
      {
         if ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['decimal_db'] == ".")
         {
             $nm_aspas  = "";
             $nm_aspas1 = "";
         }
         if (is_array($campo))
         {
             foreach ($campo as $Ind => $Cmp)
             {
                if (!is_numeric($Cmp))
                {
                    return;
                }
                if ($Cmp == "")
                {
                    $campo[$Ind] = 0;
                }
             }
         }
         else
         {
             if (!is_numeric($campo))
             {
                 return;
             }
             if ($campo == "")
             {
                $campo = 0;
             }
         }
      }
         if (in_array($campo_join, $nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && (strtoupper($condicao) == "II" || strtoupper($condicao) == "QP" || strtoupper($condicao) == "NP"))
         {
             $nome      = "CAST ($nome AS TEXT)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
         if (in_array($campo_join, $nm_esp_postgres) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
         {
             $nome      = "CAST ($nome AS TEXT)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
         if (in_array($campo_join, $nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase) && (strtoupper($condicao) == "II" || strtoupper($condicao) == "QP" || strtoupper($condicao) == "NP"))
         {
             $nome      = "CAST ($nome AS VARCHAR)";
             $nm_aspas  = "'";
             $nm_aspas1 = "'";
         }
      $Nm_datas['ultima_compra'] = "date";$Nm_datas['ultima_venta'] = "date";
         if (isset($Nm_datas[$campo_join]))
         {
             for ($x = 0; $x < strlen($campo); $x++)
             {
                 $tst = substr($campo, $x, 1);
                 if (!is_numeric($tst) && ($tst != "-" && $tst != ":" && $tst != " "))
                 {
                     return;
                 }
             }
         }
          if (isset($Nm_datas[$campo_join]))
          {
          if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
          {
             $nm_aspas  = "#";
             $nm_aspas1 = "#";
          }
              if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['SC_sep_date']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['SC_sep_date']))
              {
                  $nm_aspas  = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['SC_sep_date'];
                  $nm_aspas1 = $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['SC_sep_date1'];
              }
          }
      if (isset($Nm_datas[$campo_join]) && (strtoupper($condicao) == "II" || strtoupper($condicao) == "QP" || strtoupper($condicao) == "NP" || strtoupper($condicao) == "DF"))
      {
          if (strtoupper($condicao) == "DF")
          {
              $condicao = "NP";
          }
          if (($Nm_datas[$campo_join] == "datetime" || $Nm_datas[$campo_join] == "timestamp") && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
          {
              $nome = "to_char (" . $nome . ", 'YYYY-MM-DD hh24:mi:ss')";
          }
          elseif ($Nm_datas[$campo_join] == "date" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
          {
              $nome = "to_char (" . $nome . ", 'YYYY-MM-DD')";
          }
          elseif ($Nm_datas[$campo_join] == "time" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
          {
              $nome = "to_char (" . $nome . ", 'hh24:mi:ss')";
          }
          elseif ($Nm_datas[$campo_join] == "date" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $nome = "convert(char(10)," . $nome . ",121)";
          }
          elseif (($Nm_datas[$campo_join] == "datetime" || $Nm_datas[$campo_join] == "timestamp") && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
          {
              $nome = "convert(char(19)," . $nome . ",121)";
          }
          elseif (($Nm_datas[$campo_join] == "times" || $Nm_datas[$campo_join] == "datetime" || $Nm_datas[$campo_join] == "timestamp") && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
          {
              $nome  = "TO_DATE(TO_CHAR(" . $nome . ", 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss')";
          }
          elseif ($Nm_datas[$campo_join] == "datetime" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $nome = "EXTEND(" . $nome . ", YEAR TO FRACTION)";
          }
          elseif ($Nm_datas[$campo_join] == "date" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
          {
              $nome = "EXTEND(" . $nome . ", YEAR TO DAY)";
          }
      }
         $comando .= (!empty($comando) ? " or " : "");
         if (is_array($campo))
         {
             $prep = "";
             foreach ($campo as $Ind => $Cmp)
             {
                 $prep .= (!empty($prep)) ? "," : "";
                 $Cmp   = substr($this->Db->qstr($Cmp), 1, -1);
                 $prep .= $nm_ini_lower . $nm_aspas . $Cmp . $nm_aspas1 . $nm_fim_lower;
             }
             $prep .= (empty($prep)) ? $nm_aspas . $nm_aspas1 : "";
             $comando .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $prep . ")";
             return;
         }
         $campo  = substr($this->Db->qstr($campo), 1, -1);
         $cond_tst = strtoupper($condicao);
         if ($cond_tst == "II" || $cond_tst == "QP" || $cond_tst == "NP")
         {
             if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && $this->NM_case_insensitive)
             {
                 $op_like      = " ilike ";
                 $nm_ini_lower = "";
                 $nm_fim_lower = "";
             }
             else
             {
                 $op_like = " like ";
             }
         }
         switch ($cond_tst)
         {
            case "EQ":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " = " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "II":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_like . $nm_ini_lower . "'" . $campo . "%'" . $nm_fim_lower;
            break;
            case "QP":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_like . $nm_ini_lower . "'%" . $campo . "%'" . $nm_fim_lower;
            break;
            case "NP":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " not" . $op_like . $nm_ini_lower . "'%" . $campo . "%'" . $nm_fim_lower;
            break;
            case "DF":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "GT":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " > " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "GE":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " >= " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "LT":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " < " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
            case "LE":     // 
               $comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <= " . $nm_ini_lower . $nm_aspas . $campo . $nm_aspas1 . $nm_fim_lower;
            break;
         }
   }
   function SC_lookup_idgrup($condicao, $campo)
   {
       $result = array();
       $campo_orig = $campo;
       $campo  = substr($this->Db->qstr($campo), 1, -1);
       $nm_comando = "SELECT nomgrupo, idgrupo FROM grupo WHERE (nomgrupo LIKE '%$campo%')" ; 
       if ($condicao == "ii")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "LIKE '$campo%'", $nm_comando);
       }
       if ($condicao == "df" || $condicao == "np")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "NOT LIKE '%$campo%'", $nm_comando);
       }
       if ($condicao == "gt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "> '$campo'", $nm_comando);
       }
       if ($condicao == "ge")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", ">= '$campo'", $nm_comando);
       }
       if ($condicao == "lt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "< '$campo'", $nm_comando);
       }
       if ($condicao == "le")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "<= '$campo'", $nm_comando);
       }
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
       $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
       if ($rx = $this->Db->Execute($nm_comando)) 
       { 
           $campo = $campo_orig;
           while (!$rx->EOF) 
           { 
               $chave = $rx->fields[1];
               $label = $rx->fields[0];
               if ($condicao == "eq" && $campo == $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
               {
                   $result[] = $chave;
               }
               if ($condicao == "qp" && strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "np" && !strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "df" && $campo != $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "gt" && $label > $campo )
               {
                   $result[] = $chave;
               }
               if ($condicao == "ge" && $label >= $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "lt" && $label < $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "le" && $label <= $campo)
               {
                   $result[] = $chave;
               }
               $rx->MoveNext() ;
           }  
           return $result;
       }  
       elseif ($GLOBALS["NM_ERRO_IBASE"] != 1)  
       { 
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
           exit; 
       } 
   }
   function SC_lookup_idpro1($condicao, $campo)
   {
       $result = array();
       $campo_orig = $campo;
       $campo  = substr($this->Db->qstr($campo), 1, -1);
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_comando = "SELECT documento + \"- \" + nombres, idtercero FROM terceros WHERE (documento + \"- \" + nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nm_comando = "SELECT concat(documento,\"- \",nombres), idtercero FROM terceros WHERE (concat(documento,\"- \",nombres) LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
      { 
          $nm_comando = "SELECT documento&\"- \"&nombres, idtercero FROM terceros WHERE (documento&\"- \"&nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "SELECT documento + \"- \" + nombres, idtercero FROM terceros WHERE (documento + \"- \" + nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      else 
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
       if ($condicao == "ii")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "LIKE '$campo%'", $nm_comando);
       }
       if ($condicao == "df" || $condicao == "np")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "NOT LIKE '%$campo%'", $nm_comando);
       }
       if ($condicao == "gt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "> '$campo'", $nm_comando);
       }
       if ($condicao == "ge")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", ">= '$campo'", $nm_comando);
       }
       if ($condicao == "lt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "< '$campo'", $nm_comando);
       }
       if ($condicao == "le")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "<= '$campo'", $nm_comando);
       }
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
       $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
       if ($rx = $this->Db->Execute($nm_comando)) 
       { 
           $campo = $campo_orig;
           while (!$rx->EOF) 
           { 
               $chave = $rx->fields[1];
               $label = $rx->fields[0];
               if ($condicao == "eq" && $campo == $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
               {
                   $result[] = $chave;
               }
               if ($condicao == "qp" && strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "np" && !strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "df" && $campo != $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "gt" && $label > $campo )
               {
                   $result[] = $chave;
               }
               if ($condicao == "ge" && $label >= $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "lt" && $label < $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "le" && $label <= $campo)
               {
                   $result[] = $chave;
               }
               $rx->MoveNext() ;
           }  
           return $result;
       }  
       elseif ($GLOBALS["NM_ERRO_IBASE"] != 1)  
       { 
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
           exit; 
       } 
   }
   function SC_lookup_idpro2($condicao, $campo)
   {
       $result = array();
       $campo_orig = $campo;
       $campo  = substr($this->Db->qstr($campo), 1, -1);
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_comando = "SELECT documento + \"- \" + nombres, idtercero FROM terceros WHERE (documento + \"- \" + nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nm_comando = "SELECT concat(documento,\"- \",nombres), idtercero FROM terceros WHERE (concat(documento,\"- \",nombres) LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
      { 
          $nm_comando = "SELECT documento&\"- \"&nombres, idtercero FROM terceros WHERE (documento&\"- \"&nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "SELECT documento + \"- \" + nombres, idtercero FROM terceros WHERE (documento + \"- \" + nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
      else 
      { 
          $nm_comando = "SELECT documento||\"- \"||nombres, idtercero FROM terceros WHERE (documento||\"- \"||nombres LIKE '%$campo%') AND (proveedor='SI')" ; 
      } 
       if ($condicao == "ii")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "LIKE '$campo%'", $nm_comando);
       }
       if ($condicao == "df" || $condicao == "np")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "NOT LIKE '%$campo%'", $nm_comando);
       }
       if ($condicao == "gt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "> '$campo'", $nm_comando);
       }
       if ($condicao == "ge")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", ">= '$campo'", $nm_comando);
       }
       if ($condicao == "lt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "< '$campo'", $nm_comando);
       }
       if ($condicao == "le")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "<= '$campo'", $nm_comando);
       }
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
       $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
       if ($rx = $this->Db->Execute($nm_comando)) 
       { 
           $campo = $campo_orig;
           while (!$rx->EOF) 
           { 
               $chave = $rx->fields[1];
               $label = $rx->fields[0];
               if ($condicao == "eq" && $campo == $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
               {
                   $result[] = $chave;
               }
               if ($condicao == "qp" && strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "np" && !strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "df" && $campo != $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "gt" && $label > $campo )
               {
                   $result[] = $chave;
               }
               if ($condicao == "ge" && $label >= $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "lt" && $label < $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "le" && $label <= $campo)
               {
                   $result[] = $chave;
               }
               $rx->MoveNext() ;
           }  
           return $result;
       }  
       elseif ($GLOBALS["NM_ERRO_IBASE"] != 1)  
       { 
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
           exit; 
       } 
   }
   function SC_lookup_idiva($condicao, $campo)
   {
       $result = array();
       $campo_orig = $campo;
       $campo  = substr($this->Db->qstr($campo), 1, -1);
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nm_comando = "SELECT trifa + ' - ' + tipo_impuesto, idiva FROM iva WHERE (trifa + ' - ' + tipo_impuesto LIKE '%$campo%')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nm_comando = "SELECT concat(trifa,' - ',tipo_impuesto), idiva FROM iva WHERE (concat(trifa,' - ',tipo_impuesto) LIKE '%$campo%')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
      { 
          $nm_comando = "SELECT trifa&' - '&tipo_impuesto, idiva FROM iva WHERE (trifa&' - '&tipo_impuesto LIKE '%$campo%')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      { 
          $nm_comando = "SELECT trifa||' - '||tipo_impuesto, idiva FROM iva WHERE (trifa||' - '||tipo_impuesto LIKE '%$campo%')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "SELECT convert(char,trifa) + ' - ' + tipo_impuesto, idiva FROM iva WHERE (convert(char,trifa) + ' - ' + tipo_impuesto LIKE '%$campo%')" ; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_db2))
      { 
          $nm_comando = "SELECT char(trifa)||' - '||tipo_impuesto, idiva FROM iva WHERE (char(trifa)||' - '||tipo_impuesto LIKE '%$campo%')" ; 
      } 
      else 
      { 
          $nm_comando = "SELECT trifa||' - '||tipo_impuesto, idiva FROM iva WHERE (trifa||' - '||tipo_impuesto LIKE '%$campo%')" ; 
      } 
       if ($condicao == "ii")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "LIKE '$campo%'", $nm_comando);
       }
       if ($condicao == "df" || $condicao == "np")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "NOT LIKE '%$campo%'", $nm_comando);
       }
       if ($condicao == "gt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "> '$campo'", $nm_comando);
       }
       if ($condicao == "ge")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", ">= '$campo'", $nm_comando);
       }
       if ($condicao == "lt")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "< '$campo'", $nm_comando);
       }
       if ($condicao == "le")
       {
           $nm_comando = str_replace("LIKE '%$campo%'", "<= '$campo'", $nm_comando);
       }
       $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
       $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
       if ($rx = $this->Db->Execute($nm_comando)) 
       { 
           $campo = $campo_orig;
           while (!$rx->EOF) 
           { 
               $chave = $rx->fields[1];
               $label = $rx->fields[0];
               if ($condicao == "eq" && $campo == $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
               {
                   $result[] = $chave;
               }
               if ($condicao == "qp" && strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "np" && !strstr($label, $campo))
               {
                   $result[] = $chave;
               }
               if ($condicao == "df" && $campo != $label)
               {
                   $result[] = $chave;
               }
               if ($condicao == "gt" && $label > $campo )
               {
                   $result[] = $chave;
               }
               if ($condicao == "ge" && $label >= $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "lt" && $label < $campo)
               {
                   $result[] = $chave;
               }
               if ($condicao == "le" && $label <= $campo)
               {
                   $result[] = $chave;
               }
               $rx->MoveNext() ;
           }  
           return $result;
       }  
       elseif ($GLOBALS["NM_ERRO_IBASE"] != 1)  
       { 
           $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
           exit; 
       } 
   }
   function SC_lookup_otro($condicao, $campo)
   {
       $data_look = array();
       $campo  = substr($this->Db->qstr($campo), 1, -1);
       $data_look['0'] = "No";
       $data_look['1'] = "Sí";
       $result = array();
       foreach ($data_look as $chave => $label) 
       {
           if ($condicao == "eq" && $campo == $label)
           {
               $result[] = $chave;
           }
           if ($condicao == "ii" && $campo == substr($label, 0, strlen($campo)))
           {
               $result[] = $chave;
           }
           if ($condicao == "qp" && strstr($label, $campo))
           {
               $result[] = $chave;
           }
           if ($condicao == "np" && !strstr($label, $campo))
           {
               $result[] = $chave;
           }
           if ($condicao == "df" && $campo != $label)
           {
               $result[] = $chave;
           }
           if ($condicao == "gt" && $label > $campo )
           {
               $result[] = $chave;
           }
           if ($condicao == "ge" && $label >= $campo)
            {
               $result[] = $chave;
           }
           if ($condicao == "lt" && $label < $campo)
           {
               $result[] = $chave;
           }
           if ($condicao == "le" && $label <= $campo)
           {
               $result[] = $chave;
           }
          
       }
       return $result;
   }
function nmgp_redireciona($tipo=0)
{
   global $nm_apl_dependente;
   if (isset($_SESSION['scriptcase']['nm_sc_retorno']) && !empty($_SESSION['scriptcase']['nm_sc_retorno']) && $_SESSION['scriptcase']['sc_tp_saida'] != "D" && $nm_apl_dependente != 1) 
   {
       $nmgp_saida_form = $_SESSION['scriptcase']['nm_sc_retorno'];
   }
   else
   {
       $nmgp_saida_form = $_SESSION['scriptcase']['sc_url_saida'][$this->Ini->sc_page];
   }
   if ($tipo == 2)
   {
       $nmgp_saida_form = "form_productos_060522_mob_fim.php";
   }
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['redir']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['redir'] == 'redir')
   {
       unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']);
   }
   unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['opc_ant']);
   if ($tipo == 2 && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['nm_run_menu']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['nm_run_menu'] == 1)
   {
       $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['nm_run_menu'] = 2;
       $nmgp_saida_form = "form_productos_060522_mob_fim.php";
   }
   $diretorio = explode("/", $nmgp_saida_form);
   $cont = count($diretorio);
   $apl = $diretorio[$cont - 1];
   $apl = str_replace(".php", "", $apl);
   $pos = strpos($apl, "?");
   if ($pos !== false)
   {
       $apl = substr($apl, 0, $pos);
   }
   if ($tipo != 1 && $tipo != 2)
   {
       unset($_SESSION['sc_session'][$this->Ini->sc_page][$apl]['where_orig']);
   }
   if ($this->NM_ajax_flag)
   {
       $sTarget = '_self';
       $this->NM_ajax_info['redir']['metodo']              = 'post';
       $this->NM_ajax_info['redir']['action']              = $nmgp_saida_form;
       $this->NM_ajax_info['redir']['target']              = $sTarget;
       $this->NM_ajax_info['redir']['script_case_init']    = $this->Ini->sc_page;
       if (0 == $tipo)
       {
           $this->NM_ajax_info['redir']['nmgp_url_saida'] = $this->nm_location;
       }
       form_productos_060522_mob_pack_ajax_response();
       exit;
   }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">

   <HTML>
   <HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php

   if (isset($_SESSION['scriptcase']['device_mobile']) && $_SESSION['scriptcase']['device_mobile'] && $_SESSION['scriptcase']['display_mobile'])
   {
?>
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<?php
   }

?>
    <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
    <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
    <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
    <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
    <META http-equiv="Pragma" content="no-cache"/>
    <link rel="shortcut icon" href="../_lib/img/grp__NM__ico__NM__favicon.ico">
   </HEAD>
   <BODY>
   <FORM name="form_ok" method="POST" action="<?php echo $this->form_encode_input($nmgp_saida_form); ?>" target="_self">
<?php
   if ($tipo == 0)
   {
?>
     <INPUT type="hidden" name="nmgp_url_saida" value="<?php echo $this->form_encode_input($this->nm_location); ?>"> 
<?php
   }
?>
     <INPUT type="hidden" name="script_case_init" value="<?php echo $this->form_encode_input($this->Ini->sc_page); ?>"> 
   </FORM>
   <SCRIPT type="text/javascript">
      bLigEditLookupCall = <?php if ($this->lig_edit_lookup_call) { ?>true<?php } else { ?>false<?php } ?>;
      function scLigEditLookupCall()
      {
<?php
   if ($this->lig_edit_lookup && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_modal']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['sc_modal'])
   {
?>
        parent.<?php echo $this->lig_edit_lookup_cb; ?>(<?php echo $this->lig_edit_lookup_row; ?>);
<?php
   }
   elseif ($this->lig_edit_lookup)
   {
?>
        opener.<?php echo $this->lig_edit_lookup_cb; ?>(<?php echo $this->lig_edit_lookup_row; ?>);
<?php
   }
?>
      }
      if (bLigEditLookupCall)
      {
        scLigEditLookupCall();
      }
<?php
if ($tipo == 2 && isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue']))
{
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['under_dashboard']) && $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['under_dashboard']) {
?>
var dbParentFrame = $(parent.document).find("[name='<?php echo $_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['dashboard_info']['parent_widget']; ?>']");
if (dbParentFrame && dbParentFrame[0] && dbParentFrame[0].contentWindow.scAjaxDetailValue)
{
<?php
        foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue'] as $cmp_master => $val_master)
        {
?>
    dbParentFrame[0].contentWindow.scAjaxDetailValue('<?php echo $cmp_master ?>', '<?php echo $val_master ?>');
<?php
        }
        unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue']);
?>
}
<?php
    }
    else {
?>
if (parent && parent.scAjaxDetailValue)
{
<?php
        foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue'] as $cmp_master => $val_master)
        {
?>
    parent.scAjaxDetailValue('<?php echo $cmp_master ?>', '<?php echo $val_master ?>');
<?php
        }
        unset($_SESSION['sc_session'][$this->Ini->sc_page]['form_productos_060522_mob']['masterValue']);
?>
}
<?php
    }
}
?>
      document.form_ok.submit();
   </SCRIPT>
   </BODY>
   </HTML>
<?php
  exit;
}
    function sc_set_focus($sFieldName)
    {
        $sFieldName = strtolower($sFieldName);
        $aFocus = array(
                        'codigoprod' => 'codigoprod',
                        'codigobar' => 'codigobar',
                        'nompro' => 'nompro',
                        'idgrup' => 'idgrup',
                        'idpro1' => 'idpro1',
                        'tipo_producto' => 'tipo_producto',
                        'idpro2' => 'idpro2',
                        'otro' => 'otro',
                        'otro2' => 'otro2',
                        'precio_editable' => 'precio_editable',
                        'maneja_tcs_lfs' => 'maneja_tcs_lfs',
                        'stockmen' => 'stockmen',
                        'unidmaymen' => 'unidmaymen',
                        'unimay' => 'unimay',
                        'unimen' => 'unimen',
                        'unidad_ma' => 'unidad_ma',
                        'unidad_' => 'unidad_',
                        'multiple_escala' => 'multiple_escala',
                        'en_base_a' => 'en_base_a',
                        'costomen' => 'costomen',
                        'costo_prom' => 'costo_prom',
                        'recmayamen' => 'recmayamen',
                        'idiva' => 'idiva',
                        'existencia' => 'existencia',
                        'u_menor' => 'u_menor',
                        'ubicacion' => 'ubicacion',
                        'activo' => 'activo',
                        'colores' => 'colores',
                        'confcolor' => 'confcolor',
                        'tallas' => 'tallas',
                        'conftalla' => 'conftalla',
                        'sabores' => 'sabores',
                        'sabor' => 'sabor',
                        'fecha_vencimiento' => 'fecha_vencimiento',
                        'lote' => 'lote',
                        'serial_codbarras' => 'serial_codbarras',
                        'relleno' => 'relleno',
                        'control_costo' => 'control_costo',
                        'por_preciominimo' => 'por_preciominimo',
                        'sugerido_mayor' => 'sugerido_mayor',
                        'sugerido_menor' => 'sugerido_menor',
                        'preciofull' => 'preciofull',
                        'precio2' => 'precio2',
                        'preciomay' => 'preciomay',
                        'preciomen' => 'preciomen',
                        'preciomen2' => 'preciomen2',
                        'preciomen3' => 'preciomen3',
                        'imagen' => 'imagen',
                        'cod_cuenta' => 'cod_cuenta',
                        'idprod' => 'idprod',
                        'id_marca' => 'id_marca',
                        'id_linea' => 'id_linea',
                        'codigobar2' => 'codigobar2',
                        'codigobar3' => 'codigobar3',
                        'para_registro_fe' => 'para_registro_fe',
                       );
        if (isset($aFocus[$sFieldName]))
        {
            $this->NM_ajax_info['focus'] = $aFocus[$sFieldName];
        }
    } // sc_set_focus
    function sc_ajax_message($sMessage, $sTitle = '', $sParam = '', $sRedirPar = '')
    {
        if ($this->NM_ajax_flag)
        {
            $this->NM_ajax_info['ajaxMessage'] = array();
            if ('' != $sParam)
            {
                $aParamList = explode('&', $sParam);
                foreach ($aParamList as $sParamItem)
                {
                    $aParamData = explode('=', $sParamItem);
                    if (2 == sizeof($aParamData) &&
                        in_array($aParamData[0], array('modal', 'timeout', 'button', 'button_label', 'top', 'left', 'width', 'height', 'redir', 'redir_target', 'show_close', 'body_icon', 'toast', 'toast_pos', 'type')))
                    {
                        $this->NM_ajax_info['ajaxMessage'][$aParamData[0]] = NM_charset_to_utf8($aParamData[1]);
                    }
                }
            }
            if (isset($this->NM_ajax_info['ajaxMessage']['redir']) && '' != $this->NM_ajax_info['ajaxMessage']['redir'] && '.php' == substr($this->NM_ajax_info['ajaxMessage']['redir'], -4) && 'http' != substr($this->NM_ajax_info['ajaxMessage']['redir'], 0, 4))
            {
                $this->NM_ajax_info['ajaxMessage']['redir'] = $this->Ini->path_link . SC_dir_app_name(substr($this->NM_ajax_info['ajaxMessage']['redir'], 0, -4)) . '/' . $this->NM_ajax_info['ajaxMessage']['redir'];
            }
            if ('' != $sRedirPar)
            {
                $this->NM_ajax_info['ajaxMessage']['redir_par'] = str_replace('=', '?#?', str_replace(';', '?@?', $sRedirPar));
            }
            else
            {
                $this->NM_ajax_info['ajaxMessage']['redir_par'] = '';
            }
            $this->NM_ajax_info['ajaxMessage']['message'] = NM_charset_to_utf8($sMessage);
            $this->NM_ajax_info['ajaxMessage']['title']   = NM_charset_to_utf8($sTitle);
            if (!isset($this->NM_ajax_info['ajaxMessage']['button']))
            {
                $this->NM_ajax_info['ajaxMessage']['button'] = 'Y';
            }
        }
    } // sc_ajax_message
    function sc_ajax_javascript($sJsFunc, $aParam = array())
    {
        if ($this->NM_ajax_flag)
        {
            foreach ($aParam as $i => $v)
            {
                $aParam[$i] = NM_charset_to_utf8($v);
            }
            $this->NM_ajax_info['ajaxJavascript'][] = array(NM_charset_to_utf8($sJsFunc), $aParam);
        }
        else
        {
            foreach ($aParam as $i => $v)
            {
                $aParam[$i] = '"' . str_replace('"', '\"', $v) . '"';
            }
            $this->NM_non_ajax_info['ajaxJavascript'][] = array($sJsFunc, $aParam);
        }
    } // sc_ajax_javascript
    function sc_field_readonly($sField, $sStatus, $iSeq = '')
    {
        if ('on' != $sStatus && 'off' != $sStatus)
        {
            return;
        }

        $sFieldDateTime = '';

        $sFlagVar        = 'bFlagRead_' . $sField;
        $this->$sFlagVar = 'on' == $sStatus;

        $this->nmgp_cmp_readonly[$sField]                = $sStatus;
        $this->NM_ajax_info['readOnly'][$sField . $iSeq] = $sStatus;
        if ('' != $sFieldDateTime)
        {
            $this->NM_ajax_info['readOnly'][$sFieldDateTime . $iSeq] = $sStatus;
        }
    } // sc_field_readonly
    function getButtonIds($buttonName) {
        switch ($buttonName) {
            case "new":
                return array("sc_b_new_t.sc-unique-btn-1", "sc_b_new_t.sc-unique-btn-17");
                break;
            case "insert":
                return array("sc_b_ins_t.sc-unique-btn-2", "sc_b_ins_t.sc-unique-btn-18");
                break;
            case "bcancelar":
                return array("sc_b_sai_t.sc-unique-btn-3", "sc_b_sai_t.sc-unique-btn-19");
                break;
            case "update":
                return array("sc_b_upd_t.sc-unique-btn-4", "sc_b_upd_t.sc-unique-btn-20");
                break;
            case "delete":
                return array("sc_b_del_t.sc-unique-btn-5", "sc_b_del_t.sc-unique-btn-21");
                break;
            case "sc_btn_0":
                return array("sc_sc_btn_0_top");
                break;
            case "sc_btn_1":
                return array("sc_sc_btn_1_top");
                break;
            case "sc_btn_2":
                return array("sc_sc_btn_2_top");
                break;
            case "escalas":
                return array("sc_escalas_top");
                break;
            case "recalcular":
                return array("sc_recalcular_top");
                break;
            case "breload":
                return array("sc_b_reload_t.sc-unique-btn-6", "sc_b_reload_t.sc-unique-btn-22");
                break;
            case "copy":
                return array("sc_b_clone_t.sc-unique-btn-7", "sc_b_clone_t.sc-unique-btn-23");
                break;
            case "exit":
                return array("sc_b_sai_t.sc-unique-btn-8", "sc_b_sai_t.sc-unique-btn-9", "sc_b_sai_t.sc-unique-btn-11", "sc_b_sai_t.sc-unique-btn-24", "sc_b_sai_t.sc-unique-btn-25", "sc_b_sai_t.sc-unique-btn-27", "sc_b_sai_t.sc-unique-btn-10", "sc_b_sai_t.sc-unique-btn-12", "sc_b_sai_t.sc-unique-btn-26", "sc_b_sai_t.sc-unique-btn-28");
                break;
            case "birpara":
                return array("brec_b");
                break;
            case "first":
                return array("sc_b_ini_b.sc-unique-btn-13", "sc_b_ini_b.sc-unique-btn-29");
                break;
            case "back":
                return array("sc_b_ret_b.sc-unique-btn-14", "sc_b_ret_b.sc-unique-btn-30");
                break;
            case "forward":
                return array("sc_b_avc_b.sc-unique-btn-15", "sc_b_avc_b.sc-unique-btn-31");
                break;
            case "last":
                return array("sc_b_fim_b.sc-unique-btn-16", "sc_b_fim_b.sc-unique-btn-32");
                break;
        }

        return array($buttonName);
    } // getButtonIds

}
?>
