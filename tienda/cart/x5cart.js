var x5CartData = {
	// General Cart Settings
	'settings': {
		'indexpage': 'cart/index.html',
		'indexsearchpage': 'cartsearch/index.html',
		'loginpage': '',
		'continue_shopping_page': 'http://localhost:9192/tienda/cartsearch/index.html',
		'searchpage_url': 'http://localhost:9192/tienda/cartsearch/index.html',
		'vat': 0.19,
		'vattype': 'included', // included|excluded|none
		'currency': '$',
		'currency_id': 'COP',
		'showShipmentFields': false,
		'cartCookie': 'x5CartProductsvmztql83h0o37m36',
		'formCookie': 'x5CartFormvmztql83h0o37m36',
		'noPreviewImage': 'cart/images/no-preview.png',
		'availabilityImage': 'cart/images/cart-available.png',
		'registerUsers': 0,
		'currency_format': '####.@@ [C]',
		'currency_format_zeroas': '0',
		'order_no_format': '[yy][mm][dd]-[A-Z][A-Z][0-9][0-9]',
		'formAutocomplete': true,
		'formValidation': 'tip',
		'minimumAmount': 0,
		'currencies': [{ "value": "AFN", "text": "Afghanistan, Afghanis, ؋" }, { "value": "ALL", "text": "Albania, Leke, Lek" }, { "value": "ARS", "text": "Argentina, Pesos, $" }, { "value": "AUD", "text": "Australia, Dollars, $" }, { "value": "BGN", "text": "Bulgarian, Lev, лв" }, { "value": "BND", "text": "Brunei Darussalam, Dollars, $" }, { "value": "BRL", "text": "Brazil, Reais, R$" }, { "value": "c", "text": "Russia, Rubles, руб" }, { "value": "CAD", "text": "Canada, Dollars, $" }, { "value": "CHF", "text": "Switzerland, Francs, CHF" }, { "value": "CNY", "text": "China, Yuan Renminbi, ¥" }, { "value": "COP", "text": "Colombia, Peso Colombiano, $" }, { "value": "CZK", "text": "Czech Republic, Koruny, Kč" }, { "value": "DKK", "text": "Denmark, Kroner, kr" }, { "value": "EGP", "text": "Egypt, Pounds, £" }, { "value": "EUR", "text": "Euro, €" }, { "value": "GBP", "text": "United Kingdom, Pounds, £" }, { "value": "GTQ", "text": "Guatemala, Quetzales, Q" }, { "value": "HKD", "text": "Hong Kong, Dollars, $" }, { "value": "HRK", "text": "Croatia, Kuna, kn" }, { "value": "HUF", "text": "Hungary, Forint, Ft" }, { "value": "IDR", "text": "Indonesia, Rupiahs, Rp" }, { "value": "ILS", "text": "Israel, New Shekels, ₪" }, { "value": "INR", "text": "India, Rupees, Rs" }, { "value": "IRR", "text": "Iran, Rials, ﷼" }, { "value": "ISK", "text": "Iceland, Kronur, kr" }, { "value": "JPY", "text": "Japan, Yen, ¥" }, { "value": "KPW", "text": "Korea (North), Won, ₩" }, { "value": "KRW", "text": "Korea (South), Won, ₩" }, { "value": "KZT", "text": "Kazakhstan, Tenge, тг" }, { "value": "LTL", "text": "Lithuania, Litai, Lt" }, { "value": "LVL", "text": "Latvia, Lati, Ls" }, { "value": "MXN", "text": "Mexico, Pesos, $" }, { "value": "MYR", "text": "Malaysia, Ringgits, RM" }, { "value": "NOK", "text": "Norway, Kroner, kr" }, { "value": "NZD", "text": "New Zealand, Dollars, $" }, { "value": "OMR", "text": "Oman, Rials, ﷼" }, { "value": "PLN", "text": "Poland, Zlotych, zł" }, { "value": "QAR", "text": "Qatar, Rials, ﷼" }, { "value": "RON", "text": "Romania, New Lei, lei" }, { "value": "RSD", "text": "Serbia, Dinars, Дин." }, { "value": "SAR", "text": "Saudi Arabia, Riyals, ﷼" }, { "value": "SEK", "text": "Sweden, Kronor, kr" }, { "value": "SGD", "text": "Singapore, Dollars, $" }, { "value": "THB", "text": "Thailand, Baht, ฿" }, { "value": "TRY", "text": "Turkey, Lira, ₺" }, { "value": "TTD", "text": "Trinidad and Tobago, Dollars, TT$" }, { "value": "TWD", "text": "Taiwan, New Dollars, NT$" }, { "value": "UAH", "text": "Ukraine, Hryvnia, ₴" }, { "value": "USD", "text": "United States of America, Dollars, $" }]
	},

	// Cart Products
	'products': {
		'e1hsqkqy': {
			'id': 'e1hsqkqy',
			'id_user': 'Accumsan Elit',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Accumsan Elit',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 336.1344538,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr01.jpg'],
			'imgObjects': [
				{
					"image": "images/pr01.jpg",
					"width": 555,
					"height": 555
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink0e06254d59c516d1b4c832bbc08c4c4b = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink0e06254d59c516d1b4c832bbc08c4c4b, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink54fc87276c594385eb3e1e585f152a94 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr01.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink54fc87276c594385eb3e1e585f152a94, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'gqxywa79': {
			'id': 'gqxywa79',
			'id_user': 'Dentoex Sample',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Dentoex Sample',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': true,
			'description': '',
			'price': 193.2773109,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr02.jpg'],
			'imgObjects': [
				{
					"image": "images/pr02.jpg",
					"width": 555,
					"height": 555
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink7f7cebd04afb6d5c9719bbb74066e148 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink7f7cebd04afb6d5c9719bbb74066e148, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink7bbf22c2c71e97874bb4cc5ebac32959 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr02.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink7bbf22c2c71e97874bb4cc5ebac32959, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'pxpsobx2': {
			'id': 'pxpsobx2',
			'id_user': 'Bima Zuma',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Bima Zuma',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 462.1848739,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr03.jpg'],
			'imgObjects': [
				{
					"image": "images/pr03.jpg",
					"width": 555,
					"height": 555
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink89ef2f0637326e280a1443494cc035d9 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink89ef2f0637326e280a1443494cc035d9, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink2172fc1d23ce01e737d7f754c49d1e42 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr03.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink2172fc1d23ce01e737d7f754c49d1e42, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'qpplxy27': {
			'id': 'qpplxy27',
			'id_user': 'Exerci Tation',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Exerci Tation',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 277.3109244,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'discount': {
				'type': 'relative',
				'amount': 0.5
			},
			'images': ['images/pr07.jpg'],
			'imgObjects': [
				{
					"image": "images/pr07.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink00ee6cf6cd6a207798435fbcbfffdd72 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink00ee6cf6cd6a207798435fbcbfffdd72, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink0b1fa30958a26642e1da5ae670a18758 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr07.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink0b1fa30958a26642e1da5ae670a18758, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'0ovw2yfc': {
			'id': '0ovw2yfc',
			'id_user': 'Gire Tima Pokem',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Gire Tima Pokem',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': true,
			'description': '',
			'price': 75.6302521,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr08.jpg'],
			'imgObjects': [
				{
					"image": "images/pr08.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink16b8558132f11d24a904f59f54bdc0a2 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink16b8558132f11d24a904f59f54bdc0a2, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlinkab76dd0f540c9559c3763e655096bd80 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr08.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkab76dd0f540c9559c3763e655096bd80, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'qn5ukhl1': {
			'id': 'qn5ukhl1',
			'id_user': 'Alor Dor',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Alor Dor',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 546.2184874,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'discount': {
				'type': 'relative',
				'amount': 0.5
			},
			'images': ['images/pr09.jpg'],
			'imgObjects': [
				{
					"image": "images/pr09.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink99a68006def34d955f0b0766c09fa026 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink99a68006def34d955f0b0766c09fa026, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink6206e52a0c4218bfd4643eaca9c259c0 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink6206e52a0c4218bfd4643eaca9c259c0, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'wf8g2cle': {
			'id': 'wf8g2cle',
			'id_user': 'Trendissime',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Trendissime',
			'category': 'wybg0gsr',
			'showThumbsInShowBox': true,
			'isNew': true,
			'description': '',
			'price': 138.6554622,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr10.jpg'],
			'imgObjects': [
				{
					"image": "images/pr10.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink5ad59c6ebd0dcaee0ab5efc8edc0bef4 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink5ad59c6ebd0dcaee0ab5efc8edc0bef4, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlinkc16fe3bde1c4a1199a59a93292ac3f7c = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr10.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkc16fe3bde1c4a1199a59a93292ac3f7c, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'8x0s1zqy': {
			'id': '8x0s1zqy',
			'id_user': 'Lextit Aduc',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Lextit Aduc',
			'category': 'du0s3hcf',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 159.6638655,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr12.jpg'],
			'imgObjects': [
				{
					"image": "images/pr12.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink2a8e1493b8c26c2b16a83b81c70ca70e = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink2a8e1493b8c26c2b16a83b81c70ca70e, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink6bab76efdd832e0f4319724038b3fa62 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr12.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink6bab76efdd832e0f4319724038b3fa62, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'bogb4zox': {
			'id': 'bogb4zox',
			'id_user': 'Adspiciture',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Adspiciture',
			'category': 'du0s3hcf',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 294.1176471,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'discount': {
				'type': 'relative',
				'amount': 0.35
			},
			'images': ['images/pr13.jpg'],
			'imgObjects': [
				{
					"image": "images/pr13.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlinkbcde8fc49cd1c75cd2a82880c86e03d2 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkbcde8fc49cd1c75cd2a82880c86e03d2, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink688124184bbce6f3c7b2f95d97e088bd = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr13.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink688124184bbce6f3c7b2f95d97e088bd, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'lilnmnel': {
			'id': 'lilnmnel',
			'id_user': 'Machus Michus',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Machus Michus',
			'category': 'du0s3hcf',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 218.487395,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'discount': {
				'type': 'relative',
				'amount': 0.5
			},
			'images': ['images/pr14.jpg'],
			'imgObjects': [
				{
					"image": "images/pr14.jpg",
					"width": 555,
					"height": 555
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlinkf4d085b580270702c724e6b5c090eb98 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkf4d085b580270702c724e6b5c090eb98, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlinkaf6b96c187c6a04b916e1313b742e127 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr14.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkaf6b96c187c6a04b916e1313b742e127, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'hjqwjyrz': {
			'id': 'hjqwjyrz',
			'id_user': 'Dail Miren Tukan',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Dail Miren Tukan',
			'category': 'hixusz7a',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 588.2352941,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'discount': {
				'type': 'relative',
				'amount': 0.5
			},
			'images': ['images/pr04.jpg'],
			'imgObjects': [
				{
					"image": "images/pr04.jpg",
					"width": 555,
					"height": 555
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink527c1eafbc36cea18b99fc540f458c88 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink527c1eafbc36cea18b99fc540f458c88, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink023103e81940de6132db868343c9ffeb = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr04.jpg','width': 555,'height': 555,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink023103e81940de6132db868343c9ffeb, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'l0b06r5n': {
			'id': 'l0b06r5n',
			'id_user': 'Tima Pokem',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Tima Pokem',
			'category': 'hixusz7a',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 193.2773109,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr06.jpg'],
			'imgObjects': [
				{
					"image": "images/pr06.jpg",
					"width": 768,
					"height": 706
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlinka8d3f7aa635bb3ec21312bac962ca0d8 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinka8d3f7aa635bb3ec21312bac962ca0d8, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink5ec6a3d5f19241aaec9b4c2c1d8a7031 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr06.jpg','width': 768,'height': 706,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink5ec6a3d5f19241aaec9b4c2c1d8a7031, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'bq6g9rbv': {
			'id': 'bq6g9rbv',
			'id_user': 'Donec Ac Tempus',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Donec Ac Tempus',
			'category': 'hixusz7a',
			'showThumbsInShowBox': true,
			'isNew': false,
			'description': '',
			'price': 142.8571429,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr05.jpg'],
			'imgObjects': [
				{
					"image": "images/pr05.jpg",
					"width": 768,
					"height": 631
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlink72bd86b78ca95b9a0ae2259bc14c7e82 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink72bd86b78ca95b9a0ae2259bc14c7e82, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink2edb51f61f04ba22885ed884d660268c = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr05.jpg','width': 768,'height': 631,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink2edb51f61f04ba22885ed884d660268c, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		},
		'93etb95m': {
			'id': '93etb95m',
			'id_user': 'Kelend Metus',
			'digital_delivery': false,
			'physical_delivery': true,
			'name': 'Kelend Metus',
			'category': 'hixusz7a',
			'showThumbsInShowBox': true,
			'isNew': true,
			'description': '',
			'price': 163.8655462,
			'staticAvailValue': 'available',
			'availabilityType': 'unset',
			'offlineAvailableItems': 0,
			'images': ['images/pr09.jpg', 'images/pr11.jpg'],
			'imgObjects': [
				{
					"image": "images/pr09.jpg",
					"width": 259,
					"height": 259
				},
				{
					"image": "images/pr11.jpg",
					"width": 259,
					"height": 259
				}
			],
			'link': null,
			'showboxLink': {
				"type": "showboxgallery",
				"tip": {
					"image": "",
					"imagePosition": "top",
					"imagePercentWidth": 50,
					"text": "",
					"width": 180
				},
				"js": {
					"upload": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					},
					"offline": {
						"jsonly": "x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);",
						"complete": "onclick=\"return x5engine.imShowBox({'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]}, 0, this);\""
					}
				},
				"html": {
					"upload": "<script> showboxlinkbd5c43e2e3eadb1acb87a299c0e0bb02 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlinkbd5c43e2e3eadb1acb87a299c0e0bb02, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>",
					"offline": "<script> showboxlink96025c2f7f71bc011ece27bd77d12b15 = {'showThumbs': true,'media': [{'type': 'image','url': '<!--base_url_placeholder-->images/pr09.jpg','width': 259,'height': 259,'description': ''},{'type': 'image','url': '<!--base_url_placeholder-->images/pr11.jpg','width': 259,'height': 259,'description': ''}]};</script>\n\n<a href=\"#\" onclick=\"return x5engine.imShowBox(showboxlink96025c2f7f71bc011ece27bd77d12b15, 0, this)\" class=\"<!--css_class_placeholder-->\"><!--html_content_placeholder--></a>"
				}
			},
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'weight': 0
		}
	},
	'products_ordered_by_price': [
		'0ovw2yfc',
		'lilnmnel',
		'qpplxy27',
		'wf8g2cle',
		'bq6g9rbv',
		'8x0s1zqy',
		'93etb95m',
		'bogb4zox',
		'gqxywa79',
		'l0b06r5n',
		'qn5ukhl1',
		'hjqwjyrz',
		'e1hsqkqy',
		'pxpsobx2'
	],

	// Cart Categories
	'categories': {
		"dataSource": [
			{
				"id": "wybg0gsr",
				"text": "Gifts",
				"type": "category",
				"items": [
					{
						"id": "e1hsqkqy",
						"text": "Accumsan Elit",
						"type": "product",
						"items": null
					},
					{
						"id": "gqxywa79",
						"text": "Dentoex Sample",
						"type": "product",
						"items": null
					},
					{
						"id": "pxpsobx2",
						"text": "Bima Zuma",
						"type": "product",
						"items": null
					},
					{
						"id": "qpplxy27",
						"text": "Exerci Tation",
						"type": "product",
						"items": null
					},
					{
						"id": "0ovw2yfc",
						"text": "Gire Tima Pokem",
						"type": "product",
						"items": null
					},
					{
						"id": "qn5ukhl1",
						"text": "Alor Dor",
						"type": "product",
						"items": null
					},
					{
						"id": "wf8g2cle",
						"text": "Trendissime",
						"type": "product",
						"items": null
					}
				]
			},
			{
				"id": "du0s3hcf",
				"text": "Art",
				"type": "category",
				"items": [
					{
						"id": "8x0s1zqy",
						"text": "Lextit Aduc",
						"type": "product",
						"items": null
					},
					{
						"id": "bogb4zox",
						"text": "Adspiciture",
						"type": "product",
						"items": null
					},
					{
						"id": "lilnmnel",
						"text": "Machus Michus",
						"type": "product",
						"items": null
					}
				]
			},
			{
				"id": "hixusz7a",
				"text": "Vintage",
				"type": "category",
				"items": [
					{
						"id": "hjqwjyrz",
						"text": "Dail Miren Tukan",
						"type": "product",
						"items": null
					},
					{
						"id": "l0b06r5n",
						"text": "Tima Pokem",
						"type": "product",
						"items": null
					},
					{
						"id": "bq6g9rbv",
						"text": "Donec Ac Tempus",
						"type": "product",
						"items": null
					},
					{
						"id": "93etb95m",
						"text": "Kelend Metus",
						"type": "product",
						"items": null
					}
				]
			}
		]
	},

	// Shipping Data
	'shippings': {
		'j48dn4la': {
			'id': 'j48dn4la',
			'name': 'Correo',
			'description': 'La mercancía se entregará en 3-5 días.',
			'vat': 0.19,
			'type': 'FIXED',
			'price': 4.2016807,
			'email': 'Envío por Correo.<br />La mercancía se entregará en 3-5 días.'
		},
		'hdj47dut': {
			'id': 'hdj47dut',
			'name': 'Envío urgente',
			'description': 'La mercancía se entregará en 1-2 días.',
			'vat': 0.19,
			'type': 'FIXED',
			'price': 8.4033613,
			'email': 'Envío por Envío urgente.<br />La mercancía se entregará en 1-2 días.'
		}
	},

	// Payment Data
	'payments': {
		'8dkejfu5': {
			'id': '8dkejfu5',
			'name': 'Transferencia bancaria',
			'description': 'Pague más tarde mediante transferencia bancaria.',
			'email': 'Estos son los datos necesarios para realizar el pago por transferencia bancaria:<br /><br />XXX YYY ZZZ<br /><br />Tenga en cuenta que una vez completado el pago, debe enviar una copia del recibo junto con el número de pedido.',
			'enableAfterPaymentEmail': false,
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'pricetype': 'fixed',
			'price': 0
		},
		'l40y53xi': {
			'id': 'l40y53xi',
			'name': 'Tarjeta de Crédito',
			'description': ' Tarjétas de Crédito',
			'email': '',
			'image': 'cart/images/pay_12.png',
			'enableAfterPaymentEmail': false,
			'vattype': 'included', // included|excluded|none
			'vat': 0.19,
			'pricetype': 'fixed',
			'price': 0
		}
	},

	// User's data form and agreement text
	'form': {
		'agreement': 'Enviando este pedido el cliente acepta los Términos y Condiciones, las normas sobre el tratamiento de datos y la información relativa a la solicitud de reembolso. Pulsando el botón \'Compre ahora\' el pedido será guardado y procesado.',
		'acceptAgreement': false,
		'fields': [
			{
				'id': 'Name',
				'uuid': 'Name',
				'name': 'Nombre',
				'value': '',
				'type': 'Text',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'LastName',
				'uuid': 'LastName',
				'name': 'Apellidos',
				'value': '',
				'type': 'Text',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'Address1',
				'uuid': 'Address1',
				'name': 'Dirección',
				'value': '',
				'type': 'Text',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'City',
				'uuid': 'City',
				'name': 'Ciudad',
				'value': '',
				'type': 'Text',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'StateRegion',
				'uuid': 'StateRegion',
				'name': 'Provincia',
				'value': '',
				'type': 'Text',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return $('#' + prefix + 'Country').val() != 'us' && $('#' + prefix + 'Country').val() != 'br' && $('#' + prefix + 'Country').val() != 'US' && $('#' + prefix + 'Country').val() != 'BR';}
			}			,{
				'id': 'StateRegion',
				'uuid': 'StateRegion-us',
				'name': 'Provincia',
				'value': [{"value": "AL","text": "Alabama"},{"value": "AK","text": "Alaska"},{"value": "AZ","text": "Arizona"},{"value": "AR","text": "Arkansas"},{"value": "CA","text": "California"},{"value": "CO","text": "Colorado"},{"value": "CT","text": "Connecticut"},{"value": "DE","text": "Delaware"},{"value": "DC","text": "District of Columbia"},{"value": "FL","text": "Florida"},{"value": "GA","text": "Georgia"},{"value": "HI","text": "Hawaii"},{"value": "ID","text": "Idaho"},{"value": "IL","text": "Illinois"},{"value": "IN","text": "Indiana"},{"value": "IA","text": "Iowa"},{"value": "KS","text": "Kansas"},{"value": "KY","text": "Kentucky"},{"value": "LA","text": "Louisiana"},{"value": "ME","text": "Maine"},{"value": "MD","text": "Maryland"},{"value": "MA","text": "Massachusetts"},{"value": "MI","text": "Michigan"},{"value": "MN","text": "Minnesota"},{"value": "MS","text": "Mississippi"},{"value": "MO","text": "Missouri"},{"value": "MT","text": "Montana"},{"value": "NE","text": "Nebraska"},{"value": "NV","text": "Nevada"},{"value": "NH","text": "New Hampshire"},{"value": "NJ","text": "New Jersey"},{"value": "NM","text": "New Mexico"},{"value": "NY","text": "New York"},{"value": "NC","text": "North Carolina"},{"value": "ND","text": "North Dakota"},{"value": "OH","text": "Ohio"},{"value": "OK","text": "Oklahoma"},{"value": "OR","text": "Oregon"},{"value": "PA","text": "Pennsylvania"},{"value": "RI","text": "Rhode Island"},{"value": "SC","text": "South Carolina"},{"value": "SD","text": "South Dakota"},{"value": "TN","text": "Tennessee"},{"value": "TX","text": "Texas"},{"value": "UT","text": "Utah"},{"value": "VT","text": "Vermont"},{"value": "VA","text": "Virginia"},{"value": "WA","text": "Washington"},{"value": "WV","text": "West Virginia"},{"value": "WI","text": "Wisconsin"},{"value": "WY","text": "Wyoming"}],
				'type': 'DropDownList',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return $('#' + prefix + 'Country').val() == 'US' || $('#' + prefix + 'Country').val() == 'us'; }
			},			{
				'id': 'StateRegion',
				'uuid': 'StateRegion-br',
				'name': 'Provincia',
				'value': [{"value": "AC","text": "Acre"},{"value": "AL","text": "Alagoas"},{"value": "AP","text": "Amapá"},{"value": "AM","text": "Amazonas"},{"value": "BA","text": "Bahia"},{"value": "CE","text": "Ceará"},{"value": "DF","text": "Distrito Federal"},{"value": "ES","text": "Espírito Santo"},{"value": "GO","text": "Goiás"},{"value": "MA","text": "Maranhão"},{"value": "MT","text": "Mato Grasso"},{"value": "MS","text": "Mato Grosso do Sul"},{"value": "MG","text": "Minas Gerais"},{"value": "PR","text": "Paraná"},{"value": "PB","text": "Paraíba"},{"value": "PA","text": "Pará"},{"value": "PE","text": "Pernambuco"},{"value": "PI","text": "Piauí"},{"value": "RN","text": "Rio Grande do Norte"},{"value": "RS","text": "Rio Grande do Sul"},{"value": "RJ","text": "Rio de Janeiro"},{"value": "RO","text": "Rondônia"},{"value": "RR","text": "Roraima"},{"value": "SC","text": "Santa Catarina"},{"value": "SE","text": "Sergipe"},{"value": "SP","text": "São Paulo"},{"value": "TO","text": "Tocantins"}],
				'type': 'DropDownList',
				'mandatory': true,
				'filter': 'mandatory',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return $('#' + prefix + 'Country').val() == 'BR' || $('#' + prefix + 'Country').val() == 'br'; }
			},
			{
				'id': 'Phone',
				'uuid': 'Phone',
				'name': 'Teléfono',
				'value': '',
				'type': 'Telephone',
				'mandatory': false,
				'filter': 'valGenTelephone',
				'shipping': true,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'Email',
				'uuid': 'Email',
				'name': 'Correo electrónico',
				'value': '',
				'type': 'Email',
				'mandatory': true,
				'filter': 'mandatory valEmail',
				'shipping': false,
				'custom': false,
				'isVisible': function (prefix) { return true;}
			},
			{
				'id': 'Note',
				'uuid': 'Note',
				'name': 'Nota',
				'value': '',
				'type': 'TextArea',
				'mandatory': false,
				'filter': '',
				'shipping': false,
				'custom': true,
				'isVisible': function (prefix) { return true;}
			}
		]
	},

	// Cart Search Page
	'searchPage': {
		"searchFieldVisibility": {
			"custom": true,
			"categories": true,
			"price": true,
			"new": true,
			"discounted": true,
			"available": false,
			"sortOrderBy": true
		},
		"cardStyle": {
			"cardsPerRow": 3,
			"rowsPerPage": 3,
			"card": {
				"type": "topcoverbottomcontents",
				"margin": 30,
				"height": 350,
				"backgroundColor": {
					"a": 255,
					"r": 255,
					"g": 255,
					"b": 255
				},
				"border": {
					"colors": {
						"top": {
							"a": 255,
							"r": 239,
							"g": 239,
							"b": 239
						},
						"bottom": {
							"a": 255,
							"r": 239,
							"g": 239,
							"b": 239
						},
						"left": {
							"a": 255,
							"r": 239,
							"g": 239,
							"b": 239
						},
						"right": {
							"a": 255,
							"r": 239,
							"g": 239,
							"b": 239
						}
					},
					"radius": {
						"topLeft": 0,
						"bottomLeft": 0,
						"topRight": 0,
						"bottomRight": 0
					},
					"widths": {
						"top": 1,
						"bottom": 1,
						"left": 1,
						"right": 1
					}
				},
				"shadow": {
					"enabled": false,
					"color": {
						"a": 255,
						"r": 128,
						"g": 128,
						"b": 128
					},
					"offset": {
						"x": 3,
						"y": 3
					},
					"spread": 0,
					"blur": 5
				},
				"image": {
					"percentSize": 70,
					"margins": {
						"top": 0,
						"bottom": 0,
						"left": 0,
						"right": 0
					},
					"sizeAdapted": false,
					"isLink": true
				},
				"txtBlock": {
					"margins": {
						"top": 10,
						"bottom": 10,
						"left": 10,
						"right": 10
					},
					"name": {
						"show": true,
						"style": {
							"backgroundColor": {
								"a": 0,
								"r": 255,
								"g": 255,
								"b": 255
							},
							"textColor": {
								"a": 255,
								"r": 0,
								"g": 0,
								"b": 0
							},
							"align": "left",
							"font": {
								"familyName": "",
								"size": 12,
								"style": "regular"
							}
						}
					},
					"description": {
						"show": false,
						"style": {
							"backgroundColor": {
								"a": 0,
								"r": 255,
								"g": 255,
								"b": 255
							},
							"textColor": {
								"a": 255,
								"r": 0,
								"g": 0,
								"b": 0
							},
							"align": "left",
							"font": {
								"familyName": "Trebuchet MS",
								"size": 9,
								"style": "regular"
							}
						}
					},
					"details": {
						"showCategory": false,
						"style": {
							"backgroundColor": {
								"a": 0,
								"r": 255,
								"g": 255,
								"b": 255
							},
							"textColor": {
								"a": 255,
								"r": 67,
								"g": 67,
								"b": 67
							},
							"align": "left",
							"font": {
								"familyName": "",
								"size": 10,
								"style": "regular"
							}
						},
						"showAvailability": false,
						"showOptions": false,
						"showQuantity": false,
						"showPrice": true,
						"includeVat": true,
						"showVat": true
					},
					"button": {
						"show": true,
						"style": {
							"backgroundColor": {
								"a": 255,
								"r": 227,
								"g": 42,
								"b": 83
							},
							"textColor": {
								"a": 255,
								"r": 255,
								"g": 255,
								"b": 255
							},
							"align": "left",
							"font": {
								"familyName": "",
								"size": 9,
								"style": "bold"
							}
						},
						"border": {
							"colors": {
								"top": {
									"a": 0,
									"r": 255,
									"g": 255,
									"b": 255
								},
								"bottom": {
									"a": 0,
									"r": 255,
									"g": 255,
									"b": 255
								},
								"left": {
									"a": 0,
									"r": 255,
									"g": 255,
									"b": 255
								},
								"right": {
									"a": 0,
									"r": 255,
									"g": 255,
									"b": 255
								}
							},
							"radius": {
								"topLeft": 0,
								"bottomLeft": 0,
								"topRight": 0,
								"bottomRight": 0
							},
							"widths": {
								"top": 0,
								"bottom": 0,
								"left": 0,
								"right": 0
							}
						},
						"margins": {
							"top": 5,
							"bottom": 5,
							"left": 15,
							"right": 15
						},
						"useAddToCartImage": "image",
						"image_obj": {
							"image": "images/148_add.png",
							"width": 32,
							"height": 32
						}
					}
				},
				"cockades": {
					"new": {
						"align": "topleft",
						"size": 30,
						"image_obj": {
							"image": "images/New_11.png",
							"width": 120,
							"height": 120
						}
					},
					"discount": {
						"align": "topleft",
						"size": 40,
						"image_obj": {
							"image": "images/Discount_38.png",
							"width": 98,
							"height": 98
						}
					}
				},
				"goCartAfterProductAdding": false
			}
		}
	}
};
x5engine.boot.push('x5engine.cart.manager = new x5engine.cart.ecommerce(x5CartData)');

// UI Setup
jQuery.extend(x5engine.cart.ui.steps, {
	"active": true,	"font": {
		"font-weight": "normal",
		"font-style": "normal",
		"text-decoration": "none",
		"font-size": "9pt",
		"color": "rgba(85, 85, 85, 1)",
		"activeColor": "rgba(0, 0, 0, 1)"
	},
	"image": {
		"url": "cart/images/cart-steps.png",
		"width": 600,
		"height": 160,
		"steps": [
			{
				'height': 40,
				'offsetX': [75, 240, 415, 580],
				'offsetY': [30, 30, 30, 30],
			},
			{
				'height': 40,
				'offsetX': [75, 240, 415, 580],
				'offsetY': [30, 30, 30, 30],
			},
			{
				'height': 40,
				'offsetX': [75, 240, 415, 580],
				'offsetY': [30, 30, 30, 30],
			},
			{
				'height': 40,
				'offsetX': [75, 240, 415, 580],
				'offsetY': [30, 30, 30, 30],
			}
		]
	}
});

// End of file x5cart.js