x5engine.settings.imBlog = {
	root: 'http://localhost:9192/tienda/blog/',
	title_tag: 'Crafty Gifts Blog - Tienda Facilweb',
	home_posts_number: 4,
	card_type: 'leftcoverrightcontents',
	show_card_title: true,
	show_card_category: true,
	show_card_description: true,
	show_card_author: true,
	show_card_date: true,
	show_card_button: true,
	article_type: 'covertitlecontents',
	file_prefix: 'x5_',
	comments: false,
	comments_source: 'external',
	comments_code: '<div id="fb-root"></div><div class="fb-comments" data-href="http://127.0.0.1:8080/blog/index.php" data-numposts="5" data-width="100%" data-colorscheme="light"></div><script>$(".fb-comments").attr("data-href", location.href);(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.6";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));</script>',
	highlight_mode: 'none',
	categories: ['Sample Articles'],
	authors: ['WebSiteX5'],
	posts: [],
	posts_cat: [],
	posts_author: [],
	posts_month: [],
	posts_ids: [],
	posts_slug: []
};

var post = null;

// Post titled "Fourth Article"
x5engine.settings.imBlog.posts['000000008'] = {
	id: '000000008',
	title: 'Fourth Article',
	title_tag: 'Fourth Article - Crafty Gifts Blog - Tienda Facilweb',
	title_heading_tag: 'h2',
	slug: 'fourth-article',
	rel_url: '?fourth-article',
	author: 'WebSiteX5',
	category: 'Sample Articles',
	cardCover: 'blog/files/large-887272_thumb.jpg',
	cover: 'blog/files/large-887272.jpg',
	summary: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	tag_description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	keywords: '',
	sources: [],
	body: '<div id="imBlogPost_000000008">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<div><br></div><div class="imHeading4">Lorem Ipsum</div><div>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div><div><br></div><div>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</div><div style="clear: both;"><!-- clear floated images --></div></div>',
	timestamp: '5/6/2020',
	utc_time: new Date(2020,5,5,11,54,0),
	month: '202006',
	comments: false,
	tag: []
};
if (x5engine.settings.imBlog.posts_cat['Sample Articles'] == null) x5engine.settings.imBlog.posts_cat['Sample Articles'] = [];
x5engine.settings.imBlog.posts_cat['Sample Articles'][x5engine.settings.imBlog.posts_cat['Sample Articles'].length] = '000000008';
if (x5engine.settings.imBlog.posts_author['WebSiteX5'] == null) x5engine.settings.imBlog.posts_author['WebSiteX5'] = [];
x5engine.settings.imBlog.posts_author['WebSiteX5'][x5engine.settings.imBlog.posts_author['WebSiteX5'].length] = '000000008';
if (x5engine.settings.imBlog.posts_month['202006'] == null) x5engine.settings.imBlog.posts_month['202006'] = [];
x5engine.settings.imBlog.posts_month['202006'][x5engine.settings.imBlog.posts_month['202006'].length] = '000000008';
x5engine.settings.imBlog.posts_slug['fourth-article'] = '000000008';

// Post titled "Third Article"
x5engine.settings.imBlog.posts['000000007'] = {
	id: '000000007',
	title: 'Third Article',
	title_tag: 'Third Article - Crafty Gifts Blog - Tienda Facilweb',
	title_heading_tag: 'h2',
	slug: 'third-article',
	rel_url: '?third-article',
	author: 'WebSiteX5',
	category: 'Sample Articles',
	cardCover: 'blog/files/large-2107599_thumb.jpg',
	cover: 'blog/files/large-2107599.jpg',
	summary: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	tag_description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	keywords: '',
	sources: [],
	body: '<div id="imBlogPost_000000007">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<div><br></div><div class="imHeading4">Lorem Ipsum</div><div>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div><div><br></div><div>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</div><div style="clear: both;"><!-- clear floated images --></div></div>',
	timestamp: '5/6/2020',
	utc_time: new Date(2020,5,5,11,54,0),
	month: '202006',
	comments: false,
	tag: []
};
if (x5engine.settings.imBlog.posts_cat['Sample Articles'] == null) x5engine.settings.imBlog.posts_cat['Sample Articles'] = [];
x5engine.settings.imBlog.posts_cat['Sample Articles'][x5engine.settings.imBlog.posts_cat['Sample Articles'].length] = '000000007';
if (x5engine.settings.imBlog.posts_author['WebSiteX5'] == null) x5engine.settings.imBlog.posts_author['WebSiteX5'] = [];
x5engine.settings.imBlog.posts_author['WebSiteX5'][x5engine.settings.imBlog.posts_author['WebSiteX5'].length] = '000000007';
if (x5engine.settings.imBlog.posts_month['202006'] == null) x5engine.settings.imBlog.posts_month['202006'] = [];
x5engine.settings.imBlog.posts_month['202006'][x5engine.settings.imBlog.posts_month['202006'].length] = '000000007';
x5engine.settings.imBlog.posts_slug['third-article'] = '000000007';

// Post titled "Second Article"
x5engine.settings.imBlog.posts['000000005'] = {
	id: '000000005',
	title: 'Second Article',
	title_tag: 'Second Article - Crafty Gifts Blog - Tienda Facilweb',
	title_heading_tag: 'h2',
	slug: 'test-article-1',
	rel_url: '?test-article-1',
	author: 'WebSiteX5',
	category: 'Sample Articles',
	cardCover: 'blog/files/large-2390136_thumb.jpg',
	cover: 'blog/files/large-2390136.jpg',
	summary: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	tag_description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	keywords: '',
	sources: [],
	body: '<div id="imBlogPost_000000005">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<div><br></div><div class="imHeading4">Lorem Ipsum</div><div>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div><div><br></div><div>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</div><div style="clear: both;"><!-- clear floated images --></div></div>',
	timestamp: '5/6/2020',
	utc_time: new Date(2020,5,5,11,54,0),
	month: '202006',
	comments: false,
	tag: []
};
if (x5engine.settings.imBlog.posts_cat['Sample Articles'] == null) x5engine.settings.imBlog.posts_cat['Sample Articles'] = [];
x5engine.settings.imBlog.posts_cat['Sample Articles'][x5engine.settings.imBlog.posts_cat['Sample Articles'].length] = '000000005';
if (x5engine.settings.imBlog.posts_author['WebSiteX5'] == null) x5engine.settings.imBlog.posts_author['WebSiteX5'] = [];
x5engine.settings.imBlog.posts_author['WebSiteX5'][x5engine.settings.imBlog.posts_author['WebSiteX5'].length] = '000000005';
if (x5engine.settings.imBlog.posts_month['202006'] == null) x5engine.settings.imBlog.posts_month['202006'] = [];
x5engine.settings.imBlog.posts_month['202006'][x5engine.settings.imBlog.posts_month['202006'].length] = '000000005';
x5engine.settings.imBlog.posts_slug['test-article-1'] = '000000005';

// Post titled "First Article"
x5engine.settings.imBlog.posts['000000004'] = {
	id: '000000004',
	title: 'First Article',
	title_tag: 'First Article - Crafty Gifts Blog - Tienda Facilweb',
	title_heading_tag: 'h2',
	slug: 'test-article',
	rel_url: '?test-article',
	author: 'WebSiteX5',
	category: 'Sample Articles',
	cardCover: 'blog/files/large-1163695_thumb.jpg',
	cover: 'blog/files/large-1163695.jpg',
	summary: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	tag_description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
	keywords: '',
	sources: [],
	body: '<div id="imBlogPost_000000004">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<div><br></div><div class="imHeading4">Lorem Ipsum</div><div>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div><div><br></div><div>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</div><div style="clear: both;"><!-- clear floated images --></div></div>',
	timestamp: '5/6/2020',
	utc_time: new Date(2020,5,5,11,54,0),
	month: '202006',
	comments: false,
	tag: []
};
if (x5engine.settings.imBlog.posts_cat['Sample Articles'] == null) x5engine.settings.imBlog.posts_cat['Sample Articles'] = [];
x5engine.settings.imBlog.posts_cat['Sample Articles'][x5engine.settings.imBlog.posts_cat['Sample Articles'].length] = '000000004';
if (x5engine.settings.imBlog.posts_author['WebSiteX5'] == null) x5engine.settings.imBlog.posts_author['WebSiteX5'] = [];
x5engine.settings.imBlog.posts_author['WebSiteX5'][x5engine.settings.imBlog.posts_author['WebSiteX5'].length] = '000000004';
if (x5engine.settings.imBlog.posts_month['202006'] == null) x5engine.settings.imBlog.posts_month['202006'] = [];
x5engine.settings.imBlog.posts_month['202006'][x5engine.settings.imBlog.posts_month['202006'].length] = '000000004';
x5engine.settings.imBlog.posts_slug['test-article'] = '000000004';

// IDs summary
x5engine.settings.imBlog.posts_ids = ['000000008', '000000007', '000000005', '000000004'];

x5engine.boot.push("x5engine.imBlog.show()");
x5engine.boot.push("x5engine.imForm.initForm('#blogComment')");

// End of file x5blog.js