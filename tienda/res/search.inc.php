<?php

/*****************
	GENERAL SETTINGS
*****************/
$imSettings['search']['general'] = array(
	'menu_position' => 'left',
	'defaultScope' => array(
		'0' => 'index.html',
		'3' => 'nosotros.html',
		'4' => '-contactenos.html'
	),
	'extendedScope' => array(
	)
);

/*****************
	PRODUCTS SEARCH
*****************/
$imSettings['search']['products'] = array(
	'e1hsqkqy' => array(
		'name' => 'Accumsan Elit',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr01.jpg" alt="Accumsan Elit" title="Accumsan Elit" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr01.jpg\', width: 555, height: 555, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '400.00 $ (IVA incl.)'
	),
	'gqxywa79' => array(
		'name' => 'Dentoex Sample',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr02.jpg" alt="Dentoex Sample" title="Dentoex Sample" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr02.jpg\', width: 555, height: 555, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '230.00 $ (IVA incl.)'
	),
	'pxpsobx2' => array(
		'name' => 'Bima Zuma',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr03.jpg" alt="Bima Zuma" title="Bima Zuma" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr03.jpg\', width: 555, height: 555, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '550.00 $ (IVA incl.)'
	),
	'hjqwjyrz' => array(
		'name' => 'Dail Miren Tukan',
		'description' => '',
		'category' => 'Vintage',
		'image' => '<img src="images/pr04.jpg" alt="Dail Miren Tukan" title="Dail Miren Tukan" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr04.jpg\', width: 555, height: 555, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '350.00 $ (IVA incl.)'
	),
	'bq6g9rbv' => array(
		'name' => 'Donec Ac Tempus',
		'description' => '',
		'category' => 'Vintage',
		'image' => '<img src="images/pr05.jpg" alt="Donec Ac Tempus" title="Donec Ac Tempus" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr05.jpg\', width: 768, height: 631, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '170.00 $ (IVA incl.)'
	),
	'l0b06r5n' => array(
		'name' => 'Tima Pokem',
		'description' => '',
		'category' => 'Vintage',
		'image' => '<img src="images/pr06.jpg" alt="Tima Pokem" title="Tima Pokem" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr06.jpg\', width: 768, height: 706, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '230.00 $ (IVA incl.)'
	),
	'8x0s1zqy' => array(
		'name' => 'Lextit Aduc',
		'description' => '',
		'category' => 'Art',
		'image' => '<img src="images/pr12.jpg" alt="Lextit Aduc" title="Lextit Aduc" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr12.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '190.00 $ (IVA incl.)'
	),
	'bogb4zox' => array(
		'name' => 'Adspiciture',
		'description' => '',
		'category' => 'Art',
		'image' => '<img src="images/pr13.jpg" alt="Adspiciture" title="Adspiciture" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr13.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '227.50 $ (IVA incl.)'
	),
	'lilnmnel' => array(
		'name' => 'Machus Michus',
		'description' => '',
		'category' => 'Art',
		'image' => '<img src="images/pr14.jpg" alt="Machus Michus" title="Machus Michus" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr14.jpg\', width: 555, height: 555, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '130.00 $ (IVA incl.)'
	),
	'qpplxy27' => array(
		'name' => 'Exerci Tation',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr07.jpg" alt="Exerci Tation" title="Exerci Tation" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr07.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '165.00 $ (IVA incl.)'
	),
	'0ovw2yfc' => array(
		'name' => 'Gire Tima Pokem',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr08.jpg" alt="Gire Tima Pokem" title="Gire Tima Pokem" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr08.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '90.00 $ (IVA incl.)'
	),
	'93etb95m' => array(
		'name' => 'Kelend Metus',
		'description' => '',
		'category' => 'Vintage',
		'image' => '<img src="images/pr09.jpg" alt="Kelend Metus" title="Kelend Metus" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr09.jpg\', width: 259, height: 259, description: \'\' }, { type: \'image\', url: \'images/pr11.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '195.00 $ (IVA incl.)'
	),
	'qn5ukhl1' => array(
		'name' => 'Alor Dor',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr09.jpg" alt="Alor Dor" title="Alor Dor" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr09.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '325.00 $ (IVA incl.)'
	),
	'wf8g2cle' => array(
		'name' => 'Trendissime',
		'description' => '',
		'category' => 'Gifts',
		'image' => '<img src="images/pr10.jpg" alt="Trendissime" title="Trendissime" onclick="x5engine.imShowBox({media:[{ type: \'image\', url: \'images/pr10.jpg\', width: 259, height: 259, description: \'\' }]}, 0, this)" style="cursor: pointer;"/>',
		'price' => '165.00 $ (IVA incl.)'
	)
);

/*****************
	IMAGES SEARCH
*****************/
$imSettings['search']['images'] = array(
);

/*****************
	VIDEOS SEARCH
*****************/
$imSettings['search']['videos'] = array(
);
$imSettings['search']['dynamicobjects'] = array(

);

// End of file search.inc.php